(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cpn-cpn-module"],{

/***/ "./node_modules/ngx-dropzone/__ivy_ngcc__/fesm2015/ngx-dropzone.js":
/*!*************************************************************************!*\
  !*** ./node_modules/ngx-dropzone/__ivy_ngcc__/fesm2015/ngx-dropzone.js ***!
  \*************************************************************************/
/*! exports provided: NgxDropzoneComponent, NgxDropzoneImagePreviewComponent, NgxDropzoneModule, NgxDropzonePreviewComponent, NgxDropzoneRemoveBadgeComponent, NgxDropzoneVideoPreviewComponent, ɵa, ɵb */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxDropzoneComponent", function() { return NgxDropzoneComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxDropzoneImagePreviewComponent", function() { return NgxDropzoneImagePreviewComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxDropzoneModule", function() { return NgxDropzoneModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxDropzonePreviewComponent", function() { return NgxDropzonePreviewComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxDropzoneRemoveBadgeComponent", function() { return NgxDropzoneRemoveBadgeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxDropzoneVideoPreviewComponent", function() { return NgxDropzoneVideoPreviewComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return NgxDropzoneService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return NgxDropzoneLabelDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");









function NgxDropzonePreviewComponent_ngx_dropzone_remove_badge_1_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ngx-dropzone-remove-badge", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NgxDropzonePreviewComponent_ngx_dropzone_remove_badge_1_Template_ngx_dropzone_remove_badge_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r1._remove($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = [[["ngx-dropzone-label"]]];
const _c1 = ["ngx-dropzone-label"];
const _c2 = ["fileInput"];
function NgxDropzoneComponent_ng_content_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](0, 2, ["*ngIf", "!_hasPreviews"]);
} }
const _c3 = [[["ngx-dropzone-preview"]], "*", [["ngx-dropzone-label"]]];
const _c4 = ["ngx-dropzone-preview", "*", "ngx-dropzone-label"];
function NgxDropzoneImagePreviewComponent_ngx_dropzone_remove_badge_2_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ngx-dropzone-remove-badge", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NgxDropzoneImagePreviewComponent_ngx_dropzone_remove_badge_2_Template_ngx_dropzone_remove_badge_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r1._remove($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function NgxDropzoneVideoPreviewComponent_video_0_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "video", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NgxDropzoneVideoPreviewComponent_video_0_Template_video_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); return $event.stopPropagation(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "source", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx_r0.sanitizedVideoSrc, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function NgxDropzoneVideoPreviewComponent_ngx_dropzone_remove_badge_2_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ngx-dropzone-remove-badge", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NgxDropzoneVideoPreviewComponent_ngx_dropzone_remove_badge_2_Template_ngx_dropzone_remove_badge_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4._remove($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
class NgxDropzoneLabelDirective {
}
NgxDropzoneLabelDirective.ɵfac = function NgxDropzoneLabelDirective_Factory(t) { return new (t || NgxDropzoneLabelDirective)(); };
NgxDropzoneLabelDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({ type: NgxDropzoneLabelDirective, selectors: [["ngx-dropzone-label"]] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxDropzoneLabelDirective, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
        args: [{
                selector: 'ngx-dropzone-label'
            }]
    }], null, null); })();

/**
 * Coerces a data-bound value (typically a string) to a boolean.
 * Taken from https://github.com/angular/components/blob/master/src/cdk/coercion/boolean-property.ts
 */
function coerceBooleanProperty(value) {
    return value != null && `${value}` !== 'false';
}
/**
 * Whether the provided value is considered a number.
 * Taken from https://github.com/angular/components/blob/master/src/cdk/coercion/number-property.ts
 */
function coerceNumberProperty(value) {
    // parseFloat(value) handles most of the cases we're interested in (it treats null, empty string,
    // and other non-number values as NaN, where Number just uses 0) but it considers the string
    // '123hello' to be a valid number. Therefore we also check if Number(value) is NaN.
    return (!isNaN(parseFloat(value)) && !isNaN(Number(value))) ? Number(value) : null;
}

var KEY_CODE;
(function (KEY_CODE) {
    KEY_CODE[KEY_CODE["BACKSPACE"] = 8] = "BACKSPACE";
    KEY_CODE[KEY_CODE["DELETE"] = 46] = "DELETE";
})(KEY_CODE || (KEY_CODE = {}));
class NgxDropzonePreviewComponent {
    constructor(sanitizer) {
        this.sanitizer = sanitizer;
        this._removable = false;
        /** Emitted when the element should be removed. */
        this.removed = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /** Make the preview item focusable using the tab key. */
        this.tabIndex = 0;
    }
    /** The file to preview. */
    set file(value) { this._file = value; }
    get file() { return this._file; }
    /** Allow the user to remove files. */
    get removable() {
        return this._removable;
    }
    set removable(value) {
        this._removable = coerceBooleanProperty(value);
    }
    keyEvent(event) {
        switch (event.keyCode) {
            case KEY_CODE.BACKSPACE:
            case KEY_CODE.DELETE:
                this.remove();
                break;
            default:
                break;
        }
    }
    /** We use the HostBinding to pass these common styles to child components. */
    get hostStyle() {
        const styles = `
			display: flex;
			height: 140px;
			min-height: 140px;
			min-width: 180px;
			max-width: 180px;
			justify-content: center;
			align-items: center;
			padding: 0 20px;
			margin: 10px;
			border-radius: 5px;
			position: relative;
		`;
        return this.sanitizer.bypassSecurityTrustStyle(styles);
    }
    /** Remove method to be used from the template. */
    _remove(event) {
        event.stopPropagation();
        this.remove();
    }
    /** Remove the preview item (use from component code). */
    remove() {
        if (this._removable) {
            this.removed.next(this.file);
        }
    }
    readFile() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.onload = e => {
                    resolve(e.target.result);
                };
                reader.onerror = e => {
                    console.error(`FileReader failed on file ${this.file.name}.`);
                    reject(e);
                };
                if (!this.file) {
                    return reject('No file to read. Please provide a file using the [file] Input property.');
                }
                reader.readAsDataURL(this.file);
            });
        });
    }
}
NgxDropzonePreviewComponent.ɵfac = function NgxDropzonePreviewComponent_Factory(t) { return new (t || NgxDropzonePreviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"])); };
NgxDropzonePreviewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NgxDropzonePreviewComponent, selectors: [["ngx-dropzone-preview"]], hostVars: 3, hostBindings: function NgxDropzonePreviewComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup", function NgxDropzonePreviewComponent_keyup_HostBindingHandler($event) { return ctx.keyEvent($event); });
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵhostProperty"]("tabindex", ctx.tabIndex);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](ctx.hostStyle);
    } }, inputs: { file: "file", removable: "removable" }, outputs: { removed: "removed" }, ngContentSelectors: _c1, decls: 2, vars: 1, consts: [[3, "click", 4, "ngIf"], [3, "click"]], template: function NgxDropzonePreviewComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojectionDef"](_c0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NgxDropzonePreviewComponent_ngx_dropzone_remove_badge_1_Template, 1, 0, "ngx-dropzone-remove-badge", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.removable);
    } }, directives: function () { return [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], NgxDropzoneRemoveBadgeComponent]; }, styles: ["[_nghost-%COMP%]{background-image:linear-gradient(0deg,#ededed,#efefef,#f1f1f1,#f4f4f4,#f6f6f6)}[_nghost-%COMP%]:focus, [_nghost-%COMP%]:hover{background-image:linear-gradient(0deg,#e3e3e3,#ebeaea,#e8e7e7,#ebeaea,#f4f4f4);outline:0}[_nghost-%COMP%]:focus   ngx-dropzone-remove-badge[_ngcontent-%COMP%], [_nghost-%COMP%]:hover   ngx-dropzone-remove-badge[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   ngx-dropzone-remove-badge[_ngcontent-%COMP%]{opacity:0}[_nghost-%COMP%]     ngx-dropzone-label{overflow-wrap:break-word}"] });
NgxDropzonePreviewComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
];
NgxDropzonePreviewComponent.propDecorators = {
    file: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    removable: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    removed: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    keyEvent: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['keyup', ['$event'],] }],
    hostStyle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['style',] }],
    tabIndex: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['tabindex',] }]
};
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxDropzonePreviewComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'ngx-dropzone-preview',
                template: `
		<ng-content select="ngx-dropzone-label"></ng-content>
		<ngx-dropzone-remove-badge *ngIf="removable" (click)="_remove($event)">
		</ngx-dropzone-remove-badge>
	`,
                styles: [":host{background-image:linear-gradient(0deg,#ededed,#efefef,#f1f1f1,#f4f4f4,#f6f6f6)}:host:focus,:host:hover{background-image:linear-gradient(0deg,#e3e3e3,#ebeaea,#e8e7e7,#ebeaea,#f4f4f4);outline:0}:host:focus ngx-dropzone-remove-badge,:host:hover ngx-dropzone-remove-badge{opacity:1}:host ngx-dropzone-remove-badge{opacity:0}:host ::ng-deep ngx-dropzone-label{overflow-wrap:break-word}"]
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }]; }, { removed: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }], tabIndex: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['tabindex']
        }], file: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], removable: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], keyEvent: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['keyup', ['$event']]
        }], hostStyle: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['style']
        }] }); })();

/**
 * This service contains the filtering logic to be applied to
 * any dropped or selected file. If a file matches all criteria
 * like maximum size or accept type, it will be emitted in the
 * addedFiles array, otherwise in the rejectedFiles array.
 */
class NgxDropzoneService {
    parseFileList(files, accept, maxFileSize, multiple) {
        const addedFiles = [];
        const rejectedFiles = [];
        for (let i = 0; i < files.length; i++) {
            const file = files.item(i);
            if (!this.isAccepted(file, accept)) {
                this.rejectFile(rejectedFiles, file, 'type');
                continue;
            }
            if (maxFileSize && file.size > maxFileSize) {
                this.rejectFile(rejectedFiles, file, 'size');
                continue;
            }
            if (!multiple && addedFiles.length >= 1) {
                this.rejectFile(rejectedFiles, file, 'no_multiple');
                continue;
            }
            addedFiles.push(file);
        }
        const result = {
            addedFiles,
            rejectedFiles
        };
        return result;
    }
    isAccepted(file, accept) {
        if (accept === '*') {
            return true;
        }
        const acceptFiletypes = accept.split(',').map(it => it.toLowerCase().trim());
        const filetype = file.type.toLowerCase();
        const filename = file.name.toLowerCase();
        const matchedFileType = acceptFiletypes.find(acceptFiletype => {
            // check for wildcard mimetype (e.g. image/*)
            if (acceptFiletype.endsWith('/*')) {
                return filetype.split('/')[0] === acceptFiletype.split('/')[0];
            }
            // check for file extension (e.g. .csv)
            if (acceptFiletype.startsWith(".")) {
                return filename.endsWith(acceptFiletype);
            }
            // check for exact mimetype match (e.g. image/jpeg)
            return acceptFiletype == filetype;
        });
        return !!matchedFileType;
    }
    rejectFile(rejectedFiles, file, reason) {
        const rejectedFile = file;
        rejectedFile.reason = reason;
        rejectedFiles.push(rejectedFile);
    }
}
NgxDropzoneService.ɵfac = function NgxDropzoneService_Factory(t) { return new (t || NgxDropzoneService)(); };
NgxDropzoneService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: NgxDropzoneService, factory: NgxDropzoneService.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxDropzoneService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], null, null); })();

class NgxDropzoneComponent {
    constructor(service) {
        this.service = service;
        /** Emitted when any files were added or rejected. */
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /** Set the accepted file types. Defaults to '*'. */
        this.accept = '*';
        this._disabled = false;
        this._multiple = true;
        this._maxFileSize = undefined;
        this._expandable = false;
        this._disableClick = false;
        this._isHovered = false;
    }
    get _hasPreviews() {
        return !!this._previewChildren.length;
    }
    /** Disable any user interaction with the component. */
    get disabled() {
        return this._disabled;
    }
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        if (this._isHovered) {
            this._isHovered = false;
        }
    }
    /** Allow the selection of multiple files. */
    get multiple() {
        return this._multiple;
    }
    set multiple(value) {
        this._multiple = coerceBooleanProperty(value);
    }
    /** Set the maximum size a single file may have. */
    get maxFileSize() {
        return this._maxFileSize;
    }
    set maxFileSize(value) {
        this._maxFileSize = coerceNumberProperty(value);
    }
    /** Allow the dropzone container to expand vertically. */
    get expandable() {
        return this._expandable;
    }
    set expandable(value) {
        this._expandable = coerceBooleanProperty(value);
    }
    /** Open the file selector on click. */
    get disableClick() {
        return this._disableClick;
    }
    set disableClick(value) {
        this._disableClick = coerceBooleanProperty(value);
    }
    /** Show the native OS file explorer to select files. */
    _onClick() {
        if (!this.disableClick) {
            this.showFileSelector();
        }
    }
    _onDragOver(event) {
        if (this.disabled) {
            return;
        }
        this.preventDefault(event);
        this._isHovered = true;
    }
    _onDragLeave() {
        this._isHovered = false;
    }
    _onDrop(event) {
        if (this.disabled) {
            return;
        }
        this.preventDefault(event);
        this._isHovered = false;
        this.handleFileDrop(event.dataTransfer.files);
    }
    showFileSelector() {
        if (!this.disabled) {
            this._fileInput.nativeElement.click();
        }
    }
    _onFilesSelected(event) {
        const files = event.target.files;
        this.handleFileDrop(files);
        // Reset the native file input element to allow selecting the same file again
        this._fileInput.nativeElement.value = '';
        // fix(#32): Prevent the default event behaviour which caused the change event to emit twice.
        this.preventDefault(event);
    }
    handleFileDrop(files) {
        const result = this.service.parseFileList(files, this.accept, this.maxFileSize, this.multiple);
        this.change.next({
            addedFiles: result.addedFiles,
            rejectedFiles: result.rejectedFiles,
            source: this
        });
    }
    preventDefault(event) {
        event.preventDefault();
        event.stopPropagation();
    }
}
NgxDropzoneComponent.ɵfac = function NgxDropzoneComponent_Factory(t) { return new (t || NgxDropzoneComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](NgxDropzoneService, 2)); };
NgxDropzoneComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NgxDropzoneComponent, selectors: [["ngx-dropzone"], ["", "ngx-dropzone", ""]], contentQueries: function NgxDropzoneComponent_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵcontentQuery"](dirIndex, NgxDropzonePreviewComponent, true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx._previewChildren = _t);
    } }, viewQuery: function NgxDropzoneComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstaticViewQuery"](_c2, true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx._fileInput = _t.first);
    } }, hostVars: 8, hostBindings: function NgxDropzoneComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NgxDropzoneComponent_click_HostBindingHandler() { return ctx._onClick(); })("dragover", function NgxDropzoneComponent_dragover_HostBindingHandler($event) { return ctx._onDragOver($event); })("dragleave", function NgxDropzoneComponent_dragleave_HostBindingHandler() { return ctx._onDragLeave(); })("drop", function NgxDropzoneComponent_drop_HostBindingHandler($event) { return ctx._onDrop($event); });
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("ngx-dz-hovered", ctx._isHovered)("ngx-dz-disabled", ctx.disabled)("expandable", ctx.expandable)("unclickable", ctx.disableClick);
    } }, inputs: { accept: "accept", disabled: "disabled", multiple: "multiple", maxFileSize: "maxFileSize", expandable: "expandable", disableClick: "disableClick", id: "id", ariaLabel: ["aria-label", "ariaLabel"], ariaLabelledby: ["aria-labelledby", "ariaLabelledby"], ariaDescribedBy: ["aria-describedby", "ariaDescribedBy"] }, outputs: { change: "change" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([NgxDropzoneService])], ngContentSelectors: _c4, decls: 5, vars: 8, consts: [["type", "file", 3, "id", "multiple", "accept", "disabled", "change"], ["fileInput", ""], [4, "ngIf"]], template: function NgxDropzoneComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojectionDef"](_c3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "input", 0, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function NgxDropzoneComponent_Template_input_change_0_listener($event) { return ctx._onFilesSelected($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, NgxDropzoneComponent_ng_content_2_Template, 1, 0, "ng-content", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](4, 1);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", ctx.id)("multiple", ctx.multiple)("accept", ctx.accept)("disabled", ctx.disabled);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("aria-label", ctx.ariaLabel)("aria-labelledby", ctx.ariaLabelledby)("aria-describedby", ctx.ariaDescribedBy);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx._hasPreviews);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"]], styles: ["[_nghost-%COMP%]{align-items:center;background:#fff;border:2px dashed #717386;border-radius:5px;color:#717386;cursor:pointer;display:flex;font-size:16px;height:180px;overflow-x:auto}.ngx-dz-hovered[_nghost-%COMP%]{border-style:solid}.ngx-dz-disabled[_nghost-%COMP%]{cursor:no-drop;opacity:.5;pointer-events:none}.expandable[_nghost-%COMP%]{flex-wrap:wrap;height:unset;min-height:180px;overflow:hidden}.unclickable[_nghost-%COMP%]{cursor:default}[_nghost-%COMP%]     ngx-dropzone-label{margin:10px auto;text-align:center;z-index:10}[_nghost-%COMP%]   input[_ngcontent-%COMP%]{height:.1px;opacity:0;overflow:hidden;position:absolute;width:.1px;z-index:-1}[_nghost-%COMP%]   input[_ngcontent-%COMP%]:focus +   ngx-dropzone-label{outline:1px dotted #000;outline:5px auto -webkit-focus-ring-color}"] });
NgxDropzoneComponent.ctorParameters = () => [
    { type: NgxDropzoneService, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Self"] }] }
];
NgxDropzoneComponent.propDecorators = {
    _previewChildren: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"], args: [NgxDropzonePreviewComponent, { descendants: true },] }],
    _fileInput: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['fileInput', { static: true },] }],
    change: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    accept: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    disabled: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ngx-dz-disabled',] }],
    multiple: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    maxFileSize: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    expandable: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.expandable',] }],
    disableClick: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.unclickable',] }],
    id: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    ariaLabel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['aria-label',] }],
    ariaLabelledby: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['aria-labelledby',] }],
    ariaDescribedBy: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['aria-describedby',] }],
    _isHovered: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ngx-dz-hovered',] }],
    _onClick: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['click',] }],
    _onDragOver: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['dragover', ['$event'],] }],
    _onDragLeave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['dragleave',] }],
    _onDrop: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['drop', ['$event'],] }]
};
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxDropzoneComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'ngx-dropzone, [ngx-dropzone]',
                template: "<input #fileInput type=\"file\" [id]=\"id\" [multiple]=\"multiple\" [accept]=\"accept\" [disabled]=\"disabled\"\n  (change)=\"_onFilesSelected($event)\" [attr.aria-label]=\"ariaLabel\" [attr.aria-labelledby]=\"ariaLabelledby\"\n  [attr.aria-describedby]=\"ariaDescribedBy\">\n<ng-content select=\"ngx-dropzone-label\" *ngIf=\"!_hasPreviews\"></ng-content>\n<ng-content select=\"ngx-dropzone-preview\"></ng-content>\n<ng-content></ng-content>\n",
                providers: [NgxDropzoneService],
                styles: [":host{align-items:center;background:#fff;border:2px dashed #717386;border-radius:5px;color:#717386;cursor:pointer;display:flex;font-size:16px;height:180px;overflow-x:auto}:host.ngx-dz-hovered{border-style:solid}:host.ngx-dz-disabled{cursor:no-drop;opacity:.5;pointer-events:none}:host.expandable{flex-wrap:wrap;height:unset;min-height:180px;overflow:hidden}:host.unclickable{cursor:default}:host ::ng-deep ngx-dropzone-label{margin:10px auto;text-align:center;z-index:10}:host input{height:.1px;opacity:0;overflow:hidden;position:absolute;width:.1px;z-index:-1}:host input:focus+::ng-deep ngx-dropzone-label{outline:1px dotted #000;outline:5px auto -webkit-focus-ring-color}"]
            }]
    }], function () { return [{ type: NgxDropzoneService, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Self"]
            }] }]; }, { change: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }], accept: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], _isHovered: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['class.ngx-dz-hovered']
        }], disabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['class.ngx-dz-disabled']
        }], multiple: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], maxFileSize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], expandable: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['class.expandable']
        }], disableClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['class.unclickable']
        }], 
    /** Show the native OS file explorer to select files. */
    _onClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['click']
        }], _onDragOver: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['dragover', ['$event']]
        }], _onDragLeave: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['dragleave']
        }], _onDrop: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['drop', ['$event']]
        }], _previewChildren: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"],
            args: [NgxDropzonePreviewComponent, { descendants: true }]
        }], _fileInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['fileInput', { static: true }]
        }], id: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], ariaLabel: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['aria-label']
        }], ariaLabelledby: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['aria-labelledby']
        }], ariaDescribedBy: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['aria-describedby']
        }] }); })();

class NgxDropzoneImagePreviewComponent extends NgxDropzonePreviewComponent {
    constructor(sanitizer) {
        super(sanitizer);
        /** The image data source. */
        this.defaultImgLoading = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBzdHlsZT0ibWFyZ2luOiBhdXRvOyBiYWNrZ3JvdW5kOiByZ2IoMjQxLCAyNDIsIDI0Mykgbm9uZSByZXBlYXQgc2Nyb2xsIDAlIDAlOyBkaXNwbGF5OiBibG9jazsgc2hhcGUtcmVuZGVyaW5nOiBhdXRvOyIgd2lkdGg9IjIyNHB4IiBoZWlnaHQ9IjIyNHB4IiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQiPgo8Y2lyY2xlIGN4PSI1MCIgY3k9IjUwIiByPSIxNCIgc3Ryb2tlLXdpZHRoPSIzIiBzdHJva2U9IiM4NWEyYjYiIHN0cm9rZS1kYXNoYXJyYXk9IjIxLjk5MTE0ODU3NTEyODU1MiAyMS45OTExNDg1NzUxMjg1NTIiIGZpbGw9Im5vbmUiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCI+CiAgPGFuaW1hdGVUcmFuc2Zvcm0gYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIiB0eXBlPSJyb3RhdGUiIGR1cj0iMS4xNjI3OTA2OTc2NzQ0MTg0cyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIGtleVRpbWVzPSIwOzEiIHZhbHVlcz0iMCA1MCA1MDszNjAgNTAgNTAiPjwvYW5pbWF0ZVRyYW5zZm9ybT4KPC9jaXJjbGU+CjxjaXJjbGUgY3g9IjUwIiBjeT0iNTAiIHI9IjEwIiBzdHJva2Utd2lkdGg9IjMiIHN0cm9rZT0iI2JiY2VkZCIgc3Ryb2tlLWRhc2hhcnJheT0iMTUuNzA3OTYzMjY3OTQ4OTY2IDE1LjcwNzk2MzI2Nzk0ODk2NiIgc3Ryb2tlLWRhc2hvZmZzZXQ9IjE1LjcwNzk2MzI2Nzk0ODk2NiIgZmlsbD0ibm9uZSIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIj4KICA8YW5pbWF0ZVRyYW5zZm9ybSBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iIHR5cGU9InJvdGF0ZSIgZHVyPSIxLjE2Mjc5MDY5NzY3NDQxODRzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIga2V5VGltZXM9IjA7MSIgdmFsdWVzPSIwIDUwIDUwOy0zNjAgNTAgNTAiPjwvYW5pbWF0ZVRyYW5zZm9ybT4KPC9jaXJjbGU+CjwhLS0gW2xkaW9dIGdlbmVyYXRlZCBieSBodHRwczovL2xvYWRpbmcuaW8vIC0tPjwvc3ZnPg==';
        this.imageSrc = this.sanitizer.bypassSecurityTrustUrl(this.defaultImgLoading);
    }
    /** The file to preview. */
    set file(value) {
        this._file = value;
        this.renderImage();
    }
    get file() { return this._file; }
    ngOnInit() {
        this.renderImage();
    }
    renderImage() {
        this.readFile()
            .then(img => setTimeout(() => this.imageSrc = img))
            .catch(err => console.error(err));
    }
}
NgxDropzoneImagePreviewComponent.ɵfac = function NgxDropzoneImagePreviewComponent_Factory(t) { return new (t || NgxDropzoneImagePreviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"])); };
NgxDropzoneImagePreviewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NgxDropzoneImagePreviewComponent, selectors: [["ngx-dropzone-image-preview"]], inputs: { file: "file" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([
            {
                provide: NgxDropzonePreviewComponent,
                useExisting: NgxDropzoneImagePreviewComponent
            }
        ]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵInheritDefinitionFeature"]], ngContentSelectors: _c1, decls: 3, vars: 2, consts: [[3, "src"], [3, "click", 4, "ngIf"], [3, "click"]], template: function NgxDropzoneImagePreviewComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojectionDef"](_c0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, NgxDropzoneImagePreviewComponent_ngx_dropzone_remove_badge_2_Template, 1, 0, "ngx-dropzone-remove-badge", 1);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.imageSrc, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.removable);
    } }, directives: function () { return [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], NgxDropzoneRemoveBadgeComponent]; }, styles: ["[_nghost-%COMP%]{max-width:unset!important;min-width:unset!important;padding:0!important}[_nghost-%COMP%]:focus   img[_ngcontent-%COMP%], [_nghost-%COMP%]:hover   img[_ngcontent-%COMP%]{opacity:.7}[_nghost-%COMP%]:focus   ngx-dropzone-remove-badge[_ngcontent-%COMP%], [_nghost-%COMP%]:hover   ngx-dropzone-remove-badge[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   ngx-dropzone-remove-badge[_ngcontent-%COMP%]{opacity:0}[_nghost-%COMP%]   img[_ngcontent-%COMP%]{border-radius:5px;max-height:100%;opacity:.8}[_nghost-%COMP%]     ngx-dropzone-label{overflow-wrap:break-word;position:absolute}"] });
NgxDropzoneImagePreviewComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
];
NgxDropzoneImagePreviewComponent.propDecorators = {
    file: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }]
};
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxDropzoneImagePreviewComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'ngx-dropzone-image-preview',
                template: `
    <img [src]="imageSrc" />
		<ng-content select="ngx-dropzone-label"></ng-content>
    <ngx-dropzone-remove-badge *ngIf="removable" (click)="_remove($event)">
    </ngx-dropzone-remove-badge>
	`,
                providers: [
                    {
                        provide: NgxDropzonePreviewComponent,
                        useExisting: NgxDropzoneImagePreviewComponent
                    }
                ],
                styles: [":host{max-width:unset!important;min-width:unset!important;padding:0!important}:host:focus img,:host:hover img{opacity:.7}:host:focus ngx-dropzone-remove-badge,:host:hover ngx-dropzone-remove-badge{opacity:1}:host ngx-dropzone-remove-badge{opacity:0}:host img{border-radius:5px;max-height:100%;opacity:.8}:host ::ng-deep ngx-dropzone-label{overflow-wrap:break-word;position:absolute}"]
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }]; }, { file: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();

class NgxDropzoneRemoveBadgeComponent {
}
NgxDropzoneRemoveBadgeComponent.ɵfac = function NgxDropzoneRemoveBadgeComponent_Factory(t) { return new (t || NgxDropzoneRemoveBadgeComponent)(); };
NgxDropzoneRemoveBadgeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NgxDropzoneRemoveBadgeComponent, selectors: [["ngx-dropzone-remove-badge"]], decls: 3, vars: 0, consts: [["x1", "0", "y1", "0", "x2", "10", "y2", "10"], ["x1", "0", "y1", "10", "x2", "10", "y2", "0"]], template: function NgxDropzoneRemoveBadgeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "svg");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "line", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "line", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["[_nghost-%COMP%]{align-items:center;background:#bbb;border-radius:50%;color:#333;cursor:pointer;display:flex;height:22px;justify-content:center;position:absolute;right:5px;top:5px;width:22px}[_nghost-%COMP%]:hover{background:#aeaeae}[_nghost-%COMP%] > svg[_ngcontent-%COMP%]{height:10px;width:10px}[_nghost-%COMP%] > svg[_ngcontent-%COMP%] > line[_ngcontent-%COMP%]{stroke:#fff;stroke-width:2px}"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxDropzoneRemoveBadgeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'ngx-dropzone-remove-badge',
                template: `
    <svg>
      <line x1="0" y1="0" x2="10" y2="10" />
      <line x1="0" y1="10" x2="10" y2="0" />
    </svg>
  `,
                styles: [":host{align-items:center;background:#bbb;border-radius:50%;color:#333;cursor:pointer;display:flex;height:22px;justify-content:center;position:absolute;right:5px;top:5px;width:22px}:host:hover{background:#aeaeae}:host>svg{height:10px;width:10px}:host>svg>line{stroke:#fff;stroke-width:2px}"]
            }]
    }], null, null); })();

class NgxDropzoneVideoPreviewComponent extends NgxDropzonePreviewComponent {
    constructor(sanitizer) {
        super(sanitizer);
    }
    ngOnInit() {
        if (!this.file) {
            console.error('No file to read. Please provide a file using the [file] Input property.');
            return;
        }
        /**
         * We sanitize the URL here to enable the preview.
         * Please note that this could cause security issues!
         **/
        this.videoSrc = URL.createObjectURL(this.file);
        this.sanitizedVideoSrc = this.sanitizer.bypassSecurityTrustUrl(this.videoSrc);
    }
    ngOnDestroy() {
        URL.revokeObjectURL(this.videoSrc);
    }
}
NgxDropzoneVideoPreviewComponent.ɵfac = function NgxDropzoneVideoPreviewComponent_Factory(t) { return new (t || NgxDropzoneVideoPreviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"])); };
NgxDropzoneVideoPreviewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NgxDropzoneVideoPreviewComponent, selectors: [["ngx-dropzone-video-preview"]], features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([
            {
                provide: NgxDropzonePreviewComponent,
                useExisting: NgxDropzoneVideoPreviewComponent
            }
        ]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵInheritDefinitionFeature"]], ngContentSelectors: _c1, decls: 3, vars: 2, consts: [["controls", "", 3, "click", 4, "ngIf"], [3, "click", 4, "ngIf"], ["controls", "", 3, "click"], [3, "src"], [3, "click"]], template: function NgxDropzoneVideoPreviewComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojectionDef"](_c0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, NgxDropzoneVideoPreviewComponent_video_0_Template, 2, 1, "video", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, NgxDropzoneVideoPreviewComponent_ngx_dropzone_remove_badge_2_Template, 1, 0, "ngx-dropzone-remove-badge", 1);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.sanitizedVideoSrc);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.removable);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], NgxDropzoneRemoveBadgeComponent], styles: ["[_nghost-%COMP%]{max-width:unset!important;min-width:unset!important;padding:0!important}[_nghost-%COMP%]:focus   video[_ngcontent-%COMP%], [_nghost-%COMP%]:hover   video[_ngcontent-%COMP%]{opacity:.7}[_nghost-%COMP%]:focus   ngx-dropzone-remove-badge[_ngcontent-%COMP%], [_nghost-%COMP%]:hover   ngx-dropzone-remove-badge[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   ngx-dropzone-remove-badge[_ngcontent-%COMP%]{opacity:0}[_nghost-%COMP%]   video[_ngcontent-%COMP%]{border-radius:5px;max-height:100%}[_nghost-%COMP%]     ngx-dropzone-label{overflow-wrap:break-word;position:absolute}"] });
NgxDropzoneVideoPreviewComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxDropzoneVideoPreviewComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'ngx-dropzone-video-preview',
                template: `
    <video *ngIf="sanitizedVideoSrc" controls (click)="$event.stopPropagation()">
      <source [src]="sanitizedVideoSrc" />
    </video>
    <ng-content select="ngx-dropzone-label"></ng-content>
    <ngx-dropzone-remove-badge *ngIf="removable" (click)="_remove($event)">
    </ngx-dropzone-remove-badge>
	`,
                providers: [
                    {
                        provide: NgxDropzonePreviewComponent,
                        useExisting: NgxDropzoneVideoPreviewComponent
                    }
                ],
                styles: [":host{max-width:unset!important;min-width:unset!important;padding:0!important}:host:focus video,:host:hover video{opacity:.7}:host:focus ngx-dropzone-remove-badge,:host:hover ngx-dropzone-remove-badge{opacity:1}:host ngx-dropzone-remove-badge{opacity:0}:host video{border-radius:5px;max-height:100%}:host ::ng-deep ngx-dropzone-label{overflow-wrap:break-word;position:absolute}"]
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }]; }, null); })();

class NgxDropzoneModule {
}
NgxDropzoneModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: NgxDropzoneModule });
NgxDropzoneModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function NgxDropzoneModule_Factory(t) { return new (t || NgxDropzoneModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](NgxDropzoneModule, { declarations: function () { return [NgxDropzoneComponent, NgxDropzoneLabelDirective, NgxDropzonePreviewComponent, NgxDropzoneImagePreviewComponent, NgxDropzoneRemoveBadgeComponent, NgxDropzoneVideoPreviewComponent]; }, imports: function () { return [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]]; }, exports: function () { return [NgxDropzoneComponent, NgxDropzoneLabelDirective, NgxDropzonePreviewComponent, NgxDropzoneImagePreviewComponent, NgxDropzoneRemoveBadgeComponent, NgxDropzoneVideoPreviewComponent]; } }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxDropzoneModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
                ],
                declarations: [
                    NgxDropzoneComponent,
                    NgxDropzoneLabelDirective,
                    NgxDropzonePreviewComponent,
                    NgxDropzoneImagePreviewComponent,
                    NgxDropzoneRemoveBadgeComponent,
                    NgxDropzoneVideoPreviewComponent,
                ],
                exports: [
                    NgxDropzoneComponent,
                    NgxDropzoneLabelDirective,
                    NgxDropzonePreviewComponent,
                    NgxDropzoneImagePreviewComponent,
                    NgxDropzoneRemoveBadgeComponent,
                    NgxDropzoneVideoPreviewComponent,
                ]
            }]
    }], null, null); })();

/*
 * Public API Surface of ngx-dropzone
 */

/**
 * Generated bundle index. Do not edit.
 */



//# sourceMappingURL=ngx-dropzone.js.map

/***/ }),

/***/ "./src/app/cpn/actualite/actualite.component.ts":
/*!******************************************************!*\
  !*** ./src/app/cpn/actualite/actualite.component.ts ***!
  \******************************************************/
/*! exports provided: ActualiteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActualiteComponent", function() { return ActualiteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../side-bar/side-bar.component */ "./src/app/cpn/side-bar/side-bar.component.ts");



class ActualiteComponent {
    constructor() { }
    ngOnInit() {
        $(document).ready(function () {
            $('.customer-logos').slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1500,
                arrows: false,
                dots: false,
                pauseOnHover: false,
                responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 4
                        }
                    }, {
                        breakpoint: 520,
                        settings: {
                            slidesToShow: 3
                        }
                    }]
            });
        });
    }
}
ActualiteComponent.ɵfac = function ActualiteComponent_Factory(t) { return new (t || ActualiteComponent)(); };
ActualiteComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ActualiteComponent, selectors: [["app-actualite"]], decls: 151, vars: 0, consts: [[1, "actuality_container", 2, "border", "1px solid #dce1e4"], [1, "actuality_wrapper"], [1, "container", "col-11"], [1, "row", "justify-content-center"], [1, "actuality_content", "col-md-12", 2, "display", "contents", "flex-direction", "row", "justify-content", "center"], [1, "row", "mb-3", "justify-content-center", 2, "margin-top", "37px"], [1, "col-auto"], [1, "m-0", 2, "font-weight", "bold", "color", "rgb(33, 7, 94)"], [1, "row", "mb-5", "g-4", "justify-content-center", 2, "margin-top", "-2px"], [1, "col-md-4", "col-lg-4", "col-xl-4"], [1, "act-wrapper", "shadow", "rounded-3", "bg-white", 2, "overflow", "hidden"], [1, "act-img", 2, "height", "230px"], ["src", "assets/cpnimages/actualites/act1.png", "alt", "actualite", 2, "width", "100%", "height", "100%", "object-fit", "cover"], [1, "act-content", "p-2", 2, "height", "190px"], [1, "act-title"], [1, "act-desc"], [1, "m-0"], [1, "act-button", "d-flex", "flex-row", "justify-content-end"], [2, "border", "none", "background", "transparent"], [1, "readMore"], [2, "border-left", "2px solid black", "padding-left", "10px"], ["src", "assets/cpnimages/actualites/act6.png", "alt", "", 2, "width", "100%", "height", "100%", "object-fit", "cover"], ["src", "assets/cpnimages/actualites/act5.png", "alt", "", 2, "width", "100%", "height", "100%", "object-fit", "cover"], [1, "rowq", "col-lg-12", "mb-5", 2, "float", "left !important"], [1, "col-md-12"], [1, "row", "justify-content-start"], [1, "m-0", 2, "font-weight", "700", "font-size", "33px", "color", "rgb(33, 7, 94)"], [1, "button-wrapper"], ["data-slide", "prev", "href", " #carouselExampleIndicators2", 1, "btn", "btn-left"], ["data-slide", "next", "href", " #carouselExampleIndicators2", 1, "btn", "btn-right"], [2, "height", "unset"], [1, "pt-5", "pb-5", 2, "margin-top", "-70px"], [1, "row", "d-flex", "align-items-center"], [1, "row"], [1, "col-6"], [1, "col-12"], ["id", "carouselExampleIndicators2", "data-ride", "carousel", 1, "carousel", "slide"], [1, "carousel-inner"], [1, "carousel-item", "active"], [1, "col-md-4", "mb-3"], [1, "card"], [1, "row", "no-gutters"], [1, "col-sm-5"], ["src", "assets/cpnimages/actualites/act1.png", "alt", "Suresh Dasari Card", 1, "card-img", 2, "height", "100%"], [1, "col-sm-7"], [1, "card-body"], [1, "card-title"], [1, "card-text"], ["src", "assets/cpnimages/actualites/act2.png", "alt", "Suresh Dasari Card", 1, "card-img", 2, "height", "100%"], ["src", "assets/cpnimages/actualites/act3.png", "alt", "Suresh Dasari Card", 1, "card-img", 2, "height", "100%"], [1, "carousel-item"], ["src", "assets/cpnimages/actualites/act4.png", "alt", "Suresh Dasari Card", 1, "card-img", 2, "height", "100%"], ["src", "assets/cpnimages/actualites/act5.png", "alt", "Suresh Dasari Card", 1, "card-img", 2, "height", "100%"], ["src", "assets/cpnimages/actualites/act6.png", "alt", "Suresh Dasari Card", 1, "card-img", 2, "height", "100%"]], template: function ActualiteComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-side-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h1", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Actualit\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "img", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, " Num\u00E9riser votre entreprise n'a jamais \u00E9t\u00E9 aussi simple !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, " Nous sommes depuis quelques ann\u00E9es rentr\u00E9 dans l'\u00E8re du digital...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, " Lire la suite..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "span", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "25 fevrier");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " La France se digitalise ! ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "En France, un bon nombre d\u2019entreprises fran\u00E7aises n'ont pas encore int\u00E9gr\u00E9 l\u2019enjeu autour de la digitalisation des entreprises... ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Lire la suite.. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "span", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "7 Mars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, " TPE - PME : Une opportunit\u00E9 \u00E0 saisir ! ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "2020 \u00E9tait une ann\u00E9e tr\u00E8s dure pour les chefs d\u2019entreprises, ind\u00E9pendants, commer\u00E7ants, TPE, PME, artisans..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, " Lire la suite..");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "span", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "25 fevrier");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "h4", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Autre actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, " < ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "button", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, ">");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "hr", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "section", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "img", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "Num\u00E9riser votre entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "Num\u00E9riser votre entreprise n'a jamais \u00E9t\u00E9 aussi simple !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "img", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Des entreprises fran\u00E7aises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Des entreprises fran\u00E7aises plus matures face \u00E0 la");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "img", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Suresh Dasari");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "Suresh Dasari is a founder and technical lead developer in tutlane.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](122, "img", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Suresh Dasari");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "Suresh Dasari is a founder and technical lead developer in tutlane.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](133, "img", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](137, "Suresh Dasari");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](139, "Suresh Dasari is a founder and technical lead developer in tutlane.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "img", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, "Suresh Dasari");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](150, "Suresh Dasari is a founder and technical lead developer in tutlane.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_1__["SideBarComponent"]], styles: ["h2[_ngcontent-%COMP%] {\n    text-align: center;\n    padding: 20px;\n}\n.actuality_container[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n}\n\n.slick-slide[_ngcontent-%COMP%] {\n    margin: 0px 20px;\n}\n.slick-slide[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 100%;\n}\n.slick-slider[_ngcontent-%COMP%] {\n    position: relative;\n    display: block;\n    box-sizing: border-box;\n    -webkit-user-select: none;\n    user-select: none;\n    -webkit-touch-callout: none;\n    -khtml-user-select: none;\n    touch-action: pan-y;\n    -webkit-tap-highlight-color: transparent;\n}\n.slick-list[_ngcontent-%COMP%] {\n    position: relative;\n    display: block;\n    overflow: hidden;\n    margin: 0;\n    padding: 0;\n}\n.slick-list[_ngcontent-%COMP%]:focus {\n    outline: none;\n}\n.slick-list.dragging[_ngcontent-%COMP%] {\n    cursor: pointer;\n    cursor: hand;\n}\n.slick-slider[_ngcontent-%COMP%]   .slick-track[_ngcontent-%COMP%], .slick-slider[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%] {\n    transform: translate3d(0, 0, 0);\n}\n.slick-track[_ngcontent-%COMP%] {\n    position: relative;\n    top: 0;\n    left: 0;\n    display: block;\n}\n.slick-track[_ngcontent-%COMP%]:before, .slick-track[_ngcontent-%COMP%]:after {\n    display: table;\n    content: '';\n}\n.slick-track[_ngcontent-%COMP%]:after {\n    clear: both;\n}\n.slick-loading[_ngcontent-%COMP%]   .slick-track[_ngcontent-%COMP%] {\n    visibility: hidden;\n}\n.slick-slide[_ngcontent-%COMP%] {\n    display: none;\n    float: left;\n    height: 100%;\n    min-height: 1px;\n}\n[dir='rtl'][_ngcontent-%COMP%]   .slick-slide[_ngcontent-%COMP%] {\n    float: right;\n}\n.slick-slide[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    display: block;\n}\n.slick-slide.slick-loading[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    display: none;\n}\n.slick-slide.dragging[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    pointer-events: none;\n}\n.slick-initialized[_ngcontent-%COMP%]   .slick-slide[_ngcontent-%COMP%] {\n    display: block;\n}\n.slick-loading[_ngcontent-%COMP%]   .slick-slide[_ngcontent-%COMP%] {\n    visibility: hidden;\n}\n.slick-vertical[_ngcontent-%COMP%]   .slick-slide[_ngcontent-%COMP%] {\n    display: block;\n    height: auto;\n    border: 1px solid transparent;\n}\n.slick-arrow.slick-hidden[_ngcontent-%COMP%] {\n    display: none;\n}\n.card[_ngcontent-%COMP%] {\n\n  border: none;\n}\n.act-title[_ngcontent-%COMP%] {\n  margin-left: 6px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vYWN0dWFsaXRlL2FjdHVhbGl0ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLGFBQWE7QUFDakI7QUFDQTtJQUNJLHlCQUF5QjtBQUM3QjtBQUNBLFdBQVc7QUFFWDtJQUNJLGdCQUFnQjtBQUNwQjtBQUVBO0lBQ0ksV0FBVztBQUNmO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFHekIsaUJBQWlCO0lBQ2pCLDJCQUEyQjtJQUMzQix3QkFBd0I7SUFFeEIsbUJBQW1CO0lBQ25CLHdDQUF3QztBQUM1QztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsU0FBUztJQUNULFVBQVU7QUFDZDtBQUVBO0lBQ0ksYUFBYTtBQUNqQjtBQUVBO0lBQ0ksZUFBZTtJQUNmLFlBQVk7QUFDaEI7QUFFQTs7SUFNSSwrQkFBK0I7QUFDbkM7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sT0FBTztJQUNQLGNBQWM7QUFDbEI7QUFFQTs7SUFFSSxjQUFjO0lBQ2QsV0FBVztBQUNmO0FBRUE7SUFDSSxXQUFXO0FBQ2Y7QUFFQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0ksYUFBYTtJQUNiLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtBQUNuQjtBQUVBO0lBQ0ksWUFBWTtBQUNoQjtBQUVBO0lBQ0ksY0FBYztBQUNsQjtBQUVBO0lBQ0ksYUFBYTtBQUNqQjtBQUVBO0lBQ0ksb0JBQW9CO0FBQ3hCO0FBRUE7SUFDSSxjQUFjO0FBQ2xCO0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7QUFFQTtJQUNJLGNBQWM7SUFDZCxZQUFZO0lBQ1osNkJBQTZCO0FBQ2pDO0FBRUE7SUFDSSxhQUFhO0FBQ2pCO0FBQ0E7O0VBRUUsWUFBWTtBQUNkO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEIiLCJmaWxlIjoiYXBwL2Nwbi9hY3R1YWxpdGUvYWN0dWFsaXRlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoMiB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDIwcHg7XG59XG4uYWN0dWFsaXR5X2NvbnRhaW5lcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xufVxuLyogU2xpZGVyICovXG5cbi5zbGljay1zbGlkZSB7XG4gICAgbWFyZ2luOiAwcHggMjBweDtcbn1cblxuLnNsaWNrLXNsaWRlIGltZyB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5zbGljay1zbGlkZXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lO1xuICAgIC1raHRtbC11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAtbXMtdG91Y2gtYWN0aW9uOiBwYW4teTtcbiAgICB0b3VjaC1hY3Rpb246IHBhbi15O1xuICAgIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbi5zbGljay1saXN0IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbn1cblxuLnNsaWNrLWxpc3Q6Zm9jdXMge1xuICAgIG91dGxpbmU6IG5vbmU7XG59XG5cbi5zbGljay1saXN0LmRyYWdnaW5nIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgY3Vyc29yOiBoYW5kO1xufVxuXG4uc2xpY2stc2xpZGVyIC5zbGljay10cmFjayxcbi5zbGljay1zbGlkZXIgLnNsaWNrLWxpc3Qge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcbiAgICAtbW96LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XG4gICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XG4gICAgLW8tdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xufVxuXG4uc2xpY2stdHJhY2sge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cblxuLnNsaWNrLXRyYWNrOmJlZm9yZSxcbi5zbGljay10cmFjazphZnRlciB7XG4gICAgZGlzcGxheTogdGFibGU7XG4gICAgY29udGVudDogJyc7XG59XG5cbi5zbGljay10cmFjazphZnRlciB7XG4gICAgY2xlYXI6IGJvdGg7XG59XG5cbi5zbGljay1sb2FkaW5nIC5zbGljay10cmFjayB7XG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xufVxuXG4uc2xpY2stc2xpZGUge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG1pbi1oZWlnaHQ6IDFweDtcbn1cblxuW2Rpcj0ncnRsJ10gLnNsaWNrLXNsaWRlIHtcbiAgICBmbG9hdDogcmlnaHQ7XG59XG5cbi5zbGljay1zbGlkZSBpbWcge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4uc2xpY2stc2xpZGUuc2xpY2stbG9hZGluZyBpbWcge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5zbGljay1zbGlkZS5kcmFnZ2luZyBpbWcge1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuXG4uc2xpY2staW5pdGlhbGl6ZWQgLnNsaWNrLXNsaWRlIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cblxuLnNsaWNrLWxvYWRpbmcgLnNsaWNrLXNsaWRlIHtcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XG59XG5cbi5zbGljay12ZXJ0aWNhbCAuc2xpY2stc2xpZGUge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGhlaWdodDogYXV0bztcbiAgICBib3JkZXI6IDFweCBzb2xpZCB0cmFuc3BhcmVudDtcbn1cblxuLnNsaWNrLWFycm93LnNsaWNrLWhpZGRlbiB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cbi5jYXJkIHtcblxuICBib3JkZXI6IG5vbmU7XG59XG4uYWN0LXRpdGxlIHtcbiAgbWFyZ2luLWxlZnQ6IDZweDtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ActualiteComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-actualite',
                templateUrl: './actualite.component.html',
                styleUrls: ['./actualite.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/agence/agence.component.ts":
/*!************************************************!*\
  !*** ./src/app/cpn/agence/agence.component.ts ***!
  \************************************************/
/*! exports provided: AgenceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgenceComponent", function() { return AgenceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var ng_lazyload_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng-lazyload-image */ "./node_modules/ng-lazyload-image/__ivy_ngcc__/fesm2015/ng-lazyload-image.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../notre-succes/notre-succes.component */ "./src/app/cpn/notre-succes/notre-succes.component.ts");
/* harmony import */ var _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../side-bar/side-bar.component */ "./src/app/cpn/side-bar/side-bar.component.ts");
/* harmony import */ var _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../map-french-region/map-french-region.component */ "./src/app/cpn/map-french-region/map-french-region.component.ts");








class AgenceComponent {
    constructor(auth) {
        this.auth = auth;
    }
    ngOnInit() {
        this.auth.getFellower().subscribe(res => {
            this.follow = res;
        });
        $('.search').mouseenter(function () {
            $(this).addClass('search--show');
            $(this).removeClass('search--hide');
        });
        $('.search').mouseleave(function () {
            $(this).addClass('search--hide');
            $(this).removeClass('search--show');
        });
    }
}
AgenceComponent.ɵfac = function AgenceComponent_Factory(t) { return new (t || AgenceComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"])); };
AgenceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AgenceComponent, selectors: [["app-agence"]], decls: 129, vars: 3, consts: [[1, "primary_body", "mb-5"], [1, "home_container"], [1, "section_heading", "mb-3"], [1, "heading_wrapper", "container-fluid", "g-0"], [1, "row", "g-0"], [1, "col-md-6"], [1, "row", "g-0", "justify-content-center"], [1, "col-auto"], [1, "img_wrapper"], ["alt", "", 1, "heading_img", 2, "margin-top", "-76px", "margin-left", "-8px", 3, "defaultImage", "lazyLoad"], [1, "col-md-6", "p-3"], [1, "row", "g-0", "justify-content-start"], [1, "col-md-auto"], [1, "title_heading"], [1, "desc_heading"], [1, "search_bloc"], [1, "search"], ["placeholder", "Quel type de subvention souhaitez vous", 1, "search__input"], [1, "carre"], [1, "row"], [1, "col", 2, "display", "initial", "flex-direction", "row", "padding", "8px"], ["src", "assets/cpnimages/home/I.png", "alt", "", 2, "width", "10%", "margin", "0 0 0 30px"], [2, "margin-left", "4px", "font-size", "13px"], [2, "text-align", "center", "margin", "0 90px 0 0"], [2, "text-align", "center", "font-size", "12px", "margin", "0 0px 0 -45px"], [2, "color", "#00FF00"], [1, "g-0", "mb-5", 2, "margin-top", "150px"], [1, "container", "px-4"], [1, "divider", "g-0", "mb-5"], [1, "divider_ligne"], [1, "g-0", "mb-5"], [1, "actu"], [1, "row", "mb-5", "g-4", "justify-content-center"], [1, "col-md-4", "col-lg-4", "col-xl-4", "feuil"], [1, "act-wrapper", "bg-white"], [1, "act-img"], ["src", "assets/cpnimages/actualites/act1.png", "alt", "actualite"], [1, "act-content", "p-2", 2, "max-height", "100px"], [1, "act-title"], [1, "act-desc"], [1, "m-0"], ["src", "assets/cpnimages/actualites/act5.png", "alt", "actualite", 2, "width", "100%", "max-height", "185px", "object-fit", "cover", "border-radius", "15px"], ["src", "assets/cpnimages/actualites/act6.png", "alt", "actualite", 2, "width", "100%", "max-height", "165px", "object-fit", "cover", "border-radius", "15px"], [2, "color", "#111d5e"], [1, "container", "px-4", "proposons"], [1, "1"], [1, "SousB"], [1, "fas", "fa-long-arrow-alt-right"], [1, "SousB", 2, "border", "3px solid #111d5e"]], template: function AgenceComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "h2", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "AGENCES");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h4", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Outils d'accompagnement de vos clients");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "form", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "span", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Followers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h4", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "2.1%");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "vs last 7 days");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "app-notre-succes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "app-side-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "section", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Choisir ma r\u00E9gion");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "app-map-french-region");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "section", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "span", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " Actualite ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "section", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "img", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, " Num\u00E9riser votre entreprise n'a jamais \u00E9t\u00E9 aussi simple!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Nous sommes depuis quelques ann\u00E9es rentr\u00E9 dans l'\u00E8re... Lire la suite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "img", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "TPE - PME : Une opportunit\u00E9 \u00E0 saisir !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "p", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "2020 \u00E9tait une ann\u00E9e tr\u00E8s dure pour les chefs d\u2019entreprises...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, " Lire la suite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "img", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "La France se digitalise !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "p", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "En France, un bon nombre d\u2019entreprises fran\u00E7aises n'ont...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "Lire la suite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, " Nous proposons ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "h3", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, " Peut importe votre secteur d\u2019activit\u00E9 nous vous offrons une subvention.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "1.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "400+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Speed Optimization");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "i", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "Lorem Ipsum is simply dummy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "2.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "600+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "Cloud Solutions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "i", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "Lorem Ipsum is simply dummy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "3.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "820+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Online Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "i", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "Lorem Ipsum is simply dummy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("defaultImage", "assets/cpnimages/home/22.png")("lazyLoad", "assets/cpnimages/home/22.png");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.follow, "K");
    } }, directives: [ng_lazyload_image__WEBPACK_IMPORTED_MODULE_2__["LazyLoadImageDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_4__["NotreSuccesComponent"], _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_5__["SideBarComponent"], _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_6__["MapFrenchRegionComponent"]], styles: [".carre[_ngcontent-%COMP%] {\n  width: 200px;\n  height: 90px;\n  background: white;\n  border-radius: 18px;\n  margin-left: -250px;\n  margin-top: 125px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n\t background:#EBECF0;\n\t min-height: -moz-fit-content;\n\t min-height: fit-content;\n\t border-bottom-right-radius: 100px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n\t height: 100%;\n\t max-height: 585px;\n\t margin-top: -35px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n\t font-size: 60px;\n\t font-weight: 800;\n\t color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n\t font-size: 40px;\n\t font-weight: 700;\n\t color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%], .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:after {\n\t box-sizing: border-box;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   body[_ngcontent-%COMP%] {\n\t background: #f5f5f5;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   main[_ngcontent-%COMP%] {\n\t left: 50%;\n\t position: absolute;\n\t top: 50%;\n\t transform: translateX(-50%) translateY(-50%);\n\t width: 300px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\n\t content: \"\";\n\t display: block;\n\t position: absolute;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before {\n\t border: 5px solid #111d5e ;\n\t border-radius: 20px;\n\t height: 40px;\n\t transition: all 0.3s ease-out;\n\t transition-delay: 0.3s;\n\t width: 40px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\n\t background: #111d5e;\n\t border-radius: 3px;\n\t height: 5px;\n\t transform: rotate(-45deg);\n\t transform-origin: 0% 100%;\n\t transition: all 0.3s ease-out;\n\t width: 15px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n\t background: transparent;\n\t border: none;\n\t border-radius: 20px;\n\t display: block;\n\t font-size: 20px;\n\t height: 40px;\n\t line-height: 40px;\n\t opacity: 0;\n\t outline: none;\n\t padding: 0 15px;\n\t position: relative;\n\t transition: all 0.3s ease-out;\n\t transition-delay: 0.6s;\n\t width: 40px;\n\t z-index: 1;\n\t color: rgb(85, 85, 85);\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:before {\n\t transition-delay: 0.3s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:after {\n\t transition-delay: 0.6s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n\t transition-delay: 0s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:after {\n\t transform: rotate(-45deg) translateX(15px) translateY(-2px);\n\t width: 0;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n\t border: 5px solid #111d5e;\n\t border-radius: 20px;\n\t height: 40px;\n\t width: 500px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n\t opacity: 1;\n\t width: 500px;\n}\n\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 5px;\n  background: #111d5e;\n  display: block;\n  position: relative;\n}\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::before {\n  content: \"\";\n  position: absolute;\n  width: 30%;\n  top: 0;\n  left: 0;\n  height: 5px;\n  background: red;\n}\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::after {\n  content: \"\";\n  position: absolute;\n  width: 30%;\n  top: 0;\n  right: 0;\n  height: 5px;\n  background: red;\n}\ninput[type=\"text\"][_ngcontent-%COMP%] {\n  height: 50px;\n  font-size: 30px;\n  display: inline-block;\n\n  font-weight: 100;\n  border: none;\n  outline: none;\n  color: white;\n  padding: 3px;\n  padding-right: 60px;\n  width: 0px;\n  position: absolute;\n  top: 0;\n  right: 0;\n  background: none;\n  z-index: 3;\n  transition: width 0.4s cubic-bezier(0, 0.795, 0, 1);\n  cursor: pointer;\n}\ninput[type=\"text\"][_ngcontent-%COMP%]:focus:hover {\n  border-bottom: 1px solid white;\n}\ninput[type=\"text\"][_ngcontent-%COMP%]:focus {\n  width: 700px;\n  z-index: 1;\n  border-bottom: 1px solid white;\n  cursor: text;\n}\ninput[type=\"submit\"][_ngcontent-%COMP%] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  color: white;\n  float: right;\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADNQTFRFU1NT9fX1lJSUXl5e1dXVfn5+c3Nz6urqv7+/tLS0iYmJqampn5+fysrK39/faWlp////Vi4ZywAAABF0Uk5T/////////////////////wAlrZliAAABLklEQVR42rSWWRbDIAhFHeOUtN3/ags1zaA4cHrKZ8JFRHwoXkwTvwGP1Qo0bYObAPwiLmbNAHBWFBZlD9j0JxflDViIObNHG/Do8PRHTJk0TezAhv7qloK0JJEBh+F8+U/hopIELOWfiZUCDOZD1RADOQKA75oq4cvVkcT+OdHnqqpQCITWAjnWVgGQUWz12lJuGwGoaWgBKzRVBcCypgUkOAoWgBX/L0CmxN40u6xwcIJ1cOzWYDffp3axsQOyvdkXiH9FKRFwPRHYZUaXMgPLeiW7QhbDRciyLXJaKheCuLbiVoqx1DVRyH26yb0hsuoOFEPsoz+BVE0MRlZNjGZcRQyHYkmMp2hBTIzdkzCTc/pLqOnBrk7/yZdAOq/q5NPBH1f7x7fGP4C3AAMAQrhzX9zhcGsAAAAASUVORK5CYII=)\n    center center no-repeat;\n  text-indent: -10000px;\n  border: none;\n  position: absolute;\n  top: 0;\n  right: 0;\n  z-index: 2;\n  cursor: pointer;\n  opacity: 0.4;\n  cursor: pointer;\n  transition: opacity 0.4s ease;\n}\ninput[type=\"submit\"][_ngcontent-%COMP%]:hover {\n  opacity: 0.8;\n}\nh1[_ngcontent-%COMP%]{\n  font-weight: bold;\n  color: #111d5e;\n\n}\nh5[_ngcontent-%COMP%]{\n  color: #111d5e;\n  font-size: 15px;\n}\np[_ngcontent-%COMP%]{\n  font-size: 12px;\n  color: #111d5e;\n\n}\n\n.actu[_ngcontent-%COMP%]{\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n}\n.actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n  width: 70%;\n}\n.feuil[_ngcontent-%COMP%]{\n  flex: 0 0 auto;\n  width: 33.33333333%;\n}\n.actu[_ngcontent-%COMP%]   .act-wrapper[_ngcontent-%COMP%]{\n  overflow: hidden;\n  border-radius: 15px;\n  background-color: #EBECF0 ;\n  box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;\n}\n.actu[_ngcontent-%COMP%]   .act-wrapper[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  width: 100%;\n   max-height:70%;\n    object-fit:cover;\n  border-radius: 15px;\n}\n\n.proposons[_ngcontent-%COMP%]{\n    height: 300px;\n    background: linear-gradient(#ffffff, #e5e6e7);\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n  }\n.proposons[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    margin: 0;\n  }\n.SousB[_ngcontent-%COMP%]{\n    border: 3px solid rgb(41, 191, 211);\n    border-radius: 15px;\n    height: 160px;\n    margin: 0;\n    padding: 20px;\n    width: 300px;\n  }\n.SousB[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n  }\n.SousB[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%]{\n    font-size: 15px;\nfont-weight: bold;\nline-height: normal;\n  }\n.SousB[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    font-size: 15px;\n  }\n.SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n    margin-left: 277px;\nz-index: auto;\nfont-size: 26px;\nmargin-top: -50px;\n  }\n\n\n@media only screen and (min-width : 320px) and (max-width : 480px)  {\n        \n      \n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n      border-bottom-right-radius: 0px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n      height: 100%;\n      max-height: 400px;\n      margin-top: -35px;\n    }\n    \n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n  font-size: 30px;\n  font-weight: 800;\n  color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n  font-size: 20px;\n  font-weight: 700;\n  color:  #111d5e;\n}\n  \n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n      border: 5px solid #111d5e;\n      border-radius: 20px;\n      height: 40px;\n      width: 300px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n      opacity: 1;\n      width: 300px;\n    }\n    .carre[_ngcontent-%COMP%] {\n      width: 200px;\n      height: 90px;\n      background: white;\n      border-radius: 18px;\n      margin-left: 0px;\n      margin-top: 125px;\n    }\n          \n          .actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n            width: 100%;\n          }\n          .feuil[_ngcontent-%COMP%]{\n            flex: 0 0 auto;\n            width: 70%;\n          }\n    \n        .proposons[_ngcontent-%COMP%]{\n          height: 800px;\n      background: linear-gradient(#ffffff, #e5e6e7);\n      display: flex;\n      flex-direction: column;\n      justify-content: space-around;\n      align-items: center;\n        }\n        .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n          margin-left: 77px;\n      z-index: auto;\n      font-size: 26px;\n      margin-top: 70px;\n      transform: rotate(90deg);\n      position: absolute;\n        }\n      \n     \n    }\n\n@media only screen and (min-width : 480px) and (max-width : 768px)  {\n  \n             \n      \n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n      border-bottom-right-radius: 0px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n      font-size: 40px;\n      font-weight: 800;\n      color:  #111d5e;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n      font-size: 30px;\n      font-weight: 700;\n      color:  #111d5e;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n      border: 5px solid #111d5e;\n      border-radius: 20px;\n      height: 40px;\n      width: 300px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n      opacity: 1;\n      width: 300px;\n    }\n    .carre[_ngcontent-%COMP%] {\n      width: 200px;\n      height: 90px;\n      background: white;\n      border-radius: 18px;\n      margin-left: 0px;\n      margin-top: 125px;\n    }\n\n          \n          .actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n            width: 100%;\n          }\n          .feuil[_ngcontent-%COMP%]{\n            flex: 0 0 auto;\n            width: 70%;\n          }\n\n    \n        .proposons[_ngcontent-%COMP%]{\n          height: 800px;\n      background: linear-gradient(#ffffff, #e5e6e7);\n      display: flex;\n      flex-direction: column;\n      justify-content: space-around;\n      align-items: center;\n        }\n        .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n          margin-left: 77px;\n      z-index: auto;\n      font-size: 26px;\n      margin-top: 70px;\n      transform: rotate(90deg);\n      position: absolute;\n        }\n      \n     \n    }\n\n@media only screen and (min-width : 768px) and (max-width : 992px)  {\n    \n             \n      \n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n      border-bottom-right-radius: 0px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n      font-size: 40px;\n      font-weight: 800;\n      color:  #111d5e;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n      font-size: 30px;\n      font-weight: 700;\n      color:  #111d5e;\n    }\n  \n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n      border: 5px solid #111d5e;\n      border-radius: 20px;\n      height: 40px;\n      width: 300px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n      opacity: 1;\n      width: 300px;\n    }\n\n    \n        .proposons[_ngcontent-%COMP%]{\n          height: 800px;\n      background: linear-gradient(#ffffff, #e5e6e7);\n      display: flex;\n      flex-direction: column;\n      justify-content: space-around;\n      align-items: center;\n        }\n        .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n          margin-left: 77px;\n      z-index: auto;\n      font-size: 26px;\n      margin-top: 70px;\n      transform: rotate(90deg);\n      position: absolute;\n        }\n      \n        .actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n          display: flex;\n          flex-direction: column;\n          justify-content: center;\n          align-items: center;\n          width: 100%;\n        }\n        .feuil[_ngcontent-%COMP%]{\n          flex: 0 0 auto;\n          width: 70%;\n        }\n     }\n\n@media only screen and (min-width : 992px) and (max-width : 1200px)  {\n        \n          \n  \n          .SousB[_ngcontent-%COMP%]{\n            border: 3px solid rgb(41, 191, 211);\n            border-radius: 15px;\n            height: 160px;\n            margin: 0;\n            padding: 20px;\n            width: 250px;\n          }\n          .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n            margin-left: 225px;\n        z-index: auto;\n        font-size: 26px;\n        margin-top: -50px;\n          }\n          \n        \n          .actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n            width: 100%;\n          }\n          .feuil[_ngcontent-%COMP%]{\n            flex: 0 0 auto;\n            width: 70%;\n          }\n        }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vYWdlbmNlL2FnZW5jZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9GQUFvRjtBQUNwRjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsaUJBQWlCO0FBQ25CO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsNEJBQXVCO0VBQXZCLHVCQUF1QjtFQUN2QixpQ0FBaUM7QUFDbkM7QUFDQztFQUNDLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsaUJBQWlCO0FBQ25CO0FBQ0M7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7QUFDakI7QUFDQztFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtBQUNqQjtBQUNDO0VBQ0Msc0JBQXNCO0FBQ3hCO0FBQ0M7RUFDQyxtQkFBbUI7QUFDckI7QUFDQztFQUNDLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLDRDQUE0QztFQUM1QyxZQUFZO0FBQ2Q7QUFDQztFQUNDLFdBQVc7RUFDWCxjQUFjO0VBQ2Qsa0JBQWtCO0FBQ3BCO0FBQ0M7RUFDQywwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWiw2QkFBNkI7RUFDN0Isc0JBQXNCO0VBQ3RCLFdBQVc7QUFDYjtBQUNDO0VBQ0MsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLHlCQUF5QjtFQUN6Qiw2QkFBNkI7RUFDN0IsV0FBVztBQUNiO0FBQ0M7RUFDQyx1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsVUFBVTtFQUNWLGFBQWE7RUFDYixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLDZCQUE2QjtFQUM3QixzQkFBc0I7RUFDdEIsV0FBVztFQUNYLFVBQVU7RUFDVixzQkFBc0I7QUFDeEI7QUFDQztFQUNDLHNCQUFzQjtBQUN4QjtBQUNDO0VBQ0Msc0JBQXNCO0FBQ3hCO0FBQ0M7RUFDQyxvQkFBb0I7QUFDdEI7QUFDQztFQUNDLDJEQUEyRDtFQUMzRCxRQUFRO0FBQ1Y7QUFDQztFQUNDLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLFlBQVk7QUFDZDtBQUNDO0VBQ0MsVUFBVTtFQUNWLFlBQVk7QUFDZDtBQUdDLG9HQUFvRztBQUNwRztFQUNDLFdBQVc7RUFDWCxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixPQUFPO0VBQ1AsV0FBVztFQUNYLGVBQWU7QUFDakI7QUFDQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixRQUFRO0VBQ1IsV0FBVztFQUNYLGVBQWU7QUFDakI7QUFJQTtFQUNFLFlBQVk7RUFDWixlQUFlO0VBQ2YscUJBQXFCOztFQUVyQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixRQUFRO0VBQ1IsZ0JBQWdCO0VBQ2hCLFVBQVU7RUFDVixtREFBbUQ7RUFDbkQsZUFBZTtBQUNqQjtBQUVBO0VBQ0UsOEJBQThCO0FBQ2hDO0FBRUE7RUFDRSxZQUFZO0VBQ1osVUFBVTtFQUNWLDhCQUE4QjtFQUM5QixZQUFZO0FBQ2Q7QUFDQTtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixZQUFZO0VBQ1o7MkJBQ3lCO0VBQ3pCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixRQUFRO0VBQ1IsVUFBVTtFQUNWLGVBQWU7RUFDZixZQUFZO0VBQ1osZUFBZTtFQUNmLDZCQUE2QjtBQUMvQjtBQUVBO0VBQ0UsWUFBWTtBQUNkO0FBSUE7RUFDRSxpQkFBaUI7RUFDakIsY0FBYzs7QUFFaEI7QUFFQTtFQUNFLGNBQWM7RUFDZCxlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYzs7QUFFaEI7QUFFQSw0R0FBNEc7QUFDNUc7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsV0FBVztBQUNiO0FBQ0E7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7QUFDckI7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLDRFQUE0RTtBQUM5RTtBQUNBO0VBQ0UsV0FBVztHQUNWLGNBQWM7SUFDYixnQkFBZ0I7RUFDbEIsbUJBQW1CO0FBQ3JCO0FBQ0EseUdBQXlHO0FBQ3ZHO0lBQ0UsYUFBYTtJQUNiLDZDQUE2QztJQUM3QyxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixtQkFBbUI7RUFDckI7QUFDQTtJQUNFLFNBQVM7RUFDWDtBQUNBO0lBQ0UsbUNBQW1DO0lBQ25DLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsU0FBUztJQUNULGFBQWE7SUFDYixZQUFZO0VBQ2Q7QUFDQTtJQUNFLGVBQWU7RUFDakI7QUFDQTtJQUNFLGVBQWU7QUFDbkIsaUJBQWlCO0FBQ2pCLG1CQUFtQjtFQUNqQjtBQUNBO0lBQ0UsZUFBZTtFQUNqQjtBQUVBO0lBQ0Usa0JBQWtCO0FBQ3RCLGFBQWE7QUFDYixlQUFlO0FBQ2YsaUJBQWlCO0VBQ2Y7QUFJSSwwR0FBMEc7QUFDMUcsMkJBQTJCO0FBQzNCOztJQUVGLDZGQUE2RjtJQUM3RjtNQUNFLCtCQUErQjtJQUNqQztJQUNBO01BQ0UsWUFBWTtNQUNaLGlCQUFpQjtNQUNqQixpQkFBaUI7SUFDbkI7O0NBRUg7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7QUFDakI7QUFDQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtBQUNqQjs7SUFFSTtNQUNFLHlCQUF5QjtNQUN6QixtQkFBbUI7TUFDbkIsWUFBWTtNQUNaLFlBQVk7SUFDZDtJQUNBO01BQ0UsVUFBVTtNQUNWLFlBQVk7SUFDZDtJQUNBO01BQ0UsWUFBWTtNQUNaLFlBQVk7TUFDWixpQkFBaUI7TUFDakIsbUJBQW1CO01BQ25CLGdCQUFnQjtNQUNoQixpQkFBaUI7SUFDbkI7VUFDTSw0R0FBNEc7VUFDNUc7WUFDRSxhQUFhO1lBQ2Isc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixtQkFBbUI7WUFDbkIsV0FBVztVQUNiO1VBQ0E7WUFDRSxjQUFjO1lBQ2QsVUFBVTtVQUNaO0lBQ04sMkZBQTJGO1FBQ3ZGO1VBQ0UsYUFBYTtNQUNqQiw2Q0FBNkM7TUFDN0MsYUFBYTtNQUNiLHNCQUFzQjtNQUN0Qiw2QkFBNkI7TUFDN0IsbUJBQW1CO1FBQ2pCO1FBQ0E7VUFDRSxpQkFBaUI7TUFDckIsYUFBYTtNQUNiLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsd0JBQXdCO01BQ3hCLGtCQUFrQjtRQUNoQjs7O0lBR0o7QUFFQSxnQ0FBZ0M7QUFDaEM7OztJQUdBLDZGQUE2RjtJQUM3RjtNQUNFLCtCQUErQjtJQUNqQztJQUNBO01BQ0UsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixlQUFlO0lBQ2pCO0lBQ0E7TUFDRSxlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLGVBQWU7SUFDakI7SUFDQTtNQUNFLHlCQUF5QjtNQUN6QixtQkFBbUI7TUFDbkIsWUFBWTtNQUNaLFlBQVk7SUFDZDtJQUNBO01BQ0UsVUFBVTtNQUNWLFlBQVk7SUFDZDtJQUNBO01BQ0UsWUFBWTtNQUNaLFlBQVk7TUFDWixpQkFBaUI7TUFDakIsbUJBQW1CO01BQ25CLGdCQUFnQjtNQUNoQixpQkFBaUI7SUFDbkI7O1VBRU0sNEdBQTRHO1VBQzVHO1lBQ0UsYUFBYTtZQUNiLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsbUJBQW1CO1lBQ25CLFdBQVc7VUFDYjtVQUNBO1lBQ0UsY0FBYztZQUNkLFVBQVU7VUFDWjs7SUFFTiwyRkFBMkY7UUFDdkY7VUFDRSxhQUFhO01BQ2pCLDZDQUE2QztNQUM3QyxhQUFhO01BQ2Isc0JBQXNCO01BQ3RCLDZCQUE2QjtNQUM3QixtQkFBbUI7UUFDakI7UUFDQTtVQUNFLGlCQUFpQjtNQUNyQixhQUFhO01BQ2IsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQix3QkFBd0I7TUFDeEIsa0JBQWtCO1FBQ2hCOzs7SUFHSjtBQUlDLDBCQUEwQjtBQUMxQjs7O0lBR0QsNkZBQTZGO0lBQzdGO01BQ0UsK0JBQStCO0lBQ2pDO0lBQ0E7TUFDRSxlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLGVBQWU7SUFDakI7SUFDQTtNQUNFLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsZUFBZTtJQUNqQjs7SUFFQTtNQUNFLHlCQUF5QjtNQUN6QixtQkFBbUI7TUFDbkIsWUFBWTtNQUNaLFlBQVk7SUFDZDtJQUNBO01BQ0UsVUFBVTtNQUNWLFlBQVk7SUFDZDs7SUFFQSwyRkFBMkY7UUFDdkY7VUFDRSxhQUFhO01BQ2pCLDZDQUE2QztNQUM3QyxhQUFhO01BQ2Isc0JBQXNCO01BQ3RCLDZCQUE2QjtNQUM3QixtQkFBbUI7UUFDakI7UUFDQTtVQUNFLGlCQUFpQjtNQUNyQixhQUFhO01BQ2IsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQix3QkFBd0I7TUFDeEIsa0JBQWtCO1FBQ2hCO01BQ0YsNEdBQTRHO1FBQzFHO1VBQ0UsYUFBYTtVQUNiLHNCQUFzQjtVQUN0Qix1QkFBdUI7VUFDdkIsbUJBQW1CO1VBQ25CLFdBQVc7UUFDYjtRQUNBO1VBQ0UsY0FBYztVQUNkLFVBQVU7UUFDWjtLQUNIO0FBSUEsNEJBQTRCO0FBQzVCOztVQUVLLDJGQUEyRjs7VUFFM0Y7WUFDRSxtQ0FBbUM7WUFDbkMsbUJBQW1CO1lBQ25CLGFBQWE7WUFDYixTQUFTO1lBQ1QsYUFBYTtZQUNiLFlBQVk7VUFDZDtVQUNBO1lBQ0Usa0JBQWtCO1FBQ3RCLGFBQWE7UUFDYixlQUFlO1FBQ2YsaUJBQWlCO1VBQ2Y7O1FBRUYsNEdBQTRHO1VBQzFHO1lBQ0UsYUFBYTtZQUNiLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsbUJBQW1CO1lBQ25CLFdBQVc7VUFDYjtVQUNBO1lBQ0UsY0FBYztZQUNkLFVBQVU7VUFDWjtRQUNGIiwiZmlsZSI6ImFwcC9jcG4vYWdlbmNlL2FnZW5jZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmNhcnJlIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBoZWlnaHQ6IDkwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAxOHB4O1xuICBtYXJnaW4tbGVmdDogLTI1MHB4O1xuICBtYXJnaW4tdG9wOiAxMjVweDtcbn1cblxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XG5cdCBiYWNrZ3JvdW5kOiNFQkVDRjA7XG5cdCBtaW4taGVpZ2h0OiBmaXQtY29udGVudDtcblx0IGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMDBweDtcbn1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XG5cdCBoZWlnaHQ6IDEwMCU7XG5cdCBtYXgtaGVpZ2h0OiA1ODVweDtcblx0IG1hcmdpbi10b3A6IC0zNXB4O1xufVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XG5cdCBmb250LXNpemU6IDYwcHg7XG5cdCBmb250LXdlaWdodDogODAwO1xuXHQgY29sb3I6ICAjMTExZDVlO1xufVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuZGVzY19oZWFkaW5nIHtcblx0IGZvbnQtc2l6ZTogNDBweDtcblx0IGZvbnQtd2VpZ2h0OiA3MDA7XG5cdCBjb2xvcjogICMxMTFkNWU7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgKiwgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgKjpiZWZvcmUsIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICo6YWZ0ZXIge1xuXHQgYm94LXNpemluZzogYm9yZGVyLWJveDtcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBib2R5IHtcblx0IGJhY2tncm91bmQ6ICNmNWY1ZjU7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgbWFpbiB7XG5cdCBsZWZ0OiA1MCU7XG5cdCBwb3NpdGlvbjogYWJzb2x1dGU7XG5cdCB0b3A6IDUwJTtcblx0IHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSB0cmFuc2xhdGVZKC01MCUpO1xuXHQgd2lkdGg6IDMwMHB4O1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YmVmb3JlLCAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmFmdGVyIHtcblx0IGNvbnRlbnQ6IFwiXCI7XG5cdCBkaXNwbGF5OiBibG9jaztcblx0IHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmJlZm9yZSB7XG5cdCBib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlIDtcblx0IGJvcmRlci1yYWRpdXM6IDIwcHg7XG5cdCBoZWlnaHQ6IDQwcHg7XG5cdCB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcblx0IHRyYW5zaXRpb24tZGVsYXk6IDAuM3M7XG5cdCB3aWR0aDogNDBweDtcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmFmdGVyIHtcblx0IGJhY2tncm91bmQ6ICMxMTFkNWU7XG5cdCBib3JkZXItcmFkaXVzOiAzcHg7XG5cdCBoZWlnaHQ6IDVweDtcblx0IHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG5cdCB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAxMDAlO1xuXHQgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1vdXQ7XG5cdCB3aWR0aDogMTVweDtcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoX19pbnB1dCB7XG5cdCBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcblx0IGJvcmRlcjogbm9uZTtcblx0IGJvcmRlci1yYWRpdXM6IDIwcHg7XG5cdCBkaXNwbGF5OiBibG9jaztcblx0IGZvbnQtc2l6ZTogMjBweDtcblx0IGhlaWdodDogNDBweDtcblx0IGxpbmUtaGVpZ2h0OiA0MHB4O1xuXHQgb3BhY2l0eTogMDtcblx0IG91dGxpbmU6IG5vbmU7XG5cdCBwYWRkaW5nOiAwIDE1cHg7XG5cdCBwb3NpdGlvbjogcmVsYXRpdmU7XG5cdCB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcblx0IHRyYW5zaXRpb24tZGVsYXk6IDAuNnM7XG5cdCB3aWR0aDogNDBweDtcblx0IHotaW5kZXg6IDE7XG5cdCBjb2xvcjogcmdiKDg1LCA4NSwgODUpO1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLWhpZGU6YmVmb3JlIHtcblx0IHRyYW5zaXRpb24tZGVsYXk6IDAuM3M7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZTphZnRlciB7XG5cdCB0cmFuc2l0aW9uLWRlbGF5OiAwLjZzO1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLWhpZGUgLnNlYXJjaF9faW5wdXQge1xuXHQgdHJhbnNpdGlvbi1kZWxheTogMHM7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzphZnRlciB7XG5cdCB0cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpIHRyYW5zbGF0ZVgoMTVweCkgdHJhbnNsYXRlWSgtMnB4KTtcblx0IHdpZHRoOiAwO1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcblx0IGJvcmRlcjogNXB4IHNvbGlkICMxMTFkNWU7XG5cdCBib3JkZXItcmFkaXVzOiAyMHB4O1xuXHQgaGVpZ2h0OiA0MHB4O1xuXHQgd2lkdGg6IDUwMHB4O1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xuXHQgb3BhY2l0eTogMTtcblx0IHdpZHRoOiA1MDBweDtcbn1cbiAgXG5cbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipkaXZpZGVyKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuIC5wcmltYXJ5X2JvZHkgLmRpdmlkZXIgLmRpdmlkZXJfbGlnbmUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQ6ICMxMTFkNWU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucHJpbWFyeV9ib2R5IC5kaXZpZGVyIC5kaXZpZGVyX2xpZ25lOjpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAzMCU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQ6IHJlZDtcbn1cbi5wcmltYXJ5X2JvZHkgLmRpdmlkZXIgLmRpdmlkZXJfbGlnbmU6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMzAlO1xuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuICBoZWlnaHQ6IDVweDtcbiAgYmFja2dyb3VuZDogcmVkO1xufVxuXG5cblxuaW5wdXRbdHlwZT1cInRleHRcIl0ge1xuICBoZWlnaHQ6IDUwcHg7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuXG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIGJvcmRlcjogbm9uZTtcbiAgb3V0bGluZTogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAzcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDYwcHg7XG4gIHdpZHRoOiAwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgei1pbmRleDogMztcbiAgdHJhbnNpdGlvbjogd2lkdGggMC40cyBjdWJpYy1iZXppZXIoMCwgMC43OTUsIDAsIDEpO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmlucHV0W3R5cGU9XCJ0ZXh0XCJdOmZvY3VzOmhvdmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xufVxuXG5pbnB1dFt0eXBlPVwidGV4dFwiXTpmb2N1cyB7XG4gIHdpZHRoOiA3MDBweDtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICBjdXJzb3I6IHRleHQ7XG59XG5pbnB1dFt0eXBlPVwic3VibWl0XCJdIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogd2hpdGU7XG4gIGZsb2F0OiByaWdodDtcbiAgYmFja2dyb3VuZDogdXJsKGRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBREFBQUFBd0NBTUFBQUJnM0FtMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBRE5RVEZSRlUxTlQ5ZlgxbEpTVVhsNWUxZFhWZm41K2MzTno2dXJxdjcrL3RMUzBpWW1KcWFtcG41K2Z5c3JLMzkvZmFXbHAvLy8vVmk0Wnl3QUFBQkYwVWs1VC8vLy8vLy8vLy8vLy8vLy8vLy8vL3dBbHJabGlBQUFCTGtsRVFWUjQyclNXV1JiRElBaEZIZU9VdE4zL2FnczF6YUE0Y0hyS1o4SkZSSHdvWGt3VHZ3R1AxUW8wYllPYkFQd2lMbWJOQUhCV0ZCWmxEOWowSnhmbERWaUlPYk5IRy9EbzhQUkhUSmswVGV6QWh2N3Fsb0swSkpFQmgrRjgrVS9ob3BJRUxPV2ZpWlVDRE9aRDFSQURPUUtBNzVvcTRjdlZrY1QrT2RIbnFxcFFDSVRXQWpuV1ZnR1FVV3oxMmxKdUd3R29hV2dCS3pSVkJjQ3lwZ1VrT0FvV2dCWC9MMENteE40MHU2eHdjSUoxY096V1lEZmZwM2F4c1FPeXZka1hpSDlGS1JGd1BSSFlaVWFYTWdQTGVpVzdRaGJEUmNpeUxYSmFLaGVDdUxiaVZvcXgxRFZSeUgyNnliMGhzdW9PRkVQc296K0JWRTBNUmxaTmpHWmNSUXlIWWttTXAyaEJUSXpka3pDVGMvcExxT25Ccms3L3laZEFPcS9xNU5QQkgxZjd4N2ZHUDRDM0FBTUFRcmh6WDl6aGNHc0FBQUFBU1VWT1JLNUNZSUk9KVxuICAgIGNlbnRlciBjZW50ZXIgbm8tcmVwZWF0O1xuICB0ZXh0LWluZGVudDogLTEwMDAwcHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuICB6LWluZGV4OiAyO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIG9wYWNpdHk6IDAuNDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuNHMgZWFzZTtcbn1cblxuaW5wdXRbdHlwZT1cInN1Ym1pdFwiXTpob3ZlciB7XG4gIG9wYWNpdHk6IDAuODtcbn1cblxuXG5cbmgxe1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMxMTFkNWU7XG5cbn1cblxuaDV7XG4gIGNvbG9yOiAjMTExZDVlO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5we1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjMTExZDVlO1xuXG59XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiphY3R1YWxpdGUqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5hY3R1e1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG59XG4uYWN0dSAucm93e1xuICB3aWR0aDogNzAlO1xufVxuLmZldWlse1xuICBmbGV4OiAwIDAgYXV0bztcbiAgd2lkdGg6IDMzLjMzMzMzMzMzJTtcbn1cbi5hY3R1IC5hY3Qtd3JhcHBlcntcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0VCRUNGMCA7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNikgMHB4IDNweCA2cHgsIHJnYmEoMCwgMCwgMCwgMC4yMykgMHB4IDNweCA2cHg7XG59XG4uYWN0dSAuYWN0LXdyYXBwZXIgaW1ne1xuICB3aWR0aDogMTAwJTtcbiAgIG1heC1oZWlnaHQ6NzAlO1xuICAgIG9iamVjdC1maXQ6Y292ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG59XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcHJvcG9zb24qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gIC5wcm9wb3NvbnN7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoI2ZmZmZmZiwgI2U1ZTZlNyk7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgLnByb3Bvc29ucyBoMXtcbiAgICBtYXJnaW46IDA7XG4gIH1cbiAgLlNvdXNCe1xuICAgIGJvcmRlcjogM3B4IHNvbGlkIHJnYig0MSwgMTkxLCAyMTEpO1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgaGVpZ2h0OiAxNjBweDtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICB3aWR0aDogMzAwcHg7XG4gIH1cbiAgLlNvdXNCIGgxe1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgfVxuICAuU291c0IgIGgze1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbmZvbnQtd2VpZ2h0OiBib2xkO1xubGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgfVxuICAuU291c0IgICBwe1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgfVxuXG4gIC5Tb3VzQiBpe1xuICAgIG1hcmdpbi1sZWZ0OiAyNzdweDtcbnotaW5kZXg6IGF1dG87XG5mb250LXNpemU6IDI2cHg7XG5tYXJnaW4tdG9wOiAtNTBweDtcbiAgfVxuICBcblxuXG4gICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipyZXNwb25zaXZlIGNzcyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgLyogQ3VzdG9tLCBpUGhvbmUgUmV0aW5hICAqL1xuICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMzIwcHgpIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpICB7XG4gICAgICAgIFxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxuICAgIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcge1xuICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDBweDtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciAuaGVhZGluZ19pbWcge1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgbWF4LWhlaWdodDogNDAwcHg7XG4gICAgICBtYXJnaW4tdG9wOiAtMzVweDtcbiAgICB9XG4gICAgXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC50aXRsZV9oZWFkaW5nIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBjb2xvcjogICMxMTFkNWU7XG59XG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgY29sb3I6ICAjMTExZDVlO1xufVxuICBcbiAgICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XG4gICAgICBib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlO1xuICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICAgIGhlaWdodDogNDBweDtcbiAgICAgIHdpZHRoOiAzMDBweDtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XG4gICAgICBvcGFjaXR5OiAxO1xuICAgICAgd2lkdGg6IDMwMHB4O1xuICAgIH1cbiAgICAuY2FycmUge1xuICAgICAgd2lkdGg6IDIwMHB4O1xuICAgICAgaGVpZ2h0OiA5MHB4O1xuICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICBib3JkZXItcmFkaXVzOiAxOHB4O1xuICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgIG1hcmdpbi10b3A6IDEyNXB4O1xuICAgIH1cbiAgICAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYWN0dWFsaXRlKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAgICAgLmFjdHUgLnJvd3tcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5mZXVpbHtcbiAgICAgICAgICAgIGZsZXg6IDAgMCBhdXRvO1xuICAgICAgICAgICAgd2lkdGg6IDcwJTtcbiAgICAgICAgICB9XG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcHJvcG9zKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAgLnByb3Bvc29uc3tcbiAgICAgICAgICBoZWlnaHQ6IDgwMHB4O1xuICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCNmZmZmZmYsICNlNWU2ZTcpO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgICAgLlNvdXNCIGl7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDc3cHg7XG4gICAgICB6LWluZGV4OiBhdXRvO1xuICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgICAgbWFyZ2luLXRvcDogNzBweDtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgfVxuICAgICAgXG4gICAgIFxuICAgIH1cbiAgICBcbiAgICAvKiBFeHRyYSBTbWFsbCBEZXZpY2VzLCBQaG9uZXMgKi9cbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA0ODBweCkgYW5kIChtYXgtd2lkdGggOiA3NjhweCkgIHtcbiAgXG4gICAgICAgICAgICAgXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4O1xuICAgIH1cbiAgICAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xuICAgICAgZm9udC1zaXplOiA0MHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICAgIGNvbG9yOiAgIzExMWQ1ZTtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5kZXNjX2hlYWRpbmcge1xuICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICAgIGNvbG9yOiAgIzExMWQ1ZTtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xuICAgICAgYm9yZGVyOiA1cHggc29saWQgIzExMWQ1ZTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICB3aWR0aDogMzAwcHg7XG4gICAgfVxuICAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xuICAgICAgb3BhY2l0eTogMTtcbiAgICAgIHdpZHRoOiAzMDBweDtcbiAgICB9XG4gICAgLmNhcnJlIHtcbiAgICAgIHdpZHRoOiAyMDBweDtcbiAgICAgIGhlaWdodDogOTBweDtcbiAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgICBtYXJnaW4tdG9wOiAxMjVweDtcbiAgICB9XG5cbiAgICAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYWN0dWFsaXRlKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAgICAgLmFjdHUgLnJvd3tcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5mZXVpbHtcbiAgICAgICAgICAgIGZsZXg6IDAgMCBhdXRvO1xuICAgICAgICAgICAgd2lkdGg6IDcwJTtcbiAgICAgICAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipwcm9wb3MqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgICAucHJvcG9zb25ze1xuICAgICAgICAgIGhlaWdodDogODAwcHg7XG4gICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoI2ZmZmZmZiwgI2U1ZTZlNyk7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgfVxuICAgICAgICAuU291c0IgaXtcbiAgICAgICAgICBtYXJnaW4tbGVmdDogNzdweDtcbiAgICAgIHotaW5kZXg6IGF1dG87XG4gICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICBtYXJnaW4tdG9wOiA3MHB4O1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB9XG4gICAgICBcbiAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgICAvKiBTbWFsbCBEZXZpY2VzLCBUYWJsZXRzKi9cbiAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogNzY4cHgpIGFuZCAobWF4LXdpZHRoIDogOTkycHgpICB7XG4gICAgXG4gICAgICAgICAgICAgXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4O1xuICAgIH1cbiAgICAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xuICAgICAgZm9udC1zaXplOiA0MHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICAgIGNvbG9yOiAgIzExMWQ1ZTtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5kZXNjX2hlYWRpbmcge1xuICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICAgIGNvbG9yOiAgIzExMWQ1ZTtcbiAgICB9XG4gIFxuICAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcbiAgICAgIGJvcmRlcjogNXB4IHNvbGlkICMxMTFkNWU7XG4gICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgd2lkdGg6IDMwMHB4O1xuICAgIH1cbiAgICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93IC5zZWFyY2hfX2lucHV0IHtcbiAgICAgIG9wYWNpdHk6IDE7XG4gICAgICB3aWR0aDogMzAwcHg7XG4gICAgfVxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcHJvcG9zKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAgLnByb3Bvc29uc3tcbiAgICAgICAgICBoZWlnaHQ6IDgwMHB4O1xuICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCNmZmZmZmYsICNlNWU2ZTcpO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgICAgLlNvdXNCIGl7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDc3cHg7XG4gICAgICB6LWluZGV4OiBhdXRvO1xuICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgICAgbWFyZ2luLXRvcDogNzBweDtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgfVxuICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmFjdHVhbGl0ZSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgICAuYWN0dSAucm93e1xuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICAgIC5mZXVpbHtcbiAgICAgICAgICBmbGV4OiAwIDAgYXV0bztcbiAgICAgICAgICB3aWR0aDogNzAlO1xuICAgICAgICB9XG4gICAgIH1cbiAgICBcbiAgICBcbiAgICAgIFxuICAgICAvKiBNZWRpdW0gRGV2aWNlcywgRGVza3RvcHMqL1xuICAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA5OTJweCkgYW5kIChtYXgtd2lkdGggOiAxMjAwcHgpICB7XG4gICAgICAgIFxuICAgICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnByb3BvcyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gIFxuICAgICAgICAgIC5Tb3VzQntcbiAgICAgICAgICAgIGJvcmRlcjogM3B4IHNvbGlkIHJnYig0MSwgMTkxLCAyMTEpO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgICAgICAgIGhlaWdodDogMTYwcHg7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDI1MHB4O1xuICAgICAgICAgIH1cbiAgICAgICAgICAuU291c0IgaXtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAyMjVweDtcbiAgICAgICAgei1pbmRleDogYXV0bztcbiAgICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAtNTBweDtcbiAgICAgICAgICB9XG4gICAgICAgICAgXG4gICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiphY3R1YWxpdGUqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAgICAuYWN0dSAucm93e1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLmZldWlse1xuICAgICAgICAgICAgZmxleDogMCAwIGF1dG87XG4gICAgICAgICAgICB3aWR0aDogNzAlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AgenceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-agence',
                templateUrl: './agence.component.html',
                styleUrls: ['./agence.component.css']
            }]
    }], function () { return [{ type: src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/agenda/agenda.component.ts":
/*!************************************************!*\
  !*** ./src/app/cpn/agenda/agenda.component.ts ***!
  \************************************************/
/*! exports provided: AgendaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendaComponent", function() { return AgendaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../side-bar/side-bar.component */ "./src/app/cpn/side-bar/side-bar.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





class AgendaComponent {
    constructor(tokenStorage) {
        this.tokenStorage = tokenStorage;
    }
    ngOnInit() {
    }
}
AgendaComponent.ɵfac = function AgendaComponent_Factory(t) { return new (t || AgendaComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"])); };
AgendaComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AgendaComponent, selectors: [["app-agenda"]], decls: 15, vars: 1, consts: [[1, "agenda_container"], [1, "agenda_wrapper"], [1, "container", "pt-4", "px-4", "g-0"], [1, "row", "g-0", "align-items-center", "justify-content-center"], [1, "col-md-6"], [1, "row", "g-0", "justify-content-center", "pb-5"], [1, "col-md-8", "d-flex", "flex-column", "justify-content-center", "align-items-center"], [2, "font-weight", "700", "text-align", "center", "color", "#111D5E"], [1, "btn", "btn-danger", 2, "border", "none", "background", "red", "border-radius", "25px", "color", "white", "padding", "5px 15px", 3, "routerLink"], [1, "row", "justify-content-center", "align-items-center"], ["src", "assets/cpnimages/home/calendar.png", "alt", "calendar", 2, "width", "100%"]], template: function AgendaComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-side-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h1", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Choisissez votre rendez-vous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "rendez-vous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Connexion");
    } }, directives: [_side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_2__["SideBarComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"]], styles: [".agenda_container[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n    height: 489px;\n}\n\n.block[_ngcontent-%COMP%]{\n    display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n  height: 100%;\n}\n\n.block-left[_ngcontent-%COMP%]{\norder: 1;\nbox-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;\nheight: 600px;\n}\n\n.block-right[_ngcontent-%COMP%]{\norder: 2;\ndisplay: flex;\n  flex-direction: column;\n  padding-top:20px ;\n  justify-content: space-around;\n  align-items: center;\nbackground-color: red;\nwidth: 30%;\ncolor: white;\nheight: 600px;\n}\n\n.block-right[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    color: white;\n}\n\np[_ngcontent-%COMP%]{\n    color: white;\n\n}\n\n.time[_ngcontent-%COMP%]{\n    width: 30%;\n}\n\nmat-divider[_ngcontent-%COMP%]{\n    color: white;\n    background-color: white;\n}\n\nmat-list[_ngcontent-%COMP%]{\n    width: 90%;\n}\n\nmat-list-item[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    align-items: flex-start;\n    width: 100%;\n}\n\n.block-left[_ngcontent-%COMP%]   mat-calendar[_ngcontent-%COMP%]{\n    width: 500px;\n    height: 500px;\n    margin: 20px;\n}\n\n.agenda_wrapper[_ngcontent-%COMP%]{\n    width: 100%;\n    height: 100%;\n}\n\n@media screen and (max-width: 768px) {\n\n    .block[_ngcontent-%COMP%]{\n        display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      width: 100%;\n      height: 100%;\n    }\n\n    .block-left[_ngcontent-%COMP%]   mat-calendar[_ngcontent-%COMP%]{\n        width: 300px;\n        height: 300px;\n        margin: 20px;\n    }\n    .block-left[_ngcontent-%COMP%]{\n        order: 1;\n        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;\n        height: 400px;\n        }\n\n    .block-right[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n            flex-direction: column;\n            padding-top:20px ;\n            justify-content: space-around;\n            align-items: center;\n        background-color: red;\n        width: 342px;\n        color: white;\n        height: 600px;\n        }\n        \n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vYWdlbmRhL2FnZW5kYS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kseUJBQXlCO0lBQ3pCLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxhQUFhO0VBQ2YsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7QUFDZDs7QUFDQTtBQUNBLFFBQVE7QUFDUiw0RUFBNEU7QUFDNUUsYUFBYTtBQUNiOztBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7RUFDWCxzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLDZCQUE2QjtFQUM3QixtQkFBbUI7QUFDckIscUJBQXFCO0FBQ3JCLFVBQVU7QUFDVixZQUFZO0FBQ1osYUFBYTtBQUNiOztBQUNBO0lBQ0ksWUFBWTtBQUNoQjs7QUFDQTtJQUNJLFlBQVk7O0FBRWhCOztBQUNBO0lBQ0ksVUFBVTtBQUNkOztBQUNBO0lBQ0ksWUFBWTtJQUNaLHVCQUF1QjtBQUMzQjs7QUFDQTtJQUNJLFVBQVU7QUFDZDs7QUFDQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLHVCQUF1QjtJQUN2QixXQUFXO0FBQ2Y7O0FBQ0E7SUFDSSxZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBR0E7SUFDSSxXQUFXO0lBQ1gsWUFBWTtBQUNoQjs7QUFHQTs7SUFFSTtRQUNJLGFBQWE7TUFDZixzQkFBc0I7TUFDdEIsdUJBQXVCO01BQ3ZCLG1CQUFtQjtNQUNuQixXQUFXO01BQ1gsWUFBWTtJQUNkOztJQUVBO1FBQ0ksWUFBWTtRQUNaLGFBQWE7UUFDYixZQUFZO0lBQ2hCO0lBQ0E7UUFDSSxRQUFRO1FBQ1IsNEVBQTRFO1FBQzVFLGFBQWE7UUFDYjs7SUFFSjtRQUNJLFFBQVE7UUFDUixhQUFhO1lBQ1Qsc0JBQXNCO1lBQ3RCLGlCQUFpQjtZQUNqQiw2QkFBNkI7WUFDN0IsbUJBQW1CO1FBQ3ZCLHFCQUFxQjtRQUNyQixZQUFZO1FBQ1osWUFBWTtRQUNaLGFBQWE7UUFDYjs7QUFFUiIsImZpbGUiOiJhcHAvY3BuL2FnZW5kYS9hZ2VuZGEuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hZ2VuZGFfY29udGFpbmVye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNFQkVDRjA7XG4gICAgaGVpZ2h0OiA0ODlweDtcbn1cblxuLmJsb2Nre1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmJsb2NrLWxlZnR7XG5vcmRlcjogMTtcbmJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNikgMHB4IDNweCA2cHgsIHJnYmEoMCwgMCwgMCwgMC4yMykgMHB4IDNweCA2cHg7XG5oZWlnaHQ6IDYwMHB4O1xufVxuLmJsb2NrLXJpZ2h0e1xub3JkZXI6IDI7XG5kaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBwYWRkaW5nLXRvcDoyMHB4IDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG53aWR0aDogMzAlO1xuY29sb3I6IHdoaXRlO1xuaGVpZ2h0OiA2MDBweDtcbn1cbi5ibG9jay1yaWdodCBoMXtcbiAgICBjb2xvcjogd2hpdGU7XG59XG5we1xuICAgIGNvbG9yOiB3aGl0ZTtcblxufVxuLnRpbWV7XG4gICAgd2lkdGg6IDMwJTtcbn1cbm1hdC1kaXZpZGVye1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbm1hdC1saXN0e1xuICAgIHdpZHRoOiA5MCU7XG59XG5tYXQtbGlzdC1pdGVte1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4uYmxvY2stbGVmdCBtYXQtY2FsZW5kYXJ7XG4gICAgd2lkdGg6IDUwMHB4O1xuICAgIGhlaWdodDogNTAwcHg7XG4gICAgbWFyZ2luOiAyMHB4O1xufVxuXG5cbi5hZ2VuZGFfd3JhcHBlcntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcblxuICAgIC5ibG9ja3tcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG5cbiAgICAuYmxvY2stbGVmdCBtYXQtY2FsZW5kYXJ7XG4gICAgICAgIHdpZHRoOiAzMDBweDtcbiAgICAgICAgaGVpZ2h0OiAzMDBweDtcbiAgICAgICAgbWFyZ2luOiAyMHB4O1xuICAgIH1cbiAgICAuYmxvY2stbGVmdHtcbiAgICAgICAgb3JkZXI6IDE7XG4gICAgICAgIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNikgMHB4IDNweCA2cHgsIHJnYmEoMCwgMCwgMCwgMC4yMykgMHB4IDNweCA2cHg7XG4gICAgICAgIGhlaWdodDogNDAwcHg7XG4gICAgICAgIH1cblxuICAgIC5ibG9jay1yaWdodHtcbiAgICAgICAgb3JkZXI6IDI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgcGFkZGluZy10b3A6MjBweCA7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgICAgICAgd2lkdGg6IDM0MnB4O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGhlaWdodDogNjAwcHg7XG4gICAgICAgIH1cbiAgICAgICAgXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AgendaComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-agenda',
                templateUrl: './agenda.component.html',
                styleUrls: ['./agenda.component.css']
            }]
    }], function () { return [{ type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/calandar/calandar.component.ts":
/*!****************************************************!*\
  !*** ./src/app/cpn/calandar/calandar.component.ts ***!
  \****************************************************/
/*! exports provided: CalandarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalandarComponent", function() { return CalandarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/datepicker.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/list.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/divider.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");







class CalandarComponent {
    constructor(tokenStorage) {
        this.tokenStorage = tokenStorage;
    }
    ngOnInit() {
        this.currentDate = new Date();
    }
}
CalandarComponent.ɵfac = function CalandarComponent_Factory(t) { return new (t || CalandarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"])); };
CalandarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CalandarComponent, selectors: [["app-calandar"]], decls: 29, vars: 6, consts: [[1, "agenda_container"], [1, "agenda_wrapper"], [1, "block"], [1, "block-left"], ["calendar", ""], [1, "block-right"], [1, "time"]], template: function CalandarComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "mat-calendar", null, 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](9, "uppercase");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](10, "date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-list");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-list-item");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "09.00");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Business meeting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-list-item");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "14.30");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Shopping with Ann meeting");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "mat-list-item");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "19.00");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Appoinment with \u00A8Jack");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](9, 1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](10, 3, ctx.currentDate, "EEEE d")), " ");
    } }, directives: [_angular_material_datepicker__WEBPACK_IMPORTED_MODULE_2__["MatCalendar"], _angular_material_list__WEBPACK_IMPORTED_MODULE_3__["MatList"], _angular_material_list__WEBPACK_IMPORTED_MODULE_3__["MatListItem"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_4__["MatDivider"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["UpperCasePipe"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]], styles: [".agenda_container[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n   padding: 50px;\n}\n\n.block[_ngcontent-%COMP%]{\n    display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n  height: 100%;\n}\n\n.block-left[_ngcontent-%COMP%]{\n    background-color: white;\norder: 1;\nbox-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;\nheight: 600px;\n}\n\n.block-right[_ngcontent-%COMP%]{\norder: 2;\ndisplay: flex;\n  flex-direction: column;\n  padding-top:20px ;\n  justify-content: space-around;\n  align-items: center;\nbackground-color: red;\nwidth: 30%;\ncolor: white;\nheight: 600px;\n}\n\n.block-right[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    color: white;\n}\n\np[_ngcontent-%COMP%]{\n    color: white;\n\n}\n\n.time[_ngcontent-%COMP%]{\n    width: 30%;\n}\n\nmat-divider[_ngcontent-%COMP%]{\n    color: white;\n    background-color: white;\n}\n\nmat-list[_ngcontent-%COMP%]{\n    width: 90%;\n}\n\nmat-list-item[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    align-items: flex-start;\n    width: 100%;\n}\n\n.block-left[_ngcontent-%COMP%]   mat-calendar[_ngcontent-%COMP%]{\n    width: 500px;\n    height: 500px;\n    margin: 20px;\n}\n\n.agenda_wrapper[_ngcontent-%COMP%]{\n    width: 100%;\n    height: 100%;\n}\n\n@media screen and (max-width: 768px) {\n\n    .block[_ngcontent-%COMP%]{\n        display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      width: 100%;\n      height: 100%;\n    }\n\n    .block-left[_ngcontent-%COMP%]   mat-calendar[_ngcontent-%COMP%]{\n        width: 300px;\n        height: 300px;\n        margin: 20px;\n    }\n    .block-left[_ngcontent-%COMP%]{\n        order: 1;\n        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;\n        height: 400px;\n        }\n\n    .block-right[_ngcontent-%COMP%]{\n        order: 2;\n        display: flex;\n            flex-direction: column;\n            padding-top:20px ;\n            justify-content: space-around;\n            align-items: center;\n        background-color: red;\n        width: 342px;\n        color: white;\n        height: 600px;\n        }\n        \n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vY2FsYW5kYXIvY2FsYW5kYXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHlCQUF5QjtHQUMxQixhQUFhO0FBQ2hCOztBQUVBO0lBQ0ksYUFBYTtFQUNmLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7O0FBQ0E7SUFDSSx1QkFBdUI7QUFDM0IsUUFBUTtBQUNSLDRFQUE0RTtBQUM1RSxhQUFhO0FBQ2I7O0FBQ0E7QUFDQSxRQUFRO0FBQ1IsYUFBYTtFQUNYLHNCQUFzQjtFQUN0QixpQkFBaUI7RUFDakIsNkJBQTZCO0VBQzdCLG1CQUFtQjtBQUNyQixxQkFBcUI7QUFDckIsVUFBVTtBQUNWLFlBQVk7QUFDWixhQUFhO0FBQ2I7O0FBQ0E7SUFDSSxZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksWUFBWTs7QUFFaEI7O0FBQ0E7SUFDSSxVQUFVO0FBQ2Q7O0FBQ0E7SUFDSSxZQUFZO0lBQ1osdUJBQXVCO0FBQzNCOztBQUNBO0lBQ0ksVUFBVTtBQUNkOztBQUNBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0IsdUJBQXVCO0lBQ3ZCLFdBQVc7QUFDZjs7QUFDQTtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFHQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0FBQ2hCOztBQUdBOztJQUVJO1FBQ0ksYUFBYTtNQUNmLHNCQUFzQjtNQUN0Qix1QkFBdUI7TUFDdkIsbUJBQW1CO01BQ25CLFdBQVc7TUFDWCxZQUFZO0lBQ2Q7O0lBRUE7UUFDSSxZQUFZO1FBQ1osYUFBYTtRQUNiLFlBQVk7SUFDaEI7SUFDQTtRQUNJLFFBQVE7UUFDUiw0RUFBNEU7UUFDNUUsYUFBYTtRQUNiOztJQUVKO1FBQ0ksUUFBUTtRQUNSLGFBQWE7WUFDVCxzQkFBc0I7WUFDdEIsaUJBQWlCO1lBQ2pCLDZCQUE2QjtZQUM3QixtQkFBbUI7UUFDdkIscUJBQXFCO1FBQ3JCLFlBQVk7UUFDWixZQUFZO1FBQ1osYUFBYTtRQUNiOztBQUVSIiwiZmlsZSI6ImFwcC9jcG4vY2FsYW5kYXIvY2FsYW5kYXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hZ2VuZGFfY29udGFpbmVye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNFQkVDRjA7XG4gICBwYWRkaW5nOiA1MHB4O1xufVxuXG4uYmxvY2t7XG4gICAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uYmxvY2stbGVmdHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbm9yZGVyOiAxO1xuYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE2KSAwcHggM3B4IDZweCwgcmdiYSgwLCAwLCAwLCAwLjIzKSAwcHggM3B4IDZweDtcbmhlaWdodDogNjAwcHg7XG59XG4uYmxvY2stcmlnaHR7XG5vcmRlcjogMjtcbmRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHBhZGRpbmctdG9wOjIwcHggO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbmJhY2tncm91bmQtY29sb3I6IHJlZDtcbndpZHRoOiAzMCU7XG5jb2xvcjogd2hpdGU7XG5oZWlnaHQ6IDYwMHB4O1xufVxuLmJsb2NrLXJpZ2h0IGgxe1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cbnB7XG4gICAgY29sb3I6IHdoaXRlO1xuXG59XG4udGltZXtcbiAgICB3aWR0aDogMzAlO1xufVxubWF0LWRpdmlkZXJ7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxubWF0LWxpc3R7XG4gICAgd2lkdGg6IDkwJTtcbn1cbm1hdC1saXN0LWl0ZW17XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5ibG9jay1sZWZ0IG1hdC1jYWxlbmRhcntcbiAgICB3aWR0aDogNTAwcHg7XG4gICAgaGVpZ2h0OiA1MDBweDtcbiAgICBtYXJnaW46IDIwcHg7XG59XG5cblxuLmFnZW5kYV93cmFwcGVye1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cblxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuXG4gICAgLmJsb2Nre1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cblxuICAgIC5ibG9jay1sZWZ0IG1hdC1jYWxlbmRhcntcbiAgICAgICAgd2lkdGg6IDMwMHB4O1xuICAgICAgICBoZWlnaHQ6IDMwMHB4O1xuICAgICAgICBtYXJnaW46IDIwcHg7XG4gICAgfVxuICAgIC5ibG9jay1sZWZ0e1xuICAgICAgICBvcmRlcjogMTtcbiAgICAgICAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE2KSAwcHggM3B4IDZweCwgcmdiYSgwLCAwLCAwLCAwLjIzKSAwcHggM3B4IDZweDtcbiAgICAgICAgaGVpZ2h0OiA0MDBweDtcbiAgICAgICAgfVxuXG4gICAgLmJsb2NrLXJpZ2h0e1xuICAgICAgICBvcmRlcjogMjtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDoyMHB4IDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICAgICAgICB3aWR0aDogMzQycHg7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgaGVpZ2h0OiA2MDBweDtcbiAgICAgICAgfVxuICAgICAgICBcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CalandarComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-calandar',
                templateUrl: './calandar.component.html',
                styleUrls: ['./calandar.component.css']
            }]
    }], function () { return [{ type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/change-pass/change-pass.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/cpn/change-pass/change-pass.component.ts ***!
  \**********************************************************/
/*! exports provided: ChangePassComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePassComponent", function() { return ChangePassComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");







class ChangePassComponent {
    constructor(fb, auth, _Activatedroute) {
        this.fb = fb;
        this.auth = auth;
        this._Activatedroute = _Activatedroute;
        this.passForm = this.fb.group({
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            password: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            password_confirmation: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
        });
    }
    ngOnInit() {
        this.token = this._Activatedroute.snapshot.paramMap.get("token");
    }
    onSubmit() {
        const formData = new FormData();
        formData.append('email', this.passForm.get('email').value);
        formData.append('password', this.passForm.get('password').value);
        formData.append('password_confirmation', this.passForm.get('password_confirmation').value);
        formData.append('token', this.token);
        this.auth.resetPass(formData).subscribe(data => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                position: 'top-end',
                icon: 'success',
                title: 'mail reussie',
                showConfirmButton: false,
                timer: 6000
            });
            location.href = '/cpn/Connexion';
        }, error => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'tous les champs est obligatoire !',
            });
        });
    }
}
ChangePassComponent.ɵfac = function ChangePassComponent_Factory(t) { return new (t || ChangePassComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"])); };
ChangePassComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ChangePassComponent, selectors: [["app-change-pass"]], decls: 47, vars: 1, consts: [[1, "login_container"], [1, "login_wrapper"], [1, "login_content"], [1, "row", "g-0", "py-2"], [1, "col-md-6"], [1, "container"], [1, "row", "row-cols-3", "g-2"], [1, "col"], ["src", "assets/cpnimages/connexion/3.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/2.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/1.png", "alt", "", 2, "width", "100%", "border-radius", "0 3.5rem 0 0", "height", "100%"], ["src", "assets/cpnimages/connexion/4.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/5.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/6.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/7.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/8.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/9.png", "alt", "", 2, "border-radius", "0 0 3.5rem 0", "width", "100%", "height", "100%"], [1, "col-md-6", 2, "display", "flex", "flex-direction", "row", "justify-content", "center"], [1, "card-body", "p-4", "p-sm-5"], [1, "r\u00E9seau-sociaux"], [1, "text-center"], ["href", ""], ["src", "assets/cpnimages/connexion/gmail.png", "alt", "", 2, "width", "10%", "margin-left", "10px"], [3, "formGroup", "ngSubmit"], [1, "col-md-12", "pb-2"], ["type", "text", "formControlName", "email", "id", "floatingInput", "placeholder", "Email", 1, "form-control"], ["type", "text", "formControlName", "password", "id", "floatingInput", "placeholder", "Password", 1, "form-control"], ["type", "text", "formControlName", "password_confirmation", "id", "floatingInput", "placeholder", "Confirmer password", 1, "form-control"], [1, "container", "overflow-hidden"], [1, "row"], [2, "text-align", "center", "color", "black"], ["type", "submit", 1, "btn", "btn-danger", 2, "background-color", "red"]], template: function ChangePassComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h3", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "nouvelle mots pass svp !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "form", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ChangePassComponent_Template_form_ngSubmit_35_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "input", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "input", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "input", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "button", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "envoyer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.passForm);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"]], styles: [".login_container[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n}\n\n.login_container[_ngcontent-%COMP%]   .login_content[_ngcontent-%COMP%]   .login_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n    margin-top:0;\n    margin-right: 0;\n    margin-left: 0;\n}\n\n.container[_ngcontent-%COMP%]{\n    margin: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vY2hhbmdlLXBhc3MvY2hhbmdlLXBhc3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLFlBQVk7SUFDWixlQUFlO0lBQ2YsY0FBYztBQUNsQjs7QUFDQTtJQUNJLFlBQVk7QUFDaEIiLCJmaWxlIjoiYXBwL2Nwbi9jaGFuZ2UtcGFzcy9jaGFuZ2UtcGFzcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luX2NvbnRhaW5lcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xufVxuXG4ubG9naW5fY29udGFpbmVyIC5sb2dpbl9jb250ZW50IC5sb2dpbl93cmFwcGVyIC5yb3d7XG4gICAgbWFyZ2luLXRvcDowO1xuICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICBtYXJnaW4tbGVmdDogMDtcbn1cbi5jb250YWluZXJ7XG4gICAgbWFyZ2luOiAzMHB4O1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ChangePassComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-change-pass',
                templateUrl: './change-pass.component.html',
                styleUrls: ['./change-pass.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/collectivite/collectivite.component.ts":
/*!************************************************************!*\
  !*** ./src/app/cpn/collectivite/collectivite.component.ts ***!
  \************************************************************/
/*! exports provided: CollectiviteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectiviteComponent", function() { return CollectiviteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var ng_lazyload_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng-lazyload-image */ "./node_modules/ng-lazyload-image/__ivy_ngcc__/fesm2015/ng-lazyload-image.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../notre-succes/notre-succes.component */ "./src/app/cpn/notre-succes/notre-succes.component.ts");
/* harmony import */ var _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../side-bar/side-bar.component */ "./src/app/cpn/side-bar/side-bar.component.ts");
/* harmony import */ var _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../map-french-region/map-french-region.component */ "./src/app/cpn/map-french-region/map-french-region.component.ts");








class CollectiviteComponent {
    constructor(auth) {
        this.auth = auth;
    }
    ngOnInit() {
        this.auth.getFellower().subscribe(res => {
            this.follow = res;
        });
        $('.search').mouseenter(function () {
            $(this).addClass('search--show');
            $(this).removeClass('search--hide');
        });
        $('.search').mouseleave(function () {
            $(this).addClass('search--hide');
            $(this).removeClass('search--show');
        });
    }
}
CollectiviteComponent.ɵfac = function CollectiviteComponent_Factory(t) { return new (t || CollectiviteComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"])); };
CollectiviteComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CollectiviteComponent, selectors: [["app-collectivite"]], decls: 129, vars: 3, consts: [[1, "primary_body", "mb-5"], [1, "home_container"], [1, "section_heading", "mb-3"], [1, "heading_wrapper", "container-fluid", "g-0"], [1, "row", "g-0"], [1, "col-md-6"], [1, "row", "g-0", "justify-content-center"], [1, "col-auto"], [1, "img_wrapper"], ["alt", "", 1, "heading_img", 2, "margin-top", "-76px", "margin-left", "-8px", 3, "defaultImage", "lazyLoad"], [1, "col-md-6", "p-3"], [1, "row", "g-0", "justify-content-start"], [1, "col-md-auto"], [1, "title_heading"], [1, "desc_heading"], [1, "search_bloc"], [1, "search"], ["placeholder", "Quel type de subvention souhaitez vous", 1, "search__input"], [1, "carre"], [1, "row"], [1, "col", 2, "display", "initial", "flex-direction", "row", "padding", "8px"], ["src", "assets/cpnimages/home/I.png", "alt", "", 2, "width", "10%", "margin", "0 0 0 30px"], [2, "margin-left", "4px", "font-size", "13px"], [2, "text-align", "center", "margin", "0 90px 0 0"], [2, "text-align", "center", "font-size", "12px", "margin", "0 0px 0 -45px"], [2, "color", "#00FF00"], [1, "g-0", "mb-5", 2, "margin-top", "150px"], [1, "container", "px-4"], [1, "divider", "g-0", "mb-5"], [1, "divider_ligne"], [1, "g-0", "mb-5"], [1, "actu"], [1, "row", "mb-5", "g-4", "justify-content-center"], [1, "col-md-4", "col-lg-4", "col-xl-4", "feuil"], [1, "act-wrapper", "bg-white"], [1, "act-img"], ["src", "assets/cpnimages/actualites/act1.png", "alt", "actualite"], [1, "act-content", "p-2", 2, "max-height", "100px"], [1, "act-title"], [1, "act-desc"], [1, "m-0"], ["src", "assets/cpnimages/actualites/act5.png", "alt", "actualite", 2, "width", "100%", "max-height", "185px", "object-fit", "cover", "border-radius", "15px"], ["src", "assets/cpnimages/actualites/act6.png", "alt", "actualite", 2, "width", "100%", "max-height", "165px", "object-fit", "cover", "border-radius", "15px"], [2, "color", "#111d5e"], [1, "container", "px-4", "proposons"], [1, "1"], [1, "SousB"], [1, "fas", "fa-long-arrow-alt-right"], [1, "SousB", 2, "border", "3px solid #111d5e"]], template: function CollectiviteComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "h2", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "COLLECTIVIT\u00C9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h4", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "vos subventions num\u00E9riques sans conditions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "form", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "span", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Followers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h4", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "2.1%");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "vs last 7 days");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "app-notre-succes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "app-side-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "section", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Choisir ma r\u00E9gion");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "app-map-french-region");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "section", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "span", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " Actualite ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "section", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "img", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, " Num\u00E9riser votre entreprise n'a jamais \u00E9t\u00E9 aussi simple!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Nous sommes depuis quelques ann\u00E9es rentr\u00E9 dans l'\u00E8re... Lire la suite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "img", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "TPE - PME : Une opportunit\u00E9 \u00E0 saisir !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "p", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "2020 \u00E9tait une ann\u00E9e tr\u00E8s dure pour les chefs d\u2019entreprises...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, " Lire la suite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "img", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "La France se digitalise !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "p", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "En France, un bon nombre d\u2019entreprises fran\u00E7aises n'ont...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "Lire la suite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, " Nous proposons ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "h3", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, " Peut importe votre secteur d\u2019activit\u00E9 nous vous offrons une subvention.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "1.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "400+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Speed Optimization");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "i", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "Lorem Ipsum is simply dummy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "2.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "600+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "Cloud Solutions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "i", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "Lorem Ipsum is simply dummy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "3.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "820+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Online Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "i", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "Lorem Ipsum is simply dummy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("defaultImage", "assets/cpnimages/home/11.png")("lazyLoad", "assets/cpnimages/home/11.png");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.follow, "K");
    } }, directives: [ng_lazyload_image__WEBPACK_IMPORTED_MODULE_2__["LazyLoadImageDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_4__["NotreSuccesComponent"], _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_5__["SideBarComponent"], _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_6__["MapFrenchRegionComponent"]], styles: [".carre[_ngcontent-%COMP%] {\n  width: 200px;\n  height: 90px;\n  background: white;\n  border-radius: 18px;\n  margin-left: -250px;\n  margin-top: 125px;\n}\n.main-content[_ngcontent-%COMP%] {\n  border-top: none!important;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n\t background:#EBECF0;\n\t min-height: -moz-fit-content;\n\t min-height: fit-content;\n\t border-bottom-right-radius: 100px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n\t height: 100%;\n\t max-height: 585px;\n\t margin-top: -35px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n\t font-size: 60px;\n\t font-weight: 800;\n\t color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n\t font-size: 40px;\n\t font-weight: 700;\n\t color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%], .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:after {\n\t box-sizing: border-box;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   body[_ngcontent-%COMP%] {\n\t background: #f5f5f5;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   main[_ngcontent-%COMP%] {\n\t left: 50%;\n\t position: absolute;\n\t top: 50%;\n\t transform: translateX(-50%) translateY(-50%);\n\t width: 300px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\n\t content: \"\";\n\t display: block;\n\t position: absolute;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before {\n\t border: 5px solid #111d5e ;\n\t border-radius: 20px;\n\t height: 40px;\n\t transition: all 0.3s ease-out;\n\t transition-delay: 0.3s;\n\t width: 40px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\n\t background: #111d5e;\n\t border-radius: 3px;\n\t height: 5px;\n\t transform: rotate(-45deg);\n\t transform-origin: 0% 100%;\n\t transition: all 0.3s ease-out;\n\t width: 15px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n\t background: transparent;\n\t border: none;\n\t border-radius: 20px;\n\t display: block;\n\t font-size: 20px;\n\t height: 40px;\n\t line-height: 40px;\n\t opacity: 0;\n\t outline: none;\n\t padding: 0 15px;\n\t position: relative;\n\t transition: all 0.3s ease-out;\n\t transition-delay: 0.6s;\n\t width: 40px;\n\t z-index: 1;\n\t color: rgb(85, 85, 85);\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:before {\n\t transition-delay: 0.3s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:after {\n\t transition-delay: 0.6s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n\t transition-delay: 0s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:after {\n\t transform: rotate(-45deg) translateX(15px) translateY(-2px);\n\t width: 0;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n\t border: 5px solid #111d5e;\n\t border-radius: 20px;\n\t height: 40px;\n\t width: 500px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n\t opacity: 1;\n\t width: 500px;\n}\n\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 5px;\n  background: #111d5e;\n  display: block;\n  position: relative;\n}\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::before {\n  content: \"\";\n  position: absolute;\n  width: 30%;\n  top: 0;\n  left: 0;\n  height: 5px;\n  background: red;\n}\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::after {\n  content: \"\";\n  position: absolute;\n  width: 30%;\n  top: 0;\n  right: 0;\n  height: 5px;\n  background: red;\n}\ninput[type=\"text\"][_ngcontent-%COMP%] {\n  height: 50px;\n  font-size: 30px;\n  display: inline-block;\n\n  font-weight: 100;\n  border: none;\n  outline: none;\n  color: white;\n  padding: 3px;\n  padding-right: 60px;\n  width: 0px;\n  position: absolute;\n  top: 0;\n  right: 0;\n  background: none;\n  z-index: 3;\n  transition: width 0.4s cubic-bezier(0, 0.795, 0, 1);\n  cursor: pointer;\n}\ninput[type=\"text\"][_ngcontent-%COMP%]:focus:hover {\n  border-bottom: 1px solid white;\n}\ninput[type=\"text\"][_ngcontent-%COMP%]:focus {\n  width: 700px;\n  z-index: 1;\n  border-bottom: 1px solid white;\n  cursor: text;\n}\ninput[type=\"submit\"][_ngcontent-%COMP%] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  color: white;\n  float: right;\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADNQTFRFU1NT9fX1lJSUXl5e1dXVfn5+c3Nz6urqv7+/tLS0iYmJqampn5+fysrK39/faWlp////Vi4ZywAAABF0Uk5T/////////////////////wAlrZliAAABLklEQVR42rSWWRbDIAhFHeOUtN3/ags1zaA4cHrKZ8JFRHwoXkwTvwGP1Qo0bYObAPwiLmbNAHBWFBZlD9j0JxflDViIObNHG/Do8PRHTJk0TezAhv7qloK0JJEBh+F8+U/hopIELOWfiZUCDOZD1RADOQKA75oq4cvVkcT+OdHnqqpQCITWAjnWVgGQUWz12lJuGwGoaWgBKzRVBcCypgUkOAoWgBX/L0CmxN40u6xwcIJ1cOzWYDffp3axsQOyvdkXiH9FKRFwPRHYZUaXMgPLeiW7QhbDRciyLXJaKheCuLbiVoqx1DVRyH26yb0hsuoOFEPsoz+BVE0MRlZNjGZcRQyHYkmMp2hBTIzdkzCTc/pLqOnBrk7/yZdAOq/q5NPBH1f7x7fGP4C3AAMAQrhzX9zhcGsAAAAASUVORK5CYII=)\n    center center no-repeat;\n  text-indent: -10000px;\n  border: none;\n  position: absolute;\n  top: 0;\n  right: 0;\n  z-index: 2;\n  cursor: pointer;\n  opacity: 0.4;\n  cursor: pointer;\n  transition: opacity 0.4s ease;\n}\ninput[type=\"submit\"][_ngcontent-%COMP%]:hover {\n  opacity: 0.8;\n}\nh1[_ngcontent-%COMP%]{\n  font-weight: bold;\n  color: #111d5e;\n\n}\nh5[_ngcontent-%COMP%]{\n  color: #111d5e;\n  font-size: 15px;\n}\np[_ngcontent-%COMP%]{\n  font-size: 12px;\n  color: #111d5e;\n\n}\n\n.actu[_ngcontent-%COMP%]{\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n}\n.actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n  width: 70%;\n}\n.feuil[_ngcontent-%COMP%]{\n  flex: 0 0 auto;\n  width: 33.33333333%;\n}\n.actu[_ngcontent-%COMP%]   .act-wrapper[_ngcontent-%COMP%]{\n  overflow: hidden;\n  border-radius: 15px;\n  background-color: #EBECF0 ;\n  box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;\n}\n.actu[_ngcontent-%COMP%]   .act-wrapper[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\n  width: 100%;\n   max-height:70%;\n    object-fit:cover;\n  border-radius: 15px;\n}\n\n.proposons[_ngcontent-%COMP%]{\n    height: 300px;\n    background: linear-gradient(#ffffff, #e5e6e7);\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n  }\n.proposons[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    margin: 0;\n  }\n.SousB[_ngcontent-%COMP%]{\n    border: 3px solid rgb(41, 191, 211);\n    border-radius: 15px;\n    height: 160px;\n    margin: 0;\n    padding: 20px;\n    width: 300px;\n  }\n.SousB[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\n    font-size: 30px;\n  }\n.SousB[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%]{\n    font-size: 15px;\nfont-weight: bold;\nline-height: normal;\n  }\n.SousB[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\n    font-size: 15px;\n  }\n.SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n    margin-left: 277px;\nz-index: auto;\nfont-size: 26px;\nmargin-top: -50px;\n  }\n\n\n@media only screen and (min-width : 320px) and (max-width : 480px)  {\n        \n      \n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n      border-bottom-right-radius: 0px;\n    }\n    \n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n  font-size: 30px;\n  font-weight: 800;\n  color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n  font-size: 20px;\n  font-weight: 700;\n  color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n  height: 100%;\n  max-height: 400px;\n  margin-top: -35px;\n}\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n      border: 5px solid #111d5e;\n      border-radius: 20px;\n      height: 40px;\n      width: 300px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n      opacity: 1;\n      width: 300px;\n    }\n    .carre[_ngcontent-%COMP%] {\n      width: 200px;\n      height: 90px;\n      background: white;\n      border-radius: 18px;\n      margin-left: 0px;\n      margin-top: 125px;\n    }\n          \n          .actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n            width: 100%;\n          }\n          .feuil[_ngcontent-%COMP%]{\n            flex: 0 0 auto;\n            width: 70%;\n          }\n    \n        .proposons[_ngcontent-%COMP%]{\n          height: 800px;\n      background: linear-gradient(#ffffff, #e5e6e7);\n      display: flex;\n      flex-direction: column;\n      justify-content: space-around;\n      align-items: center;\n        }\n        .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n          margin-left: 77px;\n      z-index: auto;\n      font-size: 26px;\n      margin-top: 70px;\n      transform: rotate(90deg);\n      position: absolute;\n        }\n      \n     \n    }\n\n@media only screen and (min-width : 480px) and (max-width : 768px)  {\n  \n             \n      \n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n      border-bottom-right-radius: 0px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n      font-size: 40px;\n      font-weight: 800;\n      color:  #111d5e;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n      font-size: 30px;\n      font-weight: 700;\n      color:  #111d5e;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n      border: 5px solid #111d5e;\n      border-radius: 20px;\n      height: 40px;\n      width: 300px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n      opacity: 1;\n      width: 300px;\n    }\n    .carre[_ngcontent-%COMP%] {\n      width: 200px;\n      height: 90px;\n      background: white;\n      border-radius: 18px;\n      margin-left: 0px;\n      margin-top: 125px;\n    }\n\n          \n          .actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n            width: 100%;\n          }\n          .feuil[_ngcontent-%COMP%]{\n            flex: 0 0 auto;\n            width: 70%;\n          }\n\n    \n        .proposons[_ngcontent-%COMP%]{\n          height: 800px;\n      background: linear-gradient(#ffffff, #e5e6e7);\n      display: flex;\n      flex-direction: column;\n      justify-content: space-around;\n      align-items: center;\n        }\n        .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n          margin-left: 77px;\n      z-index: auto;\n      font-size: 26px;\n      margin-top: 70px;\n      transform: rotate(90deg);\n      position: absolute;\n        }\n      \n     \n    }\n\n@media only screen and (min-width : 768px) and (max-width : 992px)  {\n    \n             \n      \n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n      border-bottom-right-radius: 0px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n      font-size: 40px;\n      font-weight: 800;\n      color:  #111d5e;\n    }\n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n      font-size: 30px;\n      font-weight: 700;\n      color:  #111d5e;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n      border: 5px solid #111d5e;\n      border-radius: 20px;\n      height: 40px;\n      width: 300px;\n    }\n    .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n      opacity: 1;\n      width: 300px;\n    }\n\n    \n        .proposons[_ngcontent-%COMP%]{\n          height: 800px;\n      background: linear-gradient(#ffffff, #e5e6e7);\n      display: flex;\n      flex-direction: column;\n      justify-content: space-around;\n      align-items: center;\n        }\n        .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n          margin-left: 77px;\n      z-index: auto;\n      font-size: 26px;\n      margin-top: 70px;\n      transform: rotate(90deg);\n      position: absolute;\n        }\n      \n        .actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n          display: flex;\n          flex-direction: column;\n          justify-content: center;\n          align-items: center;\n          width: 100%;\n        }\n        .feuil[_ngcontent-%COMP%]{\n          flex: 0 0 auto;\n          width: 70%;\n        }\n     }\n\n@media only screen and (min-width : 992px) and (max-width : 1200px)  {\n        \n          \n  \n          .SousB[_ngcontent-%COMP%]{\n            border: 3px solid rgb(41, 191, 211);\n            border-radius: 15px;\n            height: 160px;\n            margin: 0;\n            padding: 20px;\n            width: 250px;\n          }\n          .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\n            margin-left: 225px;\n        z-index: auto;\n        font-size: 26px;\n        margin-top: -50px;\n          }\n          \n        \n          .actu[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n            width: 100%;\n          }\n          .feuil[_ngcontent-%COMP%]{\n            flex: 0 0 auto;\n            width: 70%;\n          }\n        }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vY29sbGVjdGl2aXRlL2NvbGxlY3Rpdml0ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9GQUFvRjtBQUNwRjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSwwQkFBMEI7QUFDNUI7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQiw0QkFBdUI7RUFBdkIsdUJBQXVCO0VBQ3ZCLGlDQUFpQztBQUNuQztBQUNDO0VBQ0MsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUI7QUFDbkI7QUFDQztFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtBQUNqQjtBQUNDO0VBQ0MsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixlQUFlO0FBQ2pCO0FBQ0M7RUFDQyxzQkFBc0I7QUFDeEI7QUFDQztFQUNDLG1CQUFtQjtBQUNyQjtBQUNDO0VBQ0MsU0FBUztFQUNULGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsNENBQTRDO0VBQzVDLFlBQVk7QUFDZDtBQUNDO0VBQ0MsV0FBVztFQUNYLGNBQWM7RUFDZCxrQkFBa0I7QUFDcEI7QUFDQztFQUNDLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLDZCQUE2QjtFQUM3QixzQkFBc0I7RUFDdEIsV0FBVztBQUNiO0FBQ0M7RUFDQyxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIseUJBQXlCO0VBQ3pCLDZCQUE2QjtFQUM3QixXQUFXO0FBQ2I7QUFDQztFQUNDLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxlQUFlO0VBQ2YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsYUFBYTtFQUNiLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLHNCQUFzQjtFQUN0QixXQUFXO0VBQ1gsVUFBVTtFQUNWLHNCQUFzQjtBQUN4QjtBQUNDO0VBQ0Msc0JBQXNCO0FBQ3hCO0FBQ0M7RUFDQyxzQkFBc0I7QUFDeEI7QUFDQztFQUNDLG9CQUFvQjtBQUN0QjtBQUNDO0VBQ0MsMkRBQTJEO0VBQzNELFFBQVE7QUFDVjtBQUNDO0VBQ0MseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osWUFBWTtBQUNkO0FBQ0M7RUFDQyxVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBR0Msb0dBQW9HO0FBQ3BHO0VBQ0MsV0FBVztFQUNYLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsTUFBTTtFQUNOLE9BQU87RUFDUCxXQUFXO0VBQ1gsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsTUFBTTtFQUNOLFFBQVE7RUFDUixXQUFXO0VBQ1gsZUFBZTtBQUNqQjtBQUlBO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixxQkFBcUI7O0VBRXJCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osYUFBYTtFQUNiLFlBQVk7RUFDWixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFFBQVE7RUFDUixnQkFBZ0I7RUFDaEIsVUFBVTtFQUNWLG1EQUFtRDtFQUNuRCxlQUFlO0FBQ2pCO0FBRUE7RUFDRSw4QkFBOEI7QUFDaEM7QUFFQTtFQUNFLFlBQVk7RUFDWixVQUFVO0VBQ1YsOEJBQThCO0VBQzlCLFlBQVk7QUFDZDtBQUNBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFlBQVk7RUFDWjsyQkFDeUI7RUFDekIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFFBQVE7RUFDUixVQUFVO0VBQ1YsZUFBZTtFQUNmLFlBQVk7RUFDWixlQUFlO0VBQ2YsNkJBQTZCO0FBQy9CO0FBRUE7RUFDRSxZQUFZO0FBQ2Q7QUFJQTtFQUNFLGlCQUFpQjtFQUNqQixjQUFjOztBQUVoQjtBQUVBO0VBQ0UsY0FBYztFQUNkLGVBQWU7QUFDakI7QUFDQTtFQUNFLGVBQWU7RUFDZixjQUFjOztBQUVoQjtBQUVBLDRHQUE0RztBQUM1RztFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixXQUFXO0FBQ2I7QUFDQTtFQUNFLFVBQVU7QUFDWjtBQUNBO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsNEVBQTRFO0FBQzlFO0FBQ0E7RUFDRSxXQUFXO0dBQ1YsY0FBYztJQUNiLGdCQUFnQjtFQUNsQixtQkFBbUI7QUFDckI7QUFDQSx5R0FBeUc7QUFDdkc7SUFDRSxhQUFhO0lBQ2IsNkNBQTZDO0lBQzdDLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLG1CQUFtQjtFQUNyQjtBQUNBO0lBQ0UsU0FBUztFQUNYO0FBQ0E7SUFDRSxtQ0FBbUM7SUFDbkMsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixTQUFTO0lBQ1QsYUFBYTtJQUNiLFlBQVk7RUFDZDtBQUNBO0lBQ0UsZUFBZTtFQUNqQjtBQUNBO0lBQ0UsZUFBZTtBQUNuQixpQkFBaUI7QUFDakIsbUJBQW1CO0VBQ2pCO0FBQ0E7SUFDRSxlQUFlO0VBQ2pCO0FBRUE7SUFDRSxrQkFBa0I7QUFDdEIsYUFBYTtBQUNiLGVBQWU7QUFDZixpQkFBaUI7RUFDZjtBQUlJLDBHQUEwRztBQUMxRywyQkFBMkI7QUFDM0I7O0lBRUYsNkZBQTZGO0lBQzdGO01BQ0UsK0JBQStCO0lBQ2pDOztDQUVIO0VBQ0MsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7QUFDakI7QUFDQTtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsaUJBQWlCO0FBQ25CO0lBQ0k7TUFDRSx5QkFBeUI7TUFDekIsbUJBQW1CO01BQ25CLFlBQVk7TUFDWixZQUFZO0lBQ2Q7SUFDQTtNQUNFLFVBQVU7TUFDVixZQUFZO0lBQ2Q7SUFDQTtNQUNFLFlBQVk7TUFDWixZQUFZO01BQ1osaUJBQWlCO01BQ2pCLG1CQUFtQjtNQUNuQixnQkFBZ0I7TUFDaEIsaUJBQWlCO0lBQ25CO1VBQ00sNEdBQTRHO1VBQzVHO1lBQ0UsYUFBYTtZQUNiLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsbUJBQW1CO1lBQ25CLFdBQVc7VUFDYjtVQUNBO1lBQ0UsY0FBYztZQUNkLFVBQVU7VUFDWjtJQUNOLDJGQUEyRjtRQUN2RjtVQUNFLGFBQWE7TUFDakIsNkNBQTZDO01BQzdDLGFBQWE7TUFDYixzQkFBc0I7TUFDdEIsNkJBQTZCO01BQzdCLG1CQUFtQjtRQUNqQjtRQUNBO1VBQ0UsaUJBQWlCO01BQ3JCLGFBQWE7TUFDYixlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLHdCQUF3QjtNQUN4QixrQkFBa0I7UUFDaEI7OztJQUdKO0FBRUEsZ0NBQWdDO0FBQ2hDOzs7SUFHQSw2RkFBNkY7SUFDN0Y7TUFDRSwrQkFBK0I7SUFDakM7SUFDQTtNQUNFLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsZUFBZTtJQUNqQjtJQUNBO01BQ0UsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixlQUFlO0lBQ2pCO0lBQ0E7TUFDRSx5QkFBeUI7TUFDekIsbUJBQW1CO01BQ25CLFlBQVk7TUFDWixZQUFZO0lBQ2Q7SUFDQTtNQUNFLFVBQVU7TUFDVixZQUFZO0lBQ2Q7SUFDQTtNQUNFLFlBQVk7TUFDWixZQUFZO01BQ1osaUJBQWlCO01BQ2pCLG1CQUFtQjtNQUNuQixnQkFBZ0I7TUFDaEIsaUJBQWlCO0lBQ25COztVQUVNLDRHQUE0RztVQUM1RztZQUNFLGFBQWE7WUFDYixzQkFBc0I7WUFDdEIsdUJBQXVCO1lBQ3ZCLG1CQUFtQjtZQUNuQixXQUFXO1VBQ2I7VUFDQTtZQUNFLGNBQWM7WUFDZCxVQUFVO1VBQ1o7O0lBRU4sMkZBQTJGO1FBQ3ZGO1VBQ0UsYUFBYTtNQUNqQiw2Q0FBNkM7TUFDN0MsYUFBYTtNQUNiLHNCQUFzQjtNQUN0Qiw2QkFBNkI7TUFDN0IsbUJBQW1CO1FBQ2pCO1FBQ0E7VUFDRSxpQkFBaUI7TUFDckIsYUFBYTtNQUNiLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsd0JBQXdCO01BQ3hCLGtCQUFrQjtRQUNoQjs7O0lBR0o7QUFJQywwQkFBMEI7QUFDMUI7OztJQUdELDZGQUE2RjtJQUM3RjtNQUNFLCtCQUErQjtJQUNqQztJQUNBO01BQ0UsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixlQUFlO0lBQ2pCO0lBQ0E7TUFDRSxlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLGVBQWU7SUFDakI7SUFDQTtNQUNFLHlCQUF5QjtNQUN6QixtQkFBbUI7TUFDbkIsWUFBWTtNQUNaLFlBQVk7SUFDZDtJQUNBO01BQ0UsVUFBVTtNQUNWLFlBQVk7SUFDZDs7SUFFQSwyRkFBMkY7UUFDdkY7VUFDRSxhQUFhO01BQ2pCLDZDQUE2QztNQUM3QyxhQUFhO01BQ2Isc0JBQXNCO01BQ3RCLDZCQUE2QjtNQUM3QixtQkFBbUI7UUFDakI7UUFDQTtVQUNFLGlCQUFpQjtNQUNyQixhQUFhO01BQ2IsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQix3QkFBd0I7TUFDeEIsa0JBQWtCO1FBQ2hCO01BQ0YsNEdBQTRHO1FBQzFHO1VBQ0UsYUFBYTtVQUNiLHNCQUFzQjtVQUN0Qix1QkFBdUI7VUFDdkIsbUJBQW1CO1VBQ25CLFdBQVc7UUFDYjtRQUNBO1VBQ0UsY0FBYztVQUNkLFVBQVU7UUFDWjtLQUNIO0FBSUEsNEJBQTRCO0FBQzVCOztVQUVLLDJGQUEyRjs7VUFFM0Y7WUFDRSxtQ0FBbUM7WUFDbkMsbUJBQW1CO1lBQ25CLGFBQWE7WUFDYixTQUFTO1lBQ1QsYUFBYTtZQUNiLFlBQVk7VUFDZDtVQUNBO1lBQ0Usa0JBQWtCO1FBQ3RCLGFBQWE7UUFDYixlQUFlO1FBQ2YsaUJBQWlCO1VBQ2Y7O1FBRUYsNEdBQTRHO1VBQzFHO1lBQ0UsYUFBYTtZQUNiLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsbUJBQW1CO1lBQ25CLFdBQVc7VUFDYjtVQUNBO1lBQ0UsY0FBYztZQUNkLFVBQVU7VUFDWjtRQUNGIiwiZmlsZSI6ImFwcC9jcG4vY29sbGVjdGl2aXRlL2NvbGxlY3Rpdml0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmNhcnJlIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBoZWlnaHQ6IDkwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAxOHB4O1xuICBtYXJnaW4tbGVmdDogLTI1MHB4O1xuICBtYXJnaW4tdG9wOiAxMjVweDtcbn1cbi5tYWluLWNvbnRlbnQge1xuICBib3JkZXItdG9wOiBub25lIWltcG9ydGFudDtcbn1cblxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XG5cdCBiYWNrZ3JvdW5kOiNFQkVDRjA7XG5cdCBtaW4taGVpZ2h0OiBmaXQtY29udGVudDtcblx0IGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMDBweDtcbn1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XG5cdCBoZWlnaHQ6IDEwMCU7XG5cdCBtYXgtaGVpZ2h0OiA1ODVweDtcblx0IG1hcmdpbi10b3A6IC0zNXB4O1xufVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XG5cdCBmb250LXNpemU6IDYwcHg7XG5cdCBmb250LXdlaWdodDogODAwO1xuXHQgY29sb3I6ICAjMTExZDVlO1xufVxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuZGVzY19oZWFkaW5nIHtcblx0IGZvbnQtc2l6ZTogNDBweDtcblx0IGZvbnQtd2VpZ2h0OiA3MDA7XG5cdCBjb2xvcjogICMxMTFkNWU7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgKiwgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgKjpiZWZvcmUsIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICo6YWZ0ZXIge1xuXHQgYm94LXNpemluZzogYm9yZGVyLWJveDtcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBib2R5IHtcblx0IGJhY2tncm91bmQ6ICNmNWY1ZjU7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgbWFpbiB7XG5cdCBsZWZ0OiA1MCU7XG5cdCBwb3NpdGlvbjogYWJzb2x1dGU7XG5cdCB0b3A6IDUwJTtcblx0IHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSB0cmFuc2xhdGVZKC01MCUpO1xuXHQgd2lkdGg6IDMwMHB4O1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YmVmb3JlLCAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmFmdGVyIHtcblx0IGNvbnRlbnQ6IFwiXCI7XG5cdCBkaXNwbGF5OiBibG9jaztcblx0IHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmJlZm9yZSB7XG5cdCBib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlIDtcblx0IGJvcmRlci1yYWRpdXM6IDIwcHg7XG5cdCBoZWlnaHQ6IDQwcHg7XG5cdCB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcblx0IHRyYW5zaXRpb24tZGVsYXk6IDAuM3M7XG5cdCB3aWR0aDogNDBweDtcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmFmdGVyIHtcblx0IGJhY2tncm91bmQ6ICMxMTFkNWU7XG5cdCBib3JkZXItcmFkaXVzOiAzcHg7XG5cdCBoZWlnaHQ6IDVweDtcblx0IHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG5cdCB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAxMDAlO1xuXHQgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1vdXQ7XG5cdCB3aWR0aDogMTVweDtcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoX19pbnB1dCB7XG5cdCBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcblx0IGJvcmRlcjogbm9uZTtcblx0IGJvcmRlci1yYWRpdXM6IDIwcHg7XG5cdCBkaXNwbGF5OiBibG9jaztcblx0IGZvbnQtc2l6ZTogMjBweDtcblx0IGhlaWdodDogNDBweDtcblx0IGxpbmUtaGVpZ2h0OiA0MHB4O1xuXHQgb3BhY2l0eTogMDtcblx0IG91dGxpbmU6IG5vbmU7XG5cdCBwYWRkaW5nOiAwIDE1cHg7XG5cdCBwb3NpdGlvbjogcmVsYXRpdmU7XG5cdCB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcblx0IHRyYW5zaXRpb24tZGVsYXk6IDAuNnM7XG5cdCB3aWR0aDogNDBweDtcblx0IHotaW5kZXg6IDE7XG5cdCBjb2xvcjogcmdiKDg1LCA4NSwgODUpO1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLWhpZGU6YmVmb3JlIHtcblx0IHRyYW5zaXRpb24tZGVsYXk6IDAuM3M7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZTphZnRlciB7XG5cdCB0cmFuc2l0aW9uLWRlbGF5OiAwLjZzO1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLWhpZGUgLnNlYXJjaF9faW5wdXQge1xuXHQgdHJhbnNpdGlvbi1kZWxheTogMHM7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzphZnRlciB7XG5cdCB0cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpIHRyYW5zbGF0ZVgoMTVweCkgdHJhbnNsYXRlWSgtMnB4KTtcblx0IHdpZHRoOiAwO1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcblx0IGJvcmRlcjogNXB4IHNvbGlkICMxMTFkNWU7XG5cdCBib3JkZXItcmFkaXVzOiAyMHB4O1xuXHQgaGVpZ2h0OiA0MHB4O1xuXHQgd2lkdGg6IDUwMHB4O1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xuXHQgb3BhY2l0eTogMTtcblx0IHdpZHRoOiA1MDBweDtcbn1cbiAgXG5cbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipkaXZpZGVyKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuIC5wcmltYXJ5X2JvZHkgLmRpdmlkZXIgLmRpdmlkZXJfbGlnbmUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQ6ICMxMTFkNWU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucHJpbWFyeV9ib2R5IC5kaXZpZGVyIC5kaXZpZGVyX2xpZ25lOjpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAzMCU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQ6IHJlZDtcbn1cbi5wcmltYXJ5X2JvZHkgLmRpdmlkZXIgLmRpdmlkZXJfbGlnbmU6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMzAlO1xuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuICBoZWlnaHQ6IDVweDtcbiAgYmFja2dyb3VuZDogcmVkO1xufVxuXG5cblxuaW5wdXRbdHlwZT1cInRleHRcIl0ge1xuICBoZWlnaHQ6IDUwcHg7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuXG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIGJvcmRlcjogbm9uZTtcbiAgb3V0bGluZTogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAzcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDYwcHg7XG4gIHdpZHRoOiAwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgei1pbmRleDogMztcbiAgdHJhbnNpdGlvbjogd2lkdGggMC40cyBjdWJpYy1iZXppZXIoMCwgMC43OTUsIDAsIDEpO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmlucHV0W3R5cGU9XCJ0ZXh0XCJdOmZvY3VzOmhvdmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xufVxuXG5pbnB1dFt0eXBlPVwidGV4dFwiXTpmb2N1cyB7XG4gIHdpZHRoOiA3MDBweDtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xuICBjdXJzb3I6IHRleHQ7XG59XG5pbnB1dFt0eXBlPVwic3VibWl0XCJdIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogd2hpdGU7XG4gIGZsb2F0OiByaWdodDtcbiAgYmFja2dyb3VuZDogdXJsKGRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBREFBQUFBd0NBTUFBQUJnM0FtMUFBQUFHWFJGV0hSVGIyWjBkMkZ5WlFCQlpHOWlaU0JKYldGblpWSmxZV1I1Y2NsbFBBQUFBRE5RVEZSRlUxTlQ5ZlgxbEpTVVhsNWUxZFhWZm41K2MzTno2dXJxdjcrL3RMUzBpWW1KcWFtcG41K2Z5c3JLMzkvZmFXbHAvLy8vVmk0Wnl3QUFBQkYwVWs1VC8vLy8vLy8vLy8vLy8vLy8vLy8vL3dBbHJabGlBQUFCTGtsRVFWUjQyclNXV1JiRElBaEZIZU9VdE4zL2FnczF6YUE0Y0hyS1o4SkZSSHdvWGt3VHZ3R1AxUW8wYllPYkFQd2lMbWJOQUhCV0ZCWmxEOWowSnhmbERWaUlPYk5IRy9EbzhQUkhUSmswVGV6QWh2N3Fsb0swSkpFQmgrRjgrVS9ob3BJRUxPV2ZpWlVDRE9aRDFSQURPUUtBNzVvcTRjdlZrY1QrT2RIbnFxcFFDSVRXQWpuV1ZnR1FVV3oxMmxKdUd3R29hV2dCS3pSVkJjQ3lwZ1VrT0FvV2dCWC9MMENteE40MHU2eHdjSUoxY096V1lEZmZwM2F4c1FPeXZka1hpSDlGS1JGd1BSSFlaVWFYTWdQTGVpVzdRaGJEUmNpeUxYSmFLaGVDdUxiaVZvcXgxRFZSeUgyNnliMGhzdW9PRkVQc296K0JWRTBNUmxaTmpHWmNSUXlIWWttTXAyaEJUSXpka3pDVGMvcExxT25Ccms3L3laZEFPcS9xNU5QQkgxZjd4N2ZHUDRDM0FBTUFRcmh6WDl6aGNHc0FBQUFBU1VWT1JLNUNZSUk9KVxuICAgIGNlbnRlciBjZW50ZXIgbm8tcmVwZWF0O1xuICB0ZXh0LWluZGVudDogLTEwMDAwcHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuICB6LWluZGV4OiAyO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIG9wYWNpdHk6IDAuNDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuNHMgZWFzZTtcbn1cblxuaW5wdXRbdHlwZT1cInN1Ym1pdFwiXTpob3ZlciB7XG4gIG9wYWNpdHk6IDAuODtcbn1cblxuXG5cbmgxe1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMxMTFkNWU7XG5cbn1cblxuaDV7XG4gIGNvbG9yOiAjMTExZDVlO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5we1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjMTExZDVlO1xuXG59XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiphY3R1YWxpdGUqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5hY3R1e1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG59XG4uYWN0dSAucm93e1xuICB3aWR0aDogNzAlO1xufVxuLmZldWlse1xuICBmbGV4OiAwIDAgYXV0bztcbiAgd2lkdGg6IDMzLjMzMzMzMzMzJTtcbn1cbi5hY3R1IC5hY3Qtd3JhcHBlcntcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0VCRUNGMCA7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNikgMHB4IDNweCA2cHgsIHJnYmEoMCwgMCwgMCwgMC4yMykgMHB4IDNweCA2cHg7XG59XG4uYWN0dSAuYWN0LXdyYXBwZXIgaW1ne1xuICB3aWR0aDogMTAwJTtcbiAgIG1heC1oZWlnaHQ6NzAlO1xuICAgIG9iamVjdC1maXQ6Y292ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG59XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcHJvcG9zb24qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gIC5wcm9wb3NvbnN7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoI2ZmZmZmZiwgI2U1ZTZlNyk7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cbiAgLnByb3Bvc29ucyBoMXtcbiAgICBtYXJnaW46IDA7XG4gIH1cbiAgLlNvdXNCe1xuICAgIGJvcmRlcjogM3B4IHNvbGlkIHJnYig0MSwgMTkxLCAyMTEpO1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgaGVpZ2h0OiAxNjBweDtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICB3aWR0aDogMzAwcHg7XG4gIH1cbiAgLlNvdXNCIGgxe1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgfVxuICAuU291c0IgIGgze1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbmZvbnQtd2VpZ2h0OiBib2xkO1xubGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgfVxuICAuU291c0IgICBwe1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgfVxuXG4gIC5Tb3VzQiBpe1xuICAgIG1hcmdpbi1sZWZ0OiAyNzdweDtcbnotaW5kZXg6IGF1dG87XG5mb250LXNpemU6IDI2cHg7XG5tYXJnaW4tdG9wOiAtNTBweDtcbiAgfVxuICBcblxuXG4gICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipyZXNwb25zaXZlIGNzcyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgLyogQ3VzdG9tLCBpUGhvbmUgUmV0aW5hICAqL1xuICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMzIwcHgpIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpICB7XG4gICAgICAgIFxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxuICAgIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcge1xuICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDBweDtcbiAgICB9XG4gICAgXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC50aXRsZV9oZWFkaW5nIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBjb2xvcjogICMxMTFkNWU7XG59XG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgY29sb3I6ICAjMTExZDVlO1xufVxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciAuaGVhZGluZ19pbWcge1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1heC1oZWlnaHQ6IDQwMHB4O1xuICBtYXJnaW4tdG9wOiAtMzVweDtcbn1cbiAgICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XG4gICAgICBib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlO1xuICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICAgIGhlaWdodDogNDBweDtcbiAgICAgIHdpZHRoOiAzMDBweDtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XG4gICAgICBvcGFjaXR5OiAxO1xuICAgICAgd2lkdGg6IDMwMHB4O1xuICAgIH1cbiAgICAuY2FycmUge1xuICAgICAgd2lkdGg6IDIwMHB4O1xuICAgICAgaGVpZ2h0OiA5MHB4O1xuICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICBib3JkZXItcmFkaXVzOiAxOHB4O1xuICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgIG1hcmdpbi10b3A6IDEyNXB4O1xuICAgIH1cbiAgICAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYWN0dWFsaXRlKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAgICAgLmFjdHUgLnJvd3tcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5mZXVpbHtcbiAgICAgICAgICAgIGZsZXg6IDAgMCBhdXRvO1xuICAgICAgICAgICAgd2lkdGg6IDcwJTtcbiAgICAgICAgICB9XG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcHJvcG9zKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgICAgLnByb3Bvc29uc3tcbiAgICAgICAgICBoZWlnaHQ6IDgwMHB4O1xuICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCNmZmZmZmYsICNlNWU2ZTcpO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgICAgLlNvdXNCIGl7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDc3cHg7XG4gICAgICB6LWluZGV4OiBhdXRvO1xuICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgICAgbWFyZ2luLXRvcDogNzBweDtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgfVxuICAgICAgXG4gICAgIFxuICAgIH1cbiAgICBcbiAgICAvKiBFeHRyYSBTbWFsbCBEZXZpY2VzLCBQaG9uZXMgKi9cbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA0ODBweCkgYW5kIChtYXgtd2lkdGggOiA3NjhweCkgIHtcbiAgXG4gICAgICAgICAgICAgXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4O1xuICAgIH1cbiAgICAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xuICAgICAgZm9udC1zaXplOiA0MHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICAgIGNvbG9yOiAgIzExMWQ1ZTtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5kZXNjX2hlYWRpbmcge1xuICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICAgIGNvbG9yOiAgIzExMWQ1ZTtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xuICAgICAgYm9yZGVyOiA1cHggc29saWQgIzExMWQ1ZTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICB3aWR0aDogMzAwcHg7XG4gICAgfVxuICAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xuICAgICAgb3BhY2l0eTogMTtcbiAgICAgIHdpZHRoOiAzMDBweDtcbiAgICB9XG4gICAgLmNhcnJlIHtcbiAgICAgIHdpZHRoOiAyMDBweDtcbiAgICAgIGhlaWdodDogOTBweDtcbiAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgICBtYXJnaW4tdG9wOiAxMjVweDtcbiAgICB9XG5cbiAgICAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYWN0dWFsaXRlKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAgICAgLmFjdHUgLnJvd3tcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5mZXVpbHtcbiAgICAgICAgICAgIGZsZXg6IDAgMCBhdXRvO1xuICAgICAgICAgICAgd2lkdGg6IDcwJTtcbiAgICAgICAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipwcm9wb3MqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgICAucHJvcG9zb25ze1xuICAgICAgICAgIGhlaWdodDogODAwcHg7XG4gICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoI2ZmZmZmZiwgI2U1ZTZlNyk7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgfVxuICAgICAgICAuU291c0IgaXtcbiAgICAgICAgICBtYXJnaW4tbGVmdDogNzdweDtcbiAgICAgIHotaW5kZXg6IGF1dG87XG4gICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICBtYXJnaW4tdG9wOiA3MHB4O1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB9XG4gICAgICBcbiAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuICAgIFxuICAgICAvKiBTbWFsbCBEZXZpY2VzLCBUYWJsZXRzKi9cbiAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogNzY4cHgpIGFuZCAobWF4LXdpZHRoIDogOTkycHgpICB7XG4gICAgXG4gICAgICAgICAgICAgXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4O1xuICAgIH1cbiAgICAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xuICAgICAgZm9udC1zaXplOiA0MHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICAgIGNvbG9yOiAgIzExMWQ1ZTtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5kZXNjX2hlYWRpbmcge1xuICAgICAgZm9udC1zaXplOiAzMHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICAgIGNvbG9yOiAgIzExMWQ1ZTtcbiAgICB9XG4gICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xuICAgICAgYm9yZGVyOiA1cHggc29saWQgIzExMWQ1ZTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICB3aWR0aDogMzAwcHg7XG4gICAgfVxuICAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xuICAgICAgb3BhY2l0eTogMTtcbiAgICAgIHdpZHRoOiAzMDBweDtcbiAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipwcm9wb3MqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgICAucHJvcG9zb25ze1xuICAgICAgICAgIGhlaWdodDogODAwcHg7XG4gICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoI2ZmZmZmZiwgI2U1ZTZlNyk7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgfVxuICAgICAgICAuU291c0IgaXtcbiAgICAgICAgICBtYXJnaW4tbGVmdDogNzdweDtcbiAgICAgIHotaW5kZXg6IGF1dG87XG4gICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICBtYXJnaW4tdG9wOiA3MHB4O1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB9XG4gICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYWN0dWFsaXRlKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAgIC5hY3R1IC5yb3d7XG4gICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLmZldWlse1xuICAgICAgICAgIGZsZXg6IDAgMCBhdXRvO1xuICAgICAgICAgIHdpZHRoOiA3MCU7XG4gICAgICAgIH1cbiAgICAgfVxuICAgIFxuICAgIFxuICAgICAgXG4gICAgIC8qIE1lZGl1bSBEZXZpY2VzLCBEZXNrdG9wcyovXG4gICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDk5MnB4KSBhbmQgKG1heC13aWR0aCA6IDEyMDBweCkgIHtcbiAgICAgICAgXG4gICAgICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcHJvcG9zKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgXG4gICAgICAgICAgLlNvdXNCe1xuICAgICAgICAgICAgYm9yZGVyOiAzcHggc29saWQgcmdiKDQxLCAxOTEsIDIxMSk7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAxNjBweDtcbiAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgICAgICAgICB3aWR0aDogMjUwcHg7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5Tb3VzQiBpe1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDIyNXB4O1xuICAgICAgICB6LWluZGV4OiBhdXRvO1xuICAgICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IC01MHB4O1xuICAgICAgICAgIH1cbiAgICAgICAgICBcbiAgICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmFjdHVhbGl0ZSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgICAgIC5hY3R1IC5yb3d7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgIH1cbiAgICAgICAgICAuZmV1aWx7XG4gICAgICAgICAgICBmbGV4OiAwIDAgYXV0bztcbiAgICAgICAgICAgIHdpZHRoOiA3MCU7XG4gICAgICAgICAgfVxuICAgICAgICB9Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CollectiviteComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-collectivite',
                templateUrl: './collectivite.component.html',
                styleUrls: ['./collectivite.component.css']
            }]
    }], function () { return [{ type: src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/connexion/connexion.component.ts":
/*!******************************************************!*\
  !*** ./src/app/cpn/connexion/connexion.component.ts ***!
  \******************************************************/
/*! exports provided: ConnexionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnexionComponent", function() { return ConnexionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");








class ConnexionComponent {
    constructor(fb, auth, tokenStorage) {
        this.fb = fb;
        this.auth = auth;
        this.tokenStorage = tokenStorage;
        this.LoginForm = this.fb.group({
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
        });
    }
    ngOnInit() { }
    /************************login *************************/
    onSubmit() {
        const formData = new FormData();
        formData.append('email', this.LoginForm.get('email').value);
        formData.append('password', this.LoginForm.get('password').value);
        this.auth.login(formData).subscribe((res) => {
            if (res.message) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: res.message + ' !',
                });
            }
            if (!res.error) {
                this.tokenStorage.saveToken(res.data.token);
                this.tokenStorage.saveUser(res.data.user);
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    icon: 'success',
                    title: 'connecter reussie',
                    showConfirmButton: false,
                    timer: 6000,
                });
                if (res.data.role === 'entreprise') {
                    location.href = '/cpn/Home_tpe_pme';
                }
                else if (res.data.role === 'agence') {
                    location.href = '/cpn/agence';
                }
                else {
                    location.href = 'test';
                }
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: res.message + ' !',
                });
            }
        }, (error) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'error 500 !',
            });
        });
    }
}
ConnexionComponent.ɵfac = function ConnexionComponent_Factory(t) { return new (t || ConnexionComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"])); };
ConnexionComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ConnexionComponent, selectors: [["app-connexion"]], decls: 51, vars: 2, consts: [[1, "login_container"], [1, "login_wrapper"], [1, "login_content"], [1, "row", "g-0", "py-2"], [1, "col-md-6"], [1, "container"], [1, "row", "row-cols-3", "g-2"], [1, "col"], ["src", "assets/cpnimages/connexion/3.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/2.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/1.png", "alt", "", 2, "width", "100%", "border-radius", "0 3.5rem 0 0", "height", "100%"], ["src", "assets/cpnimages/connexion/4.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/5.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/6.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/7.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/8.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/9.png", "alt", "", 2, "border-radius", "0 0 3.5rem 0", "width", "100%", "height", "100%"], [1, "col-md-6", 2, "display", "flex", "flex-direction", "row", "justify-content", "center"], [1, "card-body", "p-4", "p-sm-5"], [1, "r\u00E9seau-sociaux"], [1, "text-center"], ["href", ""], ["src", "assets/cpnimages/connexion/gmail.png", "alt", "", 2, "width", "10%", "margin-left", "10px"], [1, "row", "justify-content-center"], [1, "m-0", "text-center", "or_styling"], [3, "formGroup", "ngSubmit"], [1, "col-md-12", "pb-2"], ["type", "text", "formControlName", "email", "id", "floatingInput", "placeholder", "Email", 1, "form-control"], ["type", "password", "formControlName", "password", "id", "floatingInput", "placeholder", "Mot de passe", 1, "form-control"], [1, "container", "overflow-hidden"], [1, "row"], [2, "text-align", "center", "color", "black"], [1, "pr-4", 3, "routerLink"], ["type", "submit", 1, "btn", "btn-danger", 2, "background-color", "red"]], template: function ConnexionComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h3", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Connectez-Vous Avec");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h5", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Ou Bien");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "form", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ConnexionComponent_Template_form_ngSubmit_39_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "input", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "input", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Mot de passe oublier ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Connexion");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.LoginForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/mail");
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkWithHref"]], styles: [".login_container[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n}\n\n.login_container[_ngcontent-%COMP%]   .login_content[_ngcontent-%COMP%]   .login_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n    margin-top:0;\n    margin-right: 0;\n    margin-left: 0;\n}\n\n.container[_ngcontent-%COMP%]{\n    margin: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vY29ubmV4aW9uL2Nvbm5leGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kseUJBQXlCO0FBQzdCOztBQUVBO0lBQ0ksWUFBWTtJQUNaLGVBQWU7SUFDZixjQUFjO0FBQ2xCOztBQUNBO0lBQ0ksWUFBWTtBQUNoQiIsImZpbGUiOiJhcHAvY3BuL2Nvbm5leGlvbi9jb25uZXhpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dpbl9jb250YWluZXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0VCRUNGMDtcbn1cblxuLmxvZ2luX2NvbnRhaW5lciAubG9naW5fY29udGVudCAubG9naW5fd3JhcHBlciAucm93e1xuICAgIG1hcmdpbi10b3A6MDtcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XG4gICAgbWFyZ2luLWxlZnQ6IDA7XG59XG4uY29udGFpbmVye1xuICAgIG1hcmdpbjogMzBweDtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ConnexionComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-connexion',
                templateUrl: './connexion.component.html',
                styleUrls: ['./connexion.component.css'],
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }, { type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/contact/contact.component.ts":
/*!**************************************************!*\
  !*** ./src/app/cpn/contact/contact.component.ts ***!
  \**************************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../side-bar/side-bar.component */ "./src/app/cpn/side-bar/side-bar.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");




class ContactComponent {
    constructor() { }
    ngOnInit() {
    }
}
ContactComponent.ɵfac = function ContactComponent_Factory(t) { return new (t || ContactComponent)(); };
ContactComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ContactComponent, selectors: [["app-contact"]], decls: 47, vars: 0, consts: [[1, "contact_container"], [1, "contact_wrapper"], [1, "goolge_map"], [2, "padding", "10px 0 0 0"], ["src", "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2618.488730301625!2d2.4286297150925718!3d48.982256099932144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66b3cb769345d%3A0x13d4251fce224e53!2sOff%20Agency!5e0!3m2!1sfr!2stn!4v1628594788116!5m2!1sfr!2stn", "width", "1500", "height", "600", "allowfullscreen", "", "loading", "lazy", 2, "border", "0"], [1, "section_contact"], [1, "container", "pt-4", "px-4", "g-0"], [2, "margin", "0", "font-weight", "800"], [1, "row", "mt-5", "g-5"], [1, "col-md-6"], ["action", "", "method", "post", 1, "row", "g-4", 2, "margin-bottom", "43px"], ["type", "text", "placeholder", "Nom", "name", "nom", 1, "form-control"], ["placeholder", "Pr\u00E9nom", "name", "prenom", 1, "form-control"], [1, "col-md-12"], ["placeholder", "Email", "name", "email", 1, "form-control"], ["placeholder", "Phone", "name", "phone", 1, "form-control"], ["placeholder", "Votre message", "name", "message", 1, "form-control"], ["id", "check-box", "type", "checkbox", "name", "accept", 1, ""], ["for", "check-box", 2, "color", "darkgray"], [1, "col-md-12", "d-flex", "justify-content-end"], ["type", "submit", 2, "border", "none", "background", "red", "border-radius", "25px", "color", "white", "padding", "5px 15px"], [1, "items", 2, "margin", "0", "padding", "0", "display", "flex", "flex-direction", "column", "justify-content", "center", "align-items", "center"], [1, "item_list", 2, "list-style", "none", "width", "300px"], [2, "margin", "0", "color", "#111D5E", "font-weight", "700"], [2, "color", "#111D5E"], ["href", "mailto:s.smida@jobid.fr", 2, "color", "#111D5E", "width", "max-content", "text-decoration", "none"]], template: function ContactComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-side-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "section", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "iframe", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "section", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Contactez-nous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "form", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "input", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "textarea", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "label", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " J'ai lu et j'accepte la politique de confidentialit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Envoyez");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "ul", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "li", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Notre adresse");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "31 Rue de Professeur Esclangon");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "li", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Notre num\u00E9ro");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "+33 6 73 46 65 64");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "li", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Notre email");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "a", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Votreconseiller@cpn-aide-aux-entreprise.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_1__["SideBarComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"]], styles: [".primary_body[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .form-control[_ngcontent-%COMP%]\n{\n    width: 100%;\n\n}\n.contact_container[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUksV0FBVzs7QUFFZjtBQUNBO0lBQ0kseUJBQXlCO0FBQzdCIiwiZmlsZSI6ImFwcC9jcG4vY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucHJpbWFyeV9ib2R5IC5yb3cgLmZvcm0tY29udHJvbFxue1xuICAgIHdpZHRoOiAxMDAlO1xuXG59XG4uY29udGFjdF9jb250YWluZXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0VCRUNGMDtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-contact',
                templateUrl: './contact.component.html',
                styleUrls: ['./contact.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/cpn-routing.module.ts":
/*!*******************************************!*\
  !*** ./src/app/cpn/cpn-routing.module.ts ***!
  \*******************************************/
/*! exports provided: CpnRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpnRoutingModule", function() { return CpnRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _connexion_connexion_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./connexion/connexion.component */ "./src/app/cpn/connexion/connexion.component.ts");
/* harmony import */ var _cpn_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cpn.component */ "./src/app/cpn/cpn.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/cpn/contact/contact.component.ts");
/* harmony import */ var _subvention_subvention_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./subvention/subvention.component */ "./src/app/cpn/subvention/subvention.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/cpn/profile/profile.component.ts");
/* harmony import */ var _inscription_inscription_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./inscription/inscription.component */ "./src/app/cpn/inscription/inscription.component.ts");
/* harmony import */ var _calandar_calandar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./calandar/calandar.component */ "./src/app/cpn/calandar/calandar.component.ts");
/* harmony import */ var _agenda_agenda_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./agenda/agenda.component */ "./src/app/cpn/agenda/agenda.component.ts");
/* harmony import */ var _collectivite_collectivite_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./collectivite/collectivite.component */ "./src/app/cpn/collectivite/collectivite.component.ts");
/* harmony import */ var _actualite_actualite_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./actualite/actualite.component */ "./src/app/cpn/actualite/actualite.component.ts");
/* harmony import */ var _regions_nouvelleaquantine_nouvelleaquantine_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./regions/nouvelleaquantine/nouvelleaquantine.component */ "./src/app/cpn/regions/nouvelleaquantine/nouvelleaquantine.component.ts");
/* harmony import */ var _regions_collectiviteterritorialemartinique_collectiviteterritorialemartinique_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./regions/collectiviteterritorialemartinique/collectiviteterritorialemartinique.component */ "./src/app/cpn/regions/collectiviteterritorialemartinique/collectiviteterritorialemartinique.component.ts");
/* harmony import */ var _regions_guadeloupe_guadeloupe_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./regions/guadeloupe/guadeloupe.component */ "./src/app/cpn/regions/guadeloupe/guadeloupe.component.ts");
/* harmony import */ var _regions_corse_corse_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./regions/corse/corse.component */ "./src/app/cpn/regions/corse/corse.component.ts");
/* harmony import */ var _regions_iledefrance_iledefrance_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./regions/iledefrance/iledefrance.component */ "./src/app/cpn/regions/iledefrance/iledefrance.component.ts");
/* harmony import */ var _agence_agence_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./agence/agence.component */ "./src/app/cpn/agence/agence.component.ts");
/* harmony import */ var src_app_security_auth_guard__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! src/app/security/auth.guard */ "./src/app/security/auth.guard.ts");
/* harmony import */ var _hometpepme_hometpepme_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./hometpepme/hometpepme.component */ "./src/app/cpn/hometpepme/hometpepme.component.ts");
/* harmony import */ var _regions_normandie_normandie_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./regions/normandie/normandie.component */ "./src/app/cpn/regions/normandie/normandie.component.ts");
/* harmony import */ var _regions_grandest_grandest_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./regions/grandest/grandest.component */ "./src/app/cpn/regions/grandest/grandest.component.ts");
/* harmony import */ var _regions_auvergne_auvergne_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./regions/auvergne/auvergne.component */ "./src/app/cpn/regions/auvergne/auvergne.component.ts");
/* harmony import */ var _regions_hautedefrance_hautedefrance_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./regions/hautedefrance/hautedefrance.component */ "./src/app/cpn/regions/hautedefrance/hautedefrance.component.ts");
/* harmony import */ var _regions_payedeloire_payedeloire_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./regions/payedeloire/payedeloire.component */ "./src/app/cpn/regions/payedeloire/payedeloire.component.ts");
/* harmony import */ var _pass_oublier_pass_oublier_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./pass-oublier/pass-oublier.component */ "./src/app/cpn/pass-oublier/pass-oublier.component.ts");
/* harmony import */ var _change_pass_change_pass_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./change-pass/change-pass.component */ "./src/app/cpn/change-pass/change-pass.component.ts");





























const routes = [{ path: '', component: _cpn_component__WEBPACK_IMPORTED_MODULE_3__["CpnComponent"],
        children: [
            { path: 'Connexion', component: _connexion_connexion_component__WEBPACK_IMPORTED_MODULE_2__["ConnexionComponent"] },
            { path: 'Contact', component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__["ContactComponent"] },
            { path: 'Subvention', component: _subvention_subvention_component__WEBPACK_IMPORTED_MODULE_5__["SubventionComponent"] },
            { path: 'Inscription', component: _inscription_inscription_component__WEBPACK_IMPORTED_MODULE_7__["InscriptionComponent"] },
            { path: 'Home_tpe_pme', component: _hometpepme_hometpepme_component__WEBPACK_IMPORTED_MODULE_19__["HometpepmeComponent"] },
            { path: 'Home_collectivite', component: _collectivite_collectivite_component__WEBPACK_IMPORTED_MODULE_10__["CollectiviteComponent"] },
            { path: 'Actualite', component: _actualite_actualite_component__WEBPACK_IMPORTED_MODULE_11__["ActualiteComponent"] },
            { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_6__["ProfileComponent"] },
            { path: 'agenda', component: _agenda_agenda_component__WEBPACK_IMPORTED_MODULE_9__["AgendaComponent"] },
            { path: 'calendar', component: _calandar_calandar_component__WEBPACK_IMPORTED_MODULE_8__["CalandarComponent"], canActivate: [src_app_security_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"]] },
            { path: 'agence', component: _agence_agence_component__WEBPACK_IMPORTED_MODULE_17__["AgenceComponent"] },
            { path: 'mail', component: _pass_oublier_pass_oublier_component__WEBPACK_IMPORTED_MODULE_25__["PassOublierComponent"] },
            { path: 'reset-password/:token', component: _change_pass_change_pass_component__WEBPACK_IMPORTED_MODULE_26__["ChangePassComponent"] },
            { path: 'region/nouvelle_aquantine', component: _regions_nouvelleaquantine_nouvelleaquantine_component__WEBPACK_IMPORTED_MODULE_12__["NouvelleaquantineComponent"] },
            { path: 'region/Collectivite_territoriale_de_martinique', component: _regions_collectiviteterritorialemartinique_collectiviteterritorialemartinique_component__WEBPACK_IMPORTED_MODULE_13__["CollectiviteterritorialemartiniqueComponent"] },
            { path: 'region/Guadeloupe', component: _regions_guadeloupe_guadeloupe_component__WEBPACK_IMPORTED_MODULE_14__["GuadeloupeComponent"] },
            { path: 'region/Corse', component: _regions_corse_corse_component__WEBPACK_IMPORTED_MODULE_15__["CorseComponent"] },
            { path: 'region/Ile_de_france', component: _regions_iledefrance_iledefrance_component__WEBPACK_IMPORTED_MODULE_16__["IledefranceComponent"] },
            { path: 'region/Normandie', component: _regions_normandie_normandie_component__WEBPACK_IMPORTED_MODULE_20__["NormandieComponent"] },
            { path: 'region/Grand_est', component: _regions_grandest_grandest_component__WEBPACK_IMPORTED_MODULE_21__["GrandestComponent"] },
            { path: 'region/auvergne', component: _regions_auvergne_auvergne_component__WEBPACK_IMPORTED_MODULE_22__["AuvergneComponent"] },
            { path: 'region/haute_de_france', component: _regions_hautedefrance_hautedefrance_component__WEBPACK_IMPORTED_MODULE_23__["HautedefranceComponent"] },
            { path: 'region/paye_de_loire', component: _regions_payedeloire_payedeloire_component__WEBPACK_IMPORTED_MODULE_24__["PayedeloireComponent"] },
        ]
    }];
class CpnRoutingModule {
}
CpnRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: CpnRoutingModule });
CpnRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function CpnRoutingModule_Factory(t) { return new (t || CpnRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](CpnRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CpnRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/cpn/cpn.component.ts":
/*!**************************************!*\
  !*** ./src/app/cpn/cpn.component.ts ***!
  \**************************************/
/*! exports provided: CpnComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpnComponent", function() { return CpnComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _navbar_component_navbar_component_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navbar-component/navbar-component.component */ "./src/app/cpn/navbar-component/navbar-component.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _footer_component_footer_component_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./footer-component/footer-component.component */ "./src/app/cpn/footer-component/footer-component.component.ts");





class CpnComponent {
    constructor() { }
    ngOnInit() {
    }
}
CpnComponent.ɵfac = function CpnComponent_Factory(t) { return new (t || CpnComponent)(); };
CpnComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CpnComponent, selectors: [["app-cpn"]], decls: 4, vars: 0, consts: [[1, "main-content"]], template: function CpnComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-navbar-component");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-footer-component");
    } }, directives: [_navbar_component_navbar_component_component__WEBPACK_IMPORTED_MODULE_1__["NavbarComponentComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"], _footer_component_footer_component_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponentComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvY3BuL2Nwbi5jb21wb25lbnQuY3NzIn0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CpnComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-cpn',
                templateUrl: './cpn.component.html',
                styleUrls: ['./cpn.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/cpn.module.ts":
/*!***********************************!*\
  !*** ./src/app/cpn/cpn.module.ts ***!
  \***********************************/
/*! exports provided: CpnModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpnModule", function() { return CpnModule; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _cpn_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cpn-routing.module */ "./src/app/cpn/cpn-routing.module.ts");
/* harmony import */ var _cpn_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./cpn.component */ "./src/app/cpn/cpn.component.ts");
/* harmony import */ var _navbar_component_navbar_component_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./navbar-component/navbar-component.component */ "./src/app/cpn/navbar-component/navbar-component.component.ts");
/* harmony import */ var _footer_component_footer_component_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./footer-component/footer-component.component */ "./src/app/cpn/footer-component/footer-component.component.ts");
/* harmony import */ var _connexion_connexion_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./connexion/connexion.component */ "./src/app/cpn/connexion/connexion.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/cpn/contact/contact.component.ts");
/* harmony import */ var _subvention_subvention_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./subvention/subvention.component */ "./src/app/cpn/subvention/subvention.component.ts");
/* harmony import */ var _inscription_inscription_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./inscription/inscription.component */ "./src/app/cpn/inscription/inscription.component.ts");
/* harmony import */ var _collectivite_collectivite_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./collectivite/collectivite.component */ "./src/app/cpn/collectivite/collectivite.component.ts");
/* harmony import */ var _actualite_actualite_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./actualite/actualite.component */ "./src/app/cpn/actualite/actualite.component.ts");
/* harmony import */ var _agence_agence_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./agence/agence.component */ "./src/app/cpn/agence/agence.component.ts");
/* harmony import */ var _hometpepme_hometpepme_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./hometpepme/hometpepme.component */ "./src/app/cpn/hometpepme/hometpepme.component.ts");
/* harmony import */ var _regions_nouvelleaquantine_nouvelleaquantine_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./regions/nouvelleaquantine/nouvelleaquantine.component */ "./src/app/cpn/regions/nouvelleaquantine/nouvelleaquantine.component.ts");
/* harmony import */ var _regions_collectiviteterritorialemartinique_collectiviteterritorialemartinique_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./regions/collectiviteterritorialemartinique/collectiviteterritorialemartinique.component */ "./src/app/cpn/regions/collectiviteterritorialemartinique/collectiviteterritorialemartinique.component.ts");
/* harmony import */ var _agenda_agenda_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./agenda/agenda.component */ "./src/app/cpn/agenda/agenda.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../material-module */ "./src/app/material-module.ts");
/* harmony import */ var _calandar_calandar_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./calandar/calandar.component */ "./src/app/cpn/calandar/calandar.component.ts");
/* harmony import */ var _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./map-french-region/map-french-region.component */ "./src/app/cpn/map-french-region/map-french-region.component.ts");
/* harmony import */ var _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./side-bar/side-bar.component */ "./src/app/cpn/side-bar/side-bar.component.ts");
/* harmony import */ var _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./notre-succes/notre-succes.component */ "./src/app/cpn/notre-succes/notre-succes.component.ts");
/* harmony import */ var _pass_oublier_pass_oublier_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./pass-oublier/pass-oublier.component */ "./src/app/cpn/pass-oublier/pass-oublier.component.ts");
/* harmony import */ var _change_pass_change_pass_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./change-pass/change-pass.component */ "./src/app/cpn/change-pass/change-pass.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/cpn/profile/profile.component.ts");
/* harmony import */ var ng_lazyload_image__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ng-lazyload-image */ "./node_modules/ng-lazyload-image/__ivy_ngcc__/fesm2015/ng-lazyload-image.js");
/* harmony import */ var ngx_dropzone__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ngx-dropzone */ "./node_modules/ngx-dropzone/__ivy_ngcc__/fesm2015/ngx-dropzone.js");



























 // <-- import it


class CpnModule {
}
CpnModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: CpnModule });
CpnModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function CpnModule_Factory(t) { return new (t || CpnModule)(); }, providers: [{ provide: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_27__["LAZYLOAD_IMAGE_HOOKS"], useClass: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_27__["ScrollHooks"] }], imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _cpn_routing_module__WEBPACK_IMPORTED_MODULE_3__["CpnRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_18__["ReactiveFormsModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_19__["MaterialModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClientModule"],
            ng_lazyload_image__WEBPACK_IMPORTED_MODULE_27__["LazyLoadImageModule"],
            ngx_dropzone__WEBPACK_IMPORTED_MODULE_28__["NgxDropzoneModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](CpnModule, { declarations: [_cpn_component__WEBPACK_IMPORTED_MODULE_4__["CpnComponent"],
        _navbar_component_navbar_component_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponentComponent"],
        _footer_component_footer_component_component__WEBPACK_IMPORTED_MODULE_6__["FooterComponentComponent"],
        _connexion_connexion_component__WEBPACK_IMPORTED_MODULE_7__["ConnexionComponent"],
        _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__["ContactComponent"],
        _subvention_subvention_component__WEBPACK_IMPORTED_MODULE_9__["SubventionComponent"],
        _inscription_inscription_component__WEBPACK_IMPORTED_MODULE_10__["InscriptionComponent"],
        _collectivite_collectivite_component__WEBPACK_IMPORTED_MODULE_11__["CollectiviteComponent"],
        _actualite_actualite_component__WEBPACK_IMPORTED_MODULE_12__["ActualiteComponent"],
        _regions_nouvelleaquantine_nouvelleaquantine_component__WEBPACK_IMPORTED_MODULE_15__["NouvelleaquantineComponent"],
        _regions_collectiviteterritorialemartinique_collectiviteterritorialemartinique_component__WEBPACK_IMPORTED_MODULE_16__["CollectiviteterritorialemartiniqueComponent"],
        _agenda_agenda_component__WEBPACK_IMPORTED_MODULE_17__["AgendaComponent"],
        _calandar_calandar_component__WEBPACK_IMPORTED_MODULE_20__["CalandarComponent"],
        _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_21__["MapFrenchRegionComponent"],
        _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_22__["SideBarComponent"],
        _hometpepme_hometpepme_component__WEBPACK_IMPORTED_MODULE_14__["HometpepmeComponent"],
        _agence_agence_component__WEBPACK_IMPORTED_MODULE_13__["AgenceComponent"],
        _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_23__["NotreSuccesComponent"],
        _pass_oublier_pass_oublier_component__WEBPACK_IMPORTED_MODULE_24__["PassOublierComponent"],
        _change_pass_change_pass_component__WEBPACK_IMPORTED_MODULE_25__["ChangePassComponent"],
        _profile_profile_component__WEBPACK_IMPORTED_MODULE_26__["ProfileComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
        _cpn_routing_module__WEBPACK_IMPORTED_MODULE_3__["CpnRoutingModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_18__["ReactiveFormsModule"],
        _material_module__WEBPACK_IMPORTED_MODULE_19__["MaterialModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClientModule"],
        ng_lazyload_image__WEBPACK_IMPORTED_MODULE_27__["LazyLoadImageModule"],
        ngx_dropzone__WEBPACK_IMPORTED_MODULE_28__["NgxDropzoneModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](CpnModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _cpn_component__WEBPACK_IMPORTED_MODULE_4__["CpnComponent"],
                    _navbar_component_navbar_component_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponentComponent"],
                    _footer_component_footer_component_component__WEBPACK_IMPORTED_MODULE_6__["FooterComponentComponent"],
                    _connexion_connexion_component__WEBPACK_IMPORTED_MODULE_7__["ConnexionComponent"],
                    _contact_contact_component__WEBPACK_IMPORTED_MODULE_8__["ContactComponent"],
                    _subvention_subvention_component__WEBPACK_IMPORTED_MODULE_9__["SubventionComponent"],
                    _inscription_inscription_component__WEBPACK_IMPORTED_MODULE_10__["InscriptionComponent"],
                    _collectivite_collectivite_component__WEBPACK_IMPORTED_MODULE_11__["CollectiviteComponent"],
                    _actualite_actualite_component__WEBPACK_IMPORTED_MODULE_12__["ActualiteComponent"],
                    _regions_nouvelleaquantine_nouvelleaquantine_component__WEBPACK_IMPORTED_MODULE_15__["NouvelleaquantineComponent"],
                    _regions_collectiviteterritorialemartinique_collectiviteterritorialemartinique_component__WEBPACK_IMPORTED_MODULE_16__["CollectiviteterritorialemartiniqueComponent"],
                    _agenda_agenda_component__WEBPACK_IMPORTED_MODULE_17__["AgendaComponent"],
                    _calandar_calandar_component__WEBPACK_IMPORTED_MODULE_20__["CalandarComponent"],
                    _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_21__["MapFrenchRegionComponent"],
                    _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_22__["SideBarComponent"],
                    _hometpepme_hometpepme_component__WEBPACK_IMPORTED_MODULE_14__["HometpepmeComponent"],
                    _agence_agence_component__WEBPACK_IMPORTED_MODULE_13__["AgenceComponent"],
                    _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_23__["NotreSuccesComponent"],
                    _pass_oublier_pass_oublier_component__WEBPACK_IMPORTED_MODULE_24__["PassOublierComponent"],
                    _change_pass_change_pass_component__WEBPACK_IMPORTED_MODULE_25__["ChangePassComponent"],
                    _profile_profile_component__WEBPACK_IMPORTED_MODULE_26__["ProfileComponent"]
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                    _cpn_routing_module__WEBPACK_IMPORTED_MODULE_3__["CpnRoutingModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_18__["ReactiveFormsModule"],
                    _material_module__WEBPACK_IMPORTED_MODULE_19__["MaterialModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClientModule"],
                    ng_lazyload_image__WEBPACK_IMPORTED_MODULE_27__["LazyLoadImageModule"],
                    ngx_dropzone__WEBPACK_IMPORTED_MODULE_28__["NgxDropzoneModule"]
                ],
                providers: [{ provide: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_27__["LAZYLOAD_IMAGE_HOOKS"], useClass: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_27__["ScrollHooks"] }],
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/cpn/footer-component/footer-component.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/cpn/footer-component/footer-component.component.ts ***!
  \********************************************************************/
/*! exports provided: FooterComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponentComponent", function() { return FooterComponentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class FooterComponentComponent {
    constructor() { }
    ngOnInit() {
    }
}
FooterComponentComponent.ɵfac = function FooterComponentComponent_Factory(t) { return new (t || FooterComponentComponent)(); };
FooterComponentComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FooterComponentComponent, selectors: [["app-footer-component"]], decls: 68, vars: 1, consts: [[1, "text-lg-start", "text-muted"], [1, "d-flex", "justify-content-center", "justify-content-lg-between"], [1, ""], [1, "container", "text-md-start", "mt-5", 2, "font-size", "12px"], [1, "row", "mt-3"], [1, "col-md-3", "col-lg-4", "col-xl-3", "mx-auto"], [1, "text-uppercase", "fw-bold", "mb-4"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "logo", "width", "50px", "height", "50px"], [2, "color", "white", "font-size", "12px"], [2, "font-size", "25px"], ["href", "https://www.instagram.com/cpn_aideauxentreprises/?hl=fr"], ["aria-hidden", "true", 1, "fab", "fa-instagram"], ["href", "https://www.youtube.com/channel/UC2KAUP-XzalYUGGPLEXBUBQ"], ["aria-hidden", "true", 1, "fab", "fa-youtube", 2, "margin-left", "5px"], ["href", "https://twitter.com/cpn_officiel"], ["aria-hidden", "true", 1, "fab", "fa-twitter", 2, "margin-left", "5px"], ["href", "https://www.linkedin.com/company/76078573/admin/"], ["aria-hidden", "true", 1, "fab", "fa-linkedin", 2, "margin-left", "5px"], ["href", "https://www.facebook.com/CPN.aideauxentreprises"], ["aria-hidden", "true", 1, "fab", "fa-facebook", 2, "margin-left", "5px"], [1, "col-md-2", "col-lg-2", "col-xl-2", "mx-auto"], [1, "text-uppercase", "fw-bold", "mb-4", 2, "color", "white"], ["href", "#!", 1, "text-reset", 2, "color", "white", "text-decoration", "none"], [1, "col-md-3", "col-lg-2", "col-xl-2", "mx-auto"], ["href", "#!", 1, "text-reset", "text-left", 2, "color", "white", "text-decoration", "none"], [1, "text-reset", 2, "color", "white", "text-decoration", "none", 3, "routerLink"], [1, "col-md-4", "col-lg-3", "col-xl-3", "mx-auto", "mb-md-0"], [2, "color", "white"], [1, "fas", "fa-phone", "me-3"], [2, "color", "white", "width", "max-content"], [1, "fas", "fa-envelope", "me-3"], ["href", "mailto:s.smida@jobid.fr", 2, "color", "#fff", "width", "max-content", "text-decoration", "none"], [1, "text-center", 2, "background-color", "#0c133a", "color", "#fff", "font-size", "13px"], ["href", "https://jobid.fr/", 1, "text-reset", "fw-bold", 2, "color", "white"]], template: function FooterComponentComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "footer", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "section", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h6", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Le Cabinet de Propulsion Num\u00E9rique aide les entreprises \u00E0 se propulser num\u00E9riquement et \u00E0 b\u00E9n\u00E9ficier de financement. CPN est un organisme de financement \u00E0 but non lucratif. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "i", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "i", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "h6", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Menu ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Acceuil");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Agenda");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "A propos");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Contactez-nous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "h6", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, " Support ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "a", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Inscription");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "a", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "FAQ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "a", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "CGU");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "a", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "CGV");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "h6", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, " Contact ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "i", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "+33 0184142394");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "i", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "a", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Votreconseiller@cpn-aide-aux-entreprise.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, " \u00A9 2021 Copyright:Tous droits r\u00E9serv\u00E9s ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "a", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Jobid.fr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "Contact");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], styles: ["footer[_ngcontent-%COMP%]{\n    background: #111D5E !important;\n    color: white !important;\n   \n}\na[_ngcontent-%COMP%]{\n    color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vZm9vdGVyLWNvbXBvbmVudC9mb290ZXItY29tcG9uZW50LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0lBQ0ksOEJBQThCO0lBQzlCLHVCQUF1Qjs7QUFFM0I7QUFDQTtJQUNJLFlBQVk7QUFDaEIiLCJmaWxlIjoiYXBwL2Nwbi9mb290ZXItY29tcG9uZW50L2Zvb3Rlci1jb21wb25lbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuZm9vdGVye1xuICAgIGJhY2tncm91bmQ6ICMxMTFENUUgIWltcG9ydGFudDtcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgIFxufVxuYXtcbiAgICBjb2xvcjogd2hpdGU7XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FooterComponentComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-footer-component',
                templateUrl: './footer-component.component.html',
                styleUrls: ['./footer-component.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/hometpepme/hometpepme.component.ts":
/*!********************************************************!*\
  !*** ./src/app/cpn/hometpepme/hometpepme.component.ts ***!
  \********************************************************/
/*! exports provided: HometpepmeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HometpepmeComponent", function() { return HometpepmeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var ng_lazyload_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng-lazyload-image */ "./node_modules/ng-lazyload-image/__ivy_ngcc__/fesm2015/ng-lazyload-image.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../notre-succes/notre-succes.component */ "./src/app/cpn/notre-succes/notre-succes.component.ts");
/* harmony import */ var _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../side-bar/side-bar.component */ "./src/app/cpn/side-bar/side-bar.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");








class HometpepmeComponent {
    constructor(auth) {
        this.auth = auth;
    }
    ngOnInit() {
        this.auth.getFellower().subscribe(res => {
            this.follow = res;
        });
        $('.search').mouseenter(function () {
            $(this).addClass('search--show');
            $(this).removeClass('search--hide');
        });
        $('.search').mouseleave(function () {
            $(this).addClass('search--hide');
            $(this).removeClass('search--show');
        });
    }
}
HometpepmeComponent.ɵfac = function HometpepmeComponent_Factory(t) { return new (t || HometpepmeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"])); };
HometpepmeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HometpepmeComponent, selectors: [["app-hometpepme"]], decls: 168, vars: 3, consts: [[1, "primary_body", "mb-5"], [1, "home_container"], [1, "section_heading", "mb-3"], [1, "heading_wrapper", "container-fluid", "g-0"], [1, "row", "g-0"], [1, "col-md-6"], [1, "row", "g-0", "justify-content-center"], [1, "col-auto"], [1, "img_wrapper"], ["alt", "", 1, "heading_img", 2, "margin-top", "-76px", "margin-left", "-8px", 3, "defaultImage", "lazyLoad"], [1, "col-md-6", "p-3"], [1, "row", "g-0", "justify-content-start"], [1, "col-md-auto"], [1, "title_heading"], [1, "desc_heading"], [1, "search_bloc"], [1, "search"], ["placeholder", "Quel type de subvention souhaitez vous", 1, "search__input"], [1, "desc_heading", 2, "margin-top", "100px"], [1, "carre"], [1, "row"], [1, "col", 2, "display", "initial", "flex-direction", "row", "padding", "8px"], ["src", "assets/cpnimages/home/I.png", "alt", "", 2, "width", "10%", "margin", "0 0 0 30px"], [2, "margin-left", "4px", "font-size", "13px"], [2, "text-align", "center", "margin", "0 90px 0 0"], [2, "text-align", "center", "font-size", "12px", "margin", "0 0px 0 -45px"], [2, "color", "#00FF00"], [1, "container", "text-bloc", "g-0", "mb-5"], [1, "row", "block1"], [1, "col-md-8"], [1, "container", "px-4", "block1_part1"], [1, "text_body"], [2, "color", "#111d5e"], [1, "underline"], ["width", "device-width"], [2, "font-size", "40px", "color", "#111d5e"], ["href", "#"], [1, "col-md-4", "block1_part2"], [1, "card", "cards"], [1, "card-body"], ["src", "assets/cpnimages/home/deal.png", "alt", "collaboration.png", 2, "margin-left", "25%"], [1, "card-subtitle", "mb-2", "text-muted", "text-center"], ["href", "#", 1, "card-link"], [1, "container", "px-4"], [1, "text_body", 2, "margin-left", "15px"], [2, "margin-top", "-75px", "margin-left", "-17px", "color", "#111d5e"], [1, "divider", "g-0", "mb-5"], [1, "divider_ligne"], [1, "container_box", "d-flex", "justify-content-center"], [1, "card-title", "num-card", 2, "font-size", "50px"], ["src", "assets/cpnimages/home/person.png", "alt", "card1.png", 2, "width", "50px", "margin-left", "35%"], [1, "card-text", "text-center"], ["src", "assets/cpnimages/home/circl.png", "alt", "card2.png", 2, "width", "60px", "margin-left", "35%"], ["src", "assets/cpnimages/home/chart.png", "alt", "card3.png", 2, "width", "60px", "margin-left", "35%"], ["src", "assets/cpnimages/home/moneyR.png", "alt", "card4.png", 2, "width", "60px", "margin-left", "35%"], [1, "btn_post", "d-flex", "justify-content-center"], ["routerLink", "/test", 1, "btntest"]], template: function HometpepmeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "h2", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "TPE - PME");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h4", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "vos subventions num\u00E9rique sans conditions ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "form", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "h4", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Nous vous donnons l\u2019acc\u00E8s au financement sans conditions.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "span", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Followers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h4", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "2.1%");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "vs last 7 days");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "app-notre-succes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "app-side-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "section", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "ul", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h1", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, " Pour quoi collaborer avec le ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "span", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, " CPN ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "strong", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, ".");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, " Le CPN est un acteur majeur dans la transition digital des entreprises nous subventionnons elle accompagne toutes les remboursements offerts par ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "a", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Les r\u00E9gions de France");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "p", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "strong", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, ".");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, " Le CPN est un acteur majeur dans la transition digital des entreprises nous subventionnons elle accompagne toutes les remboursements offerts par ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "a", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Les r\u00E9gions de France");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "img", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "h6", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Le CPN est un acteur majeur dans la transition digitale des entreprises... ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "a", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "section", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "ul", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, " Que ce que ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "span", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, " CPN ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "p", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "strong", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, ".");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, " Le CPN est un cabinet international actif sur 3 continents, en Afrique, en Am\u00E9rique du Nord et en Europe.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "p", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "strong", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, ".");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, " Le CPN soutient la digitalisation en vous donnant l\u2019acc\u00E8s \u00E0 un r\u00E9seau d\u2019entreprise de votre r\u00E9gion, ainsi que des subventions pour financer tout projet de d\u00E9veloppement informatique pour votre entreprise.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "p", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "strong", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, ".");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, " Les conseillers du CPN vous accompagnent aupr\u00E8s d\u2019agences de d\u00E9veloppement informatique et des agences de marketing d\u00E9sign\u00E9es et garanties, pour vous aider \u00E0 augmenter votre chiffre d\u2019affaire en ligne afin de simplifier et de moderniser votre entreprise.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "section", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](96, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "h1", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "1.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](102, "img", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "h6", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "Comment le CPN intervient-t-il aupr\u00E9s des entreprises.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](108, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "p", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, "Le CPN Favorise la transition digitale, nos subventions sont calcul\u00E9es par rapport... ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "a", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "En savoir plus ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "h5", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "2.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "img", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "h6", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "\u00C0 quel niveau le CPN intervient-t-il ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](124, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "p", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Le CPN intervient avec vous simplement et rapidement il vous fournit une analyse gratuite...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](127, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](128, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "a", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "h5", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "3.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](136, "img", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](137, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](138, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](139, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "h6", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "Quel type de projet le CPN subventionne-t-il ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](142, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "p", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "Apr\u00E8s l\u2019\u00E9tude de votre projet et le rendez-vous avec l\u2019un de nos Experts Num\u00E9riques...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](145, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](146, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "a", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "h5", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, "4.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](153, "img", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](154, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](155, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](156, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "h6", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](158, "Combien co\u00FBtent les services du CPN ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](159, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "p", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, "Les services du CPN sont totalement gratuits et sont enti\u00E8rement financ\u00E9s par les agences partenaires...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](162, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "a", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "div", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "button", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "Testez votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("defaultImage", "assets/cpnimages/home/44.png")("lazyLoad", "assets/cpnimages/home/44.png");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.follow, "K");
    } }, directives: [ng_lazyload_image__WEBPACK_IMPORTED_MODULE_2__["LazyLoadImageDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], _notre_succes_notre_succes_component__WEBPACK_IMPORTED_MODULE_4__["NotreSuccesComponent"], _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_5__["SideBarComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLink"]], styles: [".carre[_ngcontent-%COMP%] {\n  width: 200px;\n  height: 90px;\n  background: white;\n  border-radius: 18px;\n  margin-left: -250px;\n  margin-top: -90px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n\t background:#EBECF0;\n\t min-height: -moz-fit-content;\n\t min-height: fit-content;\n\t border-bottom-right-radius: 100px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n\t height: 100%;\n\t max-height: 585px;\n\t margin-top: -35px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n\t font-size: 50px;\n\t font-weight: 800;\n\t color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n\t font-size: 40px;\n\t font-weight: 700;\n\t color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%], .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:after {\n\t box-sizing: border-box;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   body[_ngcontent-%COMP%] {\n  background: #f5f5f5;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   main[_ngcontent-%COMP%] {\n  left: 50%;\n  position: absolute;\n  top: 50%;\n  transform: translateX(-50%) translateY(-50%);\n  width: 300px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\n  content: \"\";\n  display: block;\n  position: absolute;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before {\n  border: 5px solid #111d5e ;\n  border-radius: 20px;\n  height: 40px;\n  transition: all 0.3s ease-out;\n  transition-delay: 0.3s;\n  width: 40px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\n  background: #111d5e;\n  border-radius: 3px;\n  height: 5px;\n  transform: rotate(-45deg);\n  transform-origin: 0% 100%;\n  transition: all 0.3s ease-out;\n  width: 15px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n  background: transparent;\n  border: none;\n  border-radius: 20px;\n  display: block;\n  font-size: 20px;\n  height: 40px;\n  line-height: 40px;\n  opacity: 0;\n  outline: none;\n  padding: 0 15px;\n  position: relative;\n  transition: all 0.3s ease-out;\n  transition-delay: 0.6s;\n  width: 40px;\n  z-index: 1;\n  color: rgb(85, 85, 85);\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:before {\n\t transition-delay: 0.3s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:after {\n\t transition-delay: 0.6s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n\t transition-delay: 0s;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:after {\n\t transform: rotate(-45deg) translateX(15px) translateY(-2px);\n\t width: 0;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n  border: 5px solid #111d5e;\n  border-radius: 20px;\n  height: 40px;\n  width: 500px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n\t opacity: 1;\n\t width: 500px;\n}\ninput[type=\"text\"][_ngcontent-%COMP%] {\n  height: 50px;\n  font-size: 30px;\n  display: inline-block;\n\n  font-weight: 100;\n  border: none;\n  outline: none;\n  color: white;\n  padding: 3px;\n  padding-right: 60px;\n  width: 0px;\n  position: absolute;\n  top: 0;\n  right: 0;\n  background: none;\n  z-index: 3;\n  transition: width 0.4s cubic-bezier(0, 0.795, 0, 1);\n  cursor: pointer;\n}\ninput[type=\"text\"][_ngcontent-%COMP%]:focus:hover {\n  border-bottom: 1px solid white;\n}\ninput[type=\"text\"][_ngcontent-%COMP%]:focus {\n  width: 700px;\n  z-index: 1;\n  border-bottom: 1px solid white;\n  cursor: text;\n}\ninput[type=\"submit\"][_ngcontent-%COMP%] {\n  height: 50px;\n  width: 50px;\n  display: inline-block;\n  color: white;\n  float: right;\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADNQTFRFU1NT9fX1lJSUXl5e1dXVfn5+c3Nz6urqv7+/tLS0iYmJqampn5+fysrK39/faWlp////Vi4ZywAAABF0Uk5T/////////////////////wAlrZliAAABLklEQVR42rSWWRbDIAhFHeOUtN3/ags1zaA4cHrKZ8JFRHwoXkwTvwGP1Qo0bYObAPwiLmbNAHBWFBZlD9j0JxflDViIObNHG/Do8PRHTJk0TezAhv7qloK0JJEBh+F8+U/hopIELOWfiZUCDOZD1RADOQKA75oq4cvVkcT+OdHnqqpQCITWAjnWVgGQUWz12lJuGwGoaWgBKzRVBcCypgUkOAoWgBX/L0CmxN40u6xwcIJ1cOzWYDffp3axsQOyvdkXiH9FKRFwPRHYZUaXMgPLeiW7QhbDRciyLXJaKheCuLbiVoqx1DVRyH26yb0hsuoOFEPsoz+BVE0MRlZNjGZcRQyHYkmMp2hBTIzdkzCTc/pLqOnBrk7/yZdAOq/q5NPBH1f7x7fGP4C3AAMAQrhzX9zhcGsAAAAASUVORK5CYII=)\n    center center no-repeat;\n  text-indent: -10000px;\n  border: none;\n  position: absolute;\n  top: 0;\n  right: 0;\n  z-index: 2;\n  cursor: pointer;\n  opacity: 0.4;\n  cursor: pointer;\n  transition: opacity 0.4s ease;\n}\ninput[type=\"submit\"][_ngcontent-%COMP%]:hover {\n  opacity: 0.8;\n}\n\n.container_box[_ngcontent-%COMP%] {\n    width: 100%;\n    height: auto;\n    display: flex;\n    flex-direction: row;\n\n}\n.card[_ngcontent-%COMP%] {\n    width: 20%;\n    height: 300px;\n    background: white;\n    margin: 20px;\n\n}\n.card-text[_ngcontent-%COMP%]{\n  color:#111d5e !important;\n  font-size :12px !important;\n  \n}\n.cards[_ngcontent-%COMP%]{\n  transition: box-shadow .3s;\n  border: 1px solid #ccc;\n  border-radius: 15px;\n  float: left;\n  width: 25rem;\n  height: 25rem;\n}\n.cards[_ngcontent-%COMP%]:hover {\n  box-shadow: 0 0 11px rgba(33,33,33,.2); \n  border: 1px transparent;\n}\n.card-subtitle[_ngcontent-%COMP%]{\n  color:#111d5e !important;\n  font-size :16px !important;\n  font-weight:bold !important;\n\n}\n\n.underline[_ngcontent-%COMP%] {\n    text-decoration: underline;\n}\n\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 5px;\n  background: #111d5e;\n  display: block;\n  position: relative;\n}\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::before {\n  content: \"\";\n  position: absolute;\n  width: 30%;\n  top: 0;\n  left: 0;\n  height: 5px;\n  background: red;\n}\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::after {\n  content: \"\";\n  position: absolute;\n  width: 30%;\n  top: 0;\n  right: 0;\n  height: 5px;\n  background: red;\n}\n\n.num-card[_ngcontent-%COMP%]{\n  font-weight:bold; font-size :32px;color:#111d5e\n}\n.btntest[_ngcontent-%COMP%]{\n  -webkit-text-decoration:auto ;\n          text-decoration:auto ;  border: none; background: red;border-radius: 25px;color: white;padding: 5px 15px;\n}\nh1[_ngcontent-%COMP%]{\n  font-weight: bold;\n}\n\n\n@media only screen and (min-width : 320px) and (max-width : 480px)  {\n  \n\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n  border-bottom-right-radius: 0px;\n }\n   \n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\n  height: 100%;\n  max-height: 400px;\n  margin-top: -35px;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\n  font-size: 30px;\n  font-weight: 800;\n  color:  #111d5e;\n}\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\n  font-size: 20px;\n  font-weight: 700;\n  color:  #111d5e;\n}\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n    border: 5px solid #111d5e;\n    border-radius: 20px;\n    height: 40px;\n    width: 300px;\n  }\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n    opacity: 1;\n    width: 300px;\n  }\n  \n\n.carre[_ngcontent-%COMP%]{\n    width: 200px;\n    height: 90px;\n    background: white;\n    border-radius: 18px;\n     margin-left: 0px;\n     margin-top: 0px;\n    }\n \n .block1[_ngcontent-%COMP%]{\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  width: auto;\n  --bs-gutter-x: 0;\n }\n    .block1_part1[_ngcontent-%COMP%]{\n      width: 100%;\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n    }\n    .block1_part2[_ngcontent-%COMP%]{\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      width: max-content;\n    }\n  \n.container_box[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.cards[_ngcontent-%COMP%]{\n  transition: box-shadow .3s;\n  border: 1px solid #ccc;\n  border-radius: 15px;\n  float: left;\n  width: 18rem;\n  height: 25rem;\n}\n}\n\n@media only screen and (min-width : 480px) and (max-width : 768px)  {\n  \n    .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\n      border-bottom-right-radius: 0px;\n    }\n  \n\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\n  border: 5px solid #111d5e;\n  border-radius: 20px;\n  height: 40px;\n  width: 300px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\n  opacity: 1;\n  width: 300px;\n}    \n\n\n\n.container_box[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n\n}\n}\n\n@media only screen and (min-width : 768px) and (max-width : 992px)  {\n  \n\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\nborder: 5px solid #111d5e;\nborder-radius: 20px;\nheight: 40px;\nwidth: 300px;\n}\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\nopacity: 1;\nwidth: 300px;\n}    \n\n\n.block1[_ngcontent-%COMP%]{\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  width: auto;\n }\n    .block1_part1[_ngcontent-%COMP%]{\n      width: 100%;\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n    }\n    .block1_part2[_ngcontent-%COMP%]{\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      width: max-content;\n    }\n }\n\n@media only screen and (min-width : 992px) and (max-width : 1200px)  {\n      \n       .block1[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: center;\n        width: auto;\n       }\n          .block1_part1[_ngcontent-%COMP%]{\n            width: 100%;\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n          }\n          .block1_part2[_ngcontent-%COMP%]{\n            display: flex;\n            flex-direction: column;\n            justify-content: center;\n            align-items: center;\n            width: max-content;\n          }\n      }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vaG9tZXRwZXBtZS9ob21ldHBlcG1lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsMEZBQTBGO0FBQzFGO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixpQkFBaUI7QUFDbkI7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQiw0QkFBdUI7RUFBdkIsdUJBQXVCO0VBQ3ZCLGlDQUFpQztBQUNuQztBQUNDO0VBQ0MsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUI7QUFDbkI7QUFDQztFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtBQUNqQjtBQUNDO0VBQ0MsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixlQUFlO0FBQ2pCO0FBQ0M7RUFDQyxzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsU0FBUztFQUNULGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsNENBQTRDO0VBQzVDLFlBQVk7QUFDZDtBQUNBO0VBQ0UsV0FBVztFQUNYLGNBQWM7RUFDZCxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLDZCQUE2QjtFQUM3QixzQkFBc0I7RUFDdEIsV0FBVztBQUNiO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIseUJBQXlCO0VBQ3pCLDZCQUE2QjtFQUM3QixXQUFXO0FBQ2I7QUFDQTtFQUNFLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxlQUFlO0VBQ2YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsYUFBYTtFQUNiLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLHNCQUFzQjtFQUN0QixXQUFXO0VBQ1gsVUFBVTtFQUNWLHNCQUFzQjtBQUN4QjtBQUNDO0VBQ0Msc0JBQXNCO0FBQ3hCO0FBQ0M7RUFDQyxzQkFBc0I7QUFDeEI7QUFDQztFQUNDLG9CQUFvQjtBQUN0QjtBQUNDO0VBQ0MsMkRBQTJEO0VBQzNELFFBQVE7QUFDVjtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osWUFBWTtBQUNkO0FBQ0M7RUFDQyxVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBSUE7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLHFCQUFxQjs7RUFFckIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sUUFBUTtFQUNSLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YsbURBQW1EO0VBQ25ELGVBQWU7QUFDakI7QUFFQTtFQUNFLDhCQUE4QjtBQUNoQztBQUVBO0VBQ0UsWUFBWTtFQUNaLFVBQVU7RUFDViw4QkFBOEI7RUFDOUIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxZQUFZO0VBQ1osV0FBVztFQUNYLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osWUFBWTtFQUNaOzJCQUN5QjtFQUN6QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sUUFBUTtFQUNSLFVBQVU7RUFDVixlQUFlO0VBQ2YsWUFBWTtFQUNaLGVBQWU7RUFDZiw2QkFBNkI7QUFDL0I7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUNBLDhGQUE4RjtBQUM1RjtJQUNFLFdBQVc7SUFDWCxZQUFZO0lBQ1osYUFBYTtJQUNiLG1CQUFtQjs7QUFFdkI7QUFFQTtJQUNJLFVBQVU7SUFDVixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLFlBQVk7O0FBRWhCO0FBQ0E7RUFDRSx3QkFBd0I7RUFDeEIsMEJBQTBCOztBQUU1QjtBQUNBO0VBQ0UsMEJBQTBCO0VBQzFCLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0FBQ2Y7QUFDQTtFQUNFLHNDQUFzQztFQUN0Qyx1QkFBdUI7QUFDekI7QUFDQTtFQUNFLHdCQUF3QjtFQUN4QiwwQkFBMEI7RUFDMUIsMkJBQTJCOztBQUU3QjtBQUNBLHVHQUF1RztBQUV2RztJQUNJLDBCQUEwQjtBQUM5QjtBQUdBLG9HQUFvRztBQUNwRztFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixPQUFPO0VBQ1AsV0FBVztFQUNYLGVBQWU7QUFDakI7QUFDQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixRQUFRO0VBQ1IsV0FBVztFQUNYLGVBQWU7QUFDakI7QUFHQSxvR0FBb0c7QUFDcEc7RUFDRSxnQkFBZ0IsRUFBRSxlQUFlLENBQUM7QUFDcEM7QUFFQTtFQUNFLDZCQUFxQjtVQUFyQixxQkFBcUIsR0FBRyxZQUFZLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxpQkFBaUI7QUFDMUc7QUFFQTtFQUNFLGlCQUFpQjtBQUNuQjtBQUtNLDBHQUEwRztBQUM1RywyQkFBMkI7QUFDNUI7O0FBRUgsa0hBQWtIO0FBQ2xIO0VBQ0UsK0JBQStCO0NBQ2hDOztDQUVBO0VBQ0MsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixlQUFlO0FBQ2pCO0VBQ0U7SUFDRSx5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixZQUFZO0VBQ2Q7RUFDQTtJQUNFLFVBQVU7SUFDVixZQUFZO0VBQ2Q7OztBQUdGO0lBQ0ksWUFBWTtJQUNaLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsbUJBQW1CO0tBQ2xCLGdCQUFnQjtLQUNoQixlQUFlO0lBQ2hCO0NBQ0gsbUdBQW1HO0NBQ25HO0VBQ0MsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxnQkFBZ0I7Q0FDakI7SUFDRztNQUNFLFdBQVc7TUFDWCxhQUFhO01BQ2Isc0JBQXNCO01BQ3RCLHVCQUF1QjtNQUN2QixtQkFBbUI7SUFDckI7SUFDQTtNQUNFLGFBQWE7TUFDYixzQkFBc0I7TUFDdEIsdUJBQXVCO01BQ3ZCLG1CQUFtQjtNQUNuQixrQkFBa0I7SUFDcEI7RUFDRiwwSEFBMEg7QUFDNUg7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsMEJBQTBCO0VBQzFCLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0FBQ2Y7QUFDQTtBQUVBLGdDQUFnQztBQUNoQztFQUNFLGtIQUFrSDtJQUNoSDtNQUNFLCtCQUErQjtJQUNqQzs7O0FBR0o7RUFDRSx5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixZQUFZO0FBQ2Q7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0FBQ2Q7OztBQUdBLDBIQUEwSDtBQUMxSDtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1COztBQUVyQjtBQUNBO0FBSUMsMEJBQTBCO0FBQzFCO0VBQ0Msa0hBQWtIOztBQUVwSDtBQUNBLHlCQUF5QjtBQUN6QixtQkFBbUI7QUFDbkIsWUFBWTtBQUNaLFlBQVk7QUFDWjtBQUNBO0FBQ0EsVUFBVTtBQUNWLFlBQVk7QUFDWjs7QUFFQSxtR0FBbUc7QUFDbkc7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsV0FBVztDQUNaO0lBQ0c7TUFDRSxXQUFXO01BQ1gsYUFBYTtNQUNiLHNCQUFzQjtNQUN0Qix1QkFBdUI7TUFDdkIsbUJBQW1CO0lBQ3JCO0lBQ0E7TUFDRSxhQUFhO01BQ2Isc0JBQXNCO01BQ3RCLHVCQUF1QjtNQUN2QixtQkFBbUI7TUFDbkIsa0JBQWtCO0lBQ3BCO0NBQ0g7QUFHRSw2QkFBNkI7QUFDN0I7TUFDRyxtR0FBbUc7T0FDbEc7UUFDQyxhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QixtQkFBbUI7UUFDbkIsV0FBVztPQUNaO1VBQ0c7WUFDRSxXQUFXO1lBQ1gsYUFBYTtZQUNiLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsbUJBQW1CO1VBQ3JCO1VBQ0E7WUFDRSxhQUFhO1lBQ2Isc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixtQkFBbUI7WUFDbkIsa0JBQWtCO1VBQ3BCO01BQ0oiLCJmaWxlIjoiYXBwL2Nwbi9ob21ldHBlcG1lL2hvbWV0cGVwbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi5jYXJyZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiA5MHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgbWFyZ2luLWxlZnQ6IC0yNTBweDtcbiAgbWFyZ2luLXRvcDogLTkwcHg7XG59XG5cbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcge1xuXHQgYmFja2dyb3VuZDojRUJFQ0YwO1xuXHQgbWluLWhlaWdodDogZml0LWNvbnRlbnQ7XG5cdCBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTAwcHg7XG59XG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciAuaGVhZGluZ19pbWcge1xuXHQgaGVpZ2h0OiAxMDAlO1xuXHQgbWF4LWhlaWdodDogNTg1cHg7XG5cdCBtYXJnaW4tdG9wOiAtMzVweDtcbn1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xuXHQgZm9udC1zaXplOiA1MHB4O1xuXHQgZm9udC13ZWlnaHQ6IDgwMDtcblx0IGNvbG9yOiAgIzExMWQ1ZTtcbn1cbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XG5cdCBmb250LXNpemU6IDQwcHg7XG5cdCBmb250LXdlaWdodDogNzAwO1xuXHQgY29sb3I6ICAjMTExZDVlO1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICosIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICo6YmVmb3JlLCAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAqOmFmdGVyIHtcblx0IGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBib2R5IHtcbiAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcbn1cbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIG1haW4ge1xuICBsZWZ0OiA1MCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSB0cmFuc2xhdGVZKC01MCUpO1xuICB3aWR0aDogMzAwcHg7XG59XG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmJlZm9yZSwgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaDphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoOmJlZm9yZSB7XG4gIGJvcmRlcjogNXB4IHNvbGlkICMxMTFkNWUgO1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xuICB0cmFuc2l0aW9uLWRlbGF5OiAwLjNzO1xuICB3aWR0aDogNDBweDtcbn1cbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YWZ0ZXIge1xuICBiYWNrZ3JvdW5kOiAjMTExZDVlO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGhlaWdodDogNXB4O1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpO1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAxMDAlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcbiAgd2lkdGg6IDE1cHg7XG59XG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoX19pbnB1dCB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIG9wYWNpdHk6IDA7XG4gIG91dGxpbmU6IG5vbmU7XG4gIHBhZGRpbmc6IDAgMTVweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcbiAgdHJhbnNpdGlvbi1kZWxheTogMC42cztcbiAgd2lkdGg6IDQwcHg7XG4gIHotaW5kZXg6IDE7XG4gIGNvbG9yOiByZ2IoODUsIDg1LCA4NSk7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZTpiZWZvcmUge1xuXHQgdHJhbnNpdGlvbi1kZWxheTogMC4zcztcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1oaWRlOmFmdGVyIHtcblx0IHRyYW5zaXRpb24tZGVsYXk6IDAuNnM7XG59XG4gLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZSAuc2VhcmNoX19pbnB1dCB7XG5cdCB0cmFuc2l0aW9uLWRlbGF5OiAwcztcbn1cbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmFmdGVyIHtcblx0IHRyYW5zZm9ybTogcm90YXRlKC00NWRlZykgdHJhbnNsYXRlWCgxNXB4KSB0cmFuc2xhdGVZKC0ycHgpO1xuXHQgd2lkdGg6IDA7XG59XG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XG4gIGJvcmRlcjogNXB4IHNvbGlkICMxMTFkNWU7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDUwMHB4O1xufVxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xuXHQgb3BhY2l0eTogMTtcblx0IHdpZHRoOiA1MDBweDtcbn1cblxuXG5cbmlucHV0W3R5cGU9XCJ0ZXh0XCJdIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBmb250LXNpemU6IDMwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcblxuICBmb250LXdlaWdodDogMTAwO1xuICBib3JkZXI6IG5vbmU7XG4gIG91dGxpbmU6IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogM3B4O1xuICBwYWRkaW5nLXJpZ2h0OiA2MHB4O1xuICB3aWR0aDogMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6IG5vbmU7XG4gIHotaW5kZXg6IDM7XG4gIHRyYW5zaXRpb246IHdpZHRoIDAuNHMgY3ViaWMtYmV6aWVyKDAsIDAuNzk1LCAwLCAxKTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5pbnB1dFt0eXBlPVwidGV4dFwiXTpmb2N1czpob3ZlciB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbn1cblxuaW5wdXRbdHlwZT1cInRleHRcIl06Zm9jdXMge1xuICB3aWR0aDogNzAwcHg7XG4gIHotaW5kZXg6IDE7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcbiAgY3Vyc29yOiB0ZXh0O1xufVxuaW5wdXRbdHlwZT1cInN1Ym1pdFwiXSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgY29sb3I6IHdoaXRlO1xuICBmbG9hdDogcmlnaHQ7XG4gIGJhY2tncm91bmQ6IHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQURBQUFBQXdDQU1BQUFCZzNBbTFBQUFBR1hSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCSmJXRm5aVkpsWVdSNWNjbGxQQUFBQUROUVRGUkZVMU5UOWZYMWxKU1VYbDVlMWRYVmZuNStjM056NnVycXY3Ky90TFMwaVltSnFhbXBuNStmeXNySzM5L2ZhV2xwLy8vL1ZpNFp5d0FBQUJGMFVrNVQvLy8vLy8vLy8vLy8vLy8vLy8vLy93QWxyWmxpQUFBQkxrbEVRVlI0MnJTV1dSYkRJQWhGSGVPVXROMy9hZ3MxemFBNGNIcktaOEpGUkh3b1hrd1R2d0dQMVFvMGJZT2JBUHdpTG1iTkFIQldGQlpsRDlqMEp4ZmxEVmlJT2JOSEcvRG84UFJIVEprMFRlekFodjdxbG9LMEpKRUJoK0Y4K1UvaG9wSUVMT1dmaVpVQ0RPWkQxUkFET1FLQTc1b3E0Y3ZWa2NUK09kSG5xcXBRQ0lUV0FqbldWZ0dRVVd6MTJsSnVHd0dvYVdnQkt6UlZCY0N5cGdVa09Bb1dnQlgvTDBDbXhONDB1Nnh3Y0lKMWNPeldZRGZmcDNheHNRT3l2ZGtYaUg5RktSRndQUkhZWlVhWE1nUExlaVc3UWhiRFJjaXlMWEphS2hlQ3VMYmlWb3F4MURWUnlIMjZ5YjBoc3VvT0ZFUHNveitCVkUwTVJsWk5qR1pjUlF5SFlrbU1wMmhCVEl6ZGt6Q1RjL3BMcU9uQnJrNy95WmRBT3EvcTVOUEJIMWY3eDdmR1A0QzNBQU1BUXJoelg5emhjR3NBQUFBQVNVVk9SSzVDWUlJPSlcbiAgICBjZW50ZXIgY2VudGVyIG5vLXJlcGVhdDtcbiAgdGV4dC1pbmRlbnQ6IC0xMDAwMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgei1pbmRleDogMjtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBvcGFjaXR5OiAwLjQ7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjRzIGVhc2U7XG59XG5cbmlucHV0W3R5cGU9XCJzdWJtaXRcIl06aG92ZXIge1xuICBvcGFjaXR5OiAwLjg7XG59XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazEqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAuY29udGFpbmVyX2JveCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcblxufVxuXG4uY2FyZCB7XG4gICAgd2lkdGg6IDIwJTtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIG1hcmdpbjogMjBweDtcblxufVxuLmNhcmQtdGV4dHtcbiAgY29sb3I6IzExMWQ1ZSAhaW1wb3J0YW50O1xuICBmb250LXNpemUgOjEycHggIWltcG9ydGFudDtcbiAgXG59XG4uY2FyZHN7XG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgLjNzO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDI1cmVtO1xuICBoZWlnaHQ6IDI1cmVtO1xufVxuLmNhcmRzOmhvdmVyIHtcbiAgYm94LXNoYWRvdzogMCAwIDExcHggcmdiYSgzMywzMywzMywuMik7IFxuICBib3JkZXI6IDFweCB0cmFuc3BhcmVudDtcbn1cbi5jYXJkLXN1YnRpdGxle1xuICBjb2xvcjojMTExZDVlICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZSA6MTZweCAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDpib2xkICFpbXBvcnRhbnQ7XG5cbn1cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi51bmRlcmxpbmUge1xuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4ucHJpbWFyeV9ib2R5IC5kaXZpZGVyIC5kaXZpZGVyX2xpZ25lIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNXB4O1xuICBiYWNrZ3JvdW5kOiAjMTExZDVlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnByaW1hcnlfYm9keSAuZGl2aWRlciAuZGl2aWRlcl9saWduZTo6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMzAlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGhlaWdodDogNXB4O1xuICBiYWNrZ3JvdW5kOiByZWQ7XG59XG4ucHJpbWFyeV9ib2R5IC5kaXZpZGVyIC5kaXZpZGVyX2xpZ25lOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDMwJTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgaGVpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQ6IHJlZDtcbn1cblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLm51bS1jYXJke1xuICBmb250LXdlaWdodDpib2xkOyBmb250LXNpemUgOjMycHg7Y29sb3I6IzExMWQ1ZVxufVxuXG4uYnRudGVzdHtcbiAgdGV4dC1kZWNvcmF0aW9uOmF1dG8gOyAgYm9yZGVyOiBub25lOyBiYWNrZ3JvdW5kOiByZWQ7Ym9yZGVyLXJhZGl1czogMjVweDtjb2xvcjogd2hpdGU7cGFkZGluZzogNXB4IDE1cHg7XG59XG5cbmgxe1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuXG5cbiAgICBcbiAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnJlc3BvbnNpdmUgY3NzICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLyogQ3VzdG9tLCBpUGhvbmUgUmV0aW5hICAqLyAgXG4gICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAzMjBweCkgYW5kIChtYXgtd2lkdGggOiA0ODBweCkgIHtcbiAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIHtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDBweDtcbiB9XG4gICBcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgbWF4LWhlaWdodDogNDAwcHg7XG4gIG1hcmdpbi10b3A6IC0zNXB4O1xufVxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC50aXRsZV9oZWFkaW5nIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBjb2xvcjogICMxMTFkNWU7XG59XG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgY29sb3I6ICAjMTExZDVlO1xufVxuICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XG4gICAgYm9yZGVyOiA1cHggc29saWQgIzExMWQ1ZTtcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICB3aWR0aDogMzAwcHg7XG4gIH1cbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XG4gICAgb3BhY2l0eTogMTtcbiAgICB3aWR0aDogMzAwcHg7XG4gIH1cbiAgXG5cbi5jYXJyZXtcbiAgICB3aWR0aDogMjAwcHg7XG4gICAgaGVpZ2h0OiA5MHB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDE4cHg7XG4gICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICB9XG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sxKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuIC5ibG9jazF7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogYXV0bztcbiAgLS1icy1ndXR0ZXIteDogMDtcbiB9XG4gICAgLmJsb2NrMV9wYXJ0MXtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuICAgIC5ibG9jazFfcGFydDJ7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiBtYXgtY29udGVudDtcbiAgICB9XG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmNvbnRhaW5lcl9ib3gge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jYXJkc3tcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAuM3M7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGZsb2F0OiBsZWZ0O1xuICB3aWR0aDogMThyZW07XG4gIGhlaWdodDogMjVyZW07XG59XG59XG5cbi8qIEV4dHJhIFNtYWxsIERldmljZXMsIFBob25lcyAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogNDgwcHgpIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpICB7XG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIHtcbiAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwcHg7XG4gICAgfVxuICBcblxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xuICBib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlO1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiAzMDBweDtcbn1cbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xuICBvcGFjaXR5OiAxO1xuICB3aWR0aDogMzAwcHg7XG59ICAgIFxuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmNvbnRhaW5lcl9ib3gge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxufVxufVxuXG5cblxuIC8qIFNtYWxsIERldmljZXMsIFRhYmxldHMqL1xuIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDk5MnB4KSAge1xuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcbmJvcmRlcjogNXB4IHNvbGlkICMxMTFkNWU7XG5ib3JkZXItcmFkaXVzOiAyMHB4O1xuaGVpZ2h0OiA0MHB4O1xud2lkdGg6IDMwMHB4O1xufVxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XG5vcGFjaXR5OiAxO1xud2lkdGg6IDMwMHB4O1xufSAgICBcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sxKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmJsb2NrMXtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiBhdXRvO1xuIH1cbiAgICAuYmxvY2sxX3BhcnQxe1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG4gICAgLmJsb2NrMV9wYXJ0MntcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgd2lkdGg6IG1heC1jb250ZW50O1xuICAgIH1cbiB9XG5cbiBcbiAgIC8qIE1lZGl1bSBEZXZpY2VzLCBEZXNrdG9wcyAqL1xuICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogOTkycHgpIGFuZCAobWF4LXdpZHRoIDogMTIwMHB4KSAge1xuICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2sxKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgICAgIC5ibG9jazF7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICB3aWR0aDogYXV0bztcbiAgICAgICB9XG4gICAgICAgICAgLmJsb2NrMV9wYXJ0MXtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5ibG9jazFfcGFydDJ7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHdpZHRoOiBtYXgtY29udGVudDtcbiAgICAgICAgICB9XG4gICAgICB9Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HometpepmeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-hometpepme',
                templateUrl: './hometpepme.component.html',
                styleUrls: ['./hometpepme.component.css']
            }]
    }], function () { return [{ type: src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/inscription/inscription.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/cpn/inscription/inscription.component.ts ***!
  \**********************************************************/
/*! exports provided: InscriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InscriptionComponent", function() { return InscriptionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");







class InscriptionComponent {
    constructor(auth, fb, tokenStorage) {
        this.auth = auth;
        this.fb = fb;
        this.tokenStorage = tokenStorage;
        this.registerGroup = this.fb.group({
            role: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            firstname: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            lastname: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            password_confirmed: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
        });
    }
    onSubmit() {
        const formData = new FormData();
        formData.append('type', this.registerGroup.get('role').value);
        formData.append('firstname', this.registerGroup.get('firstname').value);
        formData.append('lastname', this.registerGroup.get('lastname').value);
        formData.append('email', this.registerGroup.get('email').value);
        formData.append('password', this.registerGroup.get('password').value);
        formData.append('password_confirmed', this.registerGroup.get('password_confirmed').value);
        this.auth.register(formData).subscribe(res => {
            if (!res.error) {
                this.tokenStorage.saveToken(res.data.token);
                this.tokenStorage.saveUser(res.data.user);
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                });
                if (this.registerGroup.get('role').value === "tpe") {
                    location.href = '/cpn/Home_tpe_pme';
                }
                else if (this.registerGroup.get('role').value === "age") {
                    location.href = '/cpn/agence';
                }
                else {
                    location.href = '/cpn/Home_collectivite';
                }
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: res.message + ' !',
                });
            }
        }, error => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'error 500',
            });
        });
    }
    ngOnInit() {
    }
}
InscriptionComponent.ɵfac = function InscriptionComponent_Factory(t) { return new (t || InscriptionComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"])); };
InscriptionComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: InscriptionComponent, selectors: [["app-inscription"]], decls: 73, vars: 1, consts: [[1, "register_container"], [1, "register_content"], [1, "register_wrapper"], ["method", "post"], [1, "row", "row", "d-flex", "align-items-center"], [1, "col-md-6", "g-3"], [1, "container", "py-0"], [1, "row", "row-cols-2", "g-2"], [1, "col"], ["src", "assets/cpnimages/connexion/2.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/1.png", "alt", "", 2, "width", "100%", "border-radius", "0 3.5rem 0 0", "height", "100%"], ["src", "assets/cpnimages/connexion/3.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/4.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/5.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/8.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/9.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/10.png", "alt", "", 2, "border-radius", "0 0 3.5rem 0", "width", "100%", "height", "100%"], [1, "col-md-6", 2, "height", "100%"], [3, "formGroup", "ngSubmit"], [1, "row", "g-0", "mb-3", "listradio", 2, "flex", "1"], [1, "col-sm-4", "d-flex", "justify-content-center", "radio"], ["width", "20%", "src", "assets/cpnimages/connexion/Entreprise.png", "alt", "", 2, "margin-bottom", "15px"], [1, "form-check", "form-check-inline"], ["type", "radio", "formControlName", "role", "name", "role", "id", "role", "value", "tpe", 1, "form-check-input"], ["for", "inlineRadio1", 1, "form-check-label"], ["width", "20%", "src", "assets/cpnimages/connexion/Agence.png", "alt", "", 2, "margin-bottom", "15px"], ["type", "radio", "formControlName", "role", "name", "role", "id", "role", "value", "age", 1, "form-check-input"], ["width", "20%", "src", "assets/cpnimages/connexion/Collectivit\u00E9.png", "alt", "", 2, "margin-bottom", "15px"], ["type", "radio", "formControlName", "role", "name", "role", "id", "role", "value", "col", 1, "form-check-input"], [1, "row", "mb-3", "g-4", "listinput", 2, "flex", "2"], [1, "col-md-12"], ["type", "text", "formControlName", "firstname", "placeholder", "Nom d'utilisateur", 1, "form-control"], ["type", "text", "formControlName", "lastname", "placeholder", "Prenom d'utilisateur", 1, "form-control"], ["type", "email", "formControlName", "email", "id", "email", "placeholder", "Email", 1, "form-control"], ["type", "password", "formControlName", "password", "id", "password", "placeholder", "Password", 1, "form-control"], ["type", "password", "formControlName", "password_confirmed", "id", "confirmPassword", "placeholder", "Confirmer password", 1, "form-control"], [1, "row"], [1, "col-8"], ["type", "checkbox", "value", "true", "name", "accept", "id", "flexCheckDefault", 1, "form-check-input", 2, "border", "1px solid rgba(0, 0, 0, 0.25)", "margin-left", "15px"], ["for", "flexCheckDefault", 1, "form-check-label", 2, "margin-left", "45px"], [1, "col-4"], ["type", "submit", 1, "btn", "btn-danger", 2, "border", "none", "background", "red", "border-radius", "25px", "color", "white"], ["type", "checkbox", "value", "true", "name", "accept", "id", "flexCheckDefault", 1, "form-check-input", 2, "border", "1px solid rgba(0, 0, 0, 0.25)", "margin-left", "27px"], ["for", "flexCheckDefault", 1, "form-check-label", 2, "margin-left", "59px"]], template: function InscriptionComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "img", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "img", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "img", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "form", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function InscriptionComponent_Template_form_ngSubmit_29_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "input", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "label", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "TPE-PME");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "img", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "input", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "label", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "input", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "label", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Collectivit\u00E9s");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "input", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "input", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "input", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "input", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "input", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "input", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "label", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, " Se Souvenir de moi ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "button", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "Inscription");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "input", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "label", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, " j'ai lu et j'accepte la politique de confidentialit\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.registerGroup);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RadioControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"]], styles: [".form-control[_ngcontent-%COMP%] {\n    display: block;\n    width: 100%;\n    height: calc(2em + .75rem + 2px);\n    padding: 1.375rem 1.75rem;\n    font-size: 1rem;\n    font-weight: 400;\n    line-height: 1.5;\n    color: #495057;\n    background-color: #fff;\n    background-clip: padding-box;\n    border: none;\n    border-radius: .25rem;\n    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;\n    box-shadow: 3px 3px 3px 3px rgb(0 0 0 / 10%);\n    margin: 11px;\n}\n\n.register_container[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n}\n\n.listradio[_ngcontent-%COMP%]{\n    display: flex;\nflex-direction: row;\nwidth: -moz-available;\njustify-content: space-between;\nalign-items: center;\nheight: inherit;\n}\n\n.radio[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: baseline;\n        float: initial;\n        width: 200px;\n        \n}\n\n.listinput[_ngcontent-%COMP%]{\n    display: flex;\nflex-direction: column;\nwidth: 90%;\njustify-content: center;\nalign-items: center;\nmargin: auto;\n}\n\n.register_container[_ngcontent-%COMP%]   .register_content[_ngcontent-%COMP%]   .register_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n    margin-top:0;\n    margin-right: 0;\n    margin-left: 0;\n}\n\n.container[_ngcontent-%COMP%]{\n    margin-bottom: 30px;\n}\n\n\n\n@media only screen and (min-width : 329px) and (max-width : 510px)  {\n    .register_container[_ngcontent-%COMP%]   .register_content[_ngcontent-%COMP%]   .register_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n        margin-top:0;\n        margin-right: 0;\n        margin-left: 0;\n    }\n    .listradio[_ngcontent-%COMP%]{\n        display: flex;\n    flex-direction: inherit;\n    width:100%;\n    justify-content: center;\n    align-items: center;\n    height: 200px;\n    margin: 10px;\n    height: auto;\n\n    }\n\n    .radio[_ngcontent-%COMP%]{\n        margin: auto;\n    }\n    \n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vaW5zY3JpcHRpb24vaW5zY3JpcHRpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGNBQWM7SUFDZCxXQUFXO0lBQ1gsZ0NBQWdDO0lBQ2hDLHlCQUF5QjtJQUN6QixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2Qsc0JBQXNCO0lBQ3RCLDRCQUE0QjtJQUM1QixZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLHFFQUFxRTtJQUNyRSw0Q0FBNEM7SUFDNUMsWUFBWTtBQUNoQjs7QUFFQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLGFBQWE7QUFDakIsbUJBQW1CO0FBQ25CLHFCQUFxQjtBQUNyQiw4QkFBOEI7QUFDOUIsbUJBQW1CO0FBQ25CLGVBQWU7QUFDZjs7QUFDQTtRQUNRLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLHFCQUFxQjtRQUNyQixjQUFjO1FBQ2QsWUFBWTs7QUFFcEI7O0FBQ0E7SUFDSSxhQUFhO0FBQ2pCLHNCQUFzQjtBQUN0QixVQUFVO0FBQ1YsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixZQUFZO0FBQ1o7O0FBR0E7SUFDSSxZQUFZO0lBQ1osZUFBZTtJQUNmLGNBQWM7QUFDbEI7O0FBQ0E7SUFDSSxtQkFBbUI7QUFDdkI7O0FBRUUsZ0NBQWdDOztBQUNoQztJQUNFO1FBQ0ksWUFBWTtRQUNaLGVBQWU7UUFDZixjQUFjO0lBQ2xCO0lBQ0E7UUFDSSxhQUFhO0lBQ2pCLHVCQUF1QjtJQUN2QixVQUFVO0lBQ1YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtJQUNaLFlBQVk7O0lBRVo7O0lBRUE7UUFDSSxZQUFZO0lBQ2hCOzs7QUFHSiIsImZpbGUiOiJhcHAvY3BuL2luc2NyaXB0aW9uL2luc2NyaXB0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9ybS1jb250cm9sIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IGNhbGMoMmVtICsgLjc1cmVtICsgMnB4KTtcbiAgICBwYWRkaW5nOiAxLjM3NXJlbSAxLjc1cmVtO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgY29sb3I6ICM0OTUwNTc7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNsaXA6IHBhZGRpbmctYm94O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOiAuMjVyZW07XG4gICAgdHJhbnNpdGlvbjogYm9yZGVyLWNvbG9yIC4xNXMgZWFzZS1pbi1vdXQsYm94LXNoYWRvdyAuMTVzIGVhc2UtaW4tb3V0O1xuICAgIGJveC1zaGFkb3c6IDNweCAzcHggM3B4IDNweCByZ2IoMCAwIDAgLyAxMCUpO1xuICAgIG1hcmdpbjogMTFweDtcbn1cblxuLnJlZ2lzdGVyX2NvbnRhaW5lcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xufVxuXG4ubGlzdHJhZGlve1xuICAgIGRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogcm93O1xud2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbmhlaWdodDogaW5oZXJpdDtcbn1cbi5yYWRpb3tcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcbiAgICAgICAgZmxvYXQ6IGluaXRpYWw7XG4gICAgICAgIHdpZHRoOiAyMDBweDtcbiAgICAgICAgXG59XG4ubGlzdGlucHV0e1xuICAgIGRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xud2lkdGg6IDkwJTtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbm1hcmdpbjogYXV0bztcbn1cblxuXG4ucmVnaXN0ZXJfY29udGFpbmVyIC5yZWdpc3Rlcl9jb250ZW50IC5yZWdpc3Rlcl93cmFwcGVyIC5yb3d7XG4gICAgbWFyZ2luLXRvcDowO1xuICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICBtYXJnaW4tbGVmdDogMDtcbn1cbi5jb250YWluZXJ7XG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuICAvKiBFeHRyYSBTbWFsbCBEZXZpY2VzLCBQaG9uZXMgKi9cbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMzI5cHgpIGFuZCAobWF4LXdpZHRoIDogNTEwcHgpICB7XG4gICAgLnJlZ2lzdGVyX2NvbnRhaW5lciAucmVnaXN0ZXJfY29udGVudCAucmVnaXN0ZXJfd3JhcHBlciAucm93e1xuICAgICAgICBtYXJnaW4tdG9wOjA7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgfVxuICAgIC5saXN0cmFkaW97XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGluaGVyaXQ7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGhlaWdodDogMjAwcHg7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIGhlaWdodDogYXV0bztcblxuICAgIH1cblxuICAgIC5yYWRpb3tcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgIH1cbiAgICBcblxufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InscriptionComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-inscription',
                templateUrl: './inscription.component.html',
                styleUrls: ['./inscription.component.css']
            }]
    }], function () { return [{ type: src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/map-french-region/map-french-region.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/cpn/map-french-region/map-french-region.component.ts ***!
  \**********************************************************************/
/*! exports provided: MapFrenchRegionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapFrenchRegionComponent", function() { return MapFrenchRegionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class MapFrenchRegionComponent {
    constructor() { }
    ngOnInit() {
        $("path, circle").hover(function (e) {
            $('#info-box').css('display', 'block');
            $('#info-box').html($(this).data('info'));
            // console.log('hover', $( '#info-box').html($(this).data('info')))
        });
        $("path, circle").mouseleave(function (e) {
            $('#info-box').css('display', 'none');
        });
        $(document).mousemove(function (e) {
            $('#info-box').css('top', e.pageY - $('#info-box').height() - 30);
            $('#info-box').css('left', e.pageX - ($('#info-box').width()) / 2);
        }).mouseover();
    }
}
MapFrenchRegionComponent.ɵfac = function MapFrenchRegionComponent_Factory(t) { return new (t || MapFrenchRegionComponent)(); };
MapFrenchRegionComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: MapFrenchRegionComponent, selectors: [["app-map-french-region"]], decls: 56, vars: 0, consts: [["id", "info-box", 1, "round", "right-in"], [1, "divider", "g-0", "mb-5"], [1, "french_map", "mb-5"], [1, "map_wrapper", "container", "g-0"], [1, "row", "g-0", "justify-content-center"], [1, "col-md-8"], [1, "map_holder", "d-flex", "flex-column", "align-items-center", "justify-content-center"], ["id", "mapSVG", "version", "1.1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "-7 -18 1053 770", 0, "xml", "space", "preserve", 1, "map_svg"], ["href", "cpn/region/Guadeloupe"], ["data-name", "Guadeloupe", "data-department", "971", "data-code_insee", "01", 1, "region"], ["data-name", "Guadeloupe", "data-info", "<img src=\"assets/cpnimages/regions/guadeloupe.png\"/>", "data-department", "971", "d", "M44.87,452.95l2.26,-1.9l1.31,-0.53l0.63,0.19l-2.39,1.57l-1.81,0.66ZM16.07,454.56l0.99,-3.24l0.45,-0.54l1.25,-0.4l0.33,-0.98l-0.15,-1.3l-1.63,-0.93l-0.61,-3.02l1.65,-2.23l2.47,-1.44l2.87,2.33l0.67,1.2l0.24,4.01l1.84,2.68l1.77,1.09l0.71,-0.33l2.34,0.36l6.21,4.32l1.16,0.34l-0.55,0.18l-3.31,-0.89l-10.9,2.46l-3.09,1.07l-2.38,-0.65l-1.77,-1.2l-0.78,-1.34l0.24,-1.57ZM29.3,476.81l1.29,-2.59l1.05,-1.18l1.16,-0.64l1.27,0.28l1.08,0.91l0.68,1.72l0.94,0.93l0.13,1.34l-1.12,1.36l-1.47,0.73l-1.99,0.48l-1.58,-0.25l-1.44,-3.1ZM6.63,475.24l-0.25,-0.65l0.35,-0.72l-0.44,-0.85l-1.95,-1.6l-0.87,-1.97l-0.57,-0.45l-0.69,-2.06l-0.05,-4.94l-0.89,-2.29l0.25,-1.07l-1.09,-2.09l0.05,-3.66l1.5,-2.21l0.77,-0.65l0.89,-0.23l1.5,1.13l3.84,1.09l2.02,1.06l0.67,0.81l-0.4,0.18l0.01,0.74l0.87,0.07l0.72,0.74l0.65,-0.12l0.7,-0.95l0.47,-0.08l0.26,0.32l-0.35,1.27l0.32,1.1l-1.34,0.42l-0.48,0.89l0.13,2.94l0.8,2.29l0.41,3.37l-0.49,3.19l-1.83,2.19l-2.76,1.62l-2.74,1.16ZM13.68,480.6l0.09,-0.17l0.15,-0.01l-0.11,0.1l-0.13,0.09ZM10.63,481.22l0.17,0.27l-0.27,0.31l-0.19,-0.11l0.3,-0.46Z", 1, "departement"], ["href", "cpn/region/Collectivite_territoriale_de_martinique"], ["data-name", "Martinique", "data-department", "972", "data-code_insee", "02", 1, "region"], ["data-name", "Martinique", "data-info", "<img src=\"assets/cpnimages/regions/flag/collect.png\"/>", "data-department", "972", "d", "M29.58,521.59l2.04,4.09l0.61,0.13l1.69,-1.37l2.35,-1.15l2.71,-0.7l1.66,-0.01l-1.1,1.0l0.15,0.92l-1.99,2.03l-1.17,-1.91l-0.57,-0.12l-1.08,0.91l-0.34,2.42l0.25,2.72l0.56,0.33l1.58,-0.37l0.87,0.65l-2.81,1.51l-0.09,0.63l0.51,0.51l-0.97,0.5l-0.21,0.44l0.48,1.35l1.03,0.35l0.45,-0.14l0.84,-1.13l2.53,1.1l-1.26,0.61l-0.22,0.52l0.83,1.98l1.01,1.02l2.32,1.31l0.92,2.1l2.03,0.78l0.32,0.71l-0.03,1.98l0.6,0.39l0.71,-0.41l-0.25,2.4l0.41,1.46l0.77,1.07l-1.35,1.57l0.15,0.59l1.42,0.69l1.69,5.35l-1.43,0.75l-1.43,3.51l-1.14,1.51l-1.19,0.51l-1.52,0.14l-1.15,-0.51l-0.42,-1.43l0.56,-0.96l1.27,-0.41l1.36,-0.96l0.05,-2.11l-1.15,-1.71l-0.48,-0.15l-3.49,1.8l-1.39,-0.1l0.06,-0.84l-0.32,-0.42l-0.79,-0.15l-3.38,0.55l-6.91,-0.91l-2.07,0.3l-2.92,1.18l-1.59,0.24l-0.81,-0.46l-2.5,-3.48l-0.12,-1.4l-1.35,-0.99l2.86,-2.99l3.07,-1.49l0.78,-0.71l1.21,1.62l2.11,0.55l1.94,-0.83l0.87,-2.13l-2.15,-3.7l-0.25,-2.0l-0.53,-0.33l-3.16,0.87l-5.49,-0.02l-1.6,-2.4l-4.32,-2.94l-2.07,-1.98l-1.24,-3.51l-1.31,-1.93l0.78,-2.27l-0.31,-1.75l-1.64,-2.67l-3.41,-3.56l-0.79,-1.4l-0.29,-1.59l0.29,-1.51l0.74,-1.23l0.98,-0.9l4.61,-2.61l4.02,-0.06l3.94,1.27l5.23,3.33l4.71,2.09l1.84,1.19l1.36,2.81l1.45,0.5Z", 1, "departement"], ["data-name", "Guyane", "data-department", "973", "data-code_insee", "03", 1, "region"], ["data-name", "Guyane", "data-info", "<p>n'est pas disponible </p>", "data-department", "973", "d", "M0.97,715.11l0.03,-0.01l1.04,0.32l0.16,0.02l0.86,-0.15l0.51,-0.46l0.86,-2.17l2.19,-0.54l1.75,-1.7l3.1,-6.63l2.47,-3.35l0.8,-2.07l0.47,-0.34l0.06,-0.66l-0.55,-0.79l0.94,-2.39l-0.02,-0.46l-0.64,-0.76l0.44,-0.66l-0.5,-1.65l0.23,-1.91l-0.45,-0.69l-0.46,-0.14l0.86,-1.73l2.07,-2.81l1.19,-0.89l0.84,-2.29l1.16,-1.21l0.63,-1.48l-0.08,-2.67l0.59,-2.69l-0.24,-0.63l-0.99,-0.8l-0.94,0.14l-0.73,-1.11l-0.67,-2.34l-1.1,-1.52l-2.15,-0.96l-0.85,-1.38l-0.94,-0.69l-0.78,-1.45l-0.91,-0.91l-0.6,-1.53l-1.41,-1.5l0.64,-2.85l-0.13,-0.85l-2.32,-1.44l0.46,-1.46l-0.01,-1.08l-0.33,-1.55l-1.0,-2.09l-0.57,-3.53l0.9,-2.56l0.01,-1.55l-0.48,-1.52l0.02,-2.19l-1.39,-1.38l0.0,-4.12l-0.26,-0.77l1.19,-1.14l-0.22,-1.91l0.25,-0.92l2.73,-3.76l0.74,-0.61l1.11,-2.37l4.66,-4.06l2.22,-1.17l1.58,-1.93l1.87,-4.21l0.52,-3.48l1.46,-2.55l2.53,0.36l1.76,0.65l0.25,-0.36l3.4,2.05l6.81,3.12l3.05,0.38l3.49,0.88l0.86,-0.07l1.81,1.42l0.75,-0.24l0.28,-0.38l-0.11,-0.54l0.6,0.16l3.21,1.77l2.63,0.74l0.44,-0.2l3.1,1.65l2.24,2.12l1.54,0.85l1.06,1.69l7.47,6.54l0.33,0.77l1.43,0.41l2.53,2.16l0.64,1.06l-0.27,1.23l0.6,0.43l2.01,-1.77l1.2,0.54l0.54,0.96l-2.76,4.28l0.03,1.47l0.4,0.37l0.6,-0.32l0.54,-0.93l0.57,-1.94l2.11,-1.78l0.87,0.63l1.21,1.89l2.18,1.25l2.26,2.1l0.89,1.31l1.5,6.51l-0.33,1.34l-1.91,1.37l-0.89,1.13l-0.01,0.47l0.45,0.15l2.11,-0.67l1.15,-0.61l0.64,-0.84l0.53,-2.04l-0.28,-4.87l0.65,-1.95l1.05,-0.2l1.97,1.65l0.0,0.74l1.13,1.9l0.28,1.83l0.66,0.89l-0.25,0.37l0.26,0.94l0.51,0.66l1.03,0.2l-0.69,0.86l-0.09,1.41l1.97,3.43l0.18,0.84l-0.06,2.16l-0.64,2.0l-0.3,0.58l-2.4,1.19l-1.04,0.92l-1.71,3.75l-2.1,2.32l-1.35,0.82l-0.13,1.42l-0.89,0.24l-0.67,0.68l-0.88,2.96l-1.99,3.34l-0.78,0.63l0.04,0.74l-4.48,7.35l-0.56,0.42l-1.13,0.12l-0.77,0.72l-0.64,1.51l-1.27,1.01l-0.24,0.8l0.31,2.33l-2.32,4.42l0.33,0.96l-1.39,1.05l-2.96,7.18l-1.09,0.6l0.67,2.17l-2.13,3.57l-1.21,1.07l-1.71,2.4l-5.24,2.87l-0.69,0.6l-0.84,1.79l-2.17,0.93l-2.57,-0.42l-1.92,-1.13l-5.09,0.79l1.41,-1.11l-0.13,-0.65l-2.09,-1.75l-0.88,-1.21l-0.87,-0.45l-0.43,0.08l-1.13,1.43l-2.74,1.58l-2.14,0.34l-1.52,-0.76l-2.22,-0.27l-2.22,-0.96l-0.87,-0.06l0.23,-1.21l-0.6,-0.69l-0.75,-0.14l-1.33,0.55l-1.05,1.47l-1.27,0.26l-0.98,1.22l-1.24,-0.04l-0.49,1.63l-0.46,0.33l-1.97,0.97l-0.71,-0.32l-1.16,0.23l-0.87,2.01l-0.86,0.39l-1.72,-1.68l-2.66,0.42l-1.6,-0.21l-2.03,-1.41l-3.41,-0.59l-1.86,-1.51l0.34,-0.7l-0.6,-0.59l-1.7,-0.92Z", 1, "departement"], ["href", "cpn/region/Ile_de_france"], ["data-name", "\u00CEle-de-France", "data-code_insee", "11", 1, "region"], ["data-name", "Paris", "data-info", "<img src=\"assets/cpnimages/regions/flag/il.png\"/>", "data-department", "75", "d", "M472.43,160.67l2.91,-0.96l1.28,0.25l1.79,1.42l0.99,0.02l1.16,-0.44l1.27,0.83l1.39,0.28l2.4,-1.49l4.01,0.91l5.72,-1.07l1.57,0.83l2.35,-1.11l1.82,-0.03l2.03,-1.04l3.48,0.96l0.98,1.05l0.87,1.67l-0.52,2.79l0.73,1.41l8.88,6.73l0.87,0.26l1.23,-0.5l0.48,0.14l0.61,2.97l0.7,1.03l3.28,1.81l2.3,-0.04l-0.11,1.1l-0.51,0.67l-2.55,0.08l-0.94,0.42l-0.16,0.75l1.09,1.07l-1.54,1.52l-0.27,1.55l0.54,0.66l1.24,-0.02l1.67,1.83l0.7,2.85l-0.5,1.9l0.28,1.65l0.9,0.56l2.22,-0.3l1.49,1.83l-2.45,1.85l-1.06,1.42l-0.92,2.34l-0.48,0.4l-2.35,0.42l-0.63,0.51l-0.08,0.73l0.94,1.41l-1.79,1.84l-0.05,0.79l0.65,1.23l-0.56,2.16l0.16,0.69l-2.61,2.88l-2.24,-0.49l-6.28,0.91l-4.68,-0.48l-2.32,0.9l-3.26,0.0l-0.8,0.78l-1.14,3.17l0.16,2.32l0.83,1.42l-1.58,2.76l-3.77,2.32l-0.95,3.37l-5.9,1.83l-1.36,-0.07l0.46,-1.24l-0.4,-0.63l-0.91,-0.28l-2.7,0.45l-1.78,1.66l-1.87,0.81l-8.32,-0.58l-3.25,0.65l-1.6,-0.18l-0.78,-0.59l0.23,-0.94l0.72,-0.24l1.65,0.3l0.91,-0.65l0.07,-0.64l-0.51,-1.19l0.65,-0.98l0.11,-1.36l-1.39,-1.54l-3.36,-1.92l-1.3,-3.89l-1.33,-1.28l-2.27,0.3l-2.83,-1.36l-0.86,0.16l-1.36,1.23l-1.4,0.35l-0.76,-0.38l-0.47,-1.03l-0.68,-0.61l-0.85,-0.52l-0.84,-0.03l-0.66,0.4l-1.38,1.92l-1.99,0.8l-8.7,1.71l-0.72,-0.42l-0.0,-0.42l1.65,-1.87l0.02,-0.74l-1.91,-1.77l-0.62,-2.84l-0.52,-0.9l-1.62,-1.5l-0.35,-2.59l-0.81,-0.45l-2.11,0.38l-4.37,-1.8l-0.92,-0.66l-1.17,-1.71l0.01,-2.65l-0.44,-1.51l-0.7,-0.87l-2.25,-1.46l-0.79,-2.45l-3.87,-2.43l-1.32,-1.5l-0.63,-1.5l-1.41,-0.91l0.0,-0.93l1.6,-2.06l0.18,-1.09l-0.28,-0.53l-1.69,-0.85l-0.41,-0.65l-0.06,-1.41l0.53,-2.41l-0.31,-1.9l-1.49,-1.94l-3.28,-5.55l-0.4,-2.82l-2.11,-0.85l0.25,-2.13l-0.72,-1.71l0.41,-0.57l7.7,-1.56l2.46,-1.13l3.15,-8.76l1.58,-2.72l0.43,0.35l0.8,2.03l0.97,0.67l3.39,0.3l5.65,1.41l6.54,-0.78l5.01,-2.69l3.66,2.09l2.47,0.26l3.97,1.5l1.83,0.1l3.18,-1.39l11.5,5.26l0.91,0.03l0.87,-0.37l1.31,1.53l1.38,0.99Z", 1, "departement"], ["data-name", "Centre-Val de Loire", "data-code_insee", "24", 1, "region"], ["data-name", "Cher", "data-info", "<p>n'est pas disponible </p>", "data-department", "18", "d", "M352.12,269.71l-0.39,-0.79l0.59,-1.42l1.29,-1.16l5.17,-2.45l1.63,-1.53l1.57,-4.34l0.07,-3.55l0.45,-1.34l2.91,-2.22l0.88,-1.62l-0.43,-1.98l0.6,-3.59l-0.6,-0.63l-1.31,-0.12l-0.88,-2.08l2.47,-0.44l0.71,-0.46l0.07,-1.0l-1.12,-0.94l0.07,-0.52l3.99,-2.03l0.86,-0.92l-0.08,-0.73l-0.51,-0.52l-3.66,-0.66l-2.36,-1.54l1.12,-0.96l0.25,-1.0l-1.86,-3.44l0.07,-2.76l-0.31,-0.7l-0.92,-0.77l0.03,-1.1l0.42,-0.63l2.29,-0.4l5.6,-2.3l2.13,-1.95l0.67,-1.64l1.22,-1.8l-0.16,-0.83l-1.74,-2.11l0.08,-3.7l-3.65,-3.73l-2.55,-1.62l-1.61,-2.81l-0.36,-3.23l1.82,-1.67l1.34,-0.77l5.76,-1.41l2.04,-0.07l0.74,-0.36l1.7,-2.02l3.48,-0.35l0.63,-0.53l0.22,-1.53l3.0,1.67l3.49,-0.02l1.93,0.63l4.71,-2.07l1.42,-1.16l0.69,-1.87l0.95,-1.4l1.34,-1.03l2.49,-1.16l1.05,-1.01l0.26,-1.64l-0.52,-1.68l0.12,-0.44l2.22,-1.39l3.12,5.29l1.41,1.78l0.26,1.59l-0.56,2.83l0.31,1.68l0.56,0.67l1.6,0.81l-0.36,1.11l-1.35,1.63l-0.2,0.71l0.26,0.98l1.48,0.99l0.55,1.38l1.5,1.71l3.83,2.4l0.73,2.38l2.38,1.58l0.52,0.67l0.38,1.69l-0.03,2.29l1.38,2.04l1.1,0.79l4.59,1.88l2.43,-0.21l0.24,2.38l2.15,2.42l0.66,2.91l0.56,0.84l1.26,0.74l-1.59,2.0l-0.13,0.8l0.28,0.63l1.27,0.64l8.96,-1.77l2.18,-0.89l1.74,-2.19l1.08,0.42l1.34,1.84l0.89,0.32l1.9,-0.44l1.41,-1.25l0.41,-0.04l2.61,1.32l2.42,-0.25l1.05,1.47l1.06,3.38l3.56,2.09l1.09,1.21l-0.82,1.86l0.48,1.68l-2.36,-0.11l-1.14,1.09l0.09,1.33l1.07,0.83l2.02,0.26l3.17,-0.65l8.35,0.59l2.29,-0.94l1.66,-1.6l0.83,-0.26l2.03,0.05l-0.41,1.53l0.32,0.38l1.06,0.2l7.05,-1.97l1.76,0.12l2.27,0.58l1.52,1.02l1.38,4.33l1.05,1.28l2.81,2.18l0.52,2.18l-1.42,3.21l-0.46,0.6l-2.42,1.45l-1.42,1.43l-1.36,0.14l-0.46,0.42l0.1,2.67l0.95,2.06l-0.78,1.4l0.13,1.67l-0.42,0.96l-4.41,2.06l-4.17,0.46l-1.07,0.84l-0.36,1.76l0.56,1.49l2.33,0.9l2.32,2.0l1.13,2.08l-0.6,1.62l0.07,0.78l2.52,3.57l-3.82,0.47l-2.79,1.57l-0.07,0.91l1.55,1.67l1.57,3.11l1.42,1.89l-0.51,2.82l-0.86,1.93l-2.02,2.99l-0.1,0.83l0.94,1.66l1.73,0.94l3.6,3.47l0.78,3.4l1.43,3.27l1.23,3.95l-0.07,3.7l2.22,2.27l0.43,5.38l-1.31,4.71l1.12,2.83l-0.12,0.92l-2.12,3.69l-0.31,1.64l-3.08,-0.17l-2.72,0.77l-4.06,2.3l-1.68,2.09l-2.02,-0.32l-1.56,1.24l-2.13,-0.62l-0.64,-0.74l-0.84,-0.12l-4.12,3.34l-1.44,0.06l-0.57,0.31l-0.14,1.87l-2.29,0.13l-0.55,1.04l1.04,1.73l-0.51,1.0l0.05,1.2l1.67,2.1l0.3,1.38l-5.07,1.34l-7.99,0.81l-3.73,1.66l-1.58,1.88l-1.55,0.74l-2.19,3.55l-5.83,-0.29l-3.22,0.59l-2.11,-0.72l-3.44,0.0l-2.44,-0.84l-5.56,-0.15l-1.36,0.29l-1.79,1.06l-1.02,-0.37l-1.84,-1.89l-0.73,-0.15l-0.91,0.28l-0.52,0.62l-0.16,2.28l-0.72,0.98l-3.85,-0.24l-2.6,1.5l-1.18,-0.45l-0.12,-1.0l-0.46,-0.52l-0.8,-0.24l-1.55,0.21l-2.31,-0.53l-1.04,0.29l-3.98,3.58l-1.69,0.94l-2.46,-1.47l-0.93,-1.4l-0.84,-0.54l-0.84,-0.04l-0.83,0.37l-0.7,1.07l-1.07,0.49l-3.45,0.3l-2.25,-0.75l-0.71,-0.49l0.93,-3.22l-0.72,-1.01l-1.32,-0.68l-1.11,-1.06l0.45,-2.58l-0.5,-1.16l-0.91,-1.04l-1.23,-0.68l-5.0,-0.37l-0.32,-1.41l-0.46,-0.57l-3.41,-1.35l-2.07,-1.8l-0.45,-0.65l-0.51,-2.09l-0.01,-1.46l0.61,-2.86l-0.92,-2.42l-1.83,-2.14l-2.6,-1.8l-7.16,-9.22l-1.26,-3.31l-1.36,-1.88l-6.25,-3.03l-0.71,0.68l0.16,1.39l0.73,1.37l-0.4,0.35l-5.41,0.31l-1.21,0.29l-1.78,1.09l-2.67,-0.46l-2.71,0.24l-1.01,-0.19l-0.92,-0.81l-0.25,-0.98l0.04,-2.48l0.47,-2.57l-0.35,-1.03l-2.01,-0.4l-0.72,-1.12l-0.61,-0.29l-3.43,0.61l-0.29,-0.38l0.35,-1.68l-0.32,-1.32l-0.99,-0.39l-1.98,0.41l-0.5,-1.99l-0.74,-0.2l-1.7,0.66l-1.69,-2.39l0.88,-5.76l1.43,-5.26l4.43,-6.2l-0.2,-2.84l0.98,-2.98l1.96,-3.88l-0.45,-3.6l1.51,-3.81l7.61,1.68l0.98,-0.08l0.62,-0.83l-0.48,-2.31l0.7,-0.71l2.9,0.89l2.04,-1.63l2.39,-0.48l3.32,-1.45l2.28,-0.47l0.53,-0.73l-0.22,-1.26Z", 1, "departement"], ["data-name", "Bourgogne-Franche-Comt\u00E9", "data-code_insee", "27", 1, "region"], ["data-name", "C\u00F4te-d\u2019Or", "data-info", "<p>n'est pas disponible </p>", "data-department", "21", "d", "M572.78,248.31l2.08,2.76l1.34,0.84l0.91,0.04l2.45,-1.72l13.22,-1.52l1.29,-0.82l0.21,-1.86l1.83,-1.16l3.72,0.47l2.38,-0.98l2.0,1.2l3.68,0.69l0.55,0.46l-0.52,1.12l0.41,0.79l2.84,0.99l0.59,0.52l-1.02,1.96l0.47,1.65l1.15,0.43l0.67,-0.37l0.54,-0.83l0.47,-0.03l3.02,3.37l2.43,4.0l0.05,1.03l-2.6,0.55l-0.58,0.46l-0.24,0.88l1.41,1.97l0.37,2.77l0.83,1.89l1.58,0.62l1.84,-1.46l0.41,0.07l2.15,2.8l2.67,1.55l2.02,-0.25l1.44,-1.65l0.75,-0.28l0.74,1.67l1.78,1.46l2.45,1.22l0.68,2.08l0.94,0.42l0.76,-0.18l2.08,-1.37l1.1,1.03l1.19,-0.18l2.67,-4.97l0.7,-0.81l0.64,-0.2l3.99,-0.6l3.21,1.65l1.53,0.3l6.58,-1.86l0.7,-0.44l0.69,-0.83l0.2,-0.97l-0.19,-2.21l0.55,-2.48l-0.58,-2.14l0.19,-0.51l0.63,-0.82l6.42,-3.94l2.41,-2.91l2.03,-0.75l0.4,-0.92l-0.24,-0.87l1.66,-0.91l1.8,-2.61l0.31,0.88l-0.17,1.53l0.84,0.59l3.53,-2.14l2.66,-2.84l1.54,-0.3l2.78,0.53l1.12,0.68l0.83,1.02l-0.06,1.28l0.31,0.67l3.72,1.77l2.64,-0.19l2.39,-1.21l3.0,-0.19l2.26,0.8l5.28,2.92l1.38,0.35l2.17,-0.03l1.59,-0.57l1.93,-2.36l0.72,-0.14l3.61,3.45l3.4,1.51l6.19,4.28l1.5,0.2l0.14,1.19l0.91,0.99l8.6,4.3l0.93,0.78l0.84,1.51l0.15,2.71l-0.51,1.44l-1.54,2.09l0.01,1.47l0.98,0.9l2.57,0.38l3.76,3.82l0.41,1.01l0.0,1.19l-0.38,1.14l-2.02,-0.53l-3.13,0.45l-2.79,-0.68l-2.25,0.79l-0.29,0.46l0.15,0.87l0.88,1.86l-1.14,1.05l-2.47,0.75l-0.32,0.37l-0.08,1.24l-1.37,0.7l-1.02,1.11l-1.15,2.35l0.42,0.52l7.29,-0.62l1.04,-0.41l2.04,1.73l-0.29,0.52l-1.73,0.73l-0.94,1.05l-0.92,0.22l-1.15,1.02l-0.52,1.79l0.21,1.82l-4.0,2.53l-1.78,1.58l-1.19,1.68l-3.91,3.12l-1.76,0.53l-0.28,0.41l0.06,1.05l-0.9,0.44l-0.31,0.51l-1.37,0.42l-0.72,0.57l-0.9,1.31l0.59,1.61l-1.2,1.49l-3.98,2.62l-6.53,1.77l-3.04,1.51l-1.11,2.94l1.07,2.32l0.08,1.66l-1.81,4.3l0.94,1.51l-0.16,1.55l-1.18,1.03l-2.12,1.0l-2.15,1.96l-4.43,2.12l-8.29,6.58l-0.92,1.27l0.3,1.23l1.2,1.17l-1.94,2.13l-3.09,4.33l0.33,2.88l-3.79,4.35l-2.1,1.94l-1.03,1.64l-4.26,3.58l-2.33,0.56l-7.03,-0.44l-0.45,-0.65l-0.22,-1.3l-0.66,-0.88l-3.08,-2.47l-0.52,-0.06l-2.8,2.33l-0.57,1.15l-1.28,0.24l-1.18,1.22l-1.45,0.59l-0.82,0.73l-1.63,-0.33l-0.55,-0.58l0.17,-3.09l-0.55,-0.51l-1.61,-0.3l-0.9,-1.08l-2.54,-0.97l-1.18,-3.01l-1.25,-0.94l-1.43,-0.41l-0.41,-0.65l-0.02,-2.61l-0.31,-0.39l-4.28,-1.02l-1.53,-0.8l-2.16,-1.97l-1.58,-0.52l-2.05,0.18l-3.7,1.25l-1.28,0.13l-3.18,-1.12l-1.8,-0.3l-1.05,0.23l-0.72,0.64l-1.5,3.9l-0.84,4.18l-1.47,2.56l-0.15,1.93l-3.53,6.7l-1.32,4.86l0.08,0.87l-2.89,-0.32l-0.98,-0.75l-0.98,-2.54l0.04,-1.79l-1.73,-1.29l-0.18,-1.86l-0.76,-0.6l-1.87,-0.55l-1.12,0.62l-0.53,1.93l-1.03,0.26l-2.35,-2.01l-0.72,0.04l-1.62,0.92l-1.03,0.08l-2.29,-0.51l-2.25,-0.92l-0.92,0.11l-0.68,0.9l-1.27,4.46l-1.74,2.06l-1.51,1.09l-1.62,0.57l-1.65,0.02l-4.83,-1.24l-3.08,0.65l-4.17,-0.93l-4.25,1.28l-1.42,-0.08l-2.06,-1.47l-2.77,-1.06l0.11,-2.73l-0.6,-1.7l5.26,-2.58l1.52,-1.33l0.08,-0.97l-0.86,-1.29l0.04,-1.38l-0.52,-1.25l0.64,-3.0l-0.8,-3.17l-2.08,-1.35l-4.18,-0.89l-2.7,-2.24l-1.72,-0.07l-3.83,-1.94l0.37,-3.1l-0.38,-1.88l-1.07,-0.72l-1.57,-2.64l-2.41,-2.14l-0.51,-1.65l-1.52,-2.44l-2.0,-0.46l-0.44,0.26l-0.43,1.96l-1.36,0.87l-0.44,1.79l-3.8,2.0l-1.31,0.28l-0.45,-0.19l-0.06,-1.53l-1.67,-2.57l-1.59,-0.43l-0.82,0.13l-1.33,1.43l-0.97,0.48l-2.64,-1.07l-1.82,-0.29l-1.87,0.11l-3.04,1.06l-3.34,-2.82l-1.29,-0.36l-1.77,0.04l-2.77,-1.63l-1.05,-1.28l-0.19,-0.83l0.34,-2.43l2.35,-4.46l-1.16,-3.32l1.31,-4.66l-0.44,-5.56l-2.26,-2.43l0.11,-3.58l-1.26,-4.05l-1.41,-3.21l-0.84,-3.54l-3.63,-3.57l-1.85,-1.04l-0.71,-1.21l2.92,-5.34l0.56,-3.19l-0.17,-0.62l-1.7,-2.2l-1.25,-2.57l-1.44,-1.56l0.08,-0.29l2.27,-1.1l2.94,-0.21l1.27,-0.38l0.25,-0.54l-0.6,-1.33l-2.08,-2.61l0.57,-2.21l-1.26,-2.45l-2.54,-2.22l-2.26,-0.85l-0.21,-1.48l0.17,-0.7l0.75,-0.56l4.07,-0.42l4.77,-2.29l0.53,-0.86l-0.08,-2.09l0.82,-1.64l-0.97,-2.16l-0.15,-2.18l1.55,-0.25l1.46,-1.47l2.5,-1.52l2.08,-4.18l-0.63,-2.78l-2.95,-2.35l-0.89,-1.09l-1.46,-4.45l-1.91,-1.27l-3.81,-0.73l0.73,-2.78l3.72,-2.26l1.64,-2.66l0.15,-0.86l-0.86,-1.48l-0.18,-1.43l1.15,-3.5l0.41,-0.36l3.03,0.06l2.27,-0.89l4.61,0.48l6.23,-0.91l2.51,0.47l0.72,-0.38l0.87,-1.43l1.29,-1.21l0.68,0.48l0.65,2.62l0.7,0.31l1.89,-0.13l2.82,1.56l5.42,6.18l0.37,0.67l0.39,2.99l-1.82,2.13l0.14,1.13l2.19,0.89l3.06,2.04l1.02,0.12l1.09,-0.29l0.78,-1.18l1.07,0.24l1.04,2.12l1.93,1.93l4.55,6.82l-1.58,0.69l-0.06,1.02l2.02,1.14l2.25,-0.31l0.24,0.47l-0.05,2.31l0.83,1.24l2.41,0.53l2.82,-0.18l1.96,0.66l0.71,-0.08l1.68,-1.15l1.71,0.44l2.04,-0.07l2.04,-1.61l0.63,0.05l1.22,0.92l0.82,0.12l0.66,-0.82l0.03,-1.6Z", 1, "departement"], ["href", "cpn/region/Normandie"], ["data-name", "Normandie", "data-code_insee", "28", 1, "region"], ["data-name", "Calvados", "data-info", "<img src=\"assets/cpnimages/regions/Normandie.png\"/>", "data-department", "14", "d", "M215.0,114.74l0.19,0.68l0.67,0.4l0.84,-0.17l1.4,-0.93l2.84,0.51l1.15,-0.47l3.33,-3.1l2.74,-0.72l1.03,-0.08l7.56,1.27l2.02,5.19l-1.5,1.01l-0.48,1.56l-1.27,0.04l-1.1,0.67l-0.37,2.34l1.83,2.99l0.97,2.27l4.63,4.95l0.53,1.09l0.2,1.62l-0.78,2.24l0.19,0.81l0.44,0.3l1.7,-0.5l1.69,1.31l1.46,-0.25l0.31,-0.93l-0.57,-0.85l0.33,-0.79l0.91,-0.85l0.91,-0.36l7.94,0.35l7.3,2.35l10.46,0.54l4.16,0.79l2.31,-1.02l4.08,1.27l4.12,0.0l2.91,1.4l3.03,0.6l5.69,2.93l0.43,-0.04l1.48,-1.14l9.72,-1.95l2.93,-1.18l1.7,-1.25l1.45,-0.37l2.06,-2.35l3.94,-2.42l15.26,-2.94l2.9,-1.27l2.26,-2.14l0.01,-0.51l-0.5,-0.1l-4.29,2.28l-4.93,0.6l-4.99,-0.57l-4.35,-1.26l-3.7,-0.45l-1.09,-0.56l-0.79,-1.28l-0.73,-0.42l0.09,-1.68l3.15,-4.92l2.42,-6.93l3.76,-2.82l9.31,-3.67l0.73,-0.8l7.56,-3.82l2.18,-0.58l2.37,-1.37l4.68,-1.45l5.32,0.04l25.22,-6.87l1.56,-1.02l1.59,-0.51l2.41,-2.26l6.63,-4.5l8.53,5.05l10.02,8.92l0.82,1.05l0.92,3.13l2.44,4.07l0.21,1.13l-2.03,1.49l-2.12,2.89l-0.43,1.1l0.07,0.84l0.35,0.34l2.53,-0.43l0.4,0.34l-0.82,1.37l-1.94,1.09l-0.47,0.89l0.74,1.37l0.19,3.89l0.54,1.79l0.44,3.63l0.8,0.82l2.01,-0.46l0.65,0.53l-0.4,1.31l-1.05,0.52l-0.44,0.58l-1.34,3.49l0.3,3.4l1.76,5.64l1.98,4.52l-0.91,-0.06l-0.81,-0.99l-1.21,-0.74l-0.85,0.03l-0.88,0.48l-0.92,1.37l0.38,1.6l-1.77,3.08l-2.9,8.4l-2.17,1.01l-7.75,1.57l-0.75,0.55l-0.17,1.05l0.69,1.52l-0.24,2.26l0.57,0.74l1.67,0.42l0.21,2.28l-2.16,1.15l-0.44,0.6l-0.21,0.94l0.52,1.61l-0.14,1.15l-0.79,0.76l-2.47,1.15l-1.51,1.17l-1.09,1.61l-0.67,1.82l-0.53,0.5l-4.94,2.31l-1.82,-0.62l-3.38,0.04l-2.24,-1.49l-1.25,-0.15l-0.48,0.48l-0.06,1.17l-0.31,0.42l-3.47,0.34l-2.42,2.37l-1.92,0.04l-5.91,1.45l-1.53,0.88l-2.02,1.86l0.26,3.84l0.66,1.39l1.17,1.83l2.61,1.67l3.16,2.99l0.37,0.83l-0.39,1.76l0.17,1.43l1.87,2.7l-1.12,1.47l-0.63,1.57l-1.95,1.77l-5.36,2.18l-2.34,0.41l-0.55,0.48l-0.55,1.79l1.37,1.79l-0.05,2.88l1.83,3.34l-0.85,1.08l-2.22,0.71l-2.49,-1.01l-2.47,-2.98l-1.91,-1.6l-3.5,0.2l-5.14,-1.24l-0.71,-0.36l-0.06,-1.74l-0.56,-0.94l-3.09,-0.02l-2.45,-1.44l-1.01,-1.58l-0.49,-1.75l0.16,-5.51l-0.43,-0.91l-1.29,-0.88l-2.69,-1.04l-1.85,-0.2l-5.49,1.1l-3.21,1.67l-1.67,2.12l-2.54,0.86l-0.71,1.68l-2.54,-0.46l-3.9,0.33l-0.22,-3.58l-0.39,-0.79l-0.77,-0.81l-1.89,-0.44l-3.02,-2.76l-0.65,-1.6l0.49,-1.46l-0.32,-0.81l-0.74,-0.61l-3.82,-1.27l-0.99,0.47l-0.03,1.62l-0.34,0.71l-1.15,0.57l-1.4,-0.02l-1.63,0.51l-1.2,1.45l-0.68,0.11l-4.11,-0.95l-3.98,-0.16l-1.2,0.44l-2.14,1.99l-1.05,0.56l-5.18,1.05l-1.18,-1.31l-0.96,-0.14l-3.52,0.92l-0.5,1.08l-0.81,0.48l-1.68,-1.15l-1.86,0.23l-1.74,-2.89l-1.1,-0.85l-2.45,0.29l-2.37,-0.8l-3.57,1.1l-2.0,0.01l-3.01,-0.43l-2.56,-1.35l-7.85,-1.68l-1.41,0.23l-2.22,2.41l-1.99,0.71l-2.78,1.91l-1.65,0.53l-2.09,-0.17l-2.56,-1.81l-2.2,-3.13l-0.99,-2.96l-2.54,-5.28l4.98,0.51l6.48,-1.24l2.14,0.69l0.44,-0.14l0.0,-0.47l-1.13,-1.5l-1.5,-0.64l-2.34,-0.18l-0.46,0.44l-0.7,-1.34l-2.87,-0.97l-0.88,-2.35l-2.17,-1.65l-0.47,-1.69l-0.34,-4.2l-0.74,-1.24l-1.4,-0.44l1.45,-1.15l0.44,-1.03l0.49,-3.71l0.92,-0.11l0.34,-0.9l0.0,-1.07l-1.2,-0.91l0.97,-5.38l0.51,-0.24l1.33,0.54l0.55,-0.37l-0.25,-0.86l-1.33,-0.51l-1.38,0.0l-1.32,0.6l-0.59,1.22l-0.56,-1.29l0.43,-3.35l-0.31,-0.52l-0.67,-0.12l-0.18,-0.32l0.72,-2.62l1.48,-1.06l0.02,-0.68l-0.75,-0.5l-0.56,0.09l-0.66,-2.09l-0.35,-2.8l0.3,-0.47l0.85,-0.05l2.15,0.54l0.5,-0.39l0.0,-0.49l-0.3,-0.39l-2.96,-0.99l-1.32,-0.03l-0.67,1.15l-0.44,-0.44l-0.98,-2.84l-0.71,-0.78l0.42,-0.24l0.06,-0.7l-2.38,-1.94l-0.24,-0.63l0.68,0.0l0.4,-0.4l-0.83,-2.29l-0.82,-0.22l-0.71,1.2l-3.02,-2.14l0.32,-0.66l-0.42,-0.61l-2.16,0.52l-0.37,-0.34l-0.17,-5.35l-1.15,-1.92l-0.39,-1.49l-2.39,-2.79l0.28,-0.81l1.64,-1.27l0.51,-0.95l0.27,-4.14l-1.3,-2.8l-0.63,-0.67l-4.47,-1.27l0.4,-1.53l-0.1,-1.22l0.13,-0.3l1.34,-0.15l2.46,1.07l1.75,-0.2l0.58,0.33l0.27,1.01l0.5,0.53l6.68,0.92l5.27,1.29Z", 1, "departement"], ["href", "cpn/region/haute_de_france"], ["data-name", "Hauts-de-France", "data-code_insee", "32", 1, "region"], ["data-name", "Aisne", "data-info", "<img src=\"assets/cpnimages/regions/flag/haut.png\"/>", "data-department", "02", "d", "M415.84,70.92l0.57,0.0l0.4,-0.4l0.0,-0.59l-0.39,-0.4l-2.26,-0.33l-0.8,-0.54l-1.15,-2.03l-2.54,-1.04l-0.42,-0.47l0.59,-5.43l1.08,-1.83l2.08,0.36l0.47,-0.39l-0.18,-0.83l-3.2,-2.12l1.12,-3.04l0.33,-6.83l1.74,-1.1l0.1,-0.58l-1.61,-2.08l-0.3,-1.01l-0.26,-3.74l-0.64,-3.46l0.26,-2.94l2.0,-4.41l0.34,-1.54l-1.52,-6.24l1.26,-0.84l3.05,-0.71l2.82,-2.96l1.8,-1.42l11.58,-4.22l1.24,0.13l2.03,-1.0l9.83,-1.04l11.16,-3.16l4.16,0.3l5.0,-1.09l3.16,-1.35l0.71,1.42l0.6,3.51l0.63,1.64l3.14,3.26l0.16,1.34l-1.61,1.47l-0.46,0.87l1.16,3.76l-0.52,1.99l1.22,0.87l1.03,1.61l4.25,0.56l0.92,1.61l2.23,1.78l1.46,2.61l1.24,0.91l5.27,2.03l1.32,0.13l0.92,-0.63l1.82,-2.63l2.37,-1.63l7.93,-2.08l1.41,0.36l0.92,0.73l2.64,4.35l0.72,0.59l1.48,0.27l0.38,0.58l0.61,1.69l-0.8,1.48l0.51,2.38l1.6,4.47l0.09,2.5l0.42,1.17l1.9,1.66l3.82,1.44l1.7,-0.16l2.68,-0.82l2.06,-1.38l0.72,-0.05l0.89,0.38l-0.6,1.19l0.12,0.56l0.76,0.47l4.13,0.08l2.56,0.75l1.12,0.9l0.91,1.39l1.05,10.05l2.3,1.94l1.29,-0.74l1.51,-2.44l1.09,-0.6l2.59,-0.16l2.45,0.25l3.52,1.61l1.52,-0.04l3.85,-1.36l1.15,-0.1l1.03,0.41l1.98,1.84l2.53,1.56l1.84,3.13l1.53,0.38l0.59,-0.74l-0.06,-0.72l0.39,-0.14l1.83,1.02l0.14,0.88l-2.69,2.35l-1.79,5.34l-0.17,1.55l1.11,0.78l1.48,-0.45l0.84,0.08l1.74,4.81l-3.86,2.3l-0.55,0.59l-0.59,1.62l0.62,1.5l0.04,0.67l-0.45,0.3l0.22,0.46l3.43,1.56l1.54,0.17l0.08,2.76l1.79,2.83l0.07,1.8l-1.86,3.26l-0.49,2.91l0.31,0.92l0.9,0.74l0.32,1.65l-0.7,1.08l-3.14,2.62l-1.8,1.1l-1.44,2.0l-1.21,0.65l-2.01,0.15l-1.16,1.07l0.06,0.89l1.48,3.98l-0.45,1.42l0.37,2.06l-1.37,1.28l-0.45,0.95l0.11,0.84l1.61,2.67l-1.45,0.55l-0.75,1.27l0.2,0.5l0.68,0.33l0.4,0.93l-0.07,1.86l-0.63,1.32l-0.79,0.0l-1.69,-0.75l-2.03,0.68l-2.88,-1.37l-2.38,-0.13l-1.45,0.91l-0.06,1.82l-4.11,0.52l-5.16,1.25l-3.39,1.78l-0.06,0.98l0.85,1.81l0.67,5.15l0.51,0.49l1.41,0.26l2.36,2.15l-0.2,0.54l-0.95,0.74l-1.13,0.21l-3.3,-0.33l-1.51,0.77l-0.45,1.51l0.35,1.68l-0.15,1.05l-0.98,1.37l-1.47,1.05l-0.1,1.7l0.34,0.61l1.16,0.44l3.19,-0.1l0.6,0.44l-0.02,0.44l-2.28,1.99l-0.73,1.1l-1.76,1.18l-1.05,2.05l-3.83,3.24l-2.02,3.25l-2.26,0.11l-3.0,-1.67l-0.43,-0.63l-0.47,-2.75l-0.76,-0.9l-0.81,-0.01l-0.99,0.47l-0.51,-0.14l-8.73,-6.6l-0.51,-1.18l0.6,-1.73l-0.09,-0.9l-0.97,-1.91l-1.19,-1.29l-3.91,-1.13l-2.27,1.08l-1.86,0.05l-1.84,1.04l-1.74,-0.79l-5.87,1.07l-4.04,-0.91l-0.77,0.16l-1.31,1.18l-0.79,0.15l-2.03,-1.11l-1.93,0.48l-1.66,-1.34l-1.69,-0.39l-2.67,0.99l-1.08,-0.76l-1.25,-1.57l-0.8,-0.23l-1.37,0.39l-10.67,-5.04l-1.18,-0.26l-1.21,0.29l-2.01,1.11l-1.55,-0.09l-3.94,-1.5l-2.4,-0.25l-1.47,-0.68l-1.66,-1.28l-0.74,-0.17l-1.89,0.7l-3.44,2.04l-4.01,0.26l-2.09,0.47l-5.65,-1.4l-2.68,-0.11l-1.23,-0.58l-0.69,-1.87l-0.93,-0.84l-0.28,-0.86l-0.07,-0.43l0.7,-0.94l0.92,-0.28l0.45,0.19l1.28,1.38l0.8,0.35l0.78,-0.08l0.66,-0.55l-0.41,-1.62l-1.62,-3.45l-1.74,-5.57l-0.28,-3.2l1.24,-3.11l1.48,-1.09l0.51,-1.75l-0.83,-1.22l-2.26,0.41l-0.42,-0.32l-0.42,-3.51l-0.53,-1.76l-0.18,-3.85l-0.71,-1.37l2.2,-1.38l0.74,-1.07l0.26,-1.01l-0.65,-0.89l-2.49,0.44l-0.22,-0.26l2.37,-3.65l2.12,-1.58l0.03,-1.31l-2.65,-4.66l-0.9,-3.09l-1.01,-1.33l-10.08,-8.97l-8.3,-4.93l5.17,-5.05l1.41,-4.28l1.94,-2.2l1.85,-0.21l5.97,2.46l0.78,-0.01l0.75,-0.53l0.14,-0.85l-1.42,-0.64Z", 1, "departement"], ["href", "cpn/region/Grand_est"], ["data-name", "Grand Est", "data-code_insee", "44", 1, "region"], ["data-name", "Ardennes", "data-info", "<img src=\"assets/cpnimages/regions/flag/grandest.png\"/>", "data-department", "08", "d", "M522.82,186.37l0.16,-1.02l1.68,-1.84l-0.22,-0.68l-0.89,-0.72l3.35,-0.37l0.76,-1.01l0.2,-1.75l1.48,-2.59l4.29,-3.81l1.07,-2.07l1.68,-1.1l0.81,-1.18l2.06,-1.61l0.39,-0.63l-0.02,-1.16l-0.99,-0.78l-3.39,0.04l-0.83,-0.47l0.06,-1.11l1.31,-0.86l1.17,-1.67l0.21,-0.79l-0.25,-2.97l1.17,-0.73l3.12,0.34l1.45,-0.25l1.33,-1.0l0.36,-0.86l-0.5,-1.26l-1.41,-0.81l-0.87,-0.98l-1.6,-0.47l-0.56,-4.86l-0.91,-2.12l2.95,-1.52l5.1,-1.23l4.31,-0.57l0.52,-0.78l-0.12,-1.35l0.85,-0.54l2.04,0.1l3.04,1.4l2.06,-0.68l2.32,0.84l1.14,-0.78l0.49,-1.35l0.08,-2.0l-0.52,-1.32l-0.68,-0.44l0.36,-0.58l1.56,-0.48l0.31,-0.52l-1.82,-3.65l1.76,-1.97l-0.32,-2.31l0.46,-1.54l-1.6,-4.48l0.75,-0.67l1.86,-0.09l1.55,-0.83l1.47,-2.03l1.72,-1.03l3.2,-2.67l0.94,-1.48l0.01,-1.35l-1.51,-2.11l0.44,-2.67l1.83,-3.2l0.19,-1.43l-0.27,-1.06l-1.76,-2.77l-0.03,-2.56l3.32,-0.4l9.73,2.22l1.91,-0.29l8.49,-3.54l2.64,-0.38l0.81,-0.51l1.03,-2.26l0.49,-2.73l-0.42,-1.4l0.42,-1.14l6.43,-5.47l1.16,-0.45l0.43,1.03l0.89,0.26l1.7,-0.3l0.3,0.34l-0.46,3.48l-0.42,-0.16l-0.65,0.28l-1.15,2.13l-0.07,2.33l-2.32,4.87l-0.3,1.37l0.89,1.44l2.96,1.1l0.83,1.27l-0.04,1.46l-1.51,3.13l0.87,1.86l0.09,3.19l0.8,0.73l1.7,0.19l5.18,-1.02l3.3,2.38l3.4,1.12l3.27,3.72l2.07,1.47l1.72,0.4l3.86,-0.67l2.26,2.11l0.31,0.88l-0.71,0.59l-0.19,1.13l0.55,1.39l1.85,0.16l0.84,-1.02l2.66,0.93l1.03,0.78l2.89,4.41l0.2,1.33l-0.26,1.48l0.69,1.13l1.59,0.26l4.1,-2.17l2.36,0.84l1.04,-0.1l1.27,-1.8l1.33,-0.57l4.11,0.97l2.13,-1.42l2.44,0.88l3.06,3.02l1.61,0.63l3.79,0.76l0.99,0.85l0.81,2.18l0.48,0.25l7.2,-0.85l1.6,-1.26l0.33,-1.21l1.94,-0.22l0.21,-0.79l2.88,-0.27l3.24,0.68l4.79,2.7l1.85,0.15l2.29,-0.93l0.53,0.08l5.52,2.34l0.95,0.78l0.43,1.78l3.49,3.43l-1.28,0.51l-0.34,1.05l0.39,0.92l5.12,4.46l-0.0,1.31l1.48,0.85l1.03,2.86l0.65,0.17l0.76,-0.38l0.24,0.78l-0.46,1.9l1.04,1.55l0.86,0.29l2.67,0.02l2.33,0.73l0.97,-0.14l1.37,-1.65l-0.67,-2.53l0.24,-0.75l2.79,0.42l1.85,-0.14l4.34,1.88l1.09,0.08l-0.03,2.41l0.73,2.3l1.51,1.11l2.29,-0.99l0.3,-0.66l-0.21,-0.77l0.26,-0.22l2.39,1.43l1.98,0.72l5.31,0.06l2.06,0.7l0.67,-0.41l0.98,-1.46l2.37,-0.6l0.98,-1.9l2.23,-0.77l0.85,0.06l0.55,0.9l3.47,0.42l0.22,0.64l-0.49,0.46l-0.0,0.71l3.02,3.51l1.57,0.84l2.62,0.39l1.24,0.55l0.93,1.37l0.84,0.6l1.01,0.16l2.14,-0.55l2.64,0.55l2.77,-0.96l5.0,1.23l4.59,-0.2l9.65,4.37l6.29,1.29l-1.02,1.01l-1.91,3.08l-2.79,6.52l-0.7,1.0l-3.2,1.38l-0.7,0.85l-0.46,1.3l-2.12,0.14l-0.95,0.41l-0.65,2.78l-3.8,3.45l-2.72,1.47l-2.58,3.44l-0.59,2.73l-0.01,2.81l0.72,2.0l-2.23,1.72l-0.47,2.28l-1.12,1.98l-0.77,3.01l-0.07,1.2l0.89,2.96l-0.06,0.91l-0.71,0.98l-2.24,1.43l-0.92,4.51l-4.46,5.68l-0.61,2.23l-1.16,1.77l-0.81,2.75l0.18,3.35l2.65,4.1l0.12,2.21l-2.19,2.41l-0.36,2.63l-1.24,1.91l0.21,3.21l-1.19,0.9l-1.06,3.68l0.75,2.84l-0.03,1.09l-1.25,1.28l-0.34,0.8l0.43,2.7l1.23,0.87l2.11,3.24l1.05,1.06l-0.21,1.33l-3.51,1.37l-2.73,1.91l0.1,0.7l1.12,0.69l0.01,0.32l-1.72,0.61l-0.07,0.53l0.6,1.0l-0.84,0.74l-2.31,-0.79l-0.93,0.18l-0.3,0.87l0.9,1.46l-1.22,1.75l-1.53,0.53l-4.27,-0.15l-4.24,1.21l-0.96,-0.82l-3.02,-1.12l-0.26,-0.95l1.04,-2.07l-0.39,-0.58l-1.82,0.11l0.39,-1.2l0.0,-1.45l-1.12,-2.14l-3.45,-3.28l-0.86,-0.37l-1.8,-0.05l-0.57,-0.53l0.01,-0.77l1.5,-2.01l0.61,-1.75l-0.17,-3.01l-1.03,-1.85l-1.11,-0.92l-8.58,-4.29l-0.62,-0.69l-0.07,-1.18l-0.36,-0.37l-1.57,-0.16l-6.14,-4.25l-3.36,-1.49l-2.89,-3.05l-1.0,-0.52l-0.66,-0.08l-0.72,0.37l-1.82,2.29l-1.32,0.48l-1.98,0.03l-1.09,-0.27l-5.28,-2.92l-2.42,-0.87l-3.37,0.19l-1.11,0.31l-1.36,0.92l-2.15,0.18l-3.33,-1.51l-0.16,-1.78l-1.1,-1.33l-1.36,-0.79l-2.91,-0.55l-2.05,0.41l-2.72,2.89l-3.08,1.95l0.04,-2.28l-0.66,-0.67l-0.65,0.03l-0.83,0.72l-1.26,2.11l-1.69,0.8l-0.26,0.52l0.33,0.93l-0.2,0.45l-1.94,0.67l-2.38,2.89l-6.41,3.93l-0.82,1.02l-0.32,1.1l0.57,2.0l-0.54,2.43l0.2,2.14l-0.58,1.27l-6.85,2.14l-1.15,-0.23l-3.43,-1.72l-4.29,0.62l-1.46,0.81l-2.83,5.28l-0.39,0.11l-0.67,-0.88l-0.77,-0.22l-2.89,1.56l-0.53,-0.69l-0.42,-1.51l-3.74,-2.21l-0.5,-0.48l-0.45,-1.41l-0.55,-0.47l-1.19,0.09l-1.76,1.89l-1.49,0.14l-2.23,-1.34l-2.42,-2.98l-1.06,-0.04l-1.62,1.4l-0.89,-0.42l-0.59,-1.48l-0.36,-2.77l-1.38,-1.79l0.33,-0.6l2.33,-0.37l0.83,-0.93l-0.18,-1.31l-1.18,-2.31l-4.52,-5.41l-1.18,-0.18l-1.09,1.24l-0.51,-0.21l-0.23,-0.95l0.85,-1.35l0.11,-1.07l-1.02,-0.91l-2.65,-0.91l0.44,-1.3l-0.31,-0.7l-0.84,-0.49l-3.68,-0.69l-1.99,-1.23l-2.67,0.98l-2.98,-0.51l-1.76,0.38l-1.47,1.25l-0.22,1.84l-0.81,0.45l-13.17,1.5l-2.39,1.7l-0.5,0.01l-1.12,-0.71l-1.95,-2.64l-0.78,-0.38l-0.55,0.41l-0.26,2.11l-1.42,-0.99l-1.02,-0.13l-0.91,0.39l-1.31,1.25l-1.75,0.05l-1.93,-0.43l-1.85,1.2l-2.25,-0.63l-2.87,0.17l-1.97,-0.36l-0.57,-0.86l0.08,-2.22l-0.52,-0.94l-0.98,-0.2l-1.67,0.35l-1.41,-0.73l1.56,-0.67l0.12,-0.94l-4.65,-7.0l-1.91,-1.9l-0.72,-1.79l-0.62,-0.61l-1.22,-0.42l-0.78,0.28l-0.57,1.04l-1.58,0.12l-2.82,-1.93l-2.05,-0.84l1.35,-1.41l0.55,-1.05l-0.39,-3.31l-0.49,-0.96l-5.62,-6.39l-3.0,-1.66l-2.3,-0.01l-0.47,-2.37l-1.12,-0.92l-0.15,-0.51l0.56,-2.27l-0.63,-1.75l1.81,-1.89l-0.93,-1.96l2.55,-0.54l0.75,-0.59l1.02,-2.47l0.98,-1.32l2.66,-1.99l0.06,-0.58l-1.98,-2.34l-2.8,0.05l-0.2,-1.26l0.51,-1.93l-0.76,-3.12l-2.03,-2.25l-1.36,-0.04Z", 1, "departement"], ["href", "cpn/region/paye_de_loire"], ["data-name", "Pays de la Loire", "data-code_insee", "52", 1, "region"], ["data-name", "Loire-Atlantique", "data-info", "<img src=\"assets/cpnimages/regions/flag/pay.png\"/>", "data-department", "44", "d", "M159.66,300.12l3.14,1.13l1.62,-0.32l1.05,-1.07l0.07,-0.47l-0.56,-1.04l-2.21,-2.2l-0.46,-0.11l-0.82,0.44l-0.77,-1.59l-2.05,-1.2l4.12,-2.13l1.57,0.33l-0.09,0.55l0.45,0.48l0.58,0.01l2.59,-1.53l0.4,-0.74l-0.31,-0.53l-2.95,-0.38l0.35,-0.69l-0.16,-0.53l-0.77,-0.68l0.87,-0.21l2.37,0.81l3.21,-0.37l0.81,-0.45l0.88,-1.05l1.08,-1.85l0.93,-0.29l1.83,0.9l2.88,-0.91l0.01,0.96l0.41,0.72l0.57,0.29l0.85,-0.14l0.92,-1.04l0.66,-1.38l2.05,-1.24l0.71,-1.09l0.79,-3.53l0.74,-1.69l0.12,-1.43l-0.24,-0.62l0.35,-1.02l2.91,-1.59l2.21,-0.38l2.07,0.05l5.67,-2.08l1.82,0.61l1.95,0.17l7.4,-2.04l2.87,-1.7l1.19,-1.55l1.73,-1.21l6.3,-2.55l1.6,-1.15l1.76,-0.13l1.82,0.29l1.17,1.44l1.85,0.86l6.14,0.89l0.45,-0.31l0.47,-2.16l1.14,-1.77l2.89,-9.08l0.53,-0.9l1.85,-1.62l4.09,-0.58l1.25,-0.49l0.67,-0.66l-0.07,-4.84l-1.76,-2.27l-0.71,-4.7l0.64,-0.86l0.08,-0.99l-0.82,-4.82l-1.09,-2.36l1.95,-4.01l-0.03,-2.5l-0.74,-5.78l0.2,-5.45l4.76,0.37l3.56,-1.09l2.14,0.78l2.29,-0.33l0.82,0.61l1.89,3.04l0.78,0.28l1.22,-0.43l1.2,1.0l0.96,0.2l0.9,-0.36l0.74,-1.34l3.16,-0.79l0.67,0.27l0.43,0.85l0.82,0.32l5.49,-1.07l1.32,-0.69l2.1,-1.96l0.92,-0.33l3.75,0.17l4.09,0.95l1.06,-0.14l1.34,-1.53l1.33,-0.41l1.5,0.0l1.45,-0.72l0.62,-1.2l-0.17,-1.13l0.3,-0.28l3.98,1.51l0.19,0.36l-0.49,1.44l0.18,1.02l0.67,1.13l3.2,2.92l2.03,0.53l0.36,0.46l0.28,0.51l0.24,3.91l0.44,0.37l3.61,-0.38l3.37,0.46l0.6,-0.45l0.44,-1.41l2.46,-0.81l2.23,-2.53l2.43,-1.15l5.27,-1.07l1.66,0.18l3.55,1.66l0.35,1.07l-0.32,2.68l0.3,3.32l0.87,2.03l1.63,1.4l1.84,0.94l2.82,-0.07l0.41,2.48l1.13,0.63l5.3,1.27l2.3,0.07l0.97,-0.32l1.62,1.39l1.93,2.55l2.19,1.39l1.53,0.31l1.53,-0.36l2.97,1.92l3.09,0.38l0.63,0.45l-0.41,0.52l-4.24,2.22l-0.26,1.28l1.21,1.18l-2.67,0.5l-0.56,0.47l-0.11,1.1l0.84,1.7l0.5,0.5l1.48,0.31l-0.65,3.22l0.42,1.92l-0.15,0.49l-3.53,3.05l-0.56,1.67l-0.05,3.43l-1.51,4.18l-1.33,1.2l-1.74,0.98l-3.44,1.47l-1.46,1.3l-0.79,1.81l0.64,1.76l-0.14,0.45l-2.17,0.43l-3.23,1.43l-2.52,0.52l-1.04,0.58l-0.9,0.99l-1.95,-0.85l-1.07,0.0l-0.78,0.53l-0.36,0.7l0.4,2.57l-8.48,-1.68l-0.46,0.23l-1.7,4.29l0.46,3.58l-1.91,3.72l-1.03,3.13l0.2,2.82l-4.39,6.08l-1.47,5.38l-0.85,5.56l-2.84,-0.47l-1.46,0.44l-2.01,2.22l-0.96,2.4l-2.38,0.81l-1.53,2.05l-1.11,-0.31l-0.83,-2.02l-0.97,-0.76l-0.79,-0.2l-1.88,0.35l-3.55,-0.08l-6.53,0.7l-5.83,1.67l-2.05,-0.73l-2.08,0.46l-1.21,0.8l-0.31,0.62l0.12,0.77l0.69,0.97l-0.13,0.38l-4.92,2.46l-3.85,0.33l-5.2,-0.68l-4.23,0.56l-4.16,-1.9l-0.94,-0.18l-0.84,0.51l-0.29,1.01l0.17,0.47l2.2,1.37l2.18,3.03l1.67,0.89l0.69,0.84l0.32,2.18l1.04,2.25l0.66,0.7l3.5,1.91l1.73,1.57l0.1,0.48l-0.78,1.59l0.1,0.98l3.83,6.52l0.57,3.11l1.44,2.21l0.79,4.91l-1.22,1.54l-0.25,0.91l0.97,4.27l-0.6,2.86l0.25,1.23l0.83,0.72l1.04,-0.23l1.97,1.38l0.71,1.02l-0.74,0.77l-1.26,-0.1l-1.22,0.34l-2.25,2.0l-2.76,0.16l-3.62,1.68l-4.06,-2.24l-1.7,-0.27l-6.67,1.09l-0.44,-0.27l0.9,-1.15l0.15,-0.7l-0.48,-0.9l-0.77,-0.24l-6.96,1.54l-1.16,0.7l-1.12,1.42l-0.75,-0.72l-2.95,-0.11l-2.27,1.19l-0.16,2.27l-1.02,-0.24l-2.4,-2.73l-1.56,-0.33l-4.08,-2.32l-3.43,0.87l-1.24,-0.06l-0.97,-3.42l-1.1,-1.51l-1.55,-0.91l-6.9,-0.76l0.18,-1.01l-1.0,-0.83l-3.36,-0.58l-1.97,-1.51l-2.47,-0.81l-1.08,-2.24l-0.58,-0.83l-0.45,-0.15l-0.28,0.39l0.27,2.62l-0.46,-0.56l-1.32,-5.47l0.25,-1.25l-0.25,-0.45l-0.84,-0.31l-1.14,-2.75l-2.53,-2.31l-0.74,-2.09l-0.93,-0.62l-1.98,-0.4l-2.24,-3.59l-3.47,-3.13l-4.07,-2.07l-0.39,-1.05l-0.3,-2.69l0.47,-2.19l1.48,-0.93l0.17,-0.46l-0.28,-0.85l3.12,-1.98l2.48,-5.26l1.17,-1.01l-0.16,-1.45l-0.69,-1.37l-1.7,-1.83l-3.77,-1.84l-7.12,-1.1l-1.45,-0.69l1.47,-1.03l1.91,-0.24l0.8,-0.55l0.42,-2.94l-0.66,-1.35l-0.09,-1.84l1.02,-2.31l1.32,-0.5l5.58,-0.5l1.1,-0.5l1.01,0.86l3.14,0.71l1.04,-0.02l0.24,-0.51l0.65,0.2l-0.47,0.4l0.19,0.91l2.51,1.59l2.62,0.48l1.97,1.62l1.54,0.57l3.31,0.09l0.4,-0.29l-0.19,-0.46l-1.37,-0.76l-2.42,-0.42l-2.85,-2.6l-4.32,-2.68l-2.78,-0.52l-1.38,-0.93l-2.33,-0.59l-8.74,0.54l-2.06,1.13l-1.5,1.94l-1.74,0.44l-1.54,1.27l-1.02,0.36l-2.05,-0.36l-2.32,-2.13l-1.22,-0.61l-2.17,0.0l-0.78,0.6l-0.33,0.97l-1.39,-0.03l-4.87,-2.21ZM161.53,297.57l0.02,0.14l-0.01,0.02l-0.01,-0.17ZM179.99,325.41l2.68,1.47l0.24,2.88l-0.47,-0.24l-2.28,-3.55l-1.56,-0.93l-1.75,0.59l-0.73,-0.9l-0.93,-2.89l-0.56,-0.68l2.62,-0.12l2.02,0.71l-0.04,2.78l0.75,0.89ZM174.33,345.92l-4.93,-0.12l-0.49,-0.98l0.24,-0.36l1.99,0.02l3.18,1.44Z", 1, "departement"], ["data-name", "Bretagne", "data-code_insee", "53", 1, "region", "region-53"], ["data-name", "C\u00F4tes-d\u2019Armor", "data-info", "<p>n'est pas disponible </p>", "data-department", "22", "d", "M25.63,242.25l0.98,-1.98l0.98,0.49l19.62,-3.77l3.65,0.88l0.91,-0.32l0.5,-0.76l0.62,-2.51l-0.36,-1.04l-0.81,-0.89l-0.58,-2.39l-1.01,-0.81l-2.92,-0.69l-0.66,-1.14l-0.5,-0.27l-1.37,0.44l-3.11,-1.71l-2.01,0.46l-2.96,4.7l-0.73,0.21l0.57,-1.39l-1.14,-2.33l0.91,-1.36l-0.39,-1.22l-1.09,-0.38l-3.21,-0.11l-0.03,-1.0l1.84,0.17l0.95,-0.24l0.41,-0.81l-0.22,-0.93l0.98,-2.21l0.53,-0.43l0.46,0.19l-0.71,2.55l0.23,0.48l1.87,0.72l5.36,-0.32l1.36,0.83l1.04,0.18l3.66,-0.78l2.04,-0.12l0.21,0.35l1.46,0.44l5.01,-0.91l0.33,-0.98l-0.42,-0.4l-1.83,0.11l-0.45,-0.19l-0.2,-0.74l-0.68,-0.16l-1.09,0.67l-1.22,-1.03l-2.07,0.18l0.59,-0.25l0.44,-0.73l1.79,-1.5l0.1,-0.44l-0.38,-0.25l-1.24,0.08l-1.29,0.8l0.0,-0.47l-0.85,-0.4l-0.96,1.04l-0.96,-0.2l-1.87,1.11l-0.5,0.08l-0.43,-0.77l-0.56,-0.05l-1.09,0.68l-1.02,0.2l3.24,-3.36l6.46,-3.39l0.19,-0.5l-0.47,-0.25l-4.22,1.6l-4.19,0.6l-5.77,2.77l-2.94,0.62l-1.85,0.8l-1.08,0.12l-4.35,-1.11l-0.58,0.4l-0.15,1.12l-0.27,0.13l-3.57,-0.23l-0.68,-1.74l0.68,-0.06l0.36,-0.44l-0.1,-1.01l-1.05,-2.18l-0.09,-1.34l0.28,-1.34l0.6,-0.89l1.01,-0.27l0.3,-0.88l-1.49,-1.16l0.52,-1.93l2.34,-3.42l1.07,0.45l0.46,-0.14l-0.19,-0.71l0.54,-0.32l2.86,-0.47l3.08,1.22l1.04,-0.16l0.21,-0.65l-1.67,-2.31l0.37,-0.22l2.65,0.59l1.24,-0.03l0.38,-0.34l-0.26,-0.44l-1.23,-0.43l0.07,-0.55l-0.35,-0.44l4.17,-0.0l3.43,-1.04l1.35,-0.03l0.38,-0.31l-0.2,-0.45l-0.86,-0.45l1.38,-0.23l3.24,-1.36l1.36,0.05l0.84,0.86l-0.39,1.52l0.42,0.5l1.58,-0.18l2.35,-1.07l2.95,0.58l0.4,-0.4l0.0,-0.59l-1.04,-0.57l0.11,-0.28l1.91,-1.43l2.45,-0.6l1.27,0.07l1.03,0.52l0.71,-0.39l0.36,0.69l0.75,0.24l1.01,-1.05l-0.02,-1.18l4.45,-1.51l-0.24,1.62l0.55,0.4l-0.25,0.84l1.04,1.07l-0.07,1.9l0.48,-0.03l1.68,-1.43l0.9,-0.29l-0.28,0.38l0.1,0.75l3.35,2.74l0.61,-0.01l0.45,-0.54l-0.24,-0.79l0.28,-0.74l-0.54,-1.41l0.79,0.35l0.21,-0.4l0.01,-2.72l0.24,-0.43l0.93,0.16l0.45,-0.85l1.48,0.59l2.89,-0.11l2.82,1.61l2.57,-0.38l0.28,1.21l1.61,0.42l1.96,-0.05l0.87,-0.41l0.21,-0.49l-0.71,-3.08l2.98,-0.46l0.33,-0.33l-3.03,-3.0l-0.19,-0.98l2.29,-1.26l1.58,-2.51l0.68,0.39l1.86,0.1l2.17,1.06l-0.28,0.54l0.32,0.6l2.25,0.04l1.71,-1.23l5.84,-2.09l1.17,0.0l1.67,-1.78l0.55,0.11l0.26,2.26l-0.4,1.28l-0.83,1.08l0.5,1.21l0.41,0.18l0.97,-1.9l1.78,-1.89l1.89,-1.51l0.81,0.06l1.06,-1.09l0.51,-0.13l0.85,0.83l-0.4,1.97l0.68,0.68l-1.05,2.42l-1.65,2.02l-0.1,0.85l0.4,0.4l0.8,-0.2l3.03,-4.81l0.76,0.15l1.33,-0.56l0.67,0.24l-0.02,0.75l-1.71,1.0l-0.01,1.15l2.48,1.16l4.01,0.41l0.01,1.2l-0.8,0.72l0.21,0.84l1.94,1.27l1.37,1.73l3.87,2.85l0.88,3.1l-0.26,0.69l0.22,0.77l2.78,1.74l3.18,1.32l-1.16,1.99l1.46,0.52l1.88,1.65l0.62,-0.24l0.25,-2.13l2.16,0.1l1.16,-0.4l3.22,-3.61l5.89,-2.95l0.99,-1.12l0.0,-0.53l-0.84,-0.45l1.49,-0.39l1.05,0.13l0.44,0.89l0.49,0.2l3.51,-1.61l2.1,-2.09l0.67,0.82l1.02,0.23l-1.01,1.46l-1.68,1.58l0.05,0.62l1.51,0.76l3.43,-2.2l0.62,2.37l0.78,0.42l0.63,2.14l0.57,0.3l1.25,-1.02l0.75,0.88l1.35,-0.4l0.22,-0.89l-0.64,-0.57l0.93,-0.86l1.16,0.66l0.55,-0.01l0.02,-0.55l-1.01,-1.64l2.26,-0.68l2.61,-0.22l1.31,2.11l0.42,1.64l1.16,0.92l-0.26,1.06l1.54,2.0l0.32,0.9l-0.23,1.64l0.21,0.41l0.46,-0.06l2.43,-1.55l0.01,-0.69l-1.38,-0.83l0.13,-0.69l-0.92,-1.66l0.91,0.63l0.48,-0.04l0.09,-0.48l-1.1,-1.52l-1.85,-0.63l-1.61,-3.1l0.13,-0.41l0.98,-1.09l1.07,-0.34l0.37,-1.34l0.28,-0.13l2.22,0.0l0.28,-0.7l4.99,-0.86l0.54,1.16l-1.35,1.87l-0.06,2.08l1.7,1.73l2.67,0.63l4.54,0.04l4.83,-0.64l3.02,-1.39l2.6,5.45l1.04,3.06l1.91,2.9l3.27,2.38l2.44,0.21l1.93,-0.61l2.78,-1.91l2.08,-0.77l2.22,-2.4l2.39,0.03l6.14,1.5l2.32,1.25l-0.2,5.67l0.77,8.15l-1.95,4.1l0.21,1.05l0.9,1.52l0.8,4.72l-0.73,1.74l0.74,4.88l0.57,1.17l1.18,1.08l0.09,4.44l-1.36,0.63l-4.18,0.61l-2.21,1.9l-0.63,1.08l-2.9,9.1l-1.14,1.77l-0.41,1.91l-6.47,-1.09l-0.75,-0.44l-0.77,-1.22l-1.29,-0.55l-3.34,-0.0l-1.83,1.23l-6.34,2.57l-1.86,1.31l-1.2,1.55l-2.58,1.54l-7.31,2.01l-3.72,-0.8l-4.42,1.41l-1.22,0.67l-2.07,-0.04l-2.38,0.42l-3.35,1.92l-0.46,1.34l0.13,2.04l-0.71,1.59l-0.73,3.38l-0.54,0.87l-2.15,1.35l-1.11,2.03l-0.68,0.15l-0.2,-1.32l-0.56,-0.55l-0.75,-0.02l-2.38,0.9l-1.56,-0.86l-0.9,0.05l-0.9,0.52l-1.08,1.85l-1.37,1.25l-3.37,0.29l-1.9,-0.8l-1.35,0.35l-0.47,0.66l-0.66,-0.06l-0.41,-0.22l0.4,-1.16l-0.67,-1.08l0.25,-0.19l3.7,-0.17l2.26,-0.53l1.21,0.07l0.42,-0.34l-0.3,-0.45l-0.99,-0.24l-2.8,0.23l-5.66,-1.72l-2.91,0.66l-1.93,0.0l1.05,-1.04l0.47,-1.05l-0.12,-0.48l-0.5,0.01l-0.86,0.73l-2.73,0.25l-0.32,0.4l1.11,0.66l-1.57,-0.11l-0.6,-0.37l-0.61,0.32l-0.09,1.63l-0.48,0.24l-1.89,-0.59l-1.16,0.04l-4.35,1.05l-1.51,-0.37l-2.24,-2.86l-1.34,-0.5l-0.54,-0.67l1.53,-0.13l1.13,1.56l1.15,-0.56l0.69,0.46l0.65,-0.25l0.21,-0.93l0.66,0.6l1.69,0.45l1.67,-0.14l1.46,-0.78l-0.13,-0.88l2.17,-2.6l0.37,-1.63l-1.48,-1.88l-0.44,-0.11l-0.26,0.86l0.64,0.9l-1.1,0.64l0.6,0.74l-0.39,0.02l-1.13,-0.82l0.39,-0.14l-0.02,-0.47l-0.84,-1.0l-0.86,-0.45l-1.89,-0.29l-0.44,0.26l0.31,0.55l-3.56,0.47l-0.85,0.64l-0.55,0.91l-2.13,0.65l0.0,-0.48l-0.46,-0.4l-1.32,0.49l-0.19,-1.21l0.7,-1.13l-1.61,-0.61l-0.47,-1.47l-0.85,-0.33l-0.4,0.46l0.45,2.1l0.01,1.94l0.27,0.88l1.3,1.72l-0.77,0.06l-1.84,-1.77l-0.48,-0.01l0.02,1.33l-0.45,-1.98l-0.51,-0.31l-0.67,0.4l-0.04,1.56l-1.22,-0.46l-1.86,0.87l-0.79,0.06l0.11,-0.88l-1.36,-0.9l-0.9,-0.02l-0.6,0.64l-1.75,-2.35l-1.96,-1.13l0.78,-2.82l4.43,-1.77l0.2,-0.57l-0.36,-1.98l-0.6,-0.34l-0.71,0.3l0.01,-1.53l-0.4,-0.43l-0.73,0.2l-0.33,0.57l-1.35,-0.45l-0.97,0.16l-0.33,0.36l1.51,1.12l0.5,1.38l-0.92,0.03l-0.89,0.76l-0.75,0.18l-0.19,0.67l0.52,0.61l-0.59,0.38l-0.66,1.45l-2.06,-1.94l-1.14,-0.34l-0.21,-0.79l-2.13,-0.87l-1.87,-0.17l0.38,-0.4l0.08,-1.06l0.95,0.1l0.65,-0.32l1.28,-2.54l0.88,-0.82l-0.25,-0.95l-0.44,0.09l-2.95,2.66l-1.78,0.9l-1.23,0.25l-0.32,0.34l0.22,0.41l0.92,0.77l-4.61,0.99l-0.63,-0.17l-2.02,-2.33l-2.01,-3.11l-0.6,-0.18l-0.57,0.4l-0.83,-0.02l-2.13,-0.46l-2.06,0.0l-1.4,-1.03l-2.45,0.05l-0.95,-0.29l-0.44,-0.91l-0.74,-0.41l-3.29,1.44l-0.96,-0.68l-3.48,0.2l-1.22,-1.97l-0.89,-0.77l0.11,-1.0l-0.37,-0.51l-1.34,-0.44l-0.26,-0.87l-1.15,-1.13l-1.72,-1.0l-1.75,-0.32l-0.44,0.53l0.51,1.46l-0.36,0.96l-0.24,0.22l-0.49,-0.49l-1.37,0.27l-0.79,0.62l-1.44,-1.0l-2.43,-0.67l-0.69,-0.82l-1.04,-2.67l-0.42,0.16l-0.7,1.27l0.43,1.31l1.4,1.55l-1.0,0.23l-0.97,0.66l-0.91,-1.34l-1.28,0.04l-0.48,1.28l0.73,0.75l-0.22,0.89l0.4,0.4l1.27,0.0l-1.81,1.5l-2.27,0.79l-8.52,-0.35l-0.07,-1.48l1.2,-0.18l0.52,-0.76l-0.21,-1.72l-1.54,-4.18l-0.96,-1.47l-2.99,-3.31l-6.1,-3.99l-1.18,0.45l-1.3,1.04l-1.84,-1.46l-3.15,-0.06l-3.8,-0.92ZM122.84,278.42l0.11,0.08l-0.08,0.32l-0.03,-0.4ZM122.52,282.02l0.23,1.3l1.65,2.47l-0.96,-0.23l-0.59,0.29l-0.71,-2.5l0.37,-1.33ZM133.84,278.57l0.45,0.38l-0.04,0.12l-0.41,-0.51ZM123.36,297.99l1.01,0.42l1.66,-0.07l0.33,0.33l-0.17,0.99l-1.32,0.33l-4.68,-0.38l-3.08,-0.8l-0.79,-0.48l0.14,-1.16l-1.41,-3.37l0.57,-0.67l4.02,1.8l3.72,3.05ZM1.37,208.95l2.5,-0.92l1.47,0.95l-2.48,1.11l-0.61,-1.01l-0.88,-0.14Z", 1, "departement"], ["href", "cpn/region/nouvelle_aquantine"], ["data-name", "Nouvelle-Aquitaine", "data-code_insee", "75", 1, "region"], ["data-name", "Charente", "data-info", "<img src=\"assets/cpnimages/regions/flag/nouvel.png\"/>", "data-department", "16", "d", "M204.81,598.27l0.63,0.4l0.52,-0.08l1.38,-0.84l5.64,-0.92l1.36,-1.72l2.57,-1.46l2.17,-3.85l5.18,-6.42l0.5,-2.5l1.48,-4.27l2.51,-12.23l1.46,-4.3l2.15,-9.72l3.95,-21.25l1.68,-10.56l0.42,-4.7l-0.62,-3.76l1.59,-2.05l1.5,-4.83l1.04,-1.14l1.32,-0.04l3.84,0.81l2.32,-0.34l1.43,-1.11l0.02,-1.15l-1.01,-1.12l0.28,-0.48l-0.7,-1.19l-4.63,-3.83l-1.79,-0.89l-0.92,0.36l-3.56,5.22l-0.53,2.34l3.34,-33.36l2.55,-12.04l1.09,-15.98l3.2,-5.75l1.08,-0.08l-0.29,2.7l1.44,2.01l4.22,2.89l3.51,1.53l7.32,7.13l1.65,2.15l1.65,6.53l1.42,7.99l2.13,4.49l2.13,2.43l2.35,2.0l2.44,1.49l0.52,1.41l-0.17,1.23l0.35,3.2l0.54,1.31l0.97,0.73l0.45,-0.0l0.16,-0.42l-0.81,-4.06l0.53,-2.18l-0.8,-1.78l-1.38,-1.23l1.66,0.3l2.68,1.57l0.51,-0.03l0.06,-0.51l-1.72,-2.53l-5.08,-1.82l-2.05,-1.4l-1.05,-1.86l-0.76,-2.33l-2.32,-14.51l-0.81,-2.98l-2.08,-4.88l-2.62,-4.07l-2.95,-2.95l-8.61,-5.47l-0.48,-1.77l-2.47,-1.03l-3.22,-2.81l-4.04,-1.44l-2.76,-2.39l-2.34,0.33l-0.67,-0.24l0.99,-5.73l3.71,-0.99l0.98,0.04l3.79,3.91l1.46,1.04l4.4,1.81l0.48,-0.15l-0.05,-0.5l-1.38,-1.36l-5.52,-3.9l-1.51,-1.58l-1.36,-4.22l2.03,-1.93l0.85,-0.46l1.1,-0.0l0.61,-0.43l0.19,-1.83l-0.98,-1.77l0.89,-0.77l-0.02,-0.68l-0.92,-0.54l-1.09,-2.34l1.55,0.32l1.08,-0.72l0.78,-1.42l-0.03,-0.9l-1.1,-1.07l-1.08,-0.37l-0.71,-3.23l-1.9,-1.16l0.55,-0.47l-0.04,-0.64l-1.96,-1.09l0.07,-0.62l-0.38,-0.5l-2.85,-0.2l-0.39,-0.36l0.27,-0.97l1.24,-1.31l-0.29,-1.3l0.52,-0.79l4.3,-2.4l0.5,-0.98l-0.01,-0.83l-0.87,-2.37l1.16,-1.51l0.86,-0.51l6.69,-1.49l0.44,0.35l-0.06,0.32l-1.0,1.63l0.99,0.9l1.99,-0.09l4.86,-0.96l1.46,0.21l4.42,2.32l3.79,-1.72l2.86,-0.19l2.34,-2.04l2.77,-0.34l0.86,-1.23l-0.25,-0.98l-0.76,-0.79l-2.09,-1.47l-1.48,-0.05l-0.2,-0.92l0.59,-2.98l-0.97,-4.09l1.47,-2.55l-0.66,-4.53l-1.62,-2.86l-0.6,-3.2l-3.78,-6.41l-0.05,-0.67l0.8,-2.07l-0.78,-1.19l-1.4,-1.1l-3.53,-1.93l-0.48,-0.54l-0.88,-1.94l-0.43,-2.43l-0.91,-1.07l-1.53,-0.78l-2.22,-3.06l-2.05,-1.29l0.13,-0.56l1.47,0.22l2.4,1.36l1.33,0.38l4.31,-0.56l5.13,0.68l3.99,-0.34l1.28,-0.41l1.37,-1.03l2.73,-1.24l0.34,-1.13l-0.81,-1.37l1.0,-0.85l1.73,-0.42l2.13,0.74l5.97,-1.69l6.45,-0.7l5.88,-0.12l0.56,0.42l1.0,2.22l1.71,0.55l0.45,-0.18l1.48,-2.08l2.51,-0.92l1.0,-2.45l1.89,-2.08l0.9,-0.26l2.97,0.49l1.47,2.34l0.64,0.37l0.98,-0.07l1.14,-0.55l0.13,1.56l0.57,0.55l2.68,-0.26l0.19,0.85l-0.35,1.75l0.25,0.7l0.48,0.33l1.15,0.07l2.51,-0.61l1.03,1.31l1.83,0.26l-0.34,5.65l0.36,1.29l1.37,1.15l1.25,0.23l2.61,-0.25l2.91,0.46l1.93,-1.14l1.04,-0.24l5.49,-0.33l0.79,-0.44l0.19,-0.71l-0.77,-1.6l-0.1,-0.95l5.63,2.78l1.17,1.63l1.3,3.39l7.23,9.31l2.64,1.85l1.67,1.93l0.84,2.19l-0.61,2.63l0.01,1.65l0.57,2.3l0.55,0.83l2.33,2.03l3.29,1.27l0.85,2.04l0.88,0.33l3.79,0.0l1.03,0.35l0.88,0.74l0.67,1.18l-0.49,1.9l0.16,1.06l0.74,0.86l1.78,1.01l0.54,0.76l-1.01,2.95l0.15,0.46l1.04,0.72l2.54,0.83l3.65,-0.3l1.07,-0.33l1.0,-1.33l1.05,-0.24l1.47,1.8l3.13,1.64l2.0,-1.13l3.93,-3.54l0.68,-0.15l2.17,0.53l2.02,-0.06l0.36,1.35l0.7,0.52l1.01,0.19l1.02,-0.2l1.87,-1.33l3.01,0.41l0.9,-0.2l1.03,-0.98l0.25,-2.59l0.71,-0.54l0.76,0.32l1.92,1.93l0.96,0.16l2.07,-1.12l1.08,-0.24l5.41,0.14l2.45,0.84l3.5,0.01l2.22,0.72l3.19,-0.59l5.83,0.29l2.01,2.5l1.67,1.44l0.91,2.78l0.59,0.64l0.74,0.43l2.88,0.63l0.67,0.63l1.08,2.07l0.77,0.02l0.86,-0.61l1.19,0.76l2.45,3.45l1.01,3.07l2.11,3.67l-0.39,3.35l0.22,1.7l2.06,4.03l-0.15,3.39l-1.62,1.78l-1.56,3.24l-2.36,2.46l-1.64,-0.11l-1.29,0.59l-1.33,1.29l-1.33,0.73l-0.92,1.43l0.25,0.94l1.58,1.96l0.73,2.07l0.5,0.47l1.47,0.35l0.84,0.76l1.85,2.04l0.51,1.09l-0.24,3.82l-0.55,0.67l-1.67,0.69l-0.46,0.57l-0.73,2.83l0.51,1.34l1.35,1.7l0.25,4.1l1.11,1.0l-1.14,4.37l-0.87,1.76l-2.16,-0.09l-3.12,-1.6l-1.18,-0.01l-1.52,0.59l-0.21,0.82l0.95,0.93l0.15,0.84l-1.24,2.6l-1.06,0.64l-1.96,2.84l-3.1,1.35l-0.66,2.04l-2.65,2.08l-0.16,1.81l0.96,1.75l-0.04,1.26l-2.76,5.43l-2.8,2.03l-0.55,0.87l-0.03,0.76l1.6,2.88l-0.03,0.57l-4.22,1.17l-4.52,0.2l-1.94,0.88l-1.38,-0.66l-0.83,-0.05l-4.06,1.88l-3.12,0.95l-1.23,-0.04l-3.73,-1.98l-1.24,-1.82l-2.13,-1.68l-3.07,-1.73l-1.98,-0.53l-2.06,0.56l-0.77,-1.17l-0.7,-0.11l-1.4,1.03l-1.03,0.25l-3.64,2.43l-0.29,1.46l1.3,3.78l-0.03,0.72l-0.72,1.24l-0.01,2.07l-2.54,2.21l-1.66,2.62l-2.23,0.65l-0.87,1.1l-0.29,0.94l0.72,1.44l-0.03,0.57l-1.25,1.78l-4.31,2.66l-2.39,0.56l-0.98,0.82l-3.33,5.17l-3.48,2.85l-2.93,1.29l-0.86,0.82l-0.26,0.78l1.31,2.09l0.06,1.85l0.83,2.15l2.02,2.25l-0.18,3.0l-0.59,0.58l-4.81,0.36l-1.13,-0.2l-1.77,-0.98l-0.77,0.08l-0.72,0.57l-0.27,2.47l-0.74,1.55l-0.06,0.88l0.85,1.37l2.45,0.96l0.36,0.51l-2.45,5.11l-2.16,1.72l0.1,0.72l1.14,0.8l-0.03,0.97l-1.25,0.21l-2.22,-0.7l-1.4,0.32l-0.49,0.48l-0.46,1.35l-2.26,1.33l-1.04,2.84l-3.57,1.87l-1.05,0.18l-2.71,-2.15l-1.51,-0.47l-5.15,1.53l-2.7,-0.26l-1.21,0.8l-4.35,1.91l-1.3,0.2l-3.0,1.53l-4.83,-1.62l-0.8,0.09l-1.07,0.84l-1.29,1.99l-1.07,0.78l-0.59,0.02l-0.58,-0.74l-0.76,-0.21l-2.4,0.81l-0.9,1.56l0.26,2.33l-0.72,0.77l-2.48,-0.06l-1.1,-0.37l-0.37,-0.73l0.78,-1.23l-0.06,-1.08l-0.91,-0.88l-1.22,-0.35l-1.33,0.44l-1.59,1.05l-1.8,1.72l-1.77,0.13l-2.38,-0.3l-2.89,1.82l-0.59,0.71l-0.12,1.04l1.31,1.37l1.05,2.2l-1.49,3.87l-0.05,2.41l0.82,1.36l-1.48,3.09l-1.21,0.16l-0.79,0.5l-0.11,0.65l0.54,1.41l-1.07,1.59l-0.25,1.25l0.98,0.76l0.47,1.52l0.52,0.63l2.33,-0.0l5.59,1.04l1.06,0.66l0.67,1.47l1.56,1.16l2.16,4.89l-0.31,0.26l-1.15,-0.07l-0.56,0.49l0.03,2.28l0.52,0.69l0.76,0.17l1.43,-1.29l0.75,0.02l-0.11,3.16l1.06,3.23l-0.48,0.48l-1.62,0.52l-0.96,1.06l-0.61,2.64l0.6,2.08l-0.4,0.75l-1.97,1.89l-1.1,3.23l-1.55,0.13l-1.74,2.98l-2.28,1.29l-0.98,1.23l0.15,3.83l-0.4,0.28l-2.98,0.33l-2.69,2.77l-0.17,0.81l0.39,1.61l-0.03,1.69l-1.67,2.29l-0.35,1.23l0.55,3.77l-0.88,-0.07l-0.78,1.18l-0.86,-0.35l-4.31,2.79l-1.69,0.12l-3.22,-1.34l-1.7,-0.07l-0.41,0.38l-0.09,1.49l-1.58,0.74l-0.62,-1.55l-1.55,-0.25l-0.08,-0.92l-2.44,-2.91l-3.24,-2.0l-1.85,-0.11l-0.47,-0.41l-0.25,-0.49l0.25,-1.36l-1.4,-2.86l-1.82,-0.51l-4.68,0.75l-4.79,-0.35l-2.24,-0.65l-2.95,-2.22l-3.08,-0.49l-2.02,-1.09l-2.32,0.15l-4.04,-2.44l-3.6,-0.59l-1.04,-0.56l-0.92,-0.94l-0.21,-0.75l0.38,-0.75l1.21,-0.93l-0.15,-0.71l-1.6,-0.32l-1.53,0.56l-1.05,1.06l-0.53,1.66l-0.16,1.94l-0.41,0.78l-0.81,0.24l-3.0,-0.46l-1.26,-0.64l-1.02,-1.03l-0.63,-1.32l3.7,-3.88l0.95,-2.41l0.66,-2.96l-0.35,-3.1l-2.03,-1.72l-2.04,0.3l-2.51,-1.16l-3.21,-0.56l-1.3,0.75l-0.75,1.77l-1.7,0.25l-0.8,-0.46l-0.49,-1.68l0.17,-0.84l-0.85,-0.76l-1.84,-0.58l-3.76,1.0l-0.43,-1.87l-0.91,-1.03l-1.52,-0.66l0.07,-2.41ZM309.11,602.32l1.15,-0.9l0.67,-2.47l-0.61,-1.33l-1.64,-0.44l-1.19,0.8l-0.49,2.49l0.63,1.45l1.49,0.4ZM307.84,608.3l0.94,-0.4l1.08,-1.94l-0.6,-1.74l-1.63,-0.95l-0.46,0.07l-1.45,1.45l-0.2,1.38l0.25,1.08l0.52,0.68l1.55,0.37ZM235.41,400.43l1.09,0.41l1.35,-0.28l0.64,0.57l0.15,2.37l0.68,0.73l-0.16,1.32l2.38,1.46l0.35,0.53l-0.41,3.62l-0.61,1.91l-0.67,0.91l-0.62,-0.25l-0.84,-2.53l-1.25,-2.19l-3.59,-3.54l-1.32,-0.83l-2.14,-2.59l0.33,-1.57l-1.72,-3.8l1.1,0.13l1.99,0.91l3.27,2.7ZM219.58,381.29l3.07,-0.67l0.94,0.18l0.51,0.48l-0.09,0.29l-1.95,-0.5l-0.35,0.62l1.41,1.8l1.75,-0.06l1.83,-0.69l0.25,0.77l1.02,0.43l6.19,0.88l1.32,0.74l1.5,1.94l-0.96,0.35l-3.3,-0.75l-5.87,-3.31l-3.6,0.51l-0.76,-0.27l-2.9,-2.73Z", 1, "departement"], ["data-name", "Occitanie", "data-code_insee", "76", 1, "region"], ["data-name", "Ari\u00E8ge", "data-info", "<p>n'est pas disponible </p>", "data-department", "09", "d", "M296.09,638.61l-0.69,-4.39l0.23,-0.9l1.78,-2.58l-0.28,-3.88l2.39,-2.45l2.79,-0.25l0.84,-0.59l0.32,-0.83l-0.41,-1.39l0.02,-1.75l0.72,-0.92l2.37,-1.37l1.67,-2.92l1.05,0.11l0.6,-0.32l1.2,-3.37l1.88,-1.77l0.55,-1.02l0.07,-0.67l-0.62,-1.7l0.51,-2.21l0.68,-0.76l2.02,-0.75l0.47,-0.89l-1.07,-3.38l0.21,-2.73l-0.21,-0.77l-0.56,-0.5l-0.61,-0.06l-0.78,0.37l-1.02,1.09l-0.38,-0.27l-0.07,-1.71l1.65,-0.19l0.44,-0.8l-0.43,-1.63l-1.92,-3.78l-1.59,-1.2l-0.74,-1.54l-1.23,-0.77l-5.85,-1.11l-1.94,0.13l-0.8,-1.93l-0.88,-0.6l1.33,-2.32l-0.5,-1.87l1.41,-0.23l0.84,-0.72l1.33,-3.17l-0.83,-1.53l0.05,-2.22l1.48,-3.42l-0.0,-0.64l-0.91,-2.1l-1.51,-1.7l0.06,-0.26l3.02,-2.18l2.14,0.33l2.17,-0.22l1.91,-1.79l2.5,-1.32l1.3,0.7l0.06,0.52l-0.83,1.47l0.06,0.63l0.61,0.78l1.48,0.53l2.83,0.04l0.81,-0.49l0.46,-0.83l-0.21,-2.54l0.68,-1.02l1.79,-0.56l0.96,0.87l1.31,-0.01l1.3,-0.95l2.0,-2.61l0.42,-0.08l4.36,1.63l0.81,-0.02l3.09,-1.56l1.3,-0.2l4.46,-1.96l1.1,-0.75l2.5,0.3l5.09,-1.52l1.7,0.75l2.19,1.82l1.56,-0.16l3.92,-2.03l1.14,-2.97l2.17,-1.22l0.77,-1.7l1.0,-0.23l2.2,0.69l1.76,-0.35l0.51,-0.77l-0.02,-0.8l-0.33,-0.62l-1.03,-0.71l2.05,-1.55l2.29,-4.41l0.28,-1.14l-0.68,-1.14l-2.14,-0.75l-0.89,-1.21l0.8,-1.93l0.2,-2.29l0.6,-0.27l1.68,0.95l1.42,0.26l5.01,-0.38l1.18,-1.19l0.12,-3.52l-1.84,-1.84l-0.96,-2.38l-0.09,-1.96l-1.19,-1.7l0.07,-0.49l0.56,-0.53l2.99,-1.33l3.6,-2.94l3.41,-5.27l0.78,-0.63l2.29,-0.5l4.57,-2.86l1.44,-2.18l-0.02,-1.02l-0.61,-0.82l0.11,-0.81l0.64,-0.86l2.37,-0.76l1.69,-2.66l2.13,-1.79l0.74,-1.12l-0.14,-1.73l0.68,-1.13l0.08,-1.03l-1.31,-3.89l0.24,-0.96l3.33,-2.09l1.05,-0.26l1.04,-0.9l1.16,1.27l2.28,-0.55l2.62,0.91l4.01,2.82l1.3,1.87l1.92,0.91l1.31,1.01l0.85,0.2l1.52,0.03l3.21,-0.98l3.94,-1.84l2.17,0.72l1.98,-0.89l4.23,-0.19l0.61,1.12l0.9,5.86l3.12,4.71l1.22,2.47l-0.85,4.57l-0.66,1.67l0.13,0.4l1.71,1.0l1.23,3.98l0.91,0.71l0.84,0.16l0.43,-0.29l0.49,-1.7l5.01,-1.51l1.0,0.08l1.12,1.11l0.62,0.08l1.36,-0.63l3.34,0.11l1.98,-0.21l1.25,-0.59l3.91,-4.25l0.61,-2.45l2.43,-3.52l0.63,-2.44l7.94,-7.42l2.16,2.45l0.35,2.78l0.85,0.31l2.55,-0.29l2.18,2.96l0.8,2.12l1.02,0.73l1.85,0.66l-1.03,0.82l-0.2,0.72l0.58,1.36l0.58,3.63l1.38,2.09l2.07,1.57l0.62,-0.19l0.85,-2.42l1.89,-2.92l0.36,-2.65l1.38,-4.58l0.47,-0.79l1.76,-1.22l0.98,-3.72l0.74,-0.4l1.59,1.82l1.21,-0.05l1.38,-0.79l3.42,-3.06l2.41,-0.28l3.68,-2.03l1.93,-0.24l0.36,0.35l0.79,2.6l3.5,5.9l1.31,1.29l1.23,0.42l1.33,-0.63l3.37,-0.36l1.07,-0.91l0.56,-2.31l0.89,-0.32l1.69,0.16l0.84,0.67l-0.48,1.5l0.3,0.89l4.51,0.95l4.08,3.38l2.89,1.45l1.57,0.41l-0.03,2.85l1.28,3.55l0.51,3.07l0.72,2.11l0.66,1.17l1.36,0.88l0.8,1.32l2.21,6.94l1.77,1.52l1.64,2.61l-0.83,2.07l0.49,3.26l0.42,0.65l0.92,0.24l4.17,-0.59l1.08,1.04l4.93,2.42l1.51,0.3l1.25,-0.53l1.13,-1.2l2.93,-1.99l2.23,-0.62l0.91,0.24l-0.18,1.71l0.35,0.84l1.54,0.89l0.98,-0.13l1.05,-0.78l0.46,-2.02l0.63,-0.49l3.18,-0.06l1.68,0.67l2.19,2.02l1.73,0.78l2.57,0.61l0.57,1.69l1.23,1.54l0.1,1.58l1.9,1.83l0.18,2.73l-0.33,2.14l0.45,2.79l0.62,0.47l1.22,-0.4l1.2,0.21l4.51,5.33l-0.09,0.53l-1.09,-0.09l-0.55,0.26l-0.07,2.45l-1.05,1.25l-1.67,1.19l-2.8,1.16l-5.36,3.57l-0.17,0.92l0.54,4.15l-1.95,4.79l0.01,1.9l-0.43,1.1l-4.47,-1.25l-2.03,0.57l-1.49,1.04l-1.68,2.12l-1.37,2.92l0.45,1.06l1.58,0.26l-0.52,1.33l-2.32,0.57l-0.61,1.38l-1.54,0.44l-3.46,2.46l-2.02,0.48l-1.44,1.06l-1.17,0.31l-1.37,3.01l-2.27,-0.34l-2.81,-1.63l-0.8,-1.47l0.85,-1.47l-0.07,-0.6l-1.92,-1.42l-3.63,-0.32l-3.46,0.39l-2.41,0.83l-13.11,9.5l-7.26,2.86l-2.05,1.55l-4.61,5.42l-1.04,0.41l-2.52,-1.04l-4.05,0.36l-4.36,1.63l-4.56,3.3l-1.44,1.8l-2.59,2.25l-1.5,2.23l-2.41,2.09l-2.8,6.09l-1.93,6.26l0.17,0.94l0.79,0.58l0.07,1.22l-0.96,8.67l0.17,4.88l-0.34,3.08l0.26,4.02l0.72,5.72l1.02,1.05l4.27,1.24l0.37,0.52l-0.12,1.76l1.98,2.59l0.3,1.34l-2.29,0.36l-3.05,-0.09l-1.83,-0.7l-1.61,-2.13l-2.87,-0.28l-1.16,0.57l-2.17,-0.57l-1.94,1.35l-3.1,0.24l-3.76,2.65l-4.83,-0.08l-1.22,0.53l-2.65,1.86l-0.57,0.72l0.5,1.8l0.52,0.57l-2.26,-0.17l-2.79,-0.85l-1.31,0.01l-0.83,0.63l-0.48,1.11l-0.67,0.22l-2.94,-1.07l-1.08,-0.76l-1.67,-2.44l-2.78,-0.4l-6.16,-2.91l-1.99,0.2l-3.71,1.04l-3.02,-0.29l-1.14,0.54l-1.01,1.86l-2.45,1.95l-0.62,0.36l-2.45,0.36l-0.71,-0.1l-1.77,-1.24l-1.32,-3.75l-1.07,-1.5l-3.53,-0.74l-4.73,-2.58l-4.65,-0.78l-0.31,-0.6l0.18,-1.54l0.62,-1.4l0.95,-0.83l1.41,-0.12l0.28,-0.64l-0.97,-1.12l-2.03,-0.75l0.42,-1.19l-0.34,-0.53l-6.9,-0.61l-4.12,-2.32l-2.77,0.67l-1.86,-0.09l-0.4,0.28l-0.88,2.73l-0.91,0.33l-0.66,-0.77l-1.07,-2.58l-2.23,-3.15l-1.29,-0.49l0.21,-0.83l-0.29,-0.49l-1.53,-0.36l-7.55,-0.02l-2.58,0.53l-0.9,-0.58l-1.64,-3.14l-1.32,-1.06l-1.36,-0.37l-5.14,-0.31l-1.98,-1.15l-1.93,0.82l-1.92,-1.37l-4.63,-2.09l-2.28,-0.07l-4.25,-1.04l-1.85,0.13l-1.67,0.77l-0.3,1.2l0.12,1.49l-0.82,1.99l0.47,0.91l-0.83,1.28l0.24,0.56l0.76,0.24l0.61,0.69l0.36,0.97l-0.05,1.0l-0.39,0.58l-10.29,0.14l-1.18,-0.37l-3.41,0.59l-1.07,-0.19l-0.39,-1.05l-2.06,-1.35l-1.26,0.44l-2.75,3.06l-0.55,0.07l-2.26,-3.03l-4.31,-1.22l-1.14,0.13l-1.23,0.71l-3.75,0.5l-4.68,1.54l-1.99,0.16l-2.26,-0.8l-1.02,-1.0l-1.36,-0.45l-0.27,-0.97l-1.39,-1.16l-0.66,-0.99l-0.32,-1.14l0.13,-1.21l-0.54,-0.42l-2.76,0.82l-1.36,-0.14l-0.79,-1.35l-3.39,-1.83ZM436.08,668.09l0.96,-0.22l0.2,-0.59l-1.32,-2.33l-0.63,-0.79l-0.47,-0.12l-0.9,0.48l-0.67,1.33l0.07,1.16l0.97,0.84l1.79,0.25ZM309.0,601.52l-0.8,-0.16l-0.41,-0.99l0.4,-2.01l0.84,-0.4l0.79,0.28l0.31,0.77l-0.59,2.03l-0.54,0.47ZM307.75,607.5l-0.9,-0.16l-0.53,-1.24l0.07,-0.86l1.14,-1.14l1.03,0.52l0.48,1.4l-0.86,1.31l-0.43,0.15Z", 1, "departement"], ["href", "cpn/region/auvergne"], ["data-name", "Auvergne-Rh\u00F4ne-Alpes", "data-code_insee", "84", 1, "region"], ["data-name", "Ain", "data-info", "<img src=\"assets/cpnimages/regions/flag/auver.png\"/>", "data-department", "01", "d", "M458.84,445.76l1.95,-0.35l3.03,1.58l1.95,0.27l1.37,-0.72l0.73,-1.76l1.22,-4.68l-1.19,-1.15l-0.26,-4.14l-1.44,-1.87l-0.36,-0.94l0.86,-2.7l1.65,-0.68l0.84,-1.03l0.29,-4.28l-0.66,-1.43l-1.92,-2.11l-0.96,-0.86l-1.78,-0.64l-0.67,-1.98l-1.72,-2.4l0.64,-0.91l3.14,-2.29l2.41,-0.16l2.48,-2.58l1.63,-3.33l1.7,-1.93l0.19,-3.74l-0.21,-0.9l-1.86,-3.26l-0.2,-1.44l0.37,-3.62l-2.17,-3.8l-1.06,-3.16l-3.18,-4.2l-1.27,-0.36l-1.13,0.66l-0.76,-1.76l-0.96,-0.91l-3.48,-0.95l-0.4,-0.39l-0.94,-2.84l-1.76,-1.56l-1.91,-2.38l2.1,-3.42l1.46,-0.64l1.48,-1.8l3.52,-1.57l7.94,-0.8l5.53,-1.6l0.14,-1.5l-1.97,-2.78l-0.03,-0.72l0.53,-1.33l-1.03,-1.64l0.3,-0.35l2.27,-0.17l0.41,-0.79l-0.09,-1.21l1.63,-0.12l4.07,-3.3l0.82,0.8l2.29,0.76l0.71,-0.21l1.28,-1.11l2.21,0.26l1.77,-2.17l3.83,-2.18l2.59,-0.73l2.86,0.18l0.27,1.52l1.25,1.52l3.22,1.84l2.72,0.19l2.74,2.56l0.75,0.33l1.03,0.0l2.4,-1.05l1.66,-0.1l1.61,0.25l2.97,1.11l1.41,-0.68l1.06,-1.27l1.82,0.28l1.29,2.19l0.04,1.49l0.5,0.54l0.6,0.12l1.71,-0.33l4.06,-2.15l0.58,-1.97l1.34,-0.87l0.39,-1.8l1.21,0.2l1.4,2.25l0.6,1.79l2.46,2.19l1.56,2.63l0.94,0.54l0.28,1.5l-0.41,3.04l0.32,0.67l4.03,2.05l1.74,0.08l2.67,2.22l3.67,0.71l2.19,1.13l0.73,2.83l-0.64,3.08l0.52,1.3l-0.02,1.46l0.82,1.68l-1.19,1.0l-5.58,2.73l-0.18,0.55l0.71,1.8l-0.19,2.42l0.26,0.72l1.01,0.63l2.01,0.62l2.09,1.49l0.85,0.18l1.12,-0.06l4.06,-1.25l3.56,0.89l3.56,-0.61l4.78,1.24l1.97,-0.03l1.79,-0.63l1.67,-1.19l1.89,-2.21l1.71,-5.14l5.06,1.43l1.25,-0.12l1.94,-0.93l1.54,1.64l0.93,0.37l1.05,-0.11l0.65,-0.45l0.57,-1.97l0.55,-0.28l1.79,0.68l0.27,1.99l1.62,1.08l0.05,2.15l1.07,2.48l1.36,0.98l3.59,0.4l0.43,-0.51l-0.22,-1.1l1.28,-4.71l3.52,-6.66l0.17,-1.98l1.46,-2.52l0.86,-4.24l1.47,-3.8l1.04,-0.4l5.0,1.43l1.47,-0.15l3.7,-1.24l1.74,-0.16l1.25,0.39l2.16,1.97l1.64,0.85l4.11,0.99l0.02,2.41l0.61,1.09l1.69,0.58l0.98,0.74l0.72,2.39l1.08,1.11l2.12,0.65l0.91,1.09l1.74,0.37l-0.08,3.17l0.9,0.94l1.79,0.47l2.93,-1.5l1.13,-1.19l1.49,-0.38l0.56,-1.17l2.38,-2.03l2.83,2.21l0.53,0.68l0.4,1.81l0.7,0.56l7.33,0.5l2.1,-0.36l4.85,-3.81l1.29,-1.9l5.29,-5.36l2.71,1.45l1.49,1.81l0.06,0.64l-2.15,4.4l-0.57,4.01l-1.43,0.34l-1.32,-0.11l-3.71,1.58l-1.69,1.08l-0.18,1.23l1.62,2.13l-1.39,2.81l0.08,0.46l0.46,0.07l1.43,-0.74l2.69,-0.56l2.68,-0.09l2.31,0.77l2.11,-0.97l3.09,-3.14l3.94,-2.28l1.58,-1.45l-0.19,-2.13l-0.68,-0.47l-1.16,0.35l-0.4,-0.18l-0.89,-2.03l4.08,-5.7l3.32,0.16l1.15,1.46l0.93,0.33l0.77,-0.37l0.82,-1.0l4.35,-1.83l1.15,-1.48l11.95,-1.05l5.16,0.78l0.08,0.91l-0.38,1.12l-1.55,1.58l-0.38,1.39l1.23,1.83l2.14,1.97l1.23,1.83l-2.07,3.51l-1.08,2.81l-0.54,2.86l0.83,1.53l4.77,0.91l0.69,0.58l-1.17,1.69l0.25,3.27l1.29,0.49l1.46,-0.85l1.18,0.45l4.32,4.17l1.84,4.74l-1.27,1.06l-1.66,3.08l-1.14,0.85l-2.54,0.9l-1.94,0.05l-0.44,1.16l-1.45,-0.68l-2.83,1.09l-0.84,1.25l-0.34,1.34l0.12,2.79l0.7,3.2l1.31,1.78l1.75,1.17l2.44,0.57l2.19,1.73l2.63,0.83l0.29,0.87l-0.98,2.48l1.03,2.1l0.33,3.09l0.49,1.22l5.55,3.27l1.58,2.67l3.72,1.73l-0.54,1.71l-1.23,1.93l-2.31,2.53l0.24,2.49l0.65,1.55l0.0,0.91l-1.6,1.21l-1.13,1.81l-0.21,0.18l-0.84,-0.84l-0.72,-0.2l-4.4,1.41l-2.78,3.01l-2.44,0.32l-0.53,0.84l0.54,1.12l-1.68,0.71l-4.37,-1.72l-2.37,-0.29l-2.05,1.2l-2.07,0.11l-2.41,1.23l-1.98,0.23l-2.21,1.2l-2.35,0.04l-0.74,0.29l-1.11,2.4l-0.81,0.88l-1.02,0.26l-3.86,-1.22l-0.85,-1.84l-1.43,-1.0l-4.05,-0.41l-1.45,-0.84l-0.51,0.04l-1.75,2.08l-1.05,4.98l0.01,1.51l0.51,1.03l1.24,0.41l3.69,-0.43l0.93,0.46l0.56,3.36l1.75,2.0l0.32,1.03l-0.27,3.17l-0.32,0.52l-3.52,-0.79l-3.8,0.98l-1.71,-0.15l-1.86,0.35l-2.28,-0.4l-3.91,2.12l-0.99,0.14l-1.72,-0.45l-1.28,0.35l-2.59,1.78l-0.33,0.77l0.25,1.63l-0.69,0.66l-2.32,0.66l-3.68,0.21l-1.02,0.38l-2.07,2.9l0.34,1.1l0.66,0.51l0.02,0.62l-1.63,1.7l-4.53,1.21l-3.09,-0.29l-1.24,0.51l-0.43,1.87l-2.53,3.81l-0.14,0.94l0.84,1.52l2.0,1.4l0.28,0.97l-0.33,0.33l-3.19,1.81l-4.16,-1.25l-1.98,-0.25l-1.44,0.11l-0.53,0.46l-0.21,0.76l0.41,1.66l1.65,1.36l0.07,0.45l-0.58,0.32l-2.77,-0.46l-0.85,0.34l-0.32,0.69l0.24,0.71l1.21,1.35l0.6,1.94l2.45,1.8l2.52,0.64l1.16,0.61l4.22,0.12l-0.41,1.18l0.12,0.71l1.56,1.75l2.09,1.43l-0.14,4.66l-0.66,1.16l0.12,1.87l-0.76,-0.17l-0.63,-1.31l-0.81,-0.55l-1.74,-0.29l-0.98,0.29l-3.26,3.17l-1.73,0.96l-2.39,-0.41l-3.82,-2.56l-1.16,-2.83l-0.73,-0.49l-2.0,-0.2l-4.34,-1.4l-2.82,0.21l-2.6,-0.66l-0.67,-0.77l-0.1,-1.5l0.78,-2.2l-0.29,-0.88l-0.62,-0.28l-2.47,1.42l-4.48,-0.52l-4.27,1.08l-3.5,1.47l-2.77,0.41l-2.98,1.5l-1.01,-0.71l-1.77,-4.15l-1.43,-1.32l-1.13,-0.54l-3.59,-0.26l-1.8,0.22l-0.35,0.4l0.02,2.88l-3.85,-1.27l-2.16,-2.01l-1.82,-0.73l-3.57,0.03l-1.2,0.93l-0.35,1.86l-1.3,0.63l-0.49,-0.12l-0.72,-0.87l0.27,-1.36l-0.32,-0.86l-0.67,-0.41l-0.94,-0.05l-2.47,0.7l-4.94,3.64l-1.17,-0.23l-4.88,-2.4l-1.24,-1.11l-4.78,0.52l-0.65,-3.42l0.81,-1.28l-0.0,-0.88l-1.75,-2.86l-1.71,-1.42l-2.22,-6.93l-0.89,-1.48l-1.35,-0.86l-0.56,-0.98l-0.68,-1.98l-0.52,-3.12l-1.27,-3.49l0.08,-2.96l-0.31,-0.43l-1.46,-0.31l-3.14,-1.55l-4.13,-3.41l-4.32,-0.85l0.51,-1.34l-0.08,-0.66l-1.42,-1.21l-1.93,-0.18l-1.33,0.48l-0.47,0.69l-0.31,1.9l-0.69,0.54l-3.23,0.32l-1.34,0.6l-0.62,-0.3l-1.06,-1.05l-3.45,-5.83l-0.79,-2.59l-0.84,-0.77l-2.38,0.25l-3.69,2.03l-1.69,-0.0l-0.85,0.32l-4.65,3.8l-0.58,0.1l-1.2,-1.57l-0.6,-0.32l-1.46,0.59l-0.57,0.8l-0.62,3.19l-1.64,1.03l-0.58,0.99l-1.42,4.71l-0.35,2.6l-1.83,2.78l-0.72,2.01l-1.6,-1.21l-1.15,-1.79l-0.53,-3.47l-0.56,-1.36l1.14,-0.77l-0.12,-1.16l-2.88,-1.38l-0.69,-1.95l-2.44,-3.26l-0.78,-0.22l-2.44,0.35l0.05,-1.49l-0.34,-1.19l-1.91,-2.42l-1.03,-0.42l-8.44,7.7l-0.74,2.6l-2.44,3.55l-0.58,2.41l-3.68,3.99l-0.93,0.45l-1.89,0.2l-3.45,-0.1l-1.45,0.62l-1.19,-1.14l-1.48,-0.11l-5.33,1.64l-0.67,1.8l-0.21,-0.03l-0.95,-1.01l-0.89,-3.39l-1.7,-1.05l1.55,-5.49l-0.1,-0.85l-1.32,-2.73l-3.07,-4.61l-0.85,-5.71l-0.6,-1.16l0.94,-0.4l2.23,-0.24l1.11,-0.74l0.08,-1.24l-1.43,-2.43l-0.11,-0.76l3.2,-2.58l2.85,-5.59l0.22,-1.14l-1.07,-2.31l0.11,-1.37l2.59,-1.98l0.51,-1.87l3.08,-1.33l2.04,-2.9l1.09,-0.69l1.42,-3.08l-0.14,-1.07l-0.95,-0.99ZM619.51,528.65l1.54,-0.89l1.27,-2.18l1.36,-1.26l0.07,-0.97l-0.77,-1.43l-2.49,-2.28l-2.0,-0.54l-1.01,0.36l-0.89,0.76l-3.05,4.55l-0.55,2.1l0.5,1.33l0.7,0.43l5.31,0.04Z", 1, "departement"], ["data-name", "Provence-Alpes-C\u00F4te d'Azur", "data-code_insee", "93", 1, "region"], ["data-name", "Alpes-de-Haute-Provence", "data-info", "<p>n'est pas disponible </p>", "data-department", "04", "d", "M574.87,589.37l0.92,-0.18l1.37,-1.04l2.17,-0.54l3.45,-2.45l1.63,-0.49l0.58,-1.36l1.97,-0.33l0.5,-0.38l0.72,-1.53l-0.09,-0.84l-1.94,-0.6l1.3,-2.75l1.5,-1.9l1.28,-0.92l2.03,-0.52l3.72,1.32l1.03,-0.3l0.65,-1.54l-0.04,-1.78l1.98,-4.92l-0.49,-4.74l5.08,-3.33l2.76,-1.14l1.8,-1.27l1.33,-1.63l0.03,-2.18l1.18,0.07l0.47,-0.43l0.15,-1.04l-0.37,-0.8l-4.15,-4.77l-1.27,-0.56l-1.83,0.31l-0.37,-2.47l0.33,-2.07l-0.19,-2.88l-0.5,-0.93l-1.47,-1.11l-0.09,-1.56l-1.29,-1.64l-0.54,-1.66l-0.02,-2.97l3.17,-0.14l2.43,0.62l1.57,1.66l1.7,4.01l1.31,0.65l1.08,-0.2l2.24,-1.34l2.77,-0.41l3.47,-1.46l4.07,-1.05l3.48,0.59l1.1,-0.08l2.31,-1.4l0.14,0.38l-0.71,1.56l-0.03,1.7l0.31,1.17l0.77,0.67l2.96,0.79l2.82,-0.21l4.22,1.37l1.95,0.19l0.66,0.8l0.94,2.37l4.51,2.92l2.5,0.22l1.96,-1.1l2.53,-2.56l1.32,-0.74l1.8,0.54l0.64,1.31l0.52,0.4l0.87,0.13l0.61,-0.33l0.31,-0.68l-0.23,-1.68l0.64,-1.04l0.14,-4.96l-0.53,-0.87l-1.46,-0.73l-1.74,-1.84l0.47,-1.33l-0.29,-0.71l-1.33,-0.38l-2.81,0.16l-3.98,-1.31l-2.2,-1.6l-0.54,-1.85l-1.31,-1.69l0.85,-0.19l2.33,0.46l0.67,-0.16l0.58,-0.67l-0.17,-1.21l-1.58,-1.26l-0.32,-1.32l0.2,-0.43l2.95,0.15l4.34,1.28l0.95,-0.24l2.8,-1.78l0.61,-0.81l-0.12,-1.02l-0.85,-1.05l-1.56,-0.99l-0.6,-0.98l0.34,-1.04l1.84,-2.49l0.74,-2.54l1.28,-0.26l2.49,0.32l4.9,-1.34l1.9,-1.99l0.0,-1.34l-0.94,-1.02l1.78,-2.52l4.33,-0.43l2.59,-0.77l1.06,-1.17l-0.23,-1.81l1.51,-1.3l1.93,-0.81l1.61,0.45l1.3,-0.18l3.71,-2.08l2.21,0.4l1.91,-0.35l1.7,0.15l3.74,-0.98l2.72,0.76l1.33,-0.13l0.6,-0.99l0.28,-3.45l-0.47,-1.39l-1.68,-1.88l-0.61,-3.45l-1.52,-0.82l-3.62,0.45l-0.92,-0.3l-0.21,-0.57l1.03,-6.05l1.31,-1.51l1.5,0.78l3.9,0.36l1.08,0.74l1.0,1.99l4.33,1.4l1.59,-0.46l0.96,-1.07l0.85,-2.13l2.87,-0.24l2.8,-1.28l-0.62,0.73l-0.02,0.5l2.03,2.18l0.17,1.69l0.79,1.24l0.95,0.66l2.05,0.17l1.38,0.88l0.35,1.96l-0.12,4.19l1.23,1.91l2.07,1.56l4.22,2.16l1.41,0.3l3.93,-0.45l2.94,1.06l1.2,1.26l-0.4,2.57l1.25,4.13l2.03,3.55l-3.87,-0.56l-1.7,0.81l-1.13,1.22l-0.53,1.69l0.58,1.56l-0.69,0.5l-2.06,3.14l-3.05,2.06l-0.83,1.38l0.25,2.53l1.44,2.18l2.96,2.54l-1.46,0.71l-0.59,0.79l-0.73,3.01l0.24,1.21l1.67,1.86l1.59,2.88l2.27,1.81l0.83,2.02l0.66,0.7l1.16,0.41l2.19,-0.38l0.67,0.17l6.04,2.48l6.53,3.55l3.45,0.91l0.74,1.13l1.35,0.71l2.64,0.05l10.62,-2.7l1.73,-0.12l2.57,-1.44l1.28,0.21l-0.66,2.31l0.83,1.51l1.3,1.15l0.87,1.66l-0.1,1.05l-2.22,2.08l-1.33,3.37l-4.45,3.63l-1.3,1.81l-2.63,2.56l-1.02,1.63l0.26,2.23l1.1,3.29l-1.5,0.69l0.05,1.26l-0.73,0.15l-1.26,1.13l-0.56,-0.67l-1.18,-0.6l-1.65,0.49l-1.0,1.05l-0.43,1.64l-1.32,1.04l-0.71,1.53l-2.86,-0.54l-1.41,0.13l-2.01,1.42l0.04,0.96l-1.09,1.0l-0.54,-0.78l-1.17,-0.24l-2.01,0.57l-0.96,1.09l-0.66,2.71l0.71,3.75l-0.22,0.12l-0.57,-0.92l-0.68,-0.42l-1.15,-0.07l-1.27,0.64l-1.75,1.52l-0.56,-0.6l-2.19,-0.07l-1.21,0.15l-0.99,0.57l-1.1,1.01l-0.38,1.3l0.79,0.89l-1.67,1.9l-0.35,1.79l-1.2,0.73l-0.35,0.74l-0.79,0.06l-0.5,-0.8l-0.56,-0.07l-0.45,0.47l0.04,1.32l-0.33,0.1l-1.7,-0.24l-2.06,0.51l-0.64,-0.66l-0.97,-0.32l-1.96,1.17l-0.06,2.1l-0.82,1.01l-0.09,1.11l-2.6,0.48l-0.8,1.76l-2.13,1.86l-1.64,0.87l-0.58,1.35l0.27,0.52l1.27,0.34l3.83,-0.86l1.01,0.19l-0.51,1.21l-1.15,0.87l-0.18,1.36l0.76,1.4l-2.03,1.21l-1.78,0.59l-1.1,-1.03l-3.19,0.39l-1.04,1.7l-1.07,0.59l-1.62,-0.35l-3.45,0.27l-1.36,0.42l-1.49,0.84l-0.91,1.44l0.37,1.87l-1.34,0.0l-1.03,-0.98l-2.86,-1.09l-1.75,-0.16l-3.76,0.92l-1.97,1.27l-0.72,1.7l-0.05,1.91l0.91,0.63l-3.08,0.19l0.74,-0.26l0.95,-0.93l-0.33,-1.65l-1.19,-0.96l-5.05,0.37l-0.84,-1.54l-3.51,-0.25l-1.12,-0.63l0.3,-1.17l-0.36,-0.56l-2.16,0.24l-1.78,0.81l-0.23,0.38l1.15,0.68l1.01,1.7l1.93,0.76l-3.17,0.02l-0.89,0.28l-1.2,1.7l-1.96,-0.54l-1.85,-1.41l1.88,-1.3l0.07,-0.54l-0.41,-0.57l-1.72,-0.7l-0.74,-1.26l-2.11,-0.09l-2.49,-1.0l-0.47,-1.46l-1.08,-1.07l-2.53,-0.57l-1.03,0.91l-0.53,0.98l-2.05,-0.53l-1.98,-2.66l-0.96,-0.55l-1.48,0.47l-6.6,-0.49l-1.53,0.31l-1.02,-0.43l0.34,-1.68l0.94,-1.98l-0.08,-0.43l-0.62,-0.64l-1.13,-3.95l-2.92,-1.81l-3.73,1.99l-4.48,-0.09l-2.35,0.26l-3.62,-0.16l-1.71,-0.5l-1.11,-1.51l-0.44,-1.53l1.11,-1.82l1.71,-0.86l5.36,0.0l1.8,-0.48l2.62,-2.32l0.89,-2.0l-0.21,-1.77l-0.6,-0.3l-3.34,1.54l-1.34,-0.21l-1.17,-1.2l-0.81,-2.2l-1.53,-0.53l-1.81,-0.01l-1.47,-1.47l-0.5,0.08l-0.49,0.59l-0.81,2.82l0.42,1.59l2.15,1.63l0.63,1.61l-0.57,1.4l-1.25,0.8l-1.63,0.16l-4.46,-1.86l-2.21,1.1l-1.58,0.35l-0.71,0.61l-0.46,1.44l0.98,2.54l0.42,0.33l0.81,-0.05l-1.29,0.81l-1.31,-2.62l-2.66,-1.4l-1.98,-1.65l-0.86,-1.8l-0.02,-5.7l-1.08,-2.53l-0.85,-1.1l-1.19,-0.67l-0.59,0.35l0.0,0.46l2.09,3.43l0.0,5.28l0.86,2.64l4.89,4.15l-0.11,1.02l-9.05,-0.1l-4.41,-0.9l-1.34,-0.7l-0.03,-0.85l1.11,-1.09l0.22,-1.41l-0.65,-1.13l-1.58,-1.03l-2.76,-0.96l-6.31,0.6l-10.72,-0.78l1.08,-2.45ZM708.84,625.18l0.07,-0.56l1.07,-0.35l0.67,-0.95l0.94,-0.47l-1.31,1.7l-1.44,0.64ZM692.6,626.12l1.76,-0.0l0.97,-0.54l0.94,-0.02l0.43,-0.62l0.24,0.26l0.04,0.66l-2.62,1.0l-1.76,-0.73ZM619.44,527.85l-4.96,0.01l-0.37,-0.2l-0.33,-0.9l0.44,-1.6l2.99,-4.46l1.31,-0.8l1.47,0.37l2.37,2.16l0.58,1.0l-0.0,0.54l-1.24,1.1l-1.26,2.17l-1.01,0.62Z", 1, "departement"], ["href", "cpn/region/Corse"], ["data-name", "Corse", "data-department", "20", "data-code_insee", "94", 1, "region"], ["data-name", "Corse-du-Sud", "data-info", "<img src=\"assets/cpnimages/regions/flag/corse.png\" />", "data-department", "2A", "d", "M838.53,672.37l1.08,0.06l0.88,-0.67l0.97,0.12l1.07,-2.21l1.86,-0.26l0.74,-0.59l0.25,-1.79l0.9,-1.48l-0.38,-0.73l-0.69,-0.15l0.49,-1.84l0.66,-0.7l2.07,-0.66l0.26,-0.57l-0.29,-0.57l0.29,-1.57l1.09,-0.31l1.75,0.99l1.22,-0.28l0.96,-0.92l0.83,-2.39l1.69,-0.12l3.84,-1.79l6.8,-1.3l2.03,-0.92l0.4,-1.61l3.75,-3.54l2.7,-0.22l4.3,0.91l1.55,0.91l1.19,2.4l0.37,0.25l0.75,-0.18l2.19,-3.35l0.93,-2.22l-0.53,-1.78l0.44,-1.29l-0.33,-1.53l-1.69,-2.25l1.55,-2.06l-0.41,-1.45l0.2,-1.05l1.75,-1.77l0.11,-1.69l-1.01,-1.98l0.14,-1.93l0.77,-0.45l2.42,-0.01l0.97,-0.35l0.69,0.85l1.59,0.95l-0.62,1.65l0.32,1.04l0.64,0.74l-0.24,1.34l1.15,6.71l-0.1,2.14l-1.97,6.87l-0.02,4.12l-0.45,1.01l0.91,0.83l0.24,0.8l-0.14,1.25l1.14,1.91l2.33,1.17l0.51,-0.65l0.25,0.54l0.31,7.85l0.73,1.54l-0.6,1.91l-0.19,2.68l1.67,5.79l-0.45,9.13l0.43,1.76l-0.38,2.31l-0.37,0.56l-1.68,1.43l-1.61,2.76l-5.49,7.01l-0.58,7.85l0.53,5.07l-0.08,5.26l-1.16,0.85l-0.93,1.45l0.36,0.56l0.38,0.0l-0.34,1.47l-1.74,0.45l-0.13,1.0l-2.1,-0.31l-0.71,0.1l-0.34,0.46l0.08,0.47l-0.87,1.33l0.15,0.53l1.18,0.53l2.94,-0.89l0.29,0.34l-0.16,0.77l-1.25,1.25l-1.71,0.61l-1.08,1.44l-0.92,0.16l-0.29,0.57l0.28,0.85l-0.28,0.85l0.82,0.98l-0.71,0.84l0.01,0.67l-2.14,1.17l-0.98,0.12l-0.35,0.4l0.0,0.54l0.75,0.99l-0.59,0.7l-0.15,0.77l0.54,0.37l1.71,-1.1l-1.36,2.96l-0.77,0.37l-0.56,-0.17l-1.56,-1.26l-4.09,-1.13l0.5,-1.41l0.63,-0.25l0.15,-0.7l-3.04,-1.76l0.64,-0.96l0.0,-0.46l-0.51,-0.38l-2.15,1.21l-0.36,-0.76l-1.07,0.38l-0.9,-0.82l-1.69,-0.51l-2.48,-0.05l-1.61,-1.23l-1.4,-0.31l-0.77,-0.93l-1.36,-0.18l0.42,-0.48l-0.28,-0.83l-3.45,-1.04l0.22,-1.42l-0.44,-0.73l-0.5,-0.17l1.06,-2.73l1.05,0.33l3.28,-1.35l0.76,-0.71l0.25,-0.88l2.21,-1.71l-0.28,-0.67l-8.03,-1.29l-0.49,-0.46l0.45,-1.23l-0.37,-0.54l-1.66,0.01l-3.15,0.9l0.15,-0.36l-0.39,-0.56l-1.58,-0.18l1.37,-0.22l1.43,-1.12l0.36,-1.08l-0.11,-0.68l-0.53,-0.48l2.22,-0.4l2.07,-1.1l0.18,-0.51l-0.3,-0.7l-0.72,-0.7l1.32,-0.68l0.04,-0.58l-0.51,-0.92l1.44,-2.22l-1.63,-2.04l-1.3,-0.47l-1.37,0.15l-3.3,1.36l-2.54,0.15l-1.78,0.63l-0.16,-0.36l0.64,-1.35l-0.36,-1.17l-1.16,-1.23l2.2,-0.2l1.3,-0.47l1.04,-1.45l-0.12,-0.56l-0.44,-0.27l0.27,-0.44l1.08,-0.73l3.47,-1.33l0.38,-0.36l0.09,-0.92l-0.43,-0.78l-0.97,-0.68l-1.71,-2.86l-0.82,-0.13l-1.52,0.37l-0.72,-1.14l-1.11,-0.59l-2.23,-0.31l0.1,-0.87l-0.66,-0.46l0.63,-0.4l0.19,-0.89l-0.4,-0.4l-0.84,0.0l0.49,-1.2l-0.24,-0.73l-0.53,-0.37l0.18,-0.95l-1.46,-1.0l2.12,-0.06l1.46,-0.81l2.49,-0.44l2.27,-0.94l0.28,-0.91l-1.26,-1.16l-2.16,-0.95l-0.59,-1.02l-0.54,-0.04l-0.76,0.57l-0.02,-0.4l1.8,-0.96l0.05,-0.67l-1.61,-1.07l-1.41,0.12l-1.48,0.55l-0.55,-1.32l-0.01,-0.82Z", 1, "departement"]], template: function MapFrenchRegionComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "svg", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "g", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "path", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "g", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "path", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "g", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "path", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "g", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "path", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "g", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "path", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "g", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "path", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "g", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "path", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "g", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "path", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "g", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "path", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "g", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "path", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "g", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "path", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "g", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "path", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "g", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "path", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "a", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "g", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "path", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "g", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "path", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "a", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "g", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "path", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".french_map[_ngcontent-%COMP%], .map_wrapper[_ngcontent-%COMP%], .map_holder[_ngcontent-%COMP%], .map_svg[_ngcontent-%COMP%], g[_ngcontent-%COMP%], path[_ngcontent-%COMP%] {\n  fill: gainsboro ;\n  stroke-width: 1px ;\n  stroke: gray;\n  width: 100%;\n  height: 100%;\n  cursor: pointer;\n  transition: .2s ;\n}\n\nh1[_ngcontent-%COMP%]{\n  font-weight: bold;\n  margin: 50px 0 50px 0;\n  \n}\n\np[_ngcontent-%COMP%]{\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n\n.french_map[_ngcontent-%COMP%]   .map_wrapper[_ngcontent-%COMP%]   .map_holder[_ngcontent-%COMP%]   .map_svg[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\n  fill: #111D5E ;\n  stroke: #ffd2d2 ;\n}\n\n#us-map[_ngcontent-%COMP%] {\n  display: block !important;\n  position: absolute !important;\n  top: 0 !important;\n  left: 0 !important;\n  width: 100% !important;\n  height: 100% !important;\n}\n\npath[_ngcontent-%COMP%]:hover, circle[_ngcontent-%COMP%]:hover {\n  stroke: #002868 ;\n  stroke-width: 2px ;\n  stroke-linejoin: round ;\n  fill: #002868 ;\n  cursor: pointer ;\n}\n\n#path[_ngcontent-%COMP%] {\n  fill: none ;\n  stroke: #A9A9A9 ;\n  cursor: default ;\n}\n\n#info-box[_ngcontent-%COMP%] {\n  display: none ;\n  position: absolute ;\n  top: 0px ;\n  left: 0px  ;\n  z-index: 1;\n  background-color: #ffffff ;\n  border: 2px solid #ffffff ;\n  border-radius: 5px ;\n  padding: 5px ;\n  font-family: arial ;\n  width: 100px;\n  height: 100px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  \n    }\n\n.round[_ngcontent-%COMP%]{\n      border-radius: 30px;\n      -webkit-border-radius: 30px;\n      -moz-border-radius: 30px;\n    }\n\n.right-in[_ngcontent-%COMP%]:after{\n      content: ' ';\n      position: absolute;\n      width: 0;\n      height: 0;\n      left: auto;\n      right: 35px;\n      top: 80px;\n      bottom: auto;\n      z-index: 0;\n      transform: rotate(225deg); \n      border: 12px solid;\n      border-color: white transparent transparent white;\n    }\n\n.act-content[_ngcontent-%COMP%]{\n  background-color: #EBECF0;\n\n}\n\n.act-img[_ngcontent-%COMP%]{\n      background-color: #EBECF0;\n    }\n\n.french_map[_ngcontent-%COMP%]   .map_wrapper[_ngcontent-%COMP%]   .map_holder[_ngcontent-%COMP%]   .map_svg[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\n      fill: gray;\n      stroke-width: 1px;\n      stroke: gray;\n      cursor: pointer;\n      transition: 0.3s;\n   }\n\n.french_map[_ngcontent-%COMP%]   .map_wrapper[_ngcontent-%COMP%]   .map_holder[_ngcontent-%COMP%]   .map_svg[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\n      fill: #111d5e;\n   }\n\n\n\n.french_map[_ngcontent-%COMP%], .map_wrapper[_ngcontent-%COMP%], .map_holder[_ngcontent-%COMP%], .map_svg[_ngcontent-%COMP%], g[_ngcontent-%COMP%], path[_ngcontent-%COMP%] {\n  fill: gainsboro ;\n  stroke-width: 1px ;\n  stroke: gray;\n  width: 100%;\n  height: 100%;\n  cursor: pointer;\n  transition: .2s ;\n}\n\nh1[_ngcontent-%COMP%]{\n  font-weight: bold;\n  margin: 50px 0 50px 0;\n  \n}\n\n.french_map[_ngcontent-%COMP%]   .map_wrapper[_ngcontent-%COMP%]   .map_holder[_ngcontent-%COMP%]   .map_svg[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\n  fill: #111D5E ;\n  stroke: #ffd2d2 ;\n}\n\n#us-map[_ngcontent-%COMP%] {\n  display: block !important;\n  position: absolute !important;\n  top: 0 !important;\n  left: 0 !important;\n  width: 100% !important;\n  height: 100% !important;\n}\n\npath[_ngcontent-%COMP%]:hover, circle[_ngcontent-%COMP%]:hover {\n  stroke: #002868 ;\n  stroke-width: 2px ;\n  stroke-linejoin: round ;\n  fill: #002868 ;\n  cursor: pointer ;\n}\n\n#path[_ngcontent-%COMP%] {\n  fill: none ;\n  stroke: #A9A9A9 ;\n  cursor: default ;\n}\n\n#info-box[_ngcontent-%COMP%] {\n  display: none ;\n  position: absolute ;\n  top: 0px ;\n  left: 0px  ;\n  z-index: 1;\n  background-color: #ffffff ;\n  border: 2px solid #ffffff ;\n  border-radius: 5px ;\n  padding: 5px ;\n  font-family: arial ;\n  width: 100px;\n  height: 100px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  \n    }\n\n.round[_ngcontent-%COMP%]{\n      border-radius: 30px;\n      -webkit-border-radius: 30px;\n      -moz-border-radius: 30px;\n    }\n\n.right-in[_ngcontent-%COMP%]:after{\n      content: ' ';\n      position: absolute;\n      width: 0;\n      height: 0;\n      left: auto;\n      right: 35px;\n      top: 80px;\n      bottom: auto;\n      z-index: 0;\n      transform: rotate(225deg); \n      border: 12px solid;\n      border-color: white transparent transparent white;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vbWFwLWZyZW5jaC1yZWdpb24vbWFwLWZyZW5jaC1yZWdpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBOzs7Ozs7RUFNRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxpQkFBaUI7RUFDakIscUJBQXFCOztBQUV2Qjs7QUFDQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtBQUNyQjs7QUFDQztFQUNDLGNBQWM7RUFDZCxnQkFBZ0I7QUFDbEI7O0FBR0E7RUFDRSx5QkFBeUI7RUFDekIsNkJBQTZCO0VBQzdCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtBQUN6Qjs7QUFFQTs7RUFFRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxXQUFXO0VBQ1gsVUFBVTtFQUNWLDBCQUEwQjtFQUMxQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGFBQWE7RUFDYixtREFBbUQ7O0lBRWpEOztBQUNBO01BQ0UsbUJBQW1CO01BQ25CLDJCQUEyQjtNQUMzQix3QkFBd0I7SUFDMUI7O0FBRUE7TUFDRSxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLFFBQVE7TUFDUixTQUFTO01BQ1QsVUFBVTtNQUNWLFdBQVc7TUFDWCxTQUFTO01BQ1QsWUFBWTtNQUNaLFVBQVU7TUFDVix5QkFBeUI7TUFDekIsa0JBQWtCO01BQ2xCLGlEQUFpRDtJQUNuRDs7QUFFSjtFQUNFLHlCQUF5Qjs7QUFFM0I7O0FBQ0k7TUFDRSx5QkFBeUI7SUFDM0I7O0FBSUM7TUFDQyxVQUFVO01BQ1YsaUJBQWlCO01BQ2pCLFlBQVk7TUFDWixlQUFlO01BQ2YsZ0JBQWdCO0dBQ25COztBQUNFO01BQ0MsYUFBYTtHQUNoQjs7QUFDSCxvRkFBb0Y7O0FBRXBGOzs7Ozs7RUFNRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxpQkFBaUI7RUFDakIscUJBQXFCOztBQUV2Qjs7QUFDQztFQUNDLGNBQWM7RUFDZCxnQkFBZ0I7QUFDbEI7O0FBR0E7RUFDRSx5QkFBeUI7RUFDekIsNkJBQTZCO0VBQzdCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtBQUN6Qjs7QUFFQTs7RUFFRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxXQUFXO0VBQ1gsVUFBVTtFQUNWLDBCQUEwQjtFQUMxQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGFBQWE7RUFDYixtREFBbUQ7O0lBRWpEOztBQUNBO01BQ0UsbUJBQW1CO01BQ25CLDJCQUEyQjtNQUMzQix3QkFBd0I7SUFDMUI7O0FBRUE7TUFDRSxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLFFBQVE7TUFDUixTQUFTO01BQ1QsVUFBVTtNQUNWLFdBQVc7TUFDWCxTQUFTO01BQ1QsWUFBWTtNQUNaLFVBQVU7TUFDVix5QkFBeUI7TUFDekIsa0JBQWtCO01BQ2xCLGlEQUFpRDtJQUNuRCIsImZpbGUiOiJhcHAvY3BuL21hcC1mcmVuY2gtcmVnaW9uL21hcC1mcmVuY2gtcmVnaW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuLmZyZW5jaF9tYXAsXG4ubWFwX3dyYXBwZXIsXG4ubWFwX2hvbGRlcixcbi5tYXBfc3ZnLFxuZyxcbnBhdGgge1xuICBmaWxsOiBnYWluc2Jvcm8gO1xuICBzdHJva2Utd2lkdGg6IDFweCA7XG4gIHN0cm9rZTogZ3JheTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0cmFuc2l0aW9uOiAuMnMgO1xufVxuXG5oMXtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbjogNTBweCAwIDUwcHggMDtcbiAgXG59XG5we1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbiAuZnJlbmNoX21hcCAubWFwX3dyYXBwZXIgLm1hcF9ob2xkZXIgLm1hcF9zdmcgZyBwYXRoOmhvdmVyIHtcbiAgZmlsbDogIzExMUQ1RSA7XG4gIHN0cm9rZTogI2ZmZDJkMiA7XG59XG5cblxuI3VzLW1hcCB7XG4gIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICB0b3A6IDAgIWltcG9ydGFudDtcbiAgbGVmdDogMCAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbn1cblxucGF0aDpob3ZlcixcbmNpcmNsZTpob3ZlciB7XG4gIHN0cm9rZTogIzAwMjg2OCA7XG4gIHN0cm9rZS13aWR0aDogMnB4IDtcbiAgc3Ryb2tlLWxpbmVqb2luOiByb3VuZCA7XG4gIGZpbGw6ICMwMDI4NjggO1xuICBjdXJzb3I6IHBvaW50ZXIgO1xufVxuXG4jcGF0aCB7XG4gIGZpbGw6IG5vbmUgO1xuICBzdHJva2U6ICNBOUE5QTkgO1xuICBjdXJzb3I6IGRlZmF1bHQgO1xufVxuXG4jaW5mby1ib3gge1xuICBkaXNwbGF5OiBub25lIDtcbiAgcG9zaXRpb246IGFic29sdXRlIDtcbiAgdG9wOiAwcHggO1xuICBsZWZ0OiAwcHggIDtcbiAgei1pbmRleDogMTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZiA7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmZmZmYgO1xuICBib3JkZXItcmFkaXVzOiA1cHggO1xuICBwYWRkaW5nOiA1cHggO1xuICBmb250LWZhbWlseTogYXJpYWwgO1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcbiAgXG4gICAgfVxuICAgIC5yb3VuZHtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgICAtbW96LWJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgfVxuXG4gICAgLnJpZ2h0LWluOmFmdGVye1xuICAgICAgY29udGVudDogJyAnO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgd2lkdGg6IDA7XG4gICAgICBoZWlnaHQ6IDA7XG4gICAgICBsZWZ0OiBhdXRvO1xuICAgICAgcmlnaHQ6IDM1cHg7XG4gICAgICB0b3A6IDgwcHg7XG4gICAgICBib3R0b206IGF1dG87XG4gICAgICB6LWluZGV4OiAwO1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMjI1ZGVnKTsgXG4gICAgICBib3JkZXI6IDEycHggc29saWQ7XG4gICAgICBib3JkZXItY29sb3I6IHdoaXRlIHRyYW5zcGFyZW50IHRyYW5zcGFyZW50IHdoaXRlO1xuICAgIH1cbiAgIFxuLmFjdC1jb250ZW50e1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xuXG59XG4gICAgLmFjdC1pbWd7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xuICAgIH1cblxuXG5cbiAgICAgLmZyZW5jaF9tYXAgLm1hcF93cmFwcGVyIC5tYXBfaG9sZGVyIC5tYXBfc3ZnIGcgcGF0aCB7XG4gICAgICBmaWxsOiBncmF5O1xuICAgICAgc3Ryb2tlLXdpZHRoOiAxcHg7XG4gICAgICBzdHJva2U6IGdyYXk7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICB0cmFuc2l0aW9uOiAwLjNzO1xuICAgfVxuICAgICAuZnJlbmNoX21hcCAubWFwX3dyYXBwZXIgLm1hcF9ob2xkZXIgLm1hcF9zdmcgZyBwYXRoOmhvdmVyIHtcbiAgICAgIGZpbGw6ICMxMTFkNWU7XG4gICB9XG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm1hcCoqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi5mcmVuY2hfbWFwLFxuLm1hcF93cmFwcGVyLFxuLm1hcF9ob2xkZXIsXG4ubWFwX3N2ZyxcbmcsXG5wYXRoIHtcbiAgZmlsbDogZ2FpbnNib3JvIDtcbiAgc3Ryb2tlLXdpZHRoOiAxcHggO1xuICBzdHJva2U6IGdyYXk7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgdHJhbnNpdGlvbjogLjJzIDtcbn1cblxuaDF7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBtYXJnaW46IDUwcHggMCA1MHB4IDA7XG4gIFxufVxuIC5mcmVuY2hfbWFwIC5tYXBfd3JhcHBlciAubWFwX2hvbGRlciAubWFwX3N2ZyBnIHBhdGg6aG92ZXIge1xuICBmaWxsOiAjMTExRDVFIDtcbiAgc3Ryb2tlOiAjZmZkMmQyIDtcbn1cblxuXG4jdXMtbWFwIHtcbiAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcbiAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG4gIHRvcDogMCAhaW1wb3J0YW50O1xuICBsZWZ0OiAwICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xufVxuXG5wYXRoOmhvdmVyLFxuY2lyY2xlOmhvdmVyIHtcbiAgc3Ryb2tlOiAjMDAyODY4IDtcbiAgc3Ryb2tlLXdpZHRoOiAycHggO1xuICBzdHJva2UtbGluZWpvaW46IHJvdW5kIDtcbiAgZmlsbDogIzAwMjg2OCA7XG4gIGN1cnNvcjogcG9pbnRlciA7XG59XG5cbiNwYXRoIHtcbiAgZmlsbDogbm9uZSA7XG4gIHN0cm9rZTogI0E5QTlBOSA7XG4gIGN1cnNvcjogZGVmYXVsdCA7XG59XG5cbiNpbmZvLWJveCB7XG4gIGRpc3BsYXk6IG5vbmUgO1xuICBwb3NpdGlvbjogYWJzb2x1dGUgO1xuICB0b3A6IDBweCA7XG4gIGxlZnQ6IDBweCAgO1xuICB6LWluZGV4OiAxO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmIDtcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZmZmZiA7XG4gIGJvcmRlci1yYWRpdXM6IDVweCA7XG4gIHBhZGRpbmc6IDVweCA7XG4gIGZvbnQtZmFtaWx5OiBhcmlhbCA7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xuICBcbiAgICB9XG4gICAgLnJvdW5ke1xuICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICB9XG5cbiAgICAucmlnaHQtaW46YWZ0ZXJ7XG4gICAgICBjb250ZW50OiAnICc7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB3aWR0aDogMDtcbiAgICAgIGhlaWdodDogMDtcbiAgICAgIGxlZnQ6IGF1dG87XG4gICAgICByaWdodDogMzVweDtcbiAgICAgIHRvcDogODBweDtcbiAgICAgIGJvdHRvbTogYXV0bztcbiAgICAgIHotaW5kZXg6IDA7XG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgyMjVkZWcpOyBcbiAgICAgIGJvcmRlcjogMTJweCBzb2xpZDtcbiAgICAgIGJvcmRlci1jb2xvcjogd2hpdGUgdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQgd2hpdGU7XG4gICAgfVxuICJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MapFrenchRegionComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-map-french-region',
                templateUrl: './map-french-region.component.html',
                styleUrls: ['./map-french-region.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/navbar-component/navbar-component.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/cpn/navbar-component/navbar-component.component.ts ***!
  \********************************************************************/
/*! exports provided: NavbarComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponentComponent", function() { return NavbarComponentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");





function NavbarComponentComponent_a_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
} }
function NavbarComponentComponent_a_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Home_tpe_pme");
} }
function NavbarComponentComponent_a_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/agence");
} }
function NavbarComponentComponent_a_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Home_collectivite");
} }
function NavbarComponentComponent_a_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Agenda +");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "agenda");
} }
function NavbarComponentComponent_a_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Agenda +");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "calendar");
} }
function NavbarComponentComponent_a_28_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Inscription");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "Inscription");
} }
function NavbarComponentComponent_a_30_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "Connexion");
} }
function NavbarComponentComponent_li_31_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "profile");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponentComponent_li_31_Template_a_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.logout(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "d\u00E9connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r8.user.first_name, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "profile");
} }
class NavbarComponentComponent {
    /******************************life cycle *************************/
    constructor(tokenStorage, route) {
        this.tokenStorage = tokenStorage;
        this.route = route;
        this.token = "";
        this.user = null;
        this.connect = false;
        this.role = "logout";
    }
    ngOnInit() {
        if (this.tokenStorage.getUser() != false) {
            this.token = this.tokenStorage.getUser();
            this.user = JSON.parse(this.token);
            this.role = this.user.role;
            this.connect = true;
        }
    }
    redirectTo(to) {
        location.href = to;
    }
    logout() {
        this.tokenStorage.signOut();
        this.connect = false;
        this.role = "logout";
        location.href = '/home';
    }
}
NavbarComponentComponent.ɵfac = function NavbarComponentComponent_Factory(t) { return new (t || NavbarComponentComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"])); };
NavbarComponentComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NavbarComponentComponent, selectors: [["app-navbar-component"]], decls: 32, vars: 14, consts: [[1, "navbar", "navbar-expand-lg", "nav_g"], [1, "navbar-brand", 3, "routerLink"], ["src", "assets/cpnimages/logo/logo-cpn-blue.png", "alt", "Logo", 1, "brand_logo", "d-inline-block", "align-text-top", "nav_img"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarSupportedContent", "aria-controls", "navbarSupportedContent", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler", 2, "box-shadow", "none"], [1, "far", "fa-bars", "navbar-toggler-icon", 2, "color", "#111d5e", "z-index", "1"], ["id", "navbarSupportedContent", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "ml-auto", "topnav"], [1, "nav-item"], ["class", "nav-link", 3, "routerLink", 4, "ngIf"], [1, "nav-link", 3, "routerLink"], ["class", "nav-link con", 3, "routerLink", 4, "ngIf"], ["class", "nav-item", 4, "ngIf"], [1, "nav-link", "con", 3, "routerLink"], [1, "dropdown"], ["role", "button", "id", "dropdownMenuLink", "data-toggle", "dropdown", "aria-haspopup", "true", "aria-expanded", "false", 1, "nav-link", "dropdown-toggle"], ["aria-labelledby", "dropdownMenuLink", 1, "dropdown-menu", "drop"], [1, "dropdown-item", 3, "routerLink"], [1, "dropdown-item", 3, "click"]], template: function NavbarComponentComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nav", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "i", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, NavbarComponentComponent_a_8_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, NavbarComponentComponent_a_9_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, NavbarComponentComponent_a_10_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, NavbarComponentComponent_a_11_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " Subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, NavbarComponentComponent_a_19_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, NavbarComponentComponent_a_20_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "aide-aux-entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " Contactez-nous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](28, NavbarComponentComponent_a_28_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](30, NavbarComponentComponent_a_30_Template, 2, 1, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, NavbarComponentComponent_li_31_Template, 9, 2, "li", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "logout");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "tpe");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "age");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "col");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "Actualite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "Subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.connect);
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"]], styles: [".navbar-nav[_ngcontent-%COMP%]{\nflex-direction: row;\njustify-content: space-between;\ndisplay: flex;\nfont-size: 14px;\nheight: 40px\n}\n.drop[_ngcontent-%COMP%]{\n    min-width: -moz-available;\n    margin-left: -70px;\n  }\n.nav-link[_ngcontent-%COMP%]{\n    color: #111D5E !important;\n}\n.navlinkwhit[_ngcontent-%COMP%]{\n    color: #111D5E !important;\n}\n.con[_ngcontent-%COMP%]{\n    color: white !important;\n    border: none;\n    background: red;\n    border-radius: 25px;\n    width: 120px;\n    text-align: center;\nheight: 100%;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\n}\n.topnav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n    border-bottom: 0.1px solid red;\n\n}\n.nav_t[_ngcontent-%COMP%]{\n    color: white !important;\n}\n.nav_g[_ngcontent-%COMP%]{\n    background-color: #EBECF0;}\n.navwhit[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n\n}\n.nav_img[_ngcontent-%COMP%]{\n    width: 80px;\n    margin-bottom: 10px;\n}\n.navbar-brand[_ngcontent-%COMP%] {\n    display: inline-block;\n    padding-top: .3125rem;\n    padding-bottom: .3125rem;\n    margin-right: 1rem;\n    font-size: 1.25rem;\n    line-height: inherit;\n    white-space: nowrap;\n    margin-left: 75px;\n    z-index: 5;\n}\n\n@media screen and (max-width:  992px) {\n        .navbar-brand[_ngcontent-%COMP%] {\n            display: inline-block;\n            padding-top: .3125rem;\n            padding-bottom: .3125rem;\n            margin-right: 1rem;\n            font-size: 1.25rem;\n            line-height: inherit;\n            white-space: nowrap;\n            margin-left: 0;\n            z-index: 5;\n    }\n    .navbar-nav[_ngcontent-%COMP%]{\n        flex-direction: column;\n        justify-content: space-between;\n        display: flex;\n        font-size: 14px;\n        height:max-content;\n        }\n        .drop[_ngcontent-%COMP%]{\n            min-width: -moz-available;\n            margin-left: 0px;\n          }\n\n          \n}\n\n@media only screen and (min-width : 992px) and (max-width : 1200px)  {\n    \n    .drop[_ngcontent-%COMP%]{\n     min-width: -moz-available;\n     margin-left: -70px;\n    }     \n  \n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vbmF2YmFyLWNvbXBvbmVudC9uYXZiYXItY29tcG9uZW50LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQSxtQkFBbUI7QUFDbkIsOEJBQThCO0FBQzlCLGFBQWE7QUFDYixlQUFlO0FBQ2Y7QUFDQTtBQUNBO0lBQ0kseUJBQXlCO0lBQ3pCLGtCQUFrQjtFQUNwQjtBQUNGO0lBQ0kseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSx5QkFBeUI7QUFDN0I7QUFDQTtJQUNJLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osa0JBQWtCO0FBQ3RCLFlBQVk7QUFDWixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjtBQUNBO0lBQ0ksOEJBQThCOztBQUVsQztBQUNBO0lBQ0ksdUJBQXVCO0FBQzNCO0FBQ0E7SUFDSSx5QkFBeUIsQ0FBQztBQUM5QjtJQUNJLHlCQUF5Qjs7QUFFN0I7QUFDQTtJQUNJLFdBQVc7SUFDWCxtQkFBbUI7QUFDdkI7QUFDRztJQUNDLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsVUFBVTtBQUNkO0FBSUksMEdBQTBHO0FBQzFHO1FBQ0k7WUFDSSxxQkFBcUI7WUFDckIscUJBQXFCO1lBQ3JCLHdCQUF3QjtZQUN4QixrQkFBa0I7WUFDbEIsa0JBQWtCO1lBQ2xCLG9CQUFvQjtZQUNwQixtQkFBbUI7WUFDbkIsY0FBYztZQUNkLFVBQVU7SUFDbEI7SUFDQTtRQUNJLHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsYUFBYTtRQUNiLGVBQWU7UUFDZixrQkFBa0I7UUFDbEI7UUFDQTtZQUNJLHlCQUF5QjtZQUN6QixnQkFBZ0I7VUFDbEI7OztBQUdWO0FBQ0EsNkJBQTZCO0FBQzdCO0lBQ0ksbUVBQW1FO0lBQ25FO0tBQ0MseUJBQXlCO0tBQ3pCLGtCQUFrQjtJQUNuQjs7QUFFSiIsImZpbGUiOiJhcHAvY3BuL25hdmJhci1jb21wb25lbnQvbmF2YmFyLWNvbXBvbmVudC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5hdmJhci1uYXZ7XG5mbGV4LWRpcmVjdGlvbjogcm93O1xuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuZGlzcGxheTogZmxleDtcbmZvbnQtc2l6ZTogMTRweDtcbmhlaWdodDogNDBweFxufVxuLmRyb3B7XG4gICAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgICBtYXJnaW4tbGVmdDogLTcwcHg7XG4gIH1cbi5uYXYtbGlua3tcbiAgICBjb2xvcjogIzExMUQ1RSAhaW1wb3J0YW50O1xufVxuLm5hdmxpbmt3aGl0e1xuICAgIGNvbG9yOiAjMTExRDVFICFpbXBvcnRhbnQ7XG59XG4uY29ue1xuICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBiYWNrZ3JvdW5kOiByZWQ7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICB3aWR0aDogMTIwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuaGVpZ2h0OiAxMDAlO1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi50b3BuYXYgbGkgYTpob3ZlciB7XG4gICAgYm9yZGVyLWJvdHRvbTogMC4xcHggc29saWQgcmVkO1xuXG59XG4ubmF2X3R7XG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59XG4ubmF2X2d7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0VCRUNGMDt9XG4ubmF2d2hpdHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xuXG59XG4ubmF2X2ltZ3tcbiAgICB3aWR0aDogODBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuICAgLm5hdmJhci1icmFuZCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBhZGRpbmctdG9wOiAuMzEyNXJlbTtcbiAgICBwYWRkaW5nLWJvdHRvbTogLjMxMjVyZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBsaW5lLWhlaWdodDogaW5oZXJpdDtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIG1hcmdpbi1sZWZ0OiA3NXB4O1xuICAgIHotaW5kZXg6IDU7XG59XG5cblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqcmVzcG9uc2l2ZSBjc3MgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAgOTkycHgpIHtcbiAgICAgICAgLm5hdmJhci1icmFuZCB7XG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogLjMxMjVyZW07XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLjMxMjVyZW07XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XG4gICAgICAgICAgICBmb250LXNpemU6IDEuMjVyZW07XG4gICAgICAgICAgICBsaW5lLWhlaWdodDogaW5oZXJpdDtcbiAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMDtcbiAgICAgICAgICAgIHotaW5kZXg6IDU7XG4gICAgfVxuICAgIC5uYXZiYXItbmF2e1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgaGVpZ2h0Om1heC1jb250ZW50O1xuICAgICAgICB9XG4gICAgICAgIC5kcm9we1xuICAgICAgICAgICAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgXG59XG4vKiBNZWRpdW0gRGV2aWNlcywgRGVza3RvcHMgKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDk5MnB4KSBhbmQgKG1heC13aWR0aCA6IDEyMDBweCkgIHtcbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbmF2IGJhciAqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC5kcm9we1xuICAgICBtaW4td2lkdGg6IC1tb3otYXZhaWxhYmxlO1xuICAgICBtYXJnaW4tbGVmdDogLTcwcHg7XG4gICAgfSAgICAgXG4gIFxufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NavbarComponentComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-navbar-component',
                templateUrl: './navbar-component.component.html',
                styleUrls: ['./navbar-component.component.css']
            }]
    }], function () { return [{ type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/notre-succes/notre-succes.component.ts":
/*!************************************************************!*\
  !*** ./src/app/cpn/notre-succes/notre-succes.component.ts ***!
  \************************************************************/
/*! exports provided: NotreSuccesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotreSuccesComponent", function() { return NotreSuccesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NotreSuccesComponent {
    constructor() { }
    ngOnInit() {
        $(document).ready(function () {
            $('.item_num').counterUp({
                time: 2000
            });
        });
    }
}
NotreSuccesComponent.ɵfac = function NotreSuccesComponent_Factory(t) { return new (t || NotreSuccesComponent)(); };
NotreSuccesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NotreSuccesComponent, selectors: [["app-notre-succes"]], decls: 31, vars: 0, consts: [[1, "our_success", "mb-3"], [1, "success_wrapper", "container", "px-4", "g-0"], [1, "row", "g-0"], [1, "col-md-6", "py-2", "title"], [1, "success_txt"], [1, "success_desc"], [1, "col-md-6", "py-2", "chiffre"], [1, "success_list"], [1, "success_item"], [1, "k"], [1, "item_num"], [1, "item_desc"]], template: function NotreSuccesComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h5", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Notre succ\u00E9s");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h3", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Peut importe votre secteur d'acitivit\u00E9 nous vous offrons une subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "562");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "10");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Subventions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "200");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "K+ ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Adh\u00E9rants");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]{\n    margin-bottom: 150px;\n}\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_txt[_ngcontent-%COMP%] {\n    text-transform: uppercase;\n    font-weight: 400;\n    font-size: 14px;\n    color: #111d5e;\n}\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_desc[_ngcontent-%COMP%] {\n    font-size: 28px;\n    color: #111d5e;\n}\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%] {\n    display: flex;\n    justify-content: space-between;\n    margin: 0;\n    padding: 0;\n}\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%] {\n    list-style: none;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n}\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_num[_ngcontent-%COMP%] {\n    margin: 0;\n    font-weight: 700;\n    color: #111d5e;\n}\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_desc[_ngcontent-%COMP%] {\n    margin: 0;\n    color: #111d5e;\n}\n.item_num[_ngcontent-%COMP%] {\n    margin: 0;\n    font-weight: 700;\n    color: #111d5e;\n  }\n.success_item[_ngcontent-%COMP%] {\n    list-style: none;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n  }\n.item_desc[_ngcontent-%COMP%] {\n    margin: 0;\n    color: #111d5e;\n  }\n.success_list[_ngcontent-%COMP%] {\n    display: flex;\n    justify-content: space-between;\n    margin: 0;\n    padding: 0;\n  }\n.success_txt[_ngcontent-%COMP%] {\n    text-transform: uppercase;\n    font-weight: 400;\n    font-size: 14px;\n    color: #111d5e;\n  }\n.success_desc[_ngcontent-%COMP%] {\n    font-size: 28px;\n    color: #111d5e;\n  }\n.chiffre[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    width: 40%;\n    align-items: inherit;\n}\n\n\n@media only screen and (min-width : 320px) and (max-width : 480px)  {\n  \n\n      \n      .chiffre[_ngcontent-%COMP%]{\n        display: flex;\n      flex-direction: column;\n      justify-content: center;\n      width: 100%;\n      align-items: center;\n      }\n\n      .k[_ngcontent-%COMP%]{\n        margin: initial;\n      }\n      .success_item[_ngcontent-%COMP%]{\n        margin-left: 20px;\n        list-style: none;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: center;\n      }\n\n  }\n\n@media only screen and (min-width : 480px) and (max-width : 768px)  {\n              \n      \n      .chiffre[_ngcontent-%COMP%]{\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        width: 100%;\n        align-items: center;\n        }\n        \n        .k[_ngcontent-%COMP%]{\n        margin: initial;\n        }\n        .success_item[_ngcontent-%COMP%]{\n        margin-left: 20px;\n        list-style: none;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: center;\n        }\n  \n  }\n\n@media only screen and (min-width : 768px) and (max-width : 992px)  {\n       \n      \n      .chiffre[_ngcontent-%COMP%]{\n        display: flex;\n        width: 100%;\n        }\n        \n        .k[_ngcontent-%COMP%]{\n        margin: initial;\n        }\n        .success_item[_ngcontent-%COMP%]{\n        margin-left: 20px;\n        list-style: none;\n        display: flex;\n        flex-direction: column;\n        justify-content: center;\n        align-items: center;\n        }\n        .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\n          width: -moz-available;\n        }\n        \n  \n   }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vbm90cmUtc3VjY2VzL25vdHJlLXN1Y2Nlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksb0JBQW9CO0FBQ3hCO0FBQ0E7SUFDSSx5QkFBeUI7SUFDekIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixjQUFjO0FBQ2xCO0FBQ0M7SUFDRyxlQUFlO0lBQ2YsY0FBYztBQUNsQjtBQUNDO0lBQ0csYUFBYTtJQUNiLDhCQUE4QjtJQUM5QixTQUFTO0lBQ1QsVUFBVTtBQUNkO0FBQ0M7SUFDRyxnQkFBZ0I7SUFDaEIsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsbUJBQW1CO0FBQ3ZCO0FBQ0M7SUFDRyxTQUFTO0lBQ1QsZ0JBQWdCO0lBQ2hCLGNBQWM7QUFDbEI7QUFDQztJQUNHLFNBQVM7SUFDVCxjQUFjO0FBQ2xCO0FBR0E7SUFDSSxTQUFTO0lBQ1QsZ0JBQWdCO0lBQ2hCLGNBQWM7RUFDaEI7QUFDQTtJQUNFLGdCQUFnQjtJQUNoQixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7RUFDckI7QUFDQTtJQUNFLFNBQVM7SUFDVCxjQUFjO0VBQ2hCO0FBQ0E7SUFDRSxhQUFhO0lBQ2IsOEJBQThCO0lBQzlCLFNBQVM7SUFDVCxVQUFVO0VBQ1o7QUFDQTtJQUNFLHlCQUF5QjtJQUN6QixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGNBQWM7RUFDaEI7QUFDQTtJQUNFLGVBQWU7SUFDZixjQUFjO0VBQ2hCO0FBQ0Y7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixVQUFVO0lBQ1Ysb0JBQW9CO0FBQ3hCO0FBRU0sMEdBQTBHO0FBQzFHLDJCQUEyQjtBQUM5Qjs7O01BR0csZ0hBQWdIO01BQ2hIO1FBQ0UsYUFBYTtNQUNmLHNCQUFzQjtNQUN0Qix1QkFBdUI7TUFDdkIsV0FBVztNQUNYLG1CQUFtQjtNQUNuQjs7TUFFQTtRQUNFLGVBQWU7TUFDakI7TUFDQTtRQUNFLGlCQUFpQjtRQUNqQixnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsbUJBQW1CO01BQ3JCOztFQUVKO0FBRUEsZ0NBQWdDO0FBQ2hDOztNQUVJLGdIQUFnSDtNQUNoSDtRQUNFLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkI7O1FBRUE7UUFDQSxlQUFlO1FBQ2Y7UUFDQTtRQUNBLGlCQUFpQjtRQUNqQixnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25COztFQUVOO0FBSUMsMEJBQTBCO0FBQzFCOztNQUVHLGdIQUFnSDtNQUNoSDtRQUNFLGFBQWE7UUFDYixXQUFXO1FBQ1g7O1FBRUE7UUFDQSxlQUFlO1FBQ2Y7UUFDQTtRQUNBLGlCQUFpQjtRQUNqQixnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CO1FBQ0E7VUFDRSxxQkFBcUI7UUFDdkI7OztHQUdMO0FBSUE7OztRQUdLIiwiZmlsZSI6ImFwcC9jcG4vbm90cmUtc3VjY2VzL25vdHJlLXN1Y2Nlcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXJ7XG4gICAgbWFyZ2luLWJvdHRvbTogMTUwcHg7XG59XG4ub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc190eHQge1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICMxMTFkNWU7XG59XG4gLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfZGVzYyB7XG4gICAgZm9udC1zaXplOiAyOHB4O1xuICAgIGNvbG9yOiAjMTExZDVlO1xufVxuIC5vdXJfc3VjY2VzcyAuc3VjY2Vzc193cmFwcGVyIC5zdWNjZXNzX2xpc3Qge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xufVxuIC5vdXJfc3VjY2VzcyAuc3VjY2Vzc193cmFwcGVyIC5zdWNjZXNzX2xpc3QgLnN1Y2Nlc3NfaXRlbSB7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbiAub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc19saXN0IC5zdWNjZXNzX2l0ZW0gLml0ZW1fbnVtIHtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBjb2xvcjogIzExMWQ1ZTtcbn1cbiAub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc19saXN0IC5zdWNjZXNzX2l0ZW0gLml0ZW1fZGVzYyB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGNvbG9yOiAjMTExZDVlO1xufVxuXG5cbi5pdGVtX251bSB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgY29sb3I6ICMxMTFkNWU7XG4gIH1cbiAgLnN1Y2Nlc3NfaXRlbSB7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICAuaXRlbV9kZXNjIHtcbiAgICBtYXJnaW46IDA7XG4gICAgY29sb3I6ICMxMTFkNWU7XG4gIH1cbiAgLnN1Y2Nlc3NfbGlzdCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgbWFyZ2luOiAwO1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cbiAgLnN1Y2Nlc3NfdHh0IHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjMTExZDVlO1xuICB9XG4gIC5zdWNjZXNzX2Rlc2Mge1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICBjb2xvcjogIzExMWQ1ZTtcbiAgfVxuLmNoaWZmcmV7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiA0MCU7XG4gICAgYWxpZ24taXRlbXM6IGluaGVyaXQ7XG59XG5cbiAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnJlc3BvbnNpdmUgY3NzICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAvKiBDdXN0b20sIGlQaG9uZSBSZXRpbmEgICovXG4gICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAzMjBweCkgYW5kIChtYXgtd2lkdGggOiA0ODBweCkgIHtcbiAgXG5cbiAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbm90cmUgc3VjY2VzKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgICAuY2hpZmZyZXtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIH1cblxuICAgICAgLmt7XG4gICAgICAgIG1hcmdpbjogaW5pdGlhbDtcbiAgICAgIH1cbiAgICAgIC5zdWNjZXNzX2l0ZW17XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIH1cblxuICB9XG4gIFxuICAvKiBFeHRyYSBTbWFsbCBEZXZpY2VzLCBQaG9uZXMgKi9cbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogNDgwcHgpIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpICB7XG4gICAgICAgICAgICAgIFxuICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipub3RyZSBzdWNjZXMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgIC5jaGlmZnJle1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIC5re1xuICAgICAgICBtYXJnaW46IGluaXRpYWw7XG4gICAgICAgIH1cbiAgICAgICAgLnN1Y2Nlc3NfaXRlbXtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICB9XG4gIFxuICB9XG4gIFxuICBcbiAgXG4gICAvKiBTbWFsbCBEZXZpY2VzLCBUYWJsZXRzKi9cbiAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDk5MnB4KSAge1xuICAgICAgIFxuICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipub3RyZSBzdWNjZXMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAgIC5jaGlmZnJle1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgLmt7XG4gICAgICAgIG1hcmdpbjogaW5pdGlhbDtcbiAgICAgICAgfVxuICAgICAgICAuc3VjY2Vzc19pdGVte1xuICAgICAgICBtYXJnaW4tbGVmdDogMjBweDtcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgICAgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnRpdGxle1xuICAgICAgICAgIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgXG4gICB9XG4gIFxuICBcbiAgICBcbiAgIC8qIE1lZGl1bSBEZXZpY2VzLCBEZXNrdG9wc1xuICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogOTkycHgpIGFuZCAobWF4LXdpZHRoIDogMTIwMHB4KSAge1xuICAgICAgXG4gICAgICB9Ki8gICJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotreSuccesComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-notre-succes',
                templateUrl: './notre-succes.component.html',
                styleUrls: ['./notre-succes.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/pass-oublier/pass-oublier.component.ts":
/*!************************************************************!*\
  !*** ./src/app/cpn/pass-oublier/pass-oublier.component.ts ***!
  \************************************************************/
/*! exports provided: PassOublierComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassOublierComponent", function() { return PassOublierComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");






class PassOublierComponent {
    constructor(fb, auth) {
        this.fb = fb;
        this.auth = auth;
        this.mailForm = this.fb.group({
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
        });
    }
    ngOnInit() {
    }
    onSubmit() {
        const formData = new FormData();
        formData.append('email', this.mailForm.get('email').value);
        this.auth.sendMail(formData).subscribe(data => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                position: 'top-end',
                icon: 'success',
                title: 'mail reussie',
                showConfirmButton: false,
                timer: 6000
            });
            location.reload();
        }, error => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'mail d"utilisateur saisi est introuvable !',
            });
        });
    }
}
PassOublierComponent.ɵfac = function PassOublierComponent_Factory(t) { return new (t || PassOublierComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"])); };
PassOublierComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PassOublierComponent, selectors: [["app-pass-oublier"]], decls: 43, vars: 1, consts: [[1, "login_container"], [1, "login_wrapper"], [1, "login_content"], [1, "row", "g-0", "py-2"], [1, "col-md-6"], [1, "container"], [1, "row", "row-cols-3", "g-2"], [1, "col"], ["src", "assets/cpnimages/connexion/3.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/2.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/1.png", "alt", "", 2, "width", "100%", "border-radius", "0 3.5rem 0 0", "height", "100%"], ["src", "assets/cpnimages/connexion/4.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/5.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/6.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/7.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/8.png", "alt", "", 2, "width", "100%", "height", "100%"], ["src", "assets/cpnimages/connexion/9.png", "alt", "", 2, "border-radius", "0 0 3.5rem 0", "width", "100%", "height", "100%"], [1, "col-md-6", 2, "display", "flex", "flex-direction", "row", "justify-content", "center"], [1, "card-body", "p-4", "p-sm-5"], [1, "r\u00E9seau-sociaux"], [1, "text-center"], ["href", ""], ["src", "assets/cpnimages/connexion/gmail.png", "alt", "", 2, "width", "10%", "margin-left", "10px"], [3, "formGroup", "ngSubmit"], [1, "col-md-12", "pb-2"], ["type", "text", "formControlName", "email", "id", "floatingInput", "placeholder", "Email", 1, "form-control"], [1, "container", "overflow-hidden"], [1, "row"], [2, "text-align", "center", "color", "black"], ["type", "submit", 1, "btn", "btn-danger", 2, "background-color", "red"]], template: function PassOublierComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h3", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "votre mail svp !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "form", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function PassOublierComponent_Template_form_ngSubmit_35_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "input", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "envoyer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.mailForm);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"]], styles: [".login_container[_ngcontent-%COMP%]{\n    background-color: #EBECF0;\n}\n\n.login_container[_ngcontent-%COMP%]   .login_content[_ngcontent-%COMP%]   .login_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]{\n    margin-top:0;\n    margin-right: 0;\n    margin-left: 0;\n}\n\n.container[_ngcontent-%COMP%]{\n    margin: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcGFzcy1vdWJsaWVyL3Bhc3Mtb3VibGllci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kseUJBQXlCO0FBQzdCOztBQUVBO0lBQ0ksWUFBWTtJQUNaLGVBQWU7SUFDZixjQUFjO0FBQ2xCOztBQUNBO0lBQ0ksWUFBWTtBQUNoQiIsImZpbGUiOiJhcHAvY3BuL3Bhc3Mtb3VibGllci9wYXNzLW91YmxpZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dpbl9jb250YWluZXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0VCRUNGMDtcbn1cblxuLmxvZ2luX2NvbnRhaW5lciAubG9naW5fY29udGVudCAubG9naW5fd3JhcHBlciAucm93e1xuICAgIG1hcmdpbi10b3A6MDtcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XG4gICAgbWFyZ2luLWxlZnQ6IDA7XG59XG4uY29udGFpbmVye1xuICAgIG1hcmdpbjogMzBweDtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PassOublierComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-pass-oublier',
                templateUrl: './pass-oublier.component.html',
                styleUrls: ['./pass-oublier.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/profile/profile.component.ts":
/*!**************************************************!*\
  !*** ./src/app/cpn/profile/profile.component.ts ***!
  \**************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var ngx_dropzone__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-dropzone */ "./node_modules/ngx-dropzone/__ivy_ngcc__/fesm2015/ngx-dropzone.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");









function ProfileComponent_ngx_dropzone_preview_97_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ngx-dropzone-preview", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("removed", function ProfileComponent_ngx_dropzone_preview_97_Template_ngx_dropzone_preview_removed_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const f_r1 = ctx.$implicit; const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.onRemove(f_r1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ngx-dropzone-label");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const f_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("removable", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", f_r1.name, " (", f_r1.type, ")");
} }
'src/app/baseUrl';
class ProfileComponent {
    constructor(auth, fb, tokenStorage, route) {
        this.auth = auth;
        this.fb = fb;
        this.tokenStorage = tokenStorage;
        this.route = route;
        this.id = "13";
        this.token = "";
        this.user = null;
        this.files = [];
        this.url = 'https://www.cpn-aide-aux-entreprise.jobid.fr/img/';
        this.modifierProfile = this.fb.group({
            firstname: [''],
            lastname: [''],
            email: [''],
            twitter: [''],
            instagram: [''],
            facebook: [''],
            teleph: [''],
            adresse: [''],
        });
    }
    /**********************life Cycle ***********************/
    //Add user form actions
    get f() { return this.modifierProfile.controls; }
    ngOnInit() {
        this.auth.getUser().subscribe(res => {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r;
            this.profile = res;
            this.user = res.data;
            this.socialMedia = res.usersoc;
            this.adresse = res.adress;
            this.modifierProfile.get('firstname').setValue((_b = (_a = this.profile) === null || _a === void 0 ? void 0 : _a.data) === null || _b === void 0 ? void 0 : _b.first_name);
            this.modifierProfile.get('lastname').setValue((_d = (_c = this.profile) === null || _c === void 0 ? void 0 : _c.data) === null || _d === void 0 ? void 0 : _d.last_name);
            this.modifierProfile.get('email').setValue((_f = (_e = this.profile) === null || _e === void 0 ? void 0 : _e.data) === null || _f === void 0 ? void 0 : _f.email);
            this.modifierProfile.get('twitter').setValue((_h = (_g = this.profile) === null || _g === void 0 ? void 0 : _g.social) === null || _h === void 0 ? void 0 : _h.twitter);
            this.modifierProfile.get('instagram').setValue((_k = (_j = this.profile) === null || _j === void 0 ? void 0 : _j.social) === null || _k === void 0 ? void 0 : _k.instagram);
            this.modifierProfile.get('facebook').setValue((_m = (_l = this.profile) === null || _l === void 0 ? void 0 : _l.social) === null || _m === void 0 ? void 0 : _m.facebook);
            this.modifierProfile.get('teleph').setValue((_p = (_o = this.profile) === null || _o === void 0 ? void 0 : _o.social) === null || _p === void 0 ? void 0 : _p.teleph);
            this.modifierProfile.get('adresse').setValue((_r = (_q = this.profile) === null || _q === void 0 ? void 0 : _q.adress) === null || _r === void 0 ? void 0 : _r.address);
        });
    }
    /********************************update profile *****************************/
    RegisterUser() {
        const formData = new FormData();
        formData.append('image', this.files[0]);
        formData.append('email', this.modifierProfile.get('email').value);
        formData.append('firstname', this.modifierProfile.get('firstname').value);
        formData.append('lastname', this.modifierProfile.get('lastname').value);
        formData.append('twitter', this.modifierProfile.get('twitter').value);
        formData.append('instagram', this.modifierProfile.get('instagram').value);
        formData.append('facebook', this.modifierProfile.get('facebook').value);
        formData.append('teleph', this.modifierProfile.get('teleph').value);
        formData.append('adresse', this.modifierProfile.get('adresse').value);
        if (formData) {
            this.auth.updatUser(formData).subscribe(res => {
                if (!res.error) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                        icon: 'success',
                        title: 'modifier reussie',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    location.reload();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: res.message + ' !',
                    });
                }
            }, error => {
                console.log(error);
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'error 500 !',
                });
            });
        }
    }
    onSelect(event) {
        this.files.push(...event.addedFiles);
    }
    onRemove(event) {
        this.files.splice(this.files.indexOf(event), 1);
    }
    openModal() {
        $('#cpnEditProfil').appendTo("body").modal('show');
    }
}
ProfileComponent.ɵfac = function ProfileComponent_Factory(t) { return new (t || ProfileComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"])); };
ProfileComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ProfileComponent, selectors: [["app-profile"]], decls: 121, vars: 14, consts: [[1, "body"], [1, "main-body"], [1, "row", "gutters-sm"], [1, "col-md-4", "mb-3"], [1, "card"], [1, "card-body"], [1, "d-flex", "flex-column", "align-items-center", "text-center"], ["alt", "avatar", 1, "round", 3, "src"], [1, "mt-3"], [1, "text-secondary", "mb-1"], [1, "text-muted", "font-size-sm"], [1, "card", "mt-3"], [1, "list-group", "list-group-flush"], [1, "list-group-item", "d-flex", "justify-content-between", "align-items-center", "flex-wrap"], [1, "mb-0"], ["xmlns", "http://www.w3.org/2000/svg", "width", "24", "height", "24", "viewBox", "0 0 24 24", "fill", "none", "stroke", "currentColor", "stroke-width", "2", "stroke-linecap", "round", "stroke-linejoin", "round", 1, "feather", "feather-globe", "mr-2", "icon-inline"], ["cx", "12", "cy", "12", "r", "10"], ["x1", "2", "y1", "12", "x2", "22", "y2", "12"], ["d", "M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"], [1, "text-secondary"], [3, "href"], ["xmlns", "http://www.w3.org/2000/svg", "width", "24", "height", "24", "viewBox", "0 0 24 24", "fill", "none", "stroke", "currentColor", "stroke-width", "2", "stroke-linecap", "round", "stroke-linejoin", "round", 1, "feather", "feather-twitter", "mr-2", "icon-inline", "text-info"], ["d", "M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"], ["xmlns", "http://www.w3.org/2000/svg", "width", "24", "height", "24", "viewBox", "0 0 24 24", "fill", "none", "stroke", "currentColor", "stroke-width", "2", "stroke-linecap", "round", "stroke-linejoin", "round", 1, "feather", "feather-instagram", "mr-2", "icon-inline", "text-danger"], ["x", "2", "y", "2", "width", "20", "height", "20", "rx", "5", "ry", "5"], ["d", "M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"], ["x1", "17.5", "y1", "6.5", "x2", "17.51", "y2", "6.5"], ["xmlns", "http://www.w3.org/2000/svg", "width", "24", "height", "24", "viewBox", "0 0 24 24", "fill", "none", "stroke", "currentColor", "stroke-width", "2", "stroke-linecap", "round", "stroke-linejoin", "round", 1, "feather", "feather-facebook", "mr-2", "icon-inline", "text-primary"], ["d", "M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"], [1, "col-md-8"], [1, "card", "mb-3"], [1, "row"], [1, "col-sm-3"], [1, "col-sm-9", "text-secondary"], [1, "col-sm-12"], ["type", "button", "data-toggle", "modal", "data-target", "#primary", 1, "btn", "btn-danger"], ["id", "primary", "tabindex", "-1", "role", "dialog", "aria-labelledby", "myModalLabel160", "aria-hidden", "true", 1, "modal", "fade", "text-left"], [1, "modal-dialog"], [1, "modal-content"], [1, "modal-header"], [1, "modal-title", "align-center"], [1, "modal-body"], [3, "formGroup", "ngSubmit"], [1, "row", "mb-3", "g-4", "listinput", 2, "flex", "2"], [1, "col-md-12"], [1, "custom-file", "mt-4", "h-auto"], [3, "change"], [3, "removable", "removed", 4, "ngFor", "ngForOf"], [1, "mb-0", "mt-2"], ["type", "text", "formControlName", "firstname", "placeholder", "Nom d'utilisateur", 1, "form-control"], ["type", "text", "formControlName", "lastname", "placeholder", "prenom d'utilisateur", 1, "form-control"], ["type", "email", "formControlName", "email", "id", "email", "placeholder", "Email", 1, "form-control"], ["type", "text", "formControlName", "twitter", "id", "twitter", "placeholder", "twitter", 1, "form-control"], ["type", "text", "formControlName", "instagram", "id", "instagram", "placeholder", "instagram", 1, "form-control"], ["type", "text", "formControlName", "facebook", "id", "facebook", "placeholder", "facebook", 1, "form-control"], ["type", "text", "formControlName", "teleph", "id", "teleph", "placeholder", "teleph", 1, "form-control"], ["type", "text", "formControlName", "adresse", "id", "adresse", "placeholder", "adresse", 1, "form-control"], [1, "col-12"], ["type", "submit", 1, "btn", "btn-danger", 2, "border", "none", "background", "red", "border-radius", "25px", "color", "white"], [3, "removable", "removed"]], template: function ProfileComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "ul", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h6", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "svg", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "circle", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "line", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "path", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Website ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "span", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "li", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "h6", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "svg", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "path", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " Twitter ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "span", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "li", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "h6", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "svg", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "rect", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "path", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "line", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Instagram ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "span", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "li", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "h6", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "svg", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "path", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, " Facebook ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "span", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "h6", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Nom & Pr\u00E9nom");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "h6", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Email");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "h6", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "Phone");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "h6", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Adresse");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](78, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "button", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Modifier");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "h4", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "modifier profile");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "form", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ProfileComponent_Template_form_ngSubmit_90_listener() { return ctx.RegisterUser(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "ngx-dropzone", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function ProfileComponent_Template_ngx_dropzone_change_94_listener($event) { return ctx.onSelect($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "ngx-dropzone-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Select image !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](97, ProfileComponent_ngx_dropzone_preview_97_Template, 3, 3, "ngx-dropzone-preview", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, " *minimum 500px x 500px ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "input", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "input", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "input", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "input", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "input", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "input", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "input", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "input", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "div", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "button", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "envoyer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", (ctx.profile == null ? null : ctx.profile.image == null ? null : ctx.profile.image.link) != null ? ctx.url + (ctx.profile == null ? null : ctx.profile.image == null ? null : ctx.profile.image.link) : "assets/cpnimages/home/profile.png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.profile == null ? null : ctx.profile.data == null ? null : ctx.profile.data.first_name);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.profile == null ? null : ctx.profile.data == null ? null : ctx.profile.data.email);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.profile == null ? null : ctx.profile.adress == null ? null : ctx.profile.adress.address);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("href", ctx.profile == null ? null : ctx.profile.social == null ? null : ctx.profile.social.twitter, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("href", ctx.profile == null ? null : ctx.profile.social == null ? null : ctx.profile.social.instagram, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("href", ctx.profile == null ? null : ctx.profile.social == null ? null : ctx.profile.social.facebook, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" ", ctx.profile == null ? null : ctx.profile.data == null ? null : ctx.profile.data.first_name, " ", ctx.profile == null ? null : ctx.profile.data == null ? null : ctx.profile.data.last_name, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.profile == null ? null : ctx.profile.data == null ? null : ctx.profile.data.email, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.profile == null ? null : ctx.profile.social == null ? null : ctx.profile.social.teleph, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.profile == null ? null : ctx.profile.adress == null ? null : ctx.profile.adress.address, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.modifierProfile);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.files);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], ngx_dropzone__WEBPACK_IMPORTED_MODULE_6__["NgxDropzoneComponent"], ngx_dropzone__WEBPACK_IMPORTED_MODULE_6__["ɵb"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], ngx_dropzone__WEBPACK_IMPORTED_MODULE_6__["NgxDropzonePreviewComponent"]], styles: [".body[_ngcontent-%COMP%] {\n\n  background-color: #EBECF0;\n  width: 100%;\n}\n\n.main-body[_ngcontent-%COMP%] {\n  padding: 55px;\n}\n\n.round[_ngcontent-%COMP%]{\n  width:125px;\n   height:125px;\n   border-radius: 180px;\n}\n\n.card[_ngcontent-%COMP%] {\n  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);\n}\n\n.card[_ngcontent-%COMP%] {\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  min-width: 0;\n  word-wrap: break-word;\n  background-color: #fff;\n  background-clip: border-box;\n  border: 0 solid rgba(0, 0, 0, .125);\n  border-radius: 1.25rem;\n}\n\n.card-body[_ngcontent-%COMP%] {\n  flex: 1 1 auto;\n  min-height: 1px;\n  padding: 1rem;\n}\n\n.gutters-sm[_ngcontent-%COMP%] {\n  margin-right: -8px;\n  margin-left: -8px;\n}\n\n.gutters-sm[_ngcontent-%COMP%]    > .col[_ngcontent-%COMP%], .gutters-sm[_ngcontent-%COMP%]    > [class*=col-][_ngcontent-%COMP%] {\n  padding-right: 8px;\n  padding-left: 8px;\n}\n\n.mb-3[_ngcontent-%COMP%], .my-3[_ngcontent-%COMP%] {\n  margin-bottom: 1rem !important;\n}\n\n.bg-gray-300[_ngcontent-%COMP%] {\n  background-color: #e2e8f0;\n}\n\n.h-100[_ngcontent-%COMP%] {\n  height: 100% !important;\n}\n\n.shadow-none[_ngcontent-%COMP%] {\n  box-shadow: none !important;\n}\n\n.btn[_ngcontent-%COMP%]{\ncolor: white;\nwidth: 200px;\nfloat: right;\nborder: none;\nbackground: red;\nborder-radius: 25px;\ncolor: white\n}\n\n.btn[_ngcontent-%COMP%]:hover{\n  color: white;\n  }\n\na[_ngcontent-%COMP%]{\n    text-decoration: none;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUseUJBQXlCO0VBQ3pCLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGFBQWE7QUFDZjs7QUFDQTtFQUNFLFdBQVc7R0FDVixZQUFZO0dBQ1osb0JBQW9CO0FBQ3ZCOztBQUVBO0VBQ0UseUVBQXlFO0FBQzNFOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixzQkFBc0I7RUFDdEIsMkJBQTJCO0VBQzNCLG1DQUFtQztFQUNuQyxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGFBQWE7QUFDZjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsOEJBQThCO0FBQ2hDOztBQUVBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsdUJBQXVCO0FBQ3pCOztBQUVBO0VBQ0UsMkJBQTJCO0FBQzdCOztBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixZQUFZO0FBQ1osWUFBWTtBQUNaLGVBQWU7QUFDZixtQkFBbUI7QUFDbkI7QUFDQTs7QUFDQTtFQUNFLFlBQVk7RUFDWjs7QUFFQTtJQUNFLHFCQUFxQjtFQUN2QiIsImZpbGUiOiJhcHAvY3BuL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJvZHkge1xuXG4gIGJhY2tncm91bmQtY29sb3I6ICNFQkVDRjA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubWFpbi1ib2R5IHtcbiAgcGFkZGluZzogNTVweDtcbn1cbi5yb3VuZHtcbiAgd2lkdGg6MTI1cHg7XG4gICBoZWlnaHQ6MTI1cHg7XG4gICBib3JkZXItcmFkaXVzOiAxODBweDtcbn1cblxuLmNhcmQge1xuICBib3gtc2hhZG93OiAwIDFweCAzcHggMCByZ2JhKDAsIDAsIDAsIC4xKSwgMCAxcHggMnB4IDAgcmdiYSgwLCAwLCAwLCAuMDYpO1xufVxuXG4uY2FyZCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgbWluLXdpZHRoOiAwO1xuICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQtY2xpcDogYm9yZGVyLWJveDtcbiAgYm9yZGVyOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgLjEyNSk7XG4gIGJvcmRlci1yYWRpdXM6IDEuMjVyZW07XG59XG5cbi5jYXJkLWJvZHkge1xuICBmbGV4OiAxIDEgYXV0bztcbiAgbWluLWhlaWdodDogMXB4O1xuICBwYWRkaW5nOiAxcmVtO1xufVxuXG4uZ3V0dGVycy1zbSB7XG4gIG1hcmdpbi1yaWdodDogLThweDtcbiAgbWFyZ2luLWxlZnQ6IC04cHg7XG59XG5cbi5ndXR0ZXJzLXNtID4gLmNvbCwgLmd1dHRlcnMtc20gPiBbY2xhc3MqPWNvbC1dIHtcbiAgcGFkZGluZy1yaWdodDogOHB4O1xuICBwYWRkaW5nLWxlZnQ6IDhweDtcbn1cblxuLm1iLTMsIC5teS0zIHtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbSAhaW1wb3J0YW50O1xufVxuXG4uYmctZ3JheS0zMDAge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTJlOGYwO1xufVxuXG4uaC0xMDAge1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbn1cblxuLnNoYWRvdy1ub25lIHtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xufVxuLmJ0bntcbmNvbG9yOiB3aGl0ZTtcbndpZHRoOiAyMDBweDtcbmZsb2F0OiByaWdodDtcbmJvcmRlcjogbm9uZTtcbmJhY2tncm91bmQ6IHJlZDtcbmJvcmRlci1yYWRpdXM6IDI1cHg7XG5jb2xvcjogd2hpdGVcbn1cbi5idG46aG92ZXJ7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgfVxuXG4gIGF7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICB9Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProfileComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-profile',
                templateUrl: './profile.component.html',
                styleUrls: ['./profile.component.css'],
            }]
    }], function () { return [{ type: src_app_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }, { type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] }]; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/auvergne/auvergne.component.ts":
/*!************************************************************!*\
  !*** ./src/app/cpn/regions/auvergne/auvergne.component.ts ***!
  \************************************************************/
/*! exports provided: AuvergneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuvergneComponent", function() { return AuvergneComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class AuvergneComponent {
    constructor() { }
    ngOnInit() {
    }
}
AuvergneComponent.ɵfac = function AuvergneComponent_Factory(t) { return new (t || AuvergneComponent)(); };
AuvergneComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AuvergneComponent, selectors: [["app-auvergne"]], decls: 156, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/avergne.png", "alt", "", 2, "width", "100%"], [2, "color", "red", "font-weight", "bold"], [2, "color", "#111D5E"], [1, "list-group"], [2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "red"], [1, "btn_post", "d-flex", "justify-content-center"], ["type", "submit", 2, "text-decoration", "auto", "border", "none", "background", "red", "border-radius", "25px", "color", "white", "padding", "5px 15px"]], template: function AuvergneComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Ch\u00E8que Commerce en ligne ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Dans le cadre du reconfinement et ses impacts imm\u00E9diats sur l\u2019\u00E9conomie de nombreux commer\u00E7ants et artisans, la R\u00E9gion Auvergne-Rh\u00F4ne-Alpes met en place le dispositif \"Mon Commerce en ligne\" qui va permettre le d\u00E9veloppement de la vente en ligne (e-commerce) et la pr\u00E9sence sur le web des artisans et des commer\u00E7ants. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Conditions d\u2019allocations ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h3", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "A qui s\u2019adresse ce dispositif ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Entreprises \u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "L'aide s'adresse aux commer\u00E7ants de proximit\u00E9, artisans ind\u00E9pendants, avec ou sans point de vente, s\u00E9dentaires ou non (hors franchise). L\u2019artisan ou commer\u00E7ant de proximit\u00E9 vend des produits ou services de mani\u00E8re quotidienne ou fr\u00E9quente \u00E0 des particuliers. Et \u00E9galement aux agriculteurs, \u00E9leveurs et viticulteurs qui r\u00E9alisent de la vente aux particuliers ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Crit\u00E8res d\u2019\u00E9ligibilit\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "L'aide s'adresse aux commer\u00E7ants de proximit\u00E9, artisans ind\u00E9pendants, avec ou sans point de vente, s\u00E9dentaires ou non (hors franchise). L\u2019artisan ou\ncommer\u00E7ant de proximit\u00E9 vend des produits ou services de mani\u00E8re quotidienne ou fr\u00E9quente \u00E0 des particuliers. Et \u00E9galement aux agriculteurs,\n\u00E9leveurs et viticulteurs qui r\u00E9alisent de la vente aux particuliers ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "ul", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Les entreprises doivent r\u00E9pondre aux crit\u00E8res suivants : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Avoir leur si\u00E8ge social en Auvergne-Rh\u00F4ne-Alpes. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Avoir un effectif de moins de 10 salari\u00E9s. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Inscrites au Registre du Commerce et des Soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des M\u00E9tiers, ou relevant de la liste des entreprises de m\u00E9tiers d\u2019art reconnues par l\u2019arr\u00EAt\u00E9 du 24 d\u00E9cembre 2015 ou les agriculteurs \u00E0 titre principal ou secondaire (personnes physiques ayant le statut d\u2019agriculteur \u00E0 la MSA). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h3", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "A qui s\u2019adresse ce dispositif ?\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Sont \u00E9ligibles les projets de cr\u00E9ation, de refonte ou d'optimisation d\u2019un site internet ou d\u2019un site e-commerce et \u00E9galement les projets d'optimisation de la pr\u00E9sence web. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "D\u00E9penses concern\u00E9es ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Sont \u00E9ligibles les d\u00E9penses r\u00E9alis\u00E9es entre le 1er janvier 2020 et le 30 septembre 2022. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "ul", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Les d\u00E9penses retenues sont celles inscrites \u00E0 l'actif de l'entreprise pour le d\u00E9veloppement, l\u2019optimisation, la r\u00E9alisation et l'acquisition de site internet et celles inscrites dans les charges de l'entreprise comme notamment : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Les d\u00E9penses de publicit\u00E9 digitale, solutions de fid\u00E9lisation (achat publicitaire, carte fid\u00E9lit\u00E9, envoi sms et newsletter, \u2026) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Les frais de r\u00E9f\u00E9rencement, achat de mots cl\u00E9, strat\u00E9gie de pr\u00E9sence sur les r\u00E9seaux sociaux. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "L'achat de nom de domaine. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Les frais d\u2019h\u00E9bergement, g\u00E9olocalisation de l\u2019entreprise. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "L'abonnement \u00E0 un logiciel de cr\u00E9ation de site en SaaS. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "Les frais d\u2019optimisation et de formation. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Les solutions digitales pour booster les ventes en ligne (livraison \u00E0 domicile, Marketplace, click and collect, mise en place d\u2019application de vente en ligne, \u2026.)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Quelles sont les particularit\u00E9s ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Entreprises in\u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Sont exclues de ce dispositif les professions lib\u00E9rales.\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "h2", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "Montant de l\u2019aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "h3", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "De quel type d\u2019aide il s\u2019agit ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "ul", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "L'aide est de 1 500 \u20AC maximum, selon deux modalit\u00E9s : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "Jusqu\u2019\u00E0 500 \u20AC de d\u00E9penses \u00E9ligibles : prise en charge \u00E0 100 %");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Au-del\u00E0 de 500 \u20AC de d\u00E9penses \u00E9ligibles : prise en charge \u00E0 50% des d\u00E9penses jusqu\u2019\u00E0 une aide maximum de 1 500 \u20AC");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Informations pratiques ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "h3", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, "Quelles sont les d\u00E9marches \u00E0 suivre ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "Aupr\u00E8s de quel organisme ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](102, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Pour toute information : digital@auvergnerhonealpes.fr. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "Les entreprises font la demande en ligne sur le Portail des Aides de la R\u00E9gion. Seuls les dossiers complets pourront \u00EAtre pr\u00E9sent\u00E9s. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "\u00C9l\u00E9ments \u00E0 pr\u00E9voir ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "Pour toute information : digital@auvergnerhonealpes.fr. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "Les entreprises font la demande en ligne sur le Portail des Aides de la R\u00E9gion. Seuls les dossiers complets pourront \u00EAtre pr\u00E9sent\u00E9s. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "ul", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "Les pi\u00E8ces \u00E0 transmettre au dossier sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "Un extrait Kbis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "Un RIB.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Les factures acquitt\u00E9es (\u00E0 compter du 01/01/2020). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "L'attestation sur l\u2019honneur (garantissant que le demandeur n\u2019a pas touch\u00E9 de subvention de l\u2019Etat sur le projet, la r\u00E9gion se r\u00E9servant le droit d\u2019\u00E9mettre un titre en cas de double financement).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "L'annexe de communication (avec, si cr\u00E9ation/am\u00E9lioration d\u2019un site, copie d\u2019\u00E9cran du site avec les r\u00E9f\u00E9rences de la r\u00E9gion)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](132, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "Crit\u00E8res compl\u00E9mentaires ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](135, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "h3", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](137, "Forme juridique\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](138, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "ul", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "Entreprise Individuelle ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, "Artisan ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "h3", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Donn\u00E9es suppl\u00E9mentaires ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "ul", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "Situation - R\u00E9glementation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](151, "A jour des versements fiscaux et sociaux ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](152, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](155, "Testez votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["li[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\np[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9hdXZlcmduZS9hdXZlcmduZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztBQUNsQiIsImZpbGUiOiJhcHAvY3BuL3JlZ2lvbnMvYXV2ZXJnbmUvYXV2ZXJnbmUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImxpe1xuICAgIGNvbG9yOiAjMTExRDVFO1xufVxucHtcbiAgICBjb2xvcjogIzExMUQ1RTtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuvergneComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-auvergne',
                templateUrl: './auvergne.component.html',
                styleUrls: ['./auvergne.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/collectiviteterritorialemartinique/collectiviteterritorialemartinique.component.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/cpn/regions/collectiviteterritorialemartinique/collectiviteterritorialemartinique.component.ts ***!
  \****************************************************************************************************************/
/*! exports provided: CollectiviteterritorialemartiniqueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectiviteterritorialemartiniqueComponent", function() { return CollectiviteterritorialemartiniqueComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class CollectiviteterritorialemartiniqueComponent {
    constructor() { }
    ngOnInit() {
    }
}
CollectiviteterritorialemartiniqueComponent.ɵfac = function CollectiviteterritorialemartiniqueComponent_Factory(t) { return new (t || CollectiviteterritorialemartiniqueComponent)(); };
CollectiviteterritorialemartiniqueComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CollectiviteterritorialemartiniqueComponent, selectors: [["app-collectiviteterritorialemartinique"]], decls: 193, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/martinique.png", "alt", ""], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "#111D5E"], [1, "list-group"], [2, "font-weight", "bold", "color", "#111D5E"], [2, "color", "red", "font-weight", "bold"], [2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "#00BFFF"]], template: function CollectiviteterritorialemartiniqueComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h2", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Suite aux nouvelles mesures sanitaire, la R\u00E9gion Nouvelle-Aquitaine soutient ses artisans et ses commer\u00E7ants dans leur transformation num\u00E9rique et met en place un ch\u00E8que E-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "En effet, le ch\u00E8que E-commerce vise \u00E0 am\u00E9liorer le d\u00E9veloppement commercial, la relation client et optimise la combinaison du e-commerce avec l\u2019espace physique de vente. Un diagnostic e-commerce au pr\u00E9alable vous sera soumis. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Ce dispositif a pour objectifs");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Am\u00E9liorer la visibilit\u00E9 en ligne : R\u00E9f\u00E9rencement, E-r\u00E9putation, Marketing Digital, Site Web Vitrine. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "D\u00E9velopper le e-commerce : Commande en ligne, \u201CClick and collect\u201D, Paiement en ligne, Site E-commerce, Places de march\u00E9, Num\u00E9risation du catalogue produits, Mise en valeur de ces produits. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Optimiser la gestion des stocks et de la logistique : caisse connect\u00E9e, \u00E9tiquettes intelligentes, base de donn\u00E9es en temps r\u00E9el des stocks, optimisation des flux logistiques. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "D\u00E9velopper la relation client : Am\u00E9lioration de la relation client ; strat\u00E9gie de fid\u00E9lisation ; acquisition de nouveaux clients ; enrichissement de l\u2019exp\u00E9rience client par le num\u00E9rique (R\u00E9alit\u00E9 virtuelle/R\u00E9alit\u00E9 Augment\u00E9e), prise de commande par tablette\u2026)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Accroitre la performance commerciale : strat\u00E9gie/plan de communication, strat\u00E9gie omnicanal\u2026");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "h2", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Conditions d'attributions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "A qui s'adresse ce dispositif?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Les entreprises doivent r\u00E9pondre aux conditions suivantes : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Avoir leur si\u00E8ge social ou leur \u00E9tablissement sur le territoire du Nouvelle-Aquitain. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "\u00CAtre immatricul\u00E9es au R\u00E9pertoire des M\u00E9tiers et/ou Registre du Commerce et des Soci\u00E9t\u00E9s, dont les entreprises relevant d\u2019une activit\u00E9 m\u00E9tiers d\u2019art telle que d\u00E9finie dans l\u2019arr\u00EAt\u00E9 du 24 d\u00E9cembre 2015 ou entreprises ayant un savoir-faire d\u2019excellence reconnu (labels EPV Entreprise du Patrimoine Vivant, OFG Origine France Garantie, IGIA Indications G\u00E9ographiques Industrielles et artisanales). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Avoir un effectif de moins de 10 salari\u00E9s au 01/11/2020 (R\u00E9f\u00E9rence code de la s\u00E9curit\u00E9 sociale) en ETP.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "\u00CAtre \u00E0 jour de leurs d\u00E9clarations et paiements et charges sociales et fiscales au 31/10/2020 (tenant compte des reports exceptionnels accord\u00E9s par l\u2019Etat dans le cadre de la crise COVID 19), \u00E0 l\u2019exception de celles b\u00E9n\u00E9ficiant d\u2019un plan de r\u00E8glement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Pour quel projet ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Les investissements pour les entreprises \u00E9ligibles sont les suivants : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, " Les prestations de conseils et de services (AMOA assistance \u00E0 maitrise d\u2019ouvrage, photographe, agence web, Community Management, agence de conseil digitale\u2026). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Les frais de formation. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Les frais de d\u00E9veloppement (site web, application, \u2026). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "Les frais d\u2019abonnement ou achat de logiciels (ERP, data client, caisse connect\u00E9e, stock\u2026) ou services e-commerce (click and collect, paiement, commande en ligne, service clients, suivi des commandes, fid\u00E9lisation des clients, logistique\u2026).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Les investissements mat\u00E9riels n\u00E9cessaires \u00E0 la mise en \u0153uvre du projet (tablette, casque VR, balance connect\u00E9e, douchette\u2026)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Les frais d\u2019h\u00E9bergement.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Les frais de publicit\u00E9 en ligne : r\u00E9f\u00E9rencement .");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "La campagne promotionnelle.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "Les frais de marketing digital plafonn\u00E9s \u00E0 5% des d\u00E9penses \u00E9ligibles.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](86, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "Quelles sont les particularit\u00E9s ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](89, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "Les activit\u00E9s non \u00E9ligibles sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, " Les commerces d\u2019une surface de vente sup\u00E9rieure \u00E0 300 m\u00B2 (grande distribution et autres enseignes franchis\u00E9es). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Les activit\u00E9s enregistr\u00E9es avec les codes NAF 01 et 03 (agriculture et p\u00EAche). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, "Les secteurs d\u2019activit\u00E9 exclus par les r\u00E8glements europ\u00E9ens. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Les entreprises en proc\u00E9dure collective d\u2019insolvabilit\u00E9. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "Les professions lib\u00E9rales r\u00E9glement\u00E9es.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](108, "Les professions li\u00E9es \u00E0 l\u2019\u00E9sot\u00E9risme et les activit\u00E9s de bien \u00EAtre non r\u00E8glement\u00E9es (Codes NAF 96.04 et 96.09).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Les entreprises intervenant dans les activit\u00E9s immobili\u00E8res ou de promotion immobili\u00E8re (Codes NAF 41-1 et ensemble des codes NAF de la section L), les activit\u00E9s financi\u00E8res et d\u2019assurance (ensemble des codes NAF de la section K). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](112, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "Les activit\u00E9s m\u00E9dicales et param\u00E9dicales, hors ressortissants CMA (ensemble des codes NAF de la section Q). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "Les activit\u00E9s d\u2019enseignement (ensemble des codes NAF de la section P). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "Les activit\u00E9s exclusivement propos\u00E9es en e-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "Les entreprises intervenant dans les activit\u00E9s sp\u00E9cialis\u00E9es, scientifiques et techniques (ensemble des codes NAF de la section M, \u00E0 l\u2019exception des groupes 70.2 ; 71.2 ; 71.12B ; 72.1 ; 72.2 ; 74.1 ; 74.3 ; 74.9).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](124, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Les d\u00E9penses non \u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "Les frais d\u2019adh\u00E9sion \u00E0 une place de march\u00E9 ne sont pas \u00E9ligibles au dispositif. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "Montant de l\u2019aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](132, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "De quel type d\u2019aide s\u2019agit-il ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](135, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, "Le ch\u00E8que E-commerce est octroy\u00E9 sous forme de subvention de la mani\u00E8re suivante : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, " Un taux d\u2019intervention de 50% maximum d\u2019investissement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](141, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, "Une subvention plafonn\u00E9e \u00E0 5 000 \u20AC . ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Un plancher d\u2019investissement de 2 000 \u20AC H.T ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](147, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "Avoir b\u00E9n\u00E9fici\u00E9 d\u2019un accompagnement diagnostic e-commerce de sa chambre consulaire ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](150, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](151, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "h1", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](153, "Informations pratiques");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](154, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "h3", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](156, "Aupr\u00E8s de quel organisme");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](157, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](160, " La demande d\u2019aide devra \u00EAtre d\u00E9pos\u00E9e sur une plateforme de d\u00E9p\u00F4t d\u00E9mat\u00E9rialis\u00E9e qui sera mise en place par la R\u00E9gion. Les 2 \u00E9tapes \u00E0 suivre pour d\u00E9poser la demande : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, " Etape n\u00B01 : r\u00E9aliser au pr\u00E9alable un diagnostic e-commerce gratuit de l\u2019entreprise avec un conseiller num\u00E9rique des r\u00E9seaux consulaires CCI ou CMA de Nouvelle Aquitaine, pour ce faire, remplir ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, " le test d\u2019\u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](165, " afin d\u2019\u00EAtre recontact\u00E9(e) par un conseiller num\u00E9rique consulaire. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](166, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, "Etape 2 : une fois le diagnostic r\u00E9alis\u00E9, d\u00E9poser le dossier en ligne aupr\u00E8s de la R\u00E9gion Nouvelle-Aquitaine sur une plateforme de d\u00E9p\u00F4t d\u00E9mat\u00E9rialis\u00E9e qui sera mise en place par la R\u00E9gion. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](169, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "h3", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](171, "\u00C9l\u00E9ments \u00E0 pr\u00E9voir");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](172, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](175, "Les documents \u00E0 joindre aux dossiers sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, " Le diagnostic E-commerce (r\u00E9alis\u00E9 au pr\u00E9alable par le r\u00E9seau consulaire CCI ou CMA) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "RIB ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](181, "KBis ou Extrait d\u2019immatriculation D1 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](183, "Devis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](184, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "h1", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](186, "Crit\u00E8res compl\u00E9mentaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](187, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "h2", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, "Forme juridique ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](190, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](191, "h4", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](192, "Ne sont pas \u00E9ligibles au dispositif, les professions lib\u00E9rales r\u00E9glement\u00E9es. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["li[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9jb2xsZWN0aXZpdGV0ZXJyaXRvcmlhbGVtYXJ0aW5pcXVlL2NvbGxlY3Rpdml0ZXRlcnJpdG9yaWFsZW1hcnRpbmlxdWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGNBQWM7QUFDbEIiLCJmaWxlIjoiYXBwL2Nwbi9yZWdpb25zL2NvbGxlY3Rpdml0ZXRlcnJpdG9yaWFsZW1hcnRpbmlxdWUvY29sbGVjdGl2aXRldGVycml0b3JpYWxlbWFydGluaXF1ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsibGl7XG4gICAgY29sb3I6ICMxMTFENUU7XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CollectiviteterritorialemartiniqueComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-collectiviteterritorialemartinique',
                templateUrl: './collectiviteterritorialemartinique.component.html',
                styleUrls: ['./collectiviteterritorialemartinique.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/corse/corse.component.ts":
/*!******************************************************!*\
  !*** ./src/app/cpn/regions/corse/corse.component.ts ***!
  \******************************************************/
/*! exports provided: CorseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorseComponent", function() { return CorseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class CorseComponent {
    constructor() { }
    ngOnInit() {
    }
}
CorseComponent.ɵfac = function CorseComponent_Factory(t) { return new (t || CorseComponent)(); };
CorseComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CorseComponent, selectors: [["app-corse"]], decls: 199, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/corse.png", "width", "60%", "height", "60%", "alt", ""], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "#111D5E"], [1, "list-group"], [2, "font-weight", "bold", "color", "#111D5E"], [2, "color", "red", "font-weight", "bold"], [2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "#00BFFF"]], template: function CorseComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h2", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Suite aux nouvelles mesures sanitaire, la R\u00E9gion Nouvelle-Aquitaine soutient ses artisans et ses commer\u00E7ants dans leur transformation num\u00E9rique et met en place un ch\u00E8que E-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "En effet, le ch\u00E8que E-commerce vise \u00E0 am\u00E9liorer le d\u00E9veloppement commercial, la relation client et optimise la combinaison du e-commerce avec l\u2019espace physique de vente. Un diagnostic e-commerce au pr\u00E9alable vous sera soumis. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Ce dispositif a pour objectifs");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Am\u00E9liorer la visibilit\u00E9 en ligne : R\u00E9f\u00E9rencement, E-r\u00E9putation, Marketing Digital, Site Web Vitrine. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "D\u00E9velopper le e-commerce : Commande en ligne, \u201CClick and collect\u201D, Paiement en ligne, Site E-commerce, Places de march\u00E9, Num\u00E9risation du catalogue produits, Mise en valeur de ces produits. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Optimiser la gestion des stocks et de la logistique : caisse connect\u00E9e, \u00E9tiquettes intelligentes, base de donn\u00E9es en temps r\u00E9el des stocks, optimisation des flux logistiques. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "D\u00E9velopper la relation client : Am\u00E9lioration de la relation client ; strat\u00E9gie de fid\u00E9lisation ; acquisition de nouveaux clients ; enrichissement de l\u2019exp\u00E9rience client par le num\u00E9rique (R\u00E9alit\u00E9 virtuelle/R\u00E9alit\u00E9 Augment\u00E9e), prise de commande par tablette\u2026)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Accroitre la performance commerciale : strat\u00E9gie/plan de communication, strat\u00E9gie omnicanal\u2026");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "h2", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Conditions d'attributions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "A qui s'adresse ce dispositif?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Les entreprises doivent r\u00E9pondre aux conditions suivantes : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Avoir leur si\u00E8ge social ou leur \u00E9tablissement sur le territoire du Nouvelle-Aquitain. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "\u00CAtre immatricul\u00E9es au R\u00E9pertoire des M\u00E9tiers et/ou Registre du Commerce et des Soci\u00E9t\u00E9s, dont les entreprises relevant d\u2019une activit\u00E9 m\u00E9tiers d\u2019art telle que d\u00E9finie dans l\u2019arr\u00EAt\u00E9 du 24 d\u00E9cembre 2015 ou entreprises ayant un savoir-faire d\u2019excellence reconnu (labels EPV Entreprise du Patrimoine Vivant, OFG Origine France Garantie, IGIA Indications G\u00E9ographiques Industrielles et artisanales). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Avoir un effectif de moins de 10 salari\u00E9s au 01/11/2020 (R\u00E9f\u00E9rence code de la s\u00E9curit\u00E9 sociale) en ETP.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "\u00CAtre \u00E0 jour de leurs d\u00E9clarations et paiements et charges sociales et fiscales au 31/10/2020 (tenant compte des reports exceptionnels accord\u00E9s par l\u2019Etat dans le cadre de la crise COVID 19), \u00E0 l\u2019exception de celles b\u00E9n\u00E9ficiant d\u2019un plan de r\u00E8glement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Pour quel projet ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Les investissements pour les entreprises \u00E9ligibles sont les suivants : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, " Les prestations de conseils et de services (AMOA assistance \u00E0 maitrise d\u2019ouvrage, photographe, agence web, Community Management, agence de conseil digitale\u2026). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Les frais de formation. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](66, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "Les frais de d\u00E9veloppement (site web, application, \u2026). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Les frais d\u2019abonnement ou achat de logiciels (ERP, data client, caisse connect\u00E9e, stock\u2026) ou services e-commerce (click and collect, paiement, commande en ligne, service clients, suivi des commandes, fid\u00E9lisation des clients, logistique\u2026).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "Les investissements mat\u00E9riels n\u00E9cessaires \u00E0 la mise en \u0153uvre du projet (tablette, casque VR, balance connect\u00E9e, douchette\u2026)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Les frais d\u2019h\u00E9bergement.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](78, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Les frais de publicit\u00E9 en ligne : r\u00E9f\u00E9rencement .");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](81, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "La campagne promotionnelle.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "Les frais de marketing digital plafonn\u00E9s \u00E0 5% des d\u00E9penses \u00E9ligibles.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Quelles sont les particularit\u00E9s ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](90, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "Les activit\u00E9s non \u00E9ligibles sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, " Les commerces d\u2019une surface de vente sup\u00E9rieure \u00E0 300 m\u00B2 (grande distribution et autres enseignes franchis\u00E9es). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Les activit\u00E9s enregistr\u00E9es avec les codes NAF 01 et 03 (agriculture et p\u00EAche). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](100, "Les secteurs d\u2019activit\u00E9 exclus par les r\u00E8glements europ\u00E9ens. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Les entreprises en proc\u00E9dure collective d\u2019insolvabilit\u00E9. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "Les professions lib\u00E9rales r\u00E9glement\u00E9es.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "Les professions li\u00E9es \u00E0 l\u2019\u00E9sot\u00E9risme et les activit\u00E9s de bien \u00EAtre non r\u00E8glement\u00E9es (Codes NAF 96.04 et 96.09).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "Les entreprises intervenant dans les activit\u00E9s immobili\u00E8res ou de promotion immobili\u00E8re (Codes NAF 41-1 et ensemble des codes NAF de la section L), les activit\u00E9s financi\u00E8res et d\u2019assurance (ensemble des codes NAF de la section K). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "Les activit\u00E9s m\u00E9dicales et param\u00E9dicales, hors ressortissants CMA (ensemble des codes NAF de la section Q). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "Les activit\u00E9s d\u2019enseignement (ensemble des codes NAF de la section P). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "Les activit\u00E9s exclusivement propos\u00E9es en e-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](122, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, "Les entreprises intervenant dans les activit\u00E9s sp\u00E9cialis\u00E9es, scientifiques et techniques (ensemble des codes NAF de la section M, \u00E0 l\u2019exception des groupes 70.2 ; 71.2 ; 71.12B ; 72.1 ; 72.2 ; 74.1 ; 74.3 ; 74.9).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](125, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Les d\u00E9penses non \u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "Les frais d\u2019adh\u00E9sion \u00E0 une place de march\u00E9 ne sont pas \u00E9ligibles au dispositif. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](130, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "h1", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132, "Montant de l\u2019aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](133, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "h2", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "De quel type d\u2019aide s\u2019agit-il ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](136, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](139, "Le ch\u00E8que E-commerce est octroy\u00E9 sous forme de subvention de la mani\u00E8re suivante : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, " Un taux d\u2019intervention de 50% maximum d\u2019investissement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](142, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "Une subvention plafonn\u00E9e \u00E0 5 000 \u20AC . ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](145, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](147, "Un plancher d\u2019investissement de 2 000 \u20AC H.T ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](148, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](150, "Avoir b\u00E9n\u00E9fici\u00E9 d\u2019un accompagnement diagnostic e-commerce de sa chambre consulaire ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](151, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](153, "Informations pratiques");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](154, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](156, "Quelles sont les d\u00E9marches \u00E0 suivre ?\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](158, "Aupr\u00E8s de quel organisme");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](159, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, " La demande d\u2019aide devra \u00EAtre d\u00E9pos\u00E9e sur une plateforme de d\u00E9p\u00F4t d\u00E9mat\u00E9rialis\u00E9e qui sera mise en place par la R\u00E9gion. Les 2 \u00E9tapes \u00E0 suivre pour d\u00E9poser la demande : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, " Etape n\u00B01 : r\u00E9aliser au pr\u00E9alable un diagnostic e-commerce gratuit de l\u2019entreprise avec un conseiller num\u00E9rique des r\u00E9seaux consulaires CCI ou CMA de Nouvelle Aquitaine, pour ce faire, remplir ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](166, " le test d\u2019\u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, " afin d\u2019\u00EAtre recontact\u00E9(e) par un conseiller num\u00E9rique consulaire. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](168, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](170, "Etape 2 : une fois le diagnostic r\u00E9alis\u00E9, d\u00E9poser le dossier en ligne aupr\u00E8s de la R\u00E9gion Nouvelle-Aquitaine sur une plateforme de d\u00E9p\u00F4t d\u00E9mat\u00E9rialis\u00E9e qui sera mise en place par la R\u00E9gion. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](171, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](173, "\u00C9l\u00E9ments \u00E0 pr\u00E9voir");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](174, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, "Les documents \u00E0 joindre aux dossiers sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, " Le diagnostic E-commerce (r\u00E9alis\u00E9 au pr\u00E9alable par le r\u00E9seau consulaire CCI ou CMA) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](180, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](182, "RIB ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](183, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "KBis ou Extrait d\u2019immatriculation D1 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](186, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](188, "Devis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](189, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, "Crit\u00E8res compl\u00E9mentaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](192, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](194, "Forme juridique ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](195, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](196, "h4", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](197, "Ne sont pas \u00E9ligibles au dispositif, les professions lib\u00E9rales r\u00E9glement\u00E9es. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](198, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["li[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9jb3JzZS9jb3JzZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztBQUNsQiIsImZpbGUiOiJhcHAvY3BuL3JlZ2lvbnMvY29yc2UvY29yc2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImxpe1xuICAgIGNvbG9yOiAjMTExRDVFO1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CorseComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-corse',
                templateUrl: './corse.component.html',
                styleUrls: ['./corse.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/grandest/grandest.component.ts":
/*!************************************************************!*\
  !*** ./src/app/cpn/regions/grandest/grandest.component.ts ***!
  \************************************************************/
/*! exports provided: GrandestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GrandestComponent", function() { return GrandestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class GrandestComponent {
    constructor() { }
    ngOnInit() {
    }
}
GrandestComponent.ɵfac = function GrandestComponent_Factory(t) { return new (t || GrandestComponent)(); };
GrandestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: GrandestComponent, selectors: [["app-grandest"]], decls: 66, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/grandest.png", "alt", ""], [1, "list-group"], [2, "color", "#111D5E"], [2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "red", "font-weight", "bold"], [1, "btn_post", "d-flex", "justify-content-center"], ["type", "submit", 2, "text-decoration", "auto", "border", "none", "background", "red", "border-radius", "25px", "color", "white", "padding", "5px 15px"]], template: function GrandestComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Ch\u00E8que Soutien \u00E0 la digitalisation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Via des prestataires labellis\u00E9s par la R\u00E9gion Grand-Est, les entreprises peuvent b\u00E9n\u00E9ficier d'une aide \u00E0 la digitalisation. Cette aide permet aux entreprises : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "D\u2019optimiser leur organisation.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "D\u2019adapter le design de l\u2019offre. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "De d\u00E9ployer une strat\u00E9gie de commercialisation et de communication");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "h3", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Conditions d\u2019attributions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "A qui s\u2019adresse ce dispositif ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Entreprises \u00E9ligibles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Les entreprises du territoire de la R\u00E9gion Grand Est.\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h3", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Montant de l\u2019aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "De quel type d\u2019aide s\u2019agit-il ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "h6", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "L'aide prend la forme d'un ch\u00E8que pouvant aller de 1 000 \u00E0 6 000 \u20AC d\u2019aide \u00E0 la digitalisation :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "3 000 \u20AC au maximum en soutien \u00E0 l\u2019accompagnement \u00E0 la digitalisation.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "3 000 \u20AC au maximum en soutien \u00E0 l\u2019investissement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "h2", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Informations pratiques");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "h2", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Quelles sont les d\u00E9marches \u00E0 suivre ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Aupr\u00E8s de quel organisme");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "La R\u00E9gion Grand Est en partenariat avec la CRCI et la CRMA, d\u00E9ploiera une solution r\u00E9gionale de prise de rendez-vous en ligne \u00E0 disposition de tous les commer\u00E7ants et les artisans du territoire, afin de faciliter leur transition vers le e-commerce.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Testez votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["p[_ngcontent-%COMP%]{\n    color:#111D5E;\n}\nli[_ngcontent-%COMP%]{\n    color:#111D5E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9ncmFuZGVzdC9ncmFuZGVzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksYUFBYTtBQUNqQjtBQUNBO0lBQ0ksYUFBYTtBQUNqQiIsImZpbGUiOiJhcHAvY3BuL3JlZ2lvbnMvZ3JhbmRlc3QvZ3JhbmRlc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInB7XG4gICAgY29sb3I6IzExMUQ1RTtcbn1cbmxpe1xuICAgIGNvbG9yOiMxMTFENUU7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GrandestComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-grandest',
                templateUrl: './grandest.component.html',
                styleUrls: ['./grandest.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/guadeloupe/guadeloupe.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/cpn/regions/guadeloupe/guadeloupe.component.ts ***!
  \****************************************************************/
/*! exports provided: GuadeloupeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuadeloupeComponent", function() { return GuadeloupeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class GuadeloupeComponent {
    constructor() { }
    ngOnInit() {
    }
}
GuadeloupeComponent.ɵfac = function GuadeloupeComponent_Factory(t) { return new (t || GuadeloupeComponent)(); };
GuadeloupeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: GuadeloupeComponent, selectors: [["app-guadeloupe"]], decls: 196, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/guadeloupe.png", "alt", ""], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "#111D5E"], [1, "list-group"], [2, "font-weight", "bold", "color", "#111D5E"], [2, "color", "red", "font-weight", "bold"], [2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "#00BFFF"]], template: function GuadeloupeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h2", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Suite aux nouvelles mesures sanitaire, la R\u00E9gion Nouvelle-Aquitaine soutient ses artisans et ses commer\u00E7ants dans leur transformation num\u00E9rique et met en place un ch\u00E8que E-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "En effet, le ch\u00E8que E-commerce vise \u00E0 am\u00E9liorer le d\u00E9veloppement commercial, la relation client et optimise la combinaison du e-commerce avec l\u2019espace physique de vente. Un diagnostic e-commerce au pr\u00E9alable vous sera soumis. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Ce dispositif a pour objectifs");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Am\u00E9liorer la visibilit\u00E9 en ligne : R\u00E9f\u00E9rencement, E-r\u00E9putation, Marketing Digital, Site Web Vitrine. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "D\u00E9velopper le e-commerce : Commande en ligne, \u201CClick and collect\u201D, Paiement en ligne, Site E-commerce, Places de march\u00E9, Num\u00E9risation du catalogue produits, Mise en valeur de ces produits. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Optimiser la gestion des stocks et de la logistique : caisse connect\u00E9e, \u00E9tiquettes intelligentes, base de donn\u00E9es en temps r\u00E9el des stocks, optimisation des flux logistiques. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "D\u00E9velopper la relation client : Am\u00E9lioration de la relation client ; strat\u00E9gie de fid\u00E9lisation ; acquisition de nouveaux clients ; enrichissement de l\u2019exp\u00E9rience client par le num\u00E9rique (R\u00E9alit\u00E9 virtuelle/R\u00E9alit\u00E9 Augment\u00E9e), prise de commande par tablette\u2026)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Accroitre la performance commerciale : strat\u00E9gie/plan de communication, strat\u00E9gie omnicanal\u2026");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "h2", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Conditions d'attributions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "A qui s'adresse ce dispositif?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Les entreprises doivent r\u00E9pondre aux conditions suivantes : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Avoir leur si\u00E8ge social ou leur \u00E9tablissement sur le territoire du Nouvelle-Aquitain. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "\u00CAtre immatricul\u00E9es au R\u00E9pertoire des M\u00E9tiers et/ou Registre du Commerce et des Soci\u00E9t\u00E9s, dont les entreprises relevant d\u2019une activit\u00E9 m\u00E9tiers d\u2019art telle que d\u00E9finie dans l\u2019arr\u00EAt\u00E9 du 24 d\u00E9cembre 2015 ou entreprises ayant un savoir-faire d\u2019excellence reconnu (labels EPV Entreprise du Patrimoine Vivant, OFG Origine France Garantie, IGIA Indications G\u00E9ographiques Industrielles et artisanales). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Avoir un effectif de moins de 10 salari\u00E9s au 01/11/2020 (R\u00E9f\u00E9rence code de la s\u00E9curit\u00E9 sociale) en ETP.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "\u00CAtre \u00E0 jour de leurs d\u00E9clarations et paiements et charges sociales et fiscales au 31/10/2020 (tenant compte des reports exceptionnels accord\u00E9s par l\u2019Etat dans le cadre de la crise COVID 19), \u00E0 l\u2019exception de celles b\u00E9n\u00E9ficiant d\u2019un plan de r\u00E8glement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Pour quel projet ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Les investissements pour les entreprises \u00E9ligibles sont les suivants : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, " Les prestations de conseils et de services (AMOA assistance \u00E0 maitrise d\u2019ouvrage, photographe, agence web, Community Management, agence de conseil digitale\u2026). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Les frais de formation. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Les frais de d\u00E9veloppement (site web, application, \u2026). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Les frais d\u2019abonnement ou achat de logiciels (ERP, data client, caisse connect\u00E9e, stock\u2026) ou services e-commerce (click and collect, paiement, commande en ligne, service clients, suivi des commandes, fid\u00E9lisation des clients, logistique\u2026).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "Les investissements mat\u00E9riels n\u00E9cessaires \u00E0 la mise en \u0153uvre du projet (tablette, casque VR, balance connect\u00E9e, douchette\u2026)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Les frais d\u2019h\u00E9bergement.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Les frais de publicit\u00E9 en ligne : r\u00E9f\u00E9rencement .");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "La campagne promotionnelle.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](82, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Les frais de marketing digital plafonn\u00E9s \u00E0 5% des d\u00E9penses \u00E9ligibles.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](85, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Quelles sont les particularit\u00E9s ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "Les activit\u00E9s non \u00E9ligibles sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, " Les commerces d\u2019une surface de vente sup\u00E9rieure \u00E0 300 m\u00B2 (grande distribution et autres enseignes franchis\u00E9es). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "Les activit\u00E9s enregistr\u00E9es avec les codes NAF 01 et 03 (agriculture et p\u00EAche). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](96, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "Les secteurs d\u2019activit\u00E9 exclus par les r\u00E8glements europ\u00E9ens. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](99, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "Les entreprises en proc\u00E9dure collective d\u2019insolvabilit\u00E9. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](102, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Les professions lib\u00E9rales r\u00E9glement\u00E9es.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "Les professions li\u00E9es \u00E0 l\u2019\u00E9sot\u00E9risme et les activit\u00E9s de bien \u00EAtre non r\u00E8glement\u00E9es (Codes NAF 96.04 et 96.09).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](108, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, "Les entreprises intervenant dans les activit\u00E9s immobili\u00E8res ou de promotion immobili\u00E8re (Codes NAF 41-1 et ensemble des codes NAF de la section L), les activit\u00E9s financi\u00E8res et d\u2019assurance (ensemble des codes NAF de la section K). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Les activit\u00E9s m\u00E9dicales et param\u00E9dicales, hors ressortissants CMA (ensemble des codes NAF de la section Q). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, "Les activit\u00E9s d\u2019enseignement (ensemble des codes NAF de la section P). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "Les activit\u00E9s exclusivement propos\u00E9es en e-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "Les entreprises intervenant dans les activit\u00E9s sp\u00E9cialis\u00E9es, scientifiques et techniques (ensemble des codes NAF de la section M, \u00E0 l\u2019exception des groupes 70.2 ; 71.2 ; 71.12B ; 72.1 ; 72.2 ; 74.1 ; 74.3 ; 74.9).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](124, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Les d\u00E9penses non \u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "Les frais d\u2019adh\u00E9sion \u00E0 une place de march\u00E9 ne sont pas \u00E9ligibles au dispositif. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "Montant de l\u2019aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](132, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "De quel type d\u2019aide s\u2019agit-il ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](135, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, "Le ch\u00E8que E-commerce est octroy\u00E9 sous forme de subvention de la mani\u00E8re suivante : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, " Un taux d\u2019intervention de 50% maximum d\u2019investissement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](141, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, "Une subvention plafonn\u00E9e \u00E0 5 000 \u20AC . ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Un plancher d\u2019investissement de 2 000 \u20AC H.T ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](147, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "Avoir b\u00E9n\u00E9fici\u00E9 d\u2019un accompagnement diagnostic e-commerce de sa chambre consulaire ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](150, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, "Informations pratiques");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](153, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](155, "Aupr\u00E8s de quel organisme");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](156, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](159, " La demande d\u2019aide devra \u00EAtre d\u00E9pos\u00E9e sur une plateforme de d\u00E9p\u00F4t d\u00E9mat\u00E9rialis\u00E9e qui sera mise en place par la R\u00E9gion. Les 2 \u00E9tapes \u00E0 suivre pour d\u00E9poser la demande : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, " Etape n\u00B01 : r\u00E9aliser au pr\u00E9alable un diagnostic e-commerce gratuit de l\u2019entreprise avec un conseiller num\u00E9rique des r\u00E9seaux consulaires CCI ou CMA de Nouvelle Aquitaine, pour ce faire, remplir ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](162, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](163, " le test d\u2019\u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, " afin d\u2019\u00EAtre recontact\u00E9(e) par un conseiller num\u00E9rique consulaire. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](165, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "Etape 2 : une fois le diagnostic r\u00E9alis\u00E9, d\u00E9poser le dossier en ligne aupr\u00E8s de la R\u00E9gion Nouvelle-Aquitaine sur une plateforme de d\u00E9p\u00F4t d\u00E9mat\u00E9rialis\u00E9e qui sera mise en place par la R\u00E9gion. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](168, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](170, "\u00C9l\u00E9ments \u00E0 pr\u00E9voir");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](171, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](174, "Les documents \u00E0 joindre aux dossiers sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, " Le diagnostic E-commerce (r\u00E9alis\u00E9 au pr\u00E9alable par le r\u00E9seau consulaire CCI ou CMA) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](177, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "RIB ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](180, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](182, "KBis ou Extrait d\u2019immatriculation D1 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](183, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "Devis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](186, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](188, "Crit\u00E8res compl\u00E9mentaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](189, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, "Forme juridique ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](192, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "h4", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](194, "Ne sont pas \u00E9ligibles au dispositif, les professions lib\u00E9rales r\u00E9glement\u00E9es. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](195, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["li[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9ndWFkZWxvdXBlL2d1YWRlbG91cGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGNBQWM7QUFDbEIiLCJmaWxlIjoiYXBwL2Nwbi9yZWdpb25zL2d1YWRlbG91cGUvZ3VhZGVsb3VwZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsibGl7XG4gICAgY29sb3I6ICMxMTFENUU7XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GuadeloupeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-guadeloupe',
                templateUrl: './guadeloupe.component.html',
                styleUrls: ['./guadeloupe.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/hautedefrance/hautedefrance.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/cpn/regions/hautedefrance/hautedefrance.component.ts ***!
  \**********************************************************************/
/*! exports provided: HautedefranceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HautedefranceComponent", function() { return HautedefranceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class HautedefranceComponent {
    constructor() { }
    ngOnInit() {
    }
}
HautedefranceComponent.ɵfac = function HautedefranceComponent_Factory(t) { return new (t || HautedefranceComponent)(); };
HautedefranceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HautedefranceComponent, selectors: [["app-hautedefrance"]], decls: 90, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/hautsdefrance.png", "alt", "", 2, "width", "100%"], [1, "list-group"], [2, "color", "#111D5E"], [2, "color", "red", "font-weight", "bold"], ["href", "", 2, "color", "#00BFFF"], [1, "btn_post", "d-flex", "justify-content-center"], ["type", "submit", 2, "text-decoration", "auto", "border", "none", "background", "red", "border-radius", "25px", "color", "white", "padding", "5px 15px"]], template: function HautedefranceComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Cheque Booster TPE Num\u00E9rique ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Dans le cadre du Programme Booster TPE, l\u2019accompagnement Booster performance commerciale permet aux petites entreprises de d\u00E9velopper leur activit\u00E9 et leur performance commerciale, et ce, afin de faire face \u00E0 la crise sanitaire et relancer l\u2019activit\u00E9 de l\u2019entreprise. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Cet accompagnement met en avant des solutions pratiques et rapides \u00E0 adopter.\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Booster performance commerciale a pour objectifs de : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Structurer la strat\u00E9gie commerciale de l\u2019entreprise. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Pr\u00E9ciser, s\u00E9curiser et consolider le projet de d\u00E9veloppement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Augmenter le chiffre d\u2019affaires. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Transf\u00E9rer des outils, des m\u00E9thodes et des comp\u00E9tences. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Dans le cas d\u2019une micro-entreprise, le chiffre d\u2019affaires devra \u00EAtre au minimum de 20 000 \u20AC \u00E0 la date du d\u00E9p\u00F4t dossier. En l\u2019absence de liasse fiscale, le b\u00E9n\u00E9ficiaire devra alors fournir les 2 derniers comptes de r\u00E9sultat et l\u2019attestation fiscale mentionnant le chiffre d\u2019affaires. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Conditions d\u2019allocations ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "h3", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "A qui s\u2019adresse ce dispositif ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Entreprises \u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Cette solution s\u2019adresse aux entreprises du territoire des Hauts-de-France de plus de 3 ans d\u2019existence et de 20 salari\u00E9s maximum qui souhaitent reconqu\u00E9rir leurs clients, trouver de nouveaux d\u00E9bouch\u00E9s et mettre en place une nouvelle strat\u00E9gie commerciale. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "L\u2019accompagnement Booster performance commerciale est personnalis\u00E9 et est ex\u00E9cut\u00E9 par un conseiller CCI, ce qui permettra \u00E0 l\u2019entreprise de faire le point sur ses pratiques et d\u2019\u00EAtre accompagn\u00E9e dans la mise en \u0153uvre d\u2019actions concr\u00E8tes avec un objectif de r\u00E9sultats \u00E0 court et moyen terme. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "h2", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Montant de l\u2019aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "De quel type d\u2019aide il s\u2019agit ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "C\u2019est un accompagnement d\u2019une valeur de 1 000 \u20AC HT enti\u00E8rement pris en charge (100%) par la R\u00E9gion Hauts-de-France et le FEDER, et ce, afin d\u2019accompagner les entreprises du territoire dans la reprise d\u2019activit\u00E9.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "h2", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Informations pratiques\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Quelles d\u00E9marches suivre\u202F? Aupr\u00E8s de quel organisme ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "L\u2019entreprise doit se joindre \u00E0 la CCI de son territoire. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "6 autres accompagnements sont propos\u00E9s dans le cadre du programme Booster TPE : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Booster gestion.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "Booster num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Booster relation client");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Booster ressources humaines.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Booster transmission. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Booster transition \u00E9cologique. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](85, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](86, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Testez votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["li[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\np[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9oYXV0ZWRlZnJhbmNlL2hhdXRlZGVmcmFuY2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGNBQWM7QUFDbEI7QUFDQTtJQUNJLGNBQWM7QUFDbEIiLCJmaWxlIjoiYXBwL2Nwbi9yZWdpb25zL2hhdXRlZGVmcmFuY2UvaGF1dGVkZWZyYW5jZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsibGl7XG4gICAgY29sb3I6ICMxMTFENUU7XG59XG5we1xuICAgIGNvbG9yOiAjMTExRDVFO1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HautedefranceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-hautedefrance',
                templateUrl: './hautedefrance.component.html',
                styleUrls: ['./hautedefrance.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/iledefrance/iledefrance.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/cpn/regions/iledefrance/iledefrance.component.ts ***!
  \******************************************************************/
/*! exports provided: IledefranceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IledefranceComponent", function() { return IledefranceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class IledefranceComponent {
    constructor() { }
    ngOnInit() {
    }
}
IledefranceComponent.ɵfac = function IledefranceComponent_Factory(t) { return new (t || IledefranceComponent)(); };
IledefranceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: IledefranceComponent, selectors: [["app-iledefrance"]], decls: 238, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/ile-de-france.png", "height", "100%", "width", "100%", "alt", ""], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [1, "list-group"], [2, "color", "#111D5E"], [2, "font-weight", "bold", "color", "#111D5E"], [2, "color", "red", "font-weight", "bold"], [2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "red"], [1, "btn_post", "d-flex", "justify-content-center"], ["type", "submit", 1, "btntest"]], template: function IledefranceComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h2", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Ch\u00E8que commerce connect\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h3", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " Le dispositif a pour objectif de maintenir et de d\u00E9velopper l\u2019activit\u00E9 des artisans et commer\u00E7ants de proximit\u00E9 gr\u00E2ce au digital et intervient sur trois volets : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Am\u00E9liorer sa gestion digitale ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "G\u00E9n\u00E9rer un flux dans sa boutique gr\u00E2ce au Marketing Digital ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Booster ses ventes gr\u00E2ce au e-commerce ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h2", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "A qui s\u2019adresse le dispositif ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "h2", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Peuvent b\u00E9n\u00E9ficier du dispositif :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Les entreprises ind\u00E9pendantes, commer\u00E7ants de proximit\u00E9 ou artisans, qui g\u00E8rent de fa\u00E7on autonome un point de vente fixe (hors franchise) . ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Ayant leur \u00E9tablissement en \u00CEle-de-France ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Avec un effectif inf\u00E9rieur \u00E0 10 salari\u00E9s, y compris les entreprises sans salari\u00E9s ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Inscrits au Registre du Commerce et/ou Registre des M\u00E9tiers ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "h2", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "D\u00E9penses concern\u00E9es ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "L\u2019aide concerne les d\u00E9penses de fonctionnement (inscrites dans les charges de l\u2019entreprise) : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, " Solutions digitales de gestion (logiciel de caisse, gestion des stocks, gestion client\u00E8le\u2026) sous forme d\u2019abonnement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "D\u00E9penses de publicit\u00E9 digitale, solutions de fid\u00E9lisation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Solutions de g\u00E9olocalisation, frais de r\u00E9f\u00E9rencement, achat de mots cl\u00E9s, statistiques d\u2019audience ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "R\u00E9servation de nom de domaine, frais d\u2019h\u00E9bergement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Abonnement \u00E0 un logiciel de cr\u00E9ation de site en SaaS, frais d\u2019optimisation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Abonnement ou commission sur les ventes li\u00E9s \u00E0 une solution digitale visant \u00E0 d\u00E9velopper les ventes (plateforme en ligne, marketplace, click and collect, etc.). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "L\u2019aide concerne \u00E9galement les d\u00E9penses en investissement (inscrites \u00E0 l\u2019actif de l\u2019entreprise) pour le d\u00E9veloppement, la r\u00E9alisation et l\u2019acquisition de site internet. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "h2", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "Quelles sont les particularit\u00E9s ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Entreprises non-\u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Les entreprises de la fili\u00E8re num\u00E9rique, les professions r\u00E9glement\u00E9es et lib\u00E9rales, les activit\u00E9s financi\u00E8res et immobili\u00E8res, les organismes de formation, de conseil et les bureaux d\u2019\u00E9tudes ne sont pas \u00E9ligibles. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "h3", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Montant de l\u2019aide ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Quel est le type d\u2019aide ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "Le ch\u00E8que num\u00E9rique est une aide sous forme de subvention pouvant aller jusqu\u2019\u00E0 1 500 \u20AC.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "Le montant du ch\u00E8que d\u00E9pendra du montant des d\u00E9penses \u00E9ligibles : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "\u00C0 partir de 300 \u20AC, le montant du ch\u00E8que est de 150 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](91, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "\u00C0 partir de 600 \u20AC, le montant du ch\u00E8que est de 300 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "\u00C0 partir de 600 \u20AC, le montant du ch\u00E8que est de 300 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, "\u00C0 partir de 1 200 \u20AC, le montant du ch\u00E8que est de 600 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "\u00C0 partir de 1 500 \u20AC, le montant du ch\u00E8que est de 750 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "\u00C0 partir de 1 800 \u20AC, le montant du ch\u00E8que est de 900 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](108, "\u00C0 partir de 2 100 \u20AC, le montant du ch\u00E8que est de 1 050 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "\u00C0 partir de 2 400 \u20AC, le montant du ch\u00E8que est de 1 200 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](112, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "\u00C0 partir de 2 700 \u20AC, le montant du ch\u00E8que est de 1 350 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "\u00C0 partir de 3 000 \u20AC, le montant du ch\u00E8que est de 1 500 \u20AC ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "Informations pratiques");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](122, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, "Quelle d\u00E9marche \u00E0 suivre ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](125, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Aupr\u00E8s de quel organisme ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](128, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, " Les d\u00E9p\u00F4ts des demandes d\u2019aide seront en ligne sur mes d\u00E9marches. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "h5", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, " Apr\u00E8s examen de votre demande, un avis de d\u00E9cision vous sera transmis dans un d\u00E9lai de 3 semaines. \u00C9l\u00E9ments \u00E0 pr\u00E9voir et pi\u00E8ces \u00E0 fournir pour la demande d\u2019aide : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Un extrait Kbis ou D1 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](136, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, "Une attestation de r\u00E9gularit\u00E9 fiscale et sociale de moins de 3 mois ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](139, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "Un RIB ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](142, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "Les pi\u00E8ces justificatives des d\u00E9penses pr\u00E9visionnelles (devis, grille tarifaire, ou tout document pr\u00E9cisant le montant et la nature de la d\u00E9pense envisag\u00E9e). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](145, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](147, "Un rapport de diagnostic de la situation num\u00E9rique de l\u2019entreprise. Si l\u2019entreprise n\u2019en dispose pas, il est possible de r\u00E9aliser un autodiagnostique en ligne d\u00E9di\u00E9 aux commer\u00E7ants (CCI) ou d\u00E9di\u00E9 aux artisans (CRMA). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](148, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](150, " Une fois l\u2019avis de d\u00E9cision re\u00E7u est positif, l\u2019entreprise peut commencer \u00E0 engager les d\u00E9penses. Elles portent sur une p\u00E9riode maximale de 12 mois. Avec un d\u00E9lai de r\u00E9alisation ne d\u00E9passant pas les 18 mois. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](151, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](153, " Pour recevoir la subvention, l\u2019entreprise s\u2019engage \u00E0 transmettre sa demande de versement avec les factures acquitt\u00E9es (dans un d\u00E9lai maximal ne d\u00E9passant pas un an apr\u00E8s la r\u00E9ception de l\u2019accord). Le versement peut se faire sous la forme d\u2019un paiement unique ou un paiement sur 2 fois. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](154, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "h4", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](156, "B\u00E9n\u00E9ficiaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](157, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "h5", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](160, "Accessible si : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, " L\u2019effectif est de moins de 10 salari\u00E9s. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](163, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](165, "Crit\u00E8res compl\u00E9mentaires ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](166, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, "Lieu d\u2019immatriculation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](169, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](171, "Immatriculation au Registre du Commerce et des Soci\u00E9t\u00E9s");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](172, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](174, "Immatriculation au R\u00E9pertoire des M\u00E9tiers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](175, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "h5", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](178, "Non accessible si : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](180, " L\u2019activit\u00E9 exerc\u00E9e (APE) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](181, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](183, "4773 \u2013 Commerce de d\u00E9tail de produits pharmaceutiques en magasin sp\u00E9cialis\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](184, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](186, "4774 \u2013 Commerce de d\u00E9tail d\u2019articles m\u00E9dicaux et orthop\u00E9diques en magasin sp\u00E9cialis\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](187, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, "4791 \u2013 Vente \u00E0 distance");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](190, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](191, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](192, "4932 \u2013 Transports de voyageurs par taxis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](193, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](195, "6312 \u2013 Portails internet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](196, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, "641 \u2013 Interm\u00E9diation mon\u00E9taire");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](199, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, "643 \u2013 Fonds de placement et entit\u00E9s financi\u00E8res similaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](202, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](203, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](204, "649 \u2013 Autres activit\u00E9s des services financiers, hors assurance et caisses de retraite ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](205, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](207, "65 \u2013 Assurance");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](208, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](209, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](210, "66 \u2013 Activit\u00E9s auxiliaires de services financiers et d\u2019assurance ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](211, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](212, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](213, "683 \u2013 Activit\u00E9s immobili\u00E8res pour compte de tiers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](214, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](215, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](216, "69 \u2013 Activit\u00E9s juridiques et comptables");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](217, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](218, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](219, "7120A \u2013 Contr\u00F4le technique automobile");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](220, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](221, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](222, "75 \u2013 Activit\u00E9s v\u00E9t\u00E9rinaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](223, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](224, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](225, "86 \u2013 Activit\u00E9s pour la sant\u00E9 humaine");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](226, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](228, "871 \u2013 H\u00E9bergement m\u00E9dicalis\u00E9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](229, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](230, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](231, "O \u2013 Administration publique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](232, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](233, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](234, "P \u2013 Enseignement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](236, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](237, "Testez votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["li[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\n.btntest[_ngcontent-%COMP%]{\n    -webkit-text-decoration:auto ;\n            text-decoration:auto ;  border: none; background: red;border-radius: 25px;color: white;padding: 5px 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9pbGVkZWZyYW5jZS9pbGVkZWZyYW5jZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksNkJBQXFCO1lBQXJCLHFCQUFxQixHQUFHLFlBQVksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLGlCQUFpQjtBQUM1RyIsImZpbGUiOiJhcHAvY3BuL3JlZ2lvbnMvaWxlZGVmcmFuY2UvaWxlZGVmcmFuY2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImxpe1xuICAgIGNvbG9yOiAjMTExRDVFO1xufVxuLmJ0bnRlc3R7XG4gICAgdGV4dC1kZWNvcmF0aW9uOmF1dG8gOyAgYm9yZGVyOiBub25lOyBiYWNrZ3JvdW5kOiByZWQ7Ym9yZGVyLXJhZGl1czogMjVweDtjb2xvcjogd2hpdGU7cGFkZGluZzogNXB4IDE1cHg7XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](IledefranceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-iledefrance',
                templateUrl: './iledefrance.component.html',
                styleUrls: ['./iledefrance.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/normandie/normandie.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/cpn/regions/normandie/normandie.component.ts ***!
  \**************************************************************/
/*! exports provided: NormandieComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NormandieComponent", function() { return NormandieComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NormandieComponent {
    constructor() { }
    ngOnInit() {
    }
}
NormandieComponent.ɵfac = function NormandieComponent_Factory(t) { return new (t || NormandieComponent)(); };
NormandieComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NormandieComponent, selectors: [["app-normandie"]], decls: 191, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [1, "col-sm-2"], [2, "font-weight", "bold", "color", "#111D5E"], [2, "color", "red", "font-weight", "bold"], [1, "list-group"], [1, "desc_heading", 2, "color", "#111D5E"], [1, "btn_post", "d-flex", "justify-content-center"], ["type", "submit", 2, "text-decoration", "auto", "border", "none", "background", "red", "border-radius", "25px", "color", "white", "padding", "5px 15px"]], template: function NormandieComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Impulsion Transition Num\u00E9rique ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "h1", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Impulsion transition num\u00E9rique a pour objectif d'accompagner les artisants et les commercant \u00E0 mettre en place un projet de transition num\u00E9rique pour d\u00E9velopper leurs activit\u00E9s");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "h1", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Conditions d'attributions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "A qui s'adresse ce dispositif?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Entreprises \u00E9ligibles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Crit\u00E8res d'\u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "les entreprises doivent remplir les conditions suivantes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Avoir une situztion finznci\u00E9re saine");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Etre \u00E0 jour des obligations fiscales, sociales et de l'ensembledes r\u00E9glementations qui leur sont applicables");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "D\u00E9penses concern\u00E9es");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, " sont \u00E9ligibles les d\u00E9pences li\u00E9es aux:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Etude de faisabilit\u00E9 d'ing\u00E9nierie");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Etude strat\u00E9gique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Pr\u00E9stations intellectuelles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Investissements(hors investissement r\u00E9gli\u00F9entaires et renouvellement)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Quelques exemples de d\u00E9ponses:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Audit et diagnostic");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Site web, application mobile, d\u00E9veloppement ou int\u00E9gration de progiciels");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "solutions ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Certification, design");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "formation");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Investissement mat\u00E9riels");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Quelles sont les particularit\u00E9s?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "Entreprises in\u00E9ligibles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Sont execlus du dispositif les activit\u00E9s ou les structures suivantes:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "les entreprises doivent remplir les conditions suivantes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Avoir une situztion finznci\u00E9re saine");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Etre \u00E0 jour des obligations fiscales, sociales et de l'ensembledes r\u00E9glementations qui leur sont applicables");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Porta ac consectetur ac");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "Vestibulum at eros");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "h1", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Montant de l'aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](90, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "De quel type de l'aide s'agit-il?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](93, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](100, "Quels sont les modalit\u00E9s de versement?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "Dans le cas d'une prestation intellectuelle:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "les entreprises doivent remplir les conditions suivantes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Avoir une situztion finznci\u00E9re saine");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Etre \u00E0 jour des obligations fiscales, sociales et de l'ensembledes r\u00E9glementations qui leur sont applicables");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "h1", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "Informations pratiques ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "Quels sont les d\u00E9marches \u00E0 suivre?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Aupr\u00E9s de quel organisme");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](132, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "El\u00E9ment \u00E0 pr\u00E9voir");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](135, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](137, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](138, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](141, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , employant moins de 10 personnes(et dont le chiffre d'affaire annuel n'ex\u00E9de pas 2 M");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Quel cumul possible?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](147, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](150, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "h4", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, "Crit\u00E9res compl\u00E9mentaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](153, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](155, "Forme juridique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](156, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](158, "Peuvent b\u00E9n\u00E9ficier de ce dispositif financier, toutes les formes des entreprises ayant au moins un \u00E9tablissement en Normandie inscrites au registre de commerces et des soci\u00E9t\u00E9s (RCS) ou au R\u00E9pertoire des m\u00E9tiers (RM), qui r\u00E9pondent \u00E0 la d\u00E9finition de la microentreprise , ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](159, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, "Donn\u00E9es suppl\u00E9mentaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](162, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](165, " ==> Situation-R\u00E9glementation");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "les entreprises doivent remplir les conditions suivantes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](168, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](169, "Avoir une situztion finznci\u00E9re saine");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](171, "Etre \u00E0 jour des obligations fiscales, sociales et de l'ensembledes r\u00E9glementations qui leur sont applicables");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](172, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](175, " ==> Lieu d'immarticulation");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, "les entreprises doivent remplir les conditions suivantes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "Avoir une situztion finznci\u00E9re saine");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](180, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](183, "==>Aide soumise au r\u00E8glement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "les entreprises doivent remplir les conditions suivantes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](186, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](187, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](190, "Testez votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["li[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\np[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\nh5[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9ub3JtYW5kaWUvbm9ybWFuZGllLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxjQUFjO0FBQ2xCIiwiZmlsZSI6ImFwcC9jcG4vcmVnaW9ucy9ub3JtYW5kaWUvbm9ybWFuZGllLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJsaXtcbiAgICBjb2xvcjogIzExMUQ1RTtcbn1cbnB7XG4gICAgY29sb3I6ICMxMTFENUU7XG59XG5oNXtcbiAgICBjb2xvcjogIzExMUQ1RTtcbn0iXX0= */", ""] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NormandieComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-normandie',
                templateUrl: './normandie.component.html',
                styleUrls: ['./normandie.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/nouvelleaquantine/nouvelleaquantine.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/cpn/regions/nouvelleaquantine/nouvelleaquantine.component.ts ***!
  \******************************************************************************/
/*! exports provided: NouvelleaquantineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NouvelleaquantineComponent", function() { return NouvelleaquantineComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NouvelleaquantineComponent {
    constructor() { }
    ngOnInit() {
    }
}
NouvelleaquantineComponent.ɵfac = function NouvelleaquantineComponent_Factory(t) { return new (t || NouvelleaquantineComponent)(); };
NouvelleaquantineComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NouvelleaquantineComponent, selectors: [["app-nouvelleaquantine"]], decls: 209, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/aquitaine.png", "height", "60%", "width", "60%", "alt", ""], [2, "color", "#111D5E"], [1, "list-group"], [2, "font-weight", "bold", "color", "#111D5E"], [2, "color", "red", "font-weight", "bold"], [2, "color", "#111D5E", "font-weight", "bold"], [2, "color", "#00BFFF"], [1, "btn_post", "d-flex", "justify-content-center"], ["type", "submit", 1, "btntest"]], template: function NouvelleaquantineComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Ch\u00E8que E-commerce ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Suite aux nouvelles mesures sanitaire, la R\u00E9gion Nouvelle-Aquitaine soutient ses artisans et ses commer\u00E7ants dans leur transformation num\u00E9rique et met en place un ch\u00E8que E-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "En effet, le ch\u00E8que E-commerce vise \u00E0 am\u00E9liorer le d\u00E9veloppement commercial, la relation client et optimise la combinaison du e-commerce avec l\u2019espace physique de vente. Un diagnostic e-commerce au pr\u00E9alable vous sera soumis. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Ce dispositif a pour objectifs");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Am\u00E9liorer la visibilit\u00E9 en ligne : R\u00E9f\u00E9rencement, E-r\u00E9putation, Marketing Digital, Site Web Vitri");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "D\u00E9velopper le e-commerce : Commande en ligne, \u201CClick and collect\u201D, Paiement en ligne, Site E-commerce, Places de march\u00E9, Num\u00E9risation du catalogue produits, Mise en valeur de ces produits. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Optimiser la gestion des stocks et de la logistique : caisse connect\u00E9e, \u00E9tiquettes intelligentes, base de donn\u00E9es en temps r\u00E9el des stocks, optimisation des flux logistiques. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "D\u00E9velopper la relation client : Am\u00E9lioration de la relation client ; strat\u00E9gie de fid\u00E9lisation ; acquisition de nouveaux clients ; enrichissement de l\u2019exp\u00E9rience client par le num\u00E9rique (R\u00E9alit\u00E9 virtuelle/R\u00E9alit\u00E9 Augment\u00E9e), prise de commande par tablette\u2026)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Accroitre la performance commerciale : strat\u00E9gie/plan de communication, strat\u00E9gie omnicanal\u2026");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "h2", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Conditions d'attributions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "A qui s'adresse ce dispositif?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Les entreprises doivent r\u00E9pondre aux conditions suivantes : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Avoir leur si\u00E8ge social ou leur \u00E9tablissement sur le territoire du Nouvelle-Aquitain. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "\u00CAtre immatricul\u00E9es au R\u00E9pertoire des M\u00E9tiers et/ou Registre du Commerce et des Soci\u00E9t\u00E9s, dont les entreprises relevant d\u2019une activit\u00E9 m\u00E9tiers d\u2019art telle que d\u00E9finie dans l\u2019arr\u00EAt\u00E9 du 24 d\u00E9cembre 2015 ou entreprises ayant un savoir-faire d\u2019excellence reconnu (labels EPV Entreprise du Patrimoine Vivant, OFG Origine France Garantie, IGIA Indications G\u00E9ographiques Industrielles et artisanales). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Avoir un effectif de moins de 10 salari\u00E9s au 01/11/2020 (R\u00E9f\u00E9rence code de la s\u00E9curit\u00E9 sociale) en ETP.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "\u00CAtre \u00E0 jour de leurs d\u00E9clarations et paiements et charges sociales et fiscales au 31/10/2020 (tenant compte des reports exceptionnels accord\u00E9s par l\u2019Etat dans le cadre de la crise COVID 19), \u00E0 l\u2019exception de celles b\u00E9n\u00E9ficiant d\u2019un plan de r\u00E8glement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Pour quel projet ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Les investissements pour les entreprises \u00E9ligibles sont les suivants : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Les prestations de conseils et de services (AMOA assistance \u00E0 maitrise d\u2019ouvrage, photographe, agence web, Community Management, agence de conseil digitale\u2026). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Les frais de formation. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "Les frais de d\u00E9veloppement (site web, application, \u2026). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Les frais d\u2019abonnement ou achat de logiciels (ERP, data client, caisse connect\u00E9e, stock\u2026) ou services e-commerce (click and collect, paiement, commande en ligne, service clients, suivi des commandes, fid\u00E9lisation des clients, logistique\u2026).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Les investissements mat\u00E9riels n\u00E9cessaires \u00E0 la mise en \u0153uvre du projet (tablette, casque VR, balance connect\u00E9e, douchette\u2026)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Les frais d\u2019h\u00E9bergement.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](82, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Les frais de publicit\u00E9 en ligne : r\u00E9f\u00E9rencement .");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](85, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "La campagne promotionnelle.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "Les frais de marketing digital plafonn\u00E9s \u00E0 5% des d\u00E9penses \u00E9ligibles.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](91, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Quelles sont les particularit\u00E9s ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](95, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "Les activit\u00E9s non \u00E9ligibles sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](100, " Les commerces d\u2019une surface de vente sup\u00E9rieure \u00E0 300 m\u00B2 (grande distribution et autres enseignes franchis\u00E9es). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Les activit\u00E9s enregistr\u00E9es avec les codes NAF 01 et 03 (agriculture et p\u00EAche). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "Les secteurs d\u2019activit\u00E9 exclus par les r\u00E8glements europ\u00E9ens. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "Les entreprises en proc\u00E9dure collective d\u2019insolvabilit\u00E9. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "Les professions lib\u00E9rales r\u00E9glement\u00E9es.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "Les professions li\u00E9es \u00E0 l\u2019\u00E9sot\u00E9risme et les activit\u00E9s de bien \u00EAtre non r\u00E8glement\u00E9es (Codes NAF 96.04 et 96.09).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "Les entreprises intervenant dans les activit\u00E9s immobili\u00E8res ou de promotion immobili\u00E8re (Codes NAF 41-1 et ensemble des codes NAF de la section L), les activit\u00E9s financi\u00E8res et d\u2019assurance (ensemble des codes NAF de la section K). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "Les activit\u00E9s m\u00E9dicales et param\u00E9dicales, hors ressortissants CMA (ensemble des codes NAF de la section Q). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](122, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, "Les activit\u00E9s d\u2019enseignement (ensemble des codes NAF de la section P). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](125, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Les activit\u00E9s exclusivement propos\u00E9es en e-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](128, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, "Les entreprises intervenant dans les activit\u00E9s sp\u00E9cialis\u00E9es, scientifiques et techniques (ensemble des codes NAF de la section M, \u00E0 l\u2019exception des groupes 70.2 ; 71.2 ; 71.12B ; 72.1 ; 72.2 ; 74.1 ; 74.3 ; 74.9).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](131, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](132, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "Les d\u00E9penses non \u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](136, "Les frais d\u2019adh\u00E9sion \u00E0 une place de march\u00E9 ne sont pas \u00E9ligibles au dispositif. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](137, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](139, "Montant de l\u2019aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](140, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](142, "De quel type d\u2019aide s\u2019agit-il ? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](143, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Le ch\u00E8que E-commerce est octroy\u00E9 sous forme de subvention de la mani\u00E8re suivante : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, " Un taux d\u2019intervention de 50% maximum d\u2019investissement. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](149, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](151, "Une subvention plafonn\u00E9e \u00E0 5 000 \u20AC . ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](152, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](154, "Un plancher d\u2019investissement de 2 000 \u20AC H.T ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](155, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, "Avoir b\u00E9n\u00E9fici\u00E9 d\u2019un accompagnement diagnostic e-commerce de sa chambre consulaire ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](158, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](159, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, "Informations pratiques");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](162, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, "Aupr\u00E8s de quel organisme");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](165, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, " La demande d\u2019aide devra \u00EAtre d\u00E9pos\u00E9e sur une plateforme de d\u00E9p\u00F4t d\u00E9mat\u00E9rialis\u00E9e qui sera mise en place par la R\u00E9gion. Les 2 \u00E9tapes \u00E0 suivre pour d\u00E9poser la demande : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](170, " Etape n\u00B01 : r\u00E9aliser au pr\u00E9alable un diagnostic e-commerce gratuit de l\u2019entreprise avec un conseiller num\u00E9rique des r\u00E9seaux consulaires CCI ou CMA de Nouvelle Aquitaine, pour ce faire, remplir ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](172, " le test d\u2019\u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](173, " afin d\u2019\u00EAtre recontact\u00E9(e) par un conseiller num\u00E9rique consulaire. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](174, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, "Etape 2 : une fois le diagnostic r\u00E9alis\u00E9, d\u00E9poser le dossier en ligne aupr\u00E8s de la R\u00E9gion Nouvelle-Aquitaine sur une plateforme de d\u00E9p\u00F4t d\u00E9mat\u00E9rialis\u00E9e qui sera mise en place par la R\u00E9gion. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](177, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](178, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](180, "\u00C9l\u00E9ments \u00E0 pr\u00E9voir");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](181, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "h5", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](184, "Les documents \u00E0 joindre aux dossiers sont : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](186, " Le diagnostic E-commerce (r\u00E9alis\u00E9 au pr\u00E9alable par le r\u00E9seau consulaire CCI ou CMA) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](187, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, "RIB ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](190, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](191, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](192, "KBis ou Extrait d\u2019immatriculation D1 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](193, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](195, "Devis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](196, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "h2", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, "Crit\u00E8res compl\u00E9mentaires");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](199, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "h3", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, "Forme juridique ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](202, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](203, "h4", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](204, "Ne sont pas \u00E9ligibles au dispositif, les professions lib\u00E9rales r\u00E9glement\u00E9es. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](205, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](207, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](208, "Testez votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["li[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\nul[_ngcontent-%COMP%]{\n    color: #111D5E;\n}\n.btntest[_ngcontent-%COMP%]{\n    -webkit-text-decoration:auto ;\n            text-decoration:auto ;  border: none; background: red;border-radius: 25px;color: white;padding: 5px 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9ub3V2ZWxsZWFxdWFudGluZS9ub3V2ZWxsZWFxdWFudGluZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksNkJBQXFCO1lBQXJCLHFCQUFxQixHQUFHLFlBQVksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLGlCQUFpQjtBQUM1RyIsImZpbGUiOiJhcHAvY3BuL3JlZ2lvbnMvbm91dmVsbGVhcXVhbnRpbmUvbm91dmVsbGVhcXVhbnRpbmUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImxpe1xuICAgIGNvbG9yOiAjMTExRDVFO1xufVxudWx7XG4gICAgY29sb3I6ICMxMTFENUU7XG59XG4uYnRudGVzdHtcbiAgICB0ZXh0LWRlY29yYXRpb246YXV0byA7ICBib3JkZXI6IG5vbmU7IGJhY2tncm91bmQ6IHJlZDtib3JkZXItcmFkaXVzOiAyNXB4O2NvbG9yOiB3aGl0ZTtwYWRkaW5nOiA1cHggMTVweDtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NouvelleaquantineComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-nouvelleaquantine',
                templateUrl: './nouvelleaquantine.component.html',
                styleUrls: ['./nouvelleaquantine.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/regions/payedeloire/payedeloire.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/cpn/regions/payedeloire/payedeloire.component.ts ***!
  \******************************************************************/
/*! exports provided: PayedeloireComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayedeloireComponent", function() { return PayedeloireComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class PayedeloireComponent {
    constructor() { }
    ngOnInit() {
    }
}
PayedeloireComponent.ɵfac = function PayedeloireComponent_Factory(t) { return new (t || PayedeloireComponent)(); };
PayedeloireComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PayedeloireComponent, selectors: [["app-payedeloire"]], decls: 331, vars: 0, consts: [["width", "device-width", 1, "container"], [1, "row"], [1, "col-md-10"], [1, "desc_heading", 2, "color", "#111D5E", "font-weight", "bold"], [1, "col-sm-2"], ["src", "assets/cpnimages/regions/payedeloire.png", "alt", "", 2, "width", "100%"], [2, "font-weight", "bold", "color", "#111D5E"], [2, "color", "red", "font-weight", "bold"], [2, "color", "#111D5E"], [1, "list-group"], [1, "desc_heading", 2, "color", "#111D5E"], ["href", "", 2, "color", "#00BFFF"], [1, "btn_post", "d-flex", "justify-content-center"], ["type", "submit", 2, "text-decoration", "auto", "border", "none", "background", "red", "border-radius", "25px", "color", "white", "padding", "5px 15px"]], template: function PayedeloireComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Ch\u00E8que Investissement num\u00E9rique - PDLIN ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Dispositif ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Le dispositif Pays de la Loire Investissement Num\u00E9rique vise \u00E0 aider les petites entreprises dans l\u2019acquisition et l\u2019appropriation d\u2019outils num\u00E9riques \u00E0 forte valeur ajout\u00E9e.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h1", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Conditions d'attributions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "A qui s'adresse ce dispositif?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Entreprises \u00E9ligibles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Pays de la Loire Investissement num\u00E9rique s'adresse aux entreprises qui souhaitent franchir un pas significatif dans la transition num\u00E9rique en se dotant d\u2019outils modifiant leur mode de fonctionnement. Les acquisitions doivent contribuer \u00E0 la cr\u00E9ation d\u2019une chaine num\u00E9rique globale permettant de gagner en productivit\u00E9 et cr\u00E9er de la valeur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Crit\u00E8res d\u2019\u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Les entreprises doivent r\u00E9pondre aux crit\u00E8res suivants : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Avoir moins de 50 salari\u00E9s qui r\u00E9pondent \u00E0 la d\u00E9finition communautaire de la PME \u00E0 la date du d\u00E9p\u00F4t du dossier.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Cr\u00E9\u00E9es depuis plus de 2 ans \u00E0 la date du d\u00E9p\u00F4t du dossie");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Ayant un chiffre d\u2019affaires n\u2019exc\u00E9dant pas 10 M\u20AC. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Implant\u00E9es dans la r\u00E9gion Pays de la Loire (si\u00E8ge social, filiale, \u00E9tablissement), sous r\u00E9serve que l\u2019investissement envisag\u00E9 concerne directement ladite implantation. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "\u00CAtre \u00E0 jour de leurs obligations fiscales, sociales, environnementales et sanitaires. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Ne pas \u00EAtre en difficult\u00E9. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Dans le cas d\u2019une micro-entreprise, le chiffre d\u2019affaires devra \u00EAtre au minimum de 20 000 \u20AC \u00E0 la date du d\u00E9p\u00F4t dossier. En l\u2019absence de liasse fiscale, le b\u00E9n\u00E9ficiaire devra alors fournir les 2 derniers comptes de r\u00E9sultat et l\u2019attestation fiscale mentionnant le chiffre d\u2019affaires. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "h3", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Pour quel projet ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Pr\u00E9sentation des projets");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Sont retenus les investissements suivants : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, " De progiciels (exemples : progiciel de gestion (ERP/PGI), progiciel de gestion commerciale ou de la relation client (CRM/GRC), progiciel de gestion et contr\u00F4le de la production GPAO/PMI), progiciel de gestion de la conception/fabrication (CAO/FAO), progiciel de mod\u00E9lisation ou num\u00E9risation 3D (BIM/ CAO ou FAO 3D), progiciel Transport (TMS/FMS), progiciel de r\u00E9alit\u00E9 augment\u00E9e), ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "De logiciel de gestion et/ou d'archivage \u00E9lectronique des documents (GED). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "De cr\u00E9ation ou refonte compl\u00E8te d'un site web marchand/e-commerce (site internet permettent d'acheter en ligne un bien ou un service. Exemples : boutique en ligne, portail de vente client)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "De logiciel ou application m\u00E9tier (logiciel d\u00E9velopp\u00E9 selon le cahier des charges d\u00E9finit par l\u2019entreprise et r\u00E9pondant \u00E0 un besoin sp\u00E9cifique non couvert par les progiciels g\u00E9n\u00E9riques). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "D'applications mobiles personnalis\u00E9es (exemples : suivi des chantiers/activit\u00E9s, gestion commerciale, mobilit\u00E9, gestion des ressources humaines).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "D'outil de travail collaboratif structurant T (exemples : communication collaborative (intranet), collaboration de projets (logiciel de gestion de projet global), plateforme collaborative d\u2019\u00E9changes). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "De logiciel de s\u00E9curit\u00E9 informatiques s'inscrivant dans une d\u00E9marche de cybers\u00E9curit\u00E9. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](82, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "h4", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "D\u00E9penses concern\u00E9es ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Les d\u00E9penses associ\u00E9es seront \u00E9ligibles si les investissements list\u00E9s ci-dessus s\u2019int\u00E8grent dans le m\u00EAme dossier de demande de subvention. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "==> Licences");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "Achat de licences et/ou logiciels, ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Achat de modules/extensions, ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, "Abonnement logiciel et/ou module sur un an, ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Licences serveur ou abonnement licences sur un an en lien avec l\u2019acquisition num\u00E9rique (s\u00E9curisation du syst\u00E8me informatique)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "formation");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](108, "Investissement mat\u00E9riels");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "==> Contrats ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "Contrat de maintenance sur un an, ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "Contrat d\u2019assistance sur un an. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "==> Services ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "Formation (montant restant non pris en charge par un OPCO), ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Installation, ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "Param\u00E9trage (incluant interconnexions des outils, adaptation, personnalisation), ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "Int\u00E9gration (phase de test/migration/reprise des donn\u00E9es), ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](132, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "S\u00E9curisation des donn\u00E9es, ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](135, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](137, "Suivi technique du projet. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](138, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "==> Investissements li\u00E9s \u00E0 la cr\u00E9ation du site internet e-commerce ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, "Conception graphique partie e-commerce uniquement, ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Connecteurs entre logiciels (ex : ERP) et site e-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](147, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "Logiciel d\u2019analyse et/ou de mesure statistiques du site. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](150, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, "Cr\u00E9ation d\u2019un extranet et/ou intranet e-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](153, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](155, "Syst\u00E8me de s\u00E9curisation des paiements en ligne. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](156, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](158, "H\u00E9bergement sur un an. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](159, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, "==> Investissements li\u00E9s \u00E0 la cr\u00E9ation d\u2019une application/d\u2019une plateforme ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, "Conception graphique de l\u2019application.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](165, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "Connecteurs entre logiciels (ex : ERP) et application/plateforme. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](168, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](170, "Cr\u00E9ation d\u2019un extranet et/ou intranet. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](171, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](173, "H\u00E9bergement sur un an. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](174, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, "D\u00E9veloppement de l\u2019application/plateforme (back-office, mobile, interface web) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](177, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "Quelles sont les particularit\u00E9s?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](180, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "h4", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](182, "Entreprises in\u00E9ligibles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](183, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "Ne sont pas \u00E9ligibles :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](186, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](188, "==> Les entreprises et \u00E9tablissements publics et parapublics");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](189, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, "==> Les professionnels du e-commerce. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](192, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](194, "==> Les professions lib\u00E9rales r\u00E9glement\u00E9es, notamment les entreprises titulaires des codes NAF/APE suivants ou exer\u00E7ant une activit\u00E9 correspondant \u00E0 ces codes : 4773Z, 4774Z, 4778A, 4791A et B, 4932Z, 6312Z, 6411Z, 6419Z, 6430Z \u00E0 6630Z, 6831Z \u00E0 6920Z, 7120A, 7500Z, 8411Z \u00E0 8710 C");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](195, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](196, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, "==> Ou les entreprises exer\u00E7ant une activit\u00E9 correspondant \u00E0 ces codes :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](199, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](200, "Vente \u00E0 distance.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](201, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](202, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](203, "Portail internet. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](204, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](205, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](206, "Commerce de produits pharmaceutiques ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](207, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](208, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](209, "Commerce d\u2019articles m\u00E9dicaux. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](210, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](211, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](212, "D\u00E9veloppement de l\u2019application/plateforme (back-office, mobile, interface web) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](213, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](214, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](215, "Entreprise ou activit\u00E9 ayant un objet immobilier, financier et/ou de gestion de fonds/prise de participation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](216, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](217, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](218, "Activit\u00E9 juridique et comptable, activit\u00E9 v\u00E9t\u00E9rinaire, activit\u00E9 d\u2019enseignement et profession m\u00E9dicale. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](219, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](220, "h4", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](221, "D\u00E9penses in\u00E9ligibles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](222, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](223, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](224, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](225, "Sont exclues du dispositif les d\u00E9penses suivantes : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](226, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](228, "Achat de mat\u00E9riel (serveur, ordinateur, t\u00E9l\u00E9phone, onduleur, disques durs, r\u00E9seau internet, machines de production\u2026), ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](229, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](230, "Frais de conception ou de d\u00E9veloppement d\u2019un site Internet \u00AB vitrine \u00BB ou \u00AB plaquette \u00BB ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](231, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](232, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](233, "Les acquisitions financ\u00E9es par cr\u00E9dit-bail ou location financi\u00E8re. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](234, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](236, "Frais li\u00E9s \u00E0 la publicit\u00E9 et aux r\u00E9seaux sociaux (ex : newsletters, outil de gestion de campagnes publicitaires ou li\u00E9s \u00E0 la mise en place d\u2019une campagne publicitaire, communication digitale). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](237, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](238, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](239, "Abonnements t\u00E9l\u00E9phoniques ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](240, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](241, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](242, "Mise aux normes r\u00E9glementaires (ex : logiciel de caisse).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](243, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](244, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](245, "Mise \u00E0 jour ou ajout d\u2019une licence suppl\u00E9mentaire, \u00E0 un logiciel ou une application d\u00E9j\u00E0 utilis\u00E9 au sein de l\u2019entreprise. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](246, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](247, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](248, "D\u00E9veloppement d\u2019un produit \u00E0 commercialiser");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](249, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](250, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](251, "Suite d'outils collaboratifs");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](252, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](253, "h1", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](254, "Montant de l'aide");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](255, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](256, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](257, "De quel type de l'aide s'agit-il?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](258, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](259, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](260, "Le soutien r\u00E9gional prend la forme d\u2019une subvention. Le taux d\u2019aide est de 50% du montant hors taxe des co\u00FBts \u00E9ligibles, ces derniers devant au minimum atteindre 5 000 \u20AC HT");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](261, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](262, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](263, "L\u2019aide r\u00E9gionale est plafonn\u00E9e \u00E0 15 000 \u20AC");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](264, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](265, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](266, "Quels sont les modalit\u00E9s de versement?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](267, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](268, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](269, "h6", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](270, "La subvention est vers\u00E9e en une seule fois, sauf pour les aides sup\u00E9rieures \u00E0 4 000 \u20AC qui peuvent \u00EAtre vers\u00E9es en deux fois : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](271, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](272, "Avance de 30% \u00E0 la notification de l\u2019aide, sur pr\u00E9sentation d\u2019un devis sign\u00E9 accept\u00E9 ou d\u2019un bon de commande sign\u00E9 (d\u00E9rogation au r\u00E8glement budg\u00E9taire et financier)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](273, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](274, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](275, "Solde sur pr\u00E9sentation des factures certifi\u00E9es et acquitt\u00E9es ainsi que d\u2019une attestation de fin d\u2019engagement des d\u00E9penses");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](276, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](277, "h1", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](278, "Informations pratiques ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](279, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](280, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](281, "Quels sont les d\u00E9marches \u00E0 suivre?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](282, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](283, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](284, "Aupr\u00E9s de quel organisme");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](285, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](286, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](287, "Le dossier de demande d'aide se fait directement en ligne sur le site de la R\u00E9gion Pays de la Loire avant l'engagement des d\u00E9penses.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](288, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](289, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](290, "Le b\u00E9n\u00E9ficiaire de l\u2019aide dispose, pour r\u00E9aliser son investissement num\u00E9rique, d\u2019un d\u00E9lai de 24 mois \u00E0 compter de la date de notification de l\u2019arr\u00EAt\u00E9 lui attribuant cette aide. A l\u2019\u00E9ch\u00E9ance de ce d\u00E9lai de r\u00E9alisation, le b\u00E9n\u00E9ficiaire dispose d\u2019un d\u00E9lai maximum de six mois pour fournir les pi\u00E8ces justificatives n\u00E9cessaires au versement de l\u2019aide.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](291, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](292, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](293, "Pour plus d'information : Direction Entreprises et Innovation \u00E0 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](294, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](295, " numerique@paysdelaloire.fr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](296, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](297, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](298, "Quel cumul possible ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](299, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](300, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](301, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](302, "h5", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](303, " Conditions pour un nouvel octroi de l'aide : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](304, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](305, "Une entreprise peut solliciter une 2\u00E8me aide, dans la limite globale du plafond de 15 000 \u20AC, si les investissements pr\u00E9vus correspondent \u00E0 un nouveau projet, et r\u00E9pondent \u00E0 l\u2019ensemble des crit\u00E8res d\u2019\u00E9ligibilit\u00E9 expos\u00E9s ci-dessus,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](306, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](307, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](308, "Une entreprise ayant atteint le plafond d\u2019aide de 15 000 \u20AC, ne pourra b\u00E9n\u00E9ficier \u00E0 nouveau du dispositif qu\u2019apr\u00E8s 2 ans r\u00E9volus (la date de r\u00E9f\u00E9rence \u00E9tant la date du courrier accusant r\u00E9ception du dossier complet). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](309, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](310, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](311, "L\u2019investissement projet\u00E9 dans le cadre de cette nouvelle demande devra correspondre \u00E0 un nouveau pas significatif dans la transition num\u00E9rique. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](312, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](313, "h1", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](314, "Crit\u00E8res compl\u00E9mentaires ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](315, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](316, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](317, "Donn\u00E9es suppl\u00E9mentaires ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](318, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](319, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](320, "h4", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](321, "Situation - R\u00E9glementation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](322, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](323, "A jour des versements fiscaux et sociaux ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](324, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](325, "Situation financi\u00E8re saine ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](326, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](327, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](328, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](329, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](330, "Testez votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["p[_ngcontent-%COMP%]{\n    color:#111D5E;\n}\nli[_ngcontent-%COMP%]{\n    color:#111D5E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vcmVnaW9ucy9wYXllZGVsb2lyZS9wYXllZGVsb2lyZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksYUFBYTtBQUNqQjtBQUNBO0lBQ0ksYUFBYTtBQUNqQiIsImZpbGUiOiJhcHAvY3BuL3JlZ2lvbnMvcGF5ZWRlbG9pcmUvcGF5ZWRlbG9pcmUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInB7XG4gICAgY29sb3I6IzExMUQ1RTtcbn1cbmxpe1xuICAgIGNvbG9yOiMxMTFENUU7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PayedeloireComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-payedeloire',
                templateUrl: './payedeloire.component.html',
                styleUrls: ['./payedeloire.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/side-bar/side-bar.component.ts":
/*!****************************************************!*\
  !*** ./src/app/cpn/side-bar/side-bar.component.ts ***!
  \****************************************************/
/*! exports provided: SideBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideBarComponent", function() { return SideBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class SideBarComponent {
    constructor() { }
    ngOnInit() {
        /*
            $(".float_actions .actions_content .action_items").mouseenter(function(){
              $(".float_actions .actions_content .action_items").hover(function(){
                $(".float_actions .actions_content .action_items").hover('<a href="mailto:email:test.com" style="margin-left:250px; z-index:5900">email@test.com</a>');
              })
            });*/
        //$( ".float_actions .actions_content .action_items:hover:after" ).html('<a href="mailto:email:test.com">email@test.com</a>');
        // $(".float_actions .actions_content .action_items:hover:after").css("content", "yellow");
    }
}
SideBarComponent.ɵfac = function SideBarComponent_Factory(t) { return new (t || SideBarComponent)(); };
SideBarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SideBarComponent, selectors: [["app-side-bar"]], decls: 27, vars: 3, consts: [[1, "actuality", "mb-5"], [1, "float_actions"], [1, "actions_content"], [1, "action_items"], ["href", "cpn/Home_tpe_pme", 1, "item_href"], [1, "ihref_logo"], ["width", "40%", "src", "assets/cpnimages/sidebar/Entreprise.png", "alt", ""], [1, "ihref_text"], [1, "testmegi", 3, "routerLink"], ["href", "/cpn/agence", 1, "item_href"], ["width", "40%", "src", "assets/cpnimages/sidebar/Agence.png"], ["href", "/cpn/Home_collectivite", 1, "item_href"], ["width", "40%", "src", "assets/cpnimages/sidebar/Collectivit\u00E9.png"]], template: function SideBarComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ul", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "li", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "i", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "li", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "i", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "i", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Collectivites");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], styles: ["section[_ngcontent-%COMP%]{\n  margin-top: -50px;\n  z-index: 2;\n}\n\n\n\n.float_actions[_ngcontent-%COMP%] {\n  position: fixed;\n  background: red;\n  border-radius: 10px;\n  left: 0;\n  top: 35%;\n  padding: 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 6%;\n  z-index:5900;\n  }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\n  padding: 0;\n  margin: 0 0 -30px 0;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\n  padding: 5px;\n  width: 120px;\n  height: 120px;\n  display: flex;\n  justify-content: center;\n  align-content: center;\n  position: relative;\n  }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\n  text-decoration: none;\n  display: block;\n  flex-direction: column;\n  justify-content: center;\n  font-size: 14px;\n  }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\n  width: 100px;\n  height: 100px;\n  margin-left: 25px;\n  }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 0;\n  color: white;\n  margin-top: 7px;\n  }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\n   content: \">\";\n   position: absolute;\n   right: -10px;\n   top: 15%;\n   color: white;\n   font-size: 20px;\n   width: 40%;\n   font-weight: bold;\n   }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .testmegi[_ngcontent-%COMP%]{\n    text-decoration: none;\n     position: absolute;\n     right: -180px;\n     top: 15%;\n     color: black;\n     font-size: 17px;\n     width: 40%;\n     background: white;\n     width: 180px;\n     border-radius: 25px;\n     text-align: center;\n     height: 40px;\n     display: none;\n     justify-content: center;\n     align-items: center;\n   }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover   .testmegi[_ngcontent-%COMP%]{\n   display: flex;\n   }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\n    text-decoration: none;\n    display: block;\n    flex-direction: column;\n    justify-content: center;\n    font-size: 14px;\n   }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\n    width: 100px;\n    height: 100px;\n    margin-left: 25px;\n   }\n\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\n    text-align: center;\n    margin: 0;\n    color: white;\n    margin-top: 7px;\n   }\n\n@media only screen and (min-width : 320px) and (max-width : 480px)  {\n  \n\n    \n    .float_actions[_ngcontent-%COMP%]{\n    position: fixed;\n  background: red;\n  border-radius: 10px;\n  left: 0;\n  top: 35%;\n  padding: 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 3%;\n  height: 5%;\n  z-index:5900;\n  }\n    .float_actions[_ngcontent-%COMP%]::after{\n    content: \">\";\n    color: white;\n    position: fixed;\n  background: red;\n  border-radius: 10px;\n  left: 0;\n  top: 35%;\n  padding: 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 5%;\n  height: 5%;\n  font-weight: bold;\n  font-size: 20px;\n  \n  }\n    .float_actions[_ngcontent-%COMP%]:hover:after{\n    content: \">\";\n    color: white;\n    position: fixed;\n  background: red;\n  border-radius: 10px;\n  left: 0;\n  top: 35%;\n  padding: 10px;\n  display: none;\n  justify-content: center;\n  align-items: center;\n  width: 3%;\n  height: 5%;\n  }\n   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\n    padding: 0;\n    margin: 0 0 -30px 0;\n    display: none;\n    flex-direction: column;\n    justify-content: space-between;\n  }\n    .float_actions[_ngcontent-%COMP%]:hover{\n    position: fixed;\n    background: red;\n    border-radius: 10px;\n    left: 0;\n    top: 35%;\n    padding: 10px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: 25%;\n    height: auto;\n    z-index: 99999;\n  }\n  \n   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\n  padding: 0;\n  margin: 0 0 -30px 0;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  }\n}\n\n\n\n@media only screen and (min-width : 480px) and (max-width : 768px)  {\n            \n    \n  \n  .float_actions[_ngcontent-%COMP%]{\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\nz-index:5900;\n\n}\n  .float_actions[_ngcontent-%COMP%]::after{\ncontent: \">\";\ncolor: white;\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 0px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 5%;\nheight: 5%;\nfont-weight: bold;\nfont-size: 20px;\n\n}\n  .float_actions[_ngcontent-%COMP%]:hover:after{\ncontent: \">\";\ncolor: white;\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: none;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\n}\n .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: none;\nflex-direction: column;\njustify-content: space-between;\n}\n  .float_actions[_ngcontent-%COMP%]:hover{\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 18%;\nheight: auto;\nz-index: 99999;\n}\n\n .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: flex;\nflex-direction: column;\njustify-content: space-between;\n}\n}\n\n\n\n@media only screen and (min-width : 768px) and (max-width : 992px)  {\n  \n  \n  .float_actions[_ngcontent-%COMP%]{\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\nz-index:5900;\n}\n  .float_actions[_ngcontent-%COMP%]::after{\ncontent: \">\";\ncolor: white;\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 0px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 5%;\nheight: 5%;\nfont-weight: bold;\nfont-size: 20px;\n\n}\n  .float_actions[_ngcontent-%COMP%]:hover:after{\ncontent: \">\";\ncolor: white;\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: none;\njustify-content: center;\nalign-items: center;\nwidth: 3%;\nheight: 5%;\n}\n .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: none;\nflex-direction: column;\njustify-content: space-between;\n}\n  .float_actions[_ngcontent-%COMP%]:hover{\nposition: fixed;\nbackground: red;\nborder-radius: 10px;\nleft: 0;\ntop: 35%;\npadding: 10px;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\nwidth: 12%;\nheight: auto;\nz-index: 99999;\n}\n\n .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\npadding: 0;\nmargin: 0 0 -30px 0;\ndisplay: flex;\nflex-direction: column;\njustify-content: space-between;\n}\n }\n\n\n\n@media only screen and (min-width : 992px) and (max-width : 1200px)  {\n      \n  \n  .float_actions[_ngcontent-%COMP%] {\n    position: fixed;\n    background: red;\n    border-radius: 10px;\n    left: 0;\n    top: 35%;\n    padding: 10px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: 8%;\n    z-index:5900;\n    }\n\n    .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\n    padding: 0;\n    margin: 0 0 -30px 0;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    }\n    .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\n    padding: 5px;\n    width: 120px;\n    height: 120px;\n    display: flex;\n    justify-content: center;\n    align-content: center;\n    position: relative;\n    }\n    \n    .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\n    text-decoration: none;\n    display: block;\n    flex-direction: column;\n    justify-content: center;\n    font-size: 14px;\n    }\n    .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\n    width: 100px;\n    height: 100px;\n    margin-left: 25px;\n    }\n    .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\n    text-align: center;\n    margin: 0;\n    color: white;\n    margin-top: 7px;\n    }\n    \n    \n      .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\n     content: \">\";\n     position: absolute;\n     right: -10px;\n     top: 15%;\n     color: white;\n     font-size: 20px;\n     width: 40%;\n     font-weight: bold;\n     }\n     .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .testmegi[_ngcontent-%COMP%]{\n      text-decoration: none;\n       position: absolute;\n       right: -180px;\n       top: 15%;\n       color: black;\n       font-size: 17px;\n       width: 40%;\n       background: white;\n       width: 180px;\n       border-radius: 25px;\n       text-align: center;\n       height: 40px;\n       display: none;\n       justify-content: center;\n       align-items: center;\n     }\n      .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover   .testmegi[_ngcontent-%COMP%]{\n     display: flex;\n     }\n      .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\n      text-decoration: none;\n      display: block;\n      flex-direction: column;\n      justify-content: center;\n      font-size: 14px;\n     }\n      .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\n      width: 100px;\n      height: 100px;\n      margin-left: 25px;\n     }\n      .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\n      text-align: center;\n      margin: 0;\n      color: white;\n      margin-top: 7px;\n     }\n  \n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vc2lkZS1iYXIvc2lkZS1iYXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFpQjtFQUNqQixVQUFVO0FBQ1o7O0FBRUEsNEdBQTRHOztBQUM1RztFQUNFLGVBQWU7RUFDZixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLE9BQU87RUFDUCxRQUFRO0VBQ1IsYUFBYTtFQUNiLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxZQUFZO0VBQ1o7O0FBRUE7RUFDQSxVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsOEJBQThCO0VBQzlCOztBQUNBO0VBQ0EsWUFBWTtFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCOztBQUVBO0VBQ0EscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjs7QUFDQTtFQUNBLFlBQVk7RUFDWixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCOztBQUNBO0VBQ0Esa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxZQUFZO0VBQ1osZUFBZTtFQUNmOztBQUdFO0dBQ0QsWUFBWTtHQUNaLGtCQUFrQjtHQUNsQixZQUFZO0dBQ1osUUFBUTtHQUNSLFlBQVk7R0FDWixlQUFlO0dBQ2YsVUFBVTtHQUNWLGlCQUFpQjtHQUNqQjs7QUFDQTtJQUNDLHFCQUFxQjtLQUNwQixrQkFBa0I7S0FDbEIsYUFBYTtLQUNiLFFBQVE7S0FDUixZQUFZO0tBQ1osZUFBZTtLQUNmLFVBQVU7S0FDVixpQkFBaUI7S0FDakIsWUFBWTtLQUNaLG1CQUFtQjtLQUNuQixrQkFBa0I7S0FDbEIsWUFBWTtLQUNaLGFBQWE7S0FDYix1QkFBdUI7S0FDdkIsbUJBQW1CO0dBQ3JCOztBQUNDO0dBQ0QsYUFBYTtHQUNiOztBQUVDO0lBQ0EscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGVBQWU7R0FDaEI7O0FBQ0M7SUFDQSxZQUFZO0lBQ1osYUFBYTtJQUNiLGlCQUFpQjtHQUNsQjs7QUFDQztJQUNBLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsWUFBWTtJQUNaLGVBQWU7R0FDaEI7O0FBR0E7OztFQUdELHVIQUF1SDtJQUNySDtJQUNBLGVBQWU7RUFDakIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1AsUUFBUTtFQUNSLGFBQWE7RUFDYixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsVUFBVTtFQUNWLFlBQVk7RUFDWjtJQUNFO0lBQ0EsWUFBWTtJQUNaLFlBQVk7SUFDWixlQUFlO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsT0FBTztFQUNQLFFBQVE7RUFDUixZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsU0FBUztFQUNULFVBQVU7RUFDVixpQkFBaUI7RUFDakIsZUFBZTs7RUFFZjtJQUNFO0lBQ0EsWUFBWTtJQUNaLFlBQVk7SUFDWixlQUFlO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsT0FBTztFQUNQLFFBQVE7RUFDUixhQUFhO0VBQ2IsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsU0FBUztFQUNULFVBQVU7RUFDVjtHQUNDO0lBQ0MsVUFBVTtJQUNWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtFQUNoQztJQUNFO0lBQ0EsZUFBZTtJQUNmLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsT0FBTztJQUNQLFFBQVE7SUFDUixhQUFhO0lBQ2IsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWixjQUFjO0VBQ2hCOztHQUVDO0VBQ0QsVUFBVTtFQUNWLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLDhCQUE4QjtFQUM5QjtBQUNGOztBQUVBLGdDQUFnQzs7QUFDaEM7OztBQUdBLHVIQUF1SDtFQUNySDtBQUNGLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxVQUFVO0FBQ1YsWUFBWTs7QUFFWjtFQUNFO0FBQ0YsWUFBWTtBQUNaLFlBQVk7QUFDWixlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLFlBQVk7QUFDWixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWLGlCQUFpQjtBQUNqQixlQUFlOztBQUVmO0VBQ0U7QUFDRixZQUFZO0FBQ1osWUFBWTtBQUNaLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxVQUFVO0FBQ1Y7Q0FDQztBQUNELFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7RUFDRTtBQUNGLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFVBQVU7QUFDVixZQUFZO0FBQ1osY0FBYztBQUNkOztDQUVDO0FBQ0QsVUFBVTtBQUNWLG1CQUFtQjtBQUNuQixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDhCQUE4QjtBQUM5QjtBQUNBOztBQUlDLDBCQUEwQjs7QUFDMUI7O0FBRUQsdUhBQXVIO0VBQ3JIO0FBQ0YsZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixZQUFZO0FBQ1o7RUFDRTtBQUNGLFlBQVk7QUFDWixZQUFZO0FBQ1osZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixZQUFZO0FBQ1osYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixpQkFBaUI7QUFDakIsZUFBZTs7QUFFZjtFQUNFO0FBQ0YsWUFBWTtBQUNaLFlBQVk7QUFDWixlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWO0NBQ0M7QUFDRCxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0VBQ0U7QUFDRixlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixVQUFVO0FBQ1YsWUFBWTtBQUNaLGNBQWM7QUFDZDs7Q0FFQztBQUNELFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7Q0FDQzs7QUFJQSw2QkFBNkI7O0FBQzdCOztFQUVDLDRHQUE0RztFQUM1RztJQUNFLGVBQWU7SUFDZixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLE9BQU87SUFDUCxRQUFRO0lBQ1IsYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxZQUFZO0lBQ1o7O0lBRUE7SUFDQSxVQUFVO0lBQ1YsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCO0lBQ0E7SUFDQSxZQUFZO0lBQ1osWUFBWTtJQUNaLGFBQWE7SUFDYixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLHFCQUFxQjtJQUNyQixrQkFBa0I7SUFDbEI7O0lBRUE7SUFDQSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsZUFBZTtJQUNmO0lBQ0E7SUFDQSxZQUFZO0lBQ1osYUFBYTtJQUNiLGlCQUFpQjtJQUNqQjtJQUNBO0lBQ0Esa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxZQUFZO0lBQ1osZUFBZTtJQUNmOzs7TUFHRTtLQUNELFlBQVk7S0FDWixrQkFBa0I7S0FDbEIsWUFBWTtLQUNaLFFBQVE7S0FDUixZQUFZO0tBQ1osZUFBZTtLQUNmLFVBQVU7S0FDVixpQkFBaUI7S0FDakI7S0FDQTtNQUNDLHFCQUFxQjtPQUNwQixrQkFBa0I7T0FDbEIsYUFBYTtPQUNiLFFBQVE7T0FDUixZQUFZO09BQ1osZUFBZTtPQUNmLFVBQVU7T0FDVixpQkFBaUI7T0FDakIsWUFBWTtPQUNaLG1CQUFtQjtPQUNuQixrQkFBa0I7T0FDbEIsWUFBWTtPQUNaLGFBQWE7T0FDYix1QkFBdUI7T0FDdkIsbUJBQW1CO0tBQ3JCO01BQ0M7S0FDRCxhQUFhO0tBQ2I7TUFDQztNQUNBLHFCQUFxQjtNQUNyQixjQUFjO01BQ2Qsc0JBQXNCO01BQ3RCLHVCQUF1QjtNQUN2QixlQUFlO0tBQ2hCO01BQ0M7TUFDQSxZQUFZO01BQ1osYUFBYTtNQUNiLGlCQUFpQjtLQUNsQjtNQUNDO01BQ0Esa0JBQWtCO01BQ2xCLFNBQVM7TUFDVCxZQUFZO01BQ1osZUFBZTtLQUNoQjs7SUFFRCIsImZpbGUiOiJhcHAvY3BuL3NpZGUtYmFyL3NpZGUtYmFyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJzZWN0aW9ue1xuICBtYXJnaW4tdG9wOiAtNTBweDtcbiAgei1pbmRleDogMjtcbn1cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiBzaWRlIGJhciAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmZsb2F0X2FjdGlvbnMge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJhY2tncm91bmQ6IHJlZDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbGVmdDogMDtcbiAgdG9wOiAzNSU7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogNiU7XG4gIHotaW5kZXg6NTkwMDtcbiAgfVxuXG4gIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQge1xuICBwYWRkaW5nOiAwO1xuICBtYXJnaW46IDAgMCAtMzBweCAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIH1cbiAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIHtcbiAgcGFkZGluZzogNXB4O1xuICB3aWR0aDogMTIwcHg7XG4gIGhlaWdodDogMTIwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuICBcbiAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB9XG4gIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl9sb2dvIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xuICBtYXJnaW4tbGVmdDogMjVweDtcbiAgfVxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfdGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi10b3A6IDdweDtcbiAgfVxuICBcbiAgXG4gICAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zOjpiZWZvcmV7XG4gICBjb250ZW50OiBcIj5cIjtcbiAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgIHJpZ2h0OiAtMTBweDtcbiAgIHRvcDogMTUlO1xuICAgY29sb3I6IHdoaXRlO1xuICAgZm9udC1zaXplOiAyMHB4O1xuICAgd2lkdGg6IDQwJTtcbiAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgfVxuICAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC50ZXN0bWVnaXtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgcmlnaHQ6IC0xODBweDtcbiAgICAgdG9wOiAxNSU7XG4gICAgIGNvbG9yOiBibGFjaztcbiAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgICB3aWR0aDogNDAlO1xuICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgd2lkdGg6IDE4MHB4O1xuICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgIGhlaWdodDogNDBweDtcbiAgICAgZGlzcGxheTogbm9uZTtcbiAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICB9XG4gICAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zOmhvdmVyIC50ZXN0bWVnaXtcbiAgIGRpc3BsYXk6IGZsZXg7XG4gICB9XG4gICBcbiAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgfVxuICAgIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl9sb2dvIHtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBtYXJnaW4tbGVmdDogMjVweDtcbiAgIH1cbiAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfdGV4dCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogMDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgbWFyZ2luLXRvcDogN3B4O1xuICAgfVxuICAgXG5cbiAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDMyMHB4KSBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSAge1xuICBcblxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzaWRlIGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxuICAgIC5mbG9hdF9hY3Rpb25ze1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgYmFja2dyb3VuZDogcmVkO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBsZWZ0OiAwO1xuICB0b3A6IDM1JTtcbiAgcGFkZGluZzogMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAzJTtcbiAgaGVpZ2h0OiA1JTtcbiAgei1pbmRleDo1OTAwO1xuICB9XG4gICAgLmZsb2F0X2FjdGlvbnM6OmFmdGVye1xuICAgIGNvbnRlbnQ6IFwiPlwiO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJhY2tncm91bmQ6IHJlZDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbGVmdDogMDtcbiAgdG9wOiAzNSU7XG4gIHBhZGRpbmc6IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiA1JTtcbiAgaGVpZ2h0OiA1JTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgXG4gIH1cbiAgICAuZmxvYXRfYWN0aW9uczpob3ZlcjphZnRlcntcbiAgICBjb250ZW50OiBcIj5cIjtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMzUlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBkaXNwbGF5OiBub25lO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDMlO1xuICBoZWlnaHQ6IDUlO1xuICB9XG4gICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMCAwIC0zMHB4IDA7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxuICAgIC5mbG9hdF9hY3Rpb25zOmhvdmVye1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBiYWNrZ3JvdW5kOiByZWQ7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMzUlO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAyNSU7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHotaW5kZXg6IDk5OTk5O1xuICB9XG4gIFxuICAgLmZsb2F0X2FjdGlvbnM6aG92ZXIgLmFjdGlvbnNfY29udGVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMCAwIC0zMHB4IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxufVxuXG4vKiBFeHRyYSBTbWFsbCBEZXZpY2VzLCBQaG9uZXMgKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDQ4MHB4KSBhbmQgKG1heC13aWR0aCA6IDc2OHB4KSAge1xuICAgICAgICAgICAgXG4gICAgXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzaWRlIGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxuICAuZmxvYXRfYWN0aW9uc3tcbnBvc2l0aW9uOiBmaXhlZDtcbmJhY2tncm91bmQ6IHJlZDtcbmJvcmRlci1yYWRpdXM6IDEwcHg7XG5sZWZ0OiAwO1xudG9wOiAzNSU7XG5wYWRkaW5nOiAxMHB4O1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbndpZHRoOiAzJTtcbmhlaWdodDogNSU7XG56LWluZGV4OjU5MDA7XG5cbn1cbiAgLmZsb2F0X2FjdGlvbnM6OmFmdGVye1xuY29udGVudDogXCI+XCI7XG5jb2xvcjogd2hpdGU7XG5wb3NpdGlvbjogZml4ZWQ7XG5iYWNrZ3JvdW5kOiByZWQ7XG5ib3JkZXItcmFkaXVzOiAxMHB4O1xubGVmdDogMDtcbnRvcDogMzUlO1xucGFkZGluZzogMHB4O1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbndpZHRoOiA1JTtcbmhlaWdodDogNSU7XG5mb250LXdlaWdodDogYm9sZDtcbmZvbnQtc2l6ZTogMjBweDtcblxufVxuICAuZmxvYXRfYWN0aW9uczpob3ZlcjphZnRlcntcbmNvbnRlbnQ6IFwiPlwiO1xuY29sb3I6IHdoaXRlO1xucG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDEwcHg7XG5kaXNwbGF5OiBub25lO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6IDMlO1xuaGVpZ2h0OiA1JTtcbn1cbiAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcbnBhZGRpbmc6IDA7XG5tYXJnaW46IDAgMCAtMzBweCAwO1xuZGlzcGxheTogbm9uZTtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4gIC5mbG9hdF9hY3Rpb25zOmhvdmVye1xucG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDEwcHg7XG5kaXNwbGF5OiBmbGV4O1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6IDE4JTtcbmhlaWdodDogYXV0bztcbnotaW5kZXg6IDk5OTk5O1xufVxuXG4gLmZsb2F0X2FjdGlvbnM6aG92ZXIgLmFjdGlvbnNfY29udGVudCB7XG5wYWRkaW5nOiAwO1xubWFyZ2luOiAwIDAgLTMwcHggMDtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxufVxuXG5cblxuIC8qIFNtYWxsIERldmljZXMsIFRhYmxldHMqL1xuIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDk5MnB4KSAge1xuICBcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNpZGUgYmFyKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXG4gIC5mbG9hdF9hY3Rpb25ze1xucG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDEwcHg7XG5kaXNwbGF5OiBmbGV4O1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6IDMlO1xuaGVpZ2h0OiA1JTtcbnotaW5kZXg6NTkwMDtcbn1cbiAgLmZsb2F0X2FjdGlvbnM6OmFmdGVye1xuY29udGVudDogXCI+XCI7XG5jb2xvcjogd2hpdGU7XG5wb3NpdGlvbjogZml4ZWQ7XG5iYWNrZ3JvdW5kOiByZWQ7XG5ib3JkZXItcmFkaXVzOiAxMHB4O1xubGVmdDogMDtcbnRvcDogMzUlO1xucGFkZGluZzogMHB4O1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbndpZHRoOiA1JTtcbmhlaWdodDogNSU7XG5mb250LXdlaWdodDogYm9sZDtcbmZvbnQtc2l6ZTogMjBweDtcblxufVxuICAuZmxvYXRfYWN0aW9uczpob3ZlcjphZnRlcntcbmNvbnRlbnQ6IFwiPlwiO1xuY29sb3I6IHdoaXRlO1xucG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDEwcHg7XG5kaXNwbGF5OiBub25lO1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6IDMlO1xuaGVpZ2h0OiA1JTtcbn1cbiAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcbnBhZGRpbmc6IDA7XG5tYXJnaW46IDAgMCAtMzBweCAwO1xuZGlzcGxheTogbm9uZTtcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4gIC5mbG9hdF9hY3Rpb25zOmhvdmVye1xucG9zaXRpb246IGZpeGVkO1xuYmFja2dyb3VuZDogcmVkO1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmxlZnQ6IDA7XG50b3A6IDM1JTtcbnBhZGRpbmc6IDEwcHg7XG5kaXNwbGF5OiBmbGV4O1xuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5hbGlnbi1pdGVtczogY2VudGVyO1xud2lkdGg6IDEyJTtcbmhlaWdodDogYXV0bztcbnotaW5kZXg6IDk5OTk5O1xufVxuXG4gLmZsb2F0X2FjdGlvbnM6aG92ZXIgLmFjdGlvbnNfY29udGVudCB7XG5wYWRkaW5nOiAwO1xubWFyZ2luOiAwIDAgLTMwcHggMDtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuIH1cblxuXG4gIFxuIC8qIE1lZGl1bSBEZXZpY2VzLCBEZXNrdG9wcyAqL1xuIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDk5MnB4KSBhbmQgKG1heC13aWR0aCA6IDEyMDBweCkgIHtcbiAgICAgIFxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqIHNpZGUgYmFyICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gIC5mbG9hdF9hY3Rpb25zIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYmFja2dyb3VuZDogcmVkO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDM1JTtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogOCU7XG4gICAgei1pbmRleDo1OTAwO1xuICAgIH1cblxuICAgIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQge1xuICAgIHBhZGRpbmc6IDA7XG4gICAgbWFyZ2luOiAwIDAgLTMwcHggMDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIH1cbiAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMge1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICB3aWR0aDogMTIwcHg7XG4gICAgaGVpZ2h0OiAxMjBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgfVxuICAgIFxuICAgIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIHtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgfVxuICAgIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl9sb2dvIHtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBtYXJnaW4tbGVmdDogMjVweDtcbiAgICB9XG4gICAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX3RleHQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IDA7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIG1hcmdpbi10b3A6IDdweDtcbiAgICB9XG4gICAgXG4gICAgXG4gICAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6OmJlZm9yZXtcbiAgICAgY29udGVudDogXCI+XCI7XG4gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgcmlnaHQ6IC0xMHB4O1xuICAgICB0b3A6IDE1JTtcbiAgICAgY29sb3I6IHdoaXRlO1xuICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgIHdpZHRoOiA0MCU7XG4gICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICB9XG4gICAgIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAudGVzdG1lZ2l7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgIHJpZ2h0OiAtMTgwcHg7XG4gICAgICAgdG9wOiAxNSU7XG4gICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICAgICB3aWR0aDogNDAlO1xuICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgIHdpZHRoOiAxODBweDtcbiAgICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICB9XG4gICAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6aG92ZXIgLnRlc3RtZWdpe1xuICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICB9XG4gICAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiB7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgfVxuICAgICAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX2xvZ28ge1xuICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgIG1hcmdpbi1sZWZ0OiAyNXB4O1xuICAgICB9XG4gICAgICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfdGV4dCB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtYXJnaW46IDA7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgICBtYXJnaW4tdG9wOiA3cHg7XG4gICAgIH1cbiAgXG4gICAgfSAgIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SideBarComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-side-bar',
                templateUrl: './side-bar.component.html',
                styleUrls: ['./side-bar.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/cpn/subvention/subvention.component.ts":
/*!********************************************************!*\
  !*** ./src/app/cpn/subvention/subvention.component.ts ***!
  \********************************************************/
/*! exports provided: SubventionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubventionComponent", function() { return SubventionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../side-bar/side-bar.component */ "./src/app/cpn/side-bar/side-bar.component.ts");
/* harmony import */ var _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../map-french-region/map-french-region.component */ "./src/app/cpn/map-french-region/map-french-region.component.ts");




class SubventionComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
SubventionComponent.ɵfac = function SubventionComponent_Factory(t) { return new (t || SubventionComponent)(); };
SubventionComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SubventionComponent, selectors: [["app-subvention"]], decls: 5, vars: 0, consts: [[1, "subvention_container"]], template: function SubventionComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-side-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Les r\u00E9gions de France");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-map-french-region");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_1__["SideBarComponent"], _map_french_region_map_french_region_component__WEBPACK_IMPORTED_MODULE_2__["MapFrenchRegionComponent"]], styles: ["h1[_ngcontent-%COMP%]{\n      font-weight: bold;\n      margin: 50px 0 50px 0;\n      color: #111d5e;\n      align-items: center;\n      display: flex;\n      flex-direction: row;\n      justify-content: center;\n      align-items: center;\n      height:200px;\n\n    }\n\n    .subvention_container[_ngcontent-%COMP%]{\n      background-color: #EBECF0;\n      margin-bottom: -46px !important;\n  }\n\n    \n\n    @media screen and (max-width:  992px) {\n    h1[_ngcontent-%COMP%]{\n      font-weight: bold;\n      margin: 50px 0 50px 0;\n      color: #111d5e;\n      align-items: center;\n      display: flex;\n      flex-direction: row;\n      justify-content: center;\n      align-items: center;\n      height:100px;\n\n    }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jcG4vc3VidmVudGlvbi9zdWJ2ZW50aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtJQUNJO01BQ0UsaUJBQWlCO01BQ2pCLHFCQUFxQjtNQUNyQixjQUFjO01BQ2QsbUJBQW1CO01BQ25CLGFBQWE7TUFDYixtQkFBbUI7TUFDbkIsdUJBQXVCO01BQ3ZCLG1CQUFtQjtNQUNuQixZQUFZOztJQUVkOztJQUVBO01BQ0UseUJBQXlCO01BQ3pCLCtCQUErQjtFQUNuQzs7SUFFQywwR0FBMEc7O0lBQzFHO0lBQ0M7TUFDRSxpQkFBaUI7TUFDakIscUJBQXFCO01BQ3JCLGNBQWM7TUFDZCxtQkFBbUI7TUFDbkIsYUFBYTtNQUNiLG1CQUFtQjtNQUNuQix1QkFBdUI7TUFDdkIsbUJBQW1CO01BQ25CLFlBQVk7O0lBRWQ7O0FBRUoiLCJmaWxlIjoiYXBwL2Nwbi9zdWJ2ZW50aW9uL3N1YnZlbnRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuICAgIGgxe1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICBtYXJnaW46IDUwcHggMCA1MHB4IDA7XG4gICAgICBjb2xvcjogIzExMWQ1ZTtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIGhlaWdodDoyMDBweDtcblxuICAgIH1cblxuICAgIC5zdWJ2ZW50aW9uX2NvbnRhaW5lcntcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNFQkVDRjA7XG4gICAgICBtYXJnaW4tYm90dG9tOiAtNDZweCAhaW1wb3J0YW50O1xuICB9XG5cbiAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnJlc3BvbnNpdmUgY3NzICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAgOTkycHgpIHtcbiAgICBoMXtcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgbWFyZ2luOiA1MHB4IDAgNTBweCAwO1xuICAgICAgY29sb3I6ICMxMTFkNWU7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBoZWlnaHQ6MTAwcHg7XG5cbiAgICB9XG5cbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SubventionComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-subvention',
                templateUrl: './subvention.component.html',
                styleUrls: ['./subvention.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/security/auth.guard.ts":
/*!****************************************!*\
  !*** ./src/app/security/auth.guard.ts ***!
  \****************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





class AuthGuard {
    constructor(tokenStorageService, auth, router) {
        this.tokenStorageService = tokenStorageService;
        this.auth = auth;
        this.router = router;
    }
    canActivate(next, state) {
        this.tokenStorageService.getToken();
        const token = this.tokenStorageService.getToken();
        if (token) {
            return true;
        }
        else {
            this.router.navigate(["/notfound/404"]);
            return false;
        }
    }
}
AuthGuard.ɵfac = function AuthGuard_Factory(t) { return new (t || AuthGuard)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"])); };
AuthGuard.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AuthGuard, factory: AuthGuard.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthGuard, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }, { type: _services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }]; }, null); })();


/***/ })

}]);
//# sourceMappingURL=cpn-cpn-module.js.map